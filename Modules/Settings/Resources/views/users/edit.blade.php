@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAJEMEN USERS</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data users pada sistem aplikasi</small>
        </p>
    </div>
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>EDIT DATA USERS</strong>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    @if(!empty($errors->all()))
                        <div class="alert alert-danger">
                        {{ HTML::ul($errors->all()) }}
                        </div>
                    @endif
                    {{ Form::model($user, ['route' => array('users.update', $user->id), 'method' => 'PUT']) }}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('name', 'NAMA USER') }}
                                        {{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('email', 'EMAIL') }}
                                        {{ Form::text('email', Input::old('email'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('password', 'PASSWORD') }}
                                        {{ Form::password('password', array('class' => 'form-control')) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('role_id', 'ROLE') }}
                                        {{ Form::select('role_id', ['' => 'Pilih role'] + $roles,Input::old('role_id'), ['class' => 'form-control']) }}
                                    </div>
                                </div>
                            </div>
                            {{ Form::button('<i class="icon icon-save"></i> Simpan', array('class' => 'btn btn-primary','type'=>'submit')) }}
                            <a href="{{url($prefix.'/users')}}" class="btn btn-primary"><i class="icon icon-arrow-left"></i> Kembali</a>
                        {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop

