@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAJEMEN ROLES</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data roles pada sistem aplikasi</small>
        </p>
    </div>
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>SETTING MODULE ROLE : {{$role->display_name}}</strong>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    @if(!empty($errors->all()))
                        <div class="alert alert-danger">
                        {{ HTML::ul($errors->all()) }}
                        </div>
                    @endif
                    {{ Form::open(array('url' => $prefix.'/roles/'.$role->id.'/updateModule')) }}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    @foreach($modules as $key => $module)
                                        <input type="hidden" name="data[{{$key}}][module_id]" value="{{$module->id}}">
                                        <input type="hidden" name="data[{{$key}}][allow]" value="0">
                                        <div class="i-checks"><label> <input type="checkbox" value="1" name="data[{{$key}}][allow]" {{($module->allow ? 'checked="checked"' : '')}}> <i></i> {{$module->display_name}} </label></div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        {{ Form::button('<i class="icon icon-save"></i> Simpan', array('class' => 'btn btn-primary','type'=>'submit')) }}
                        <a href="{{url($prefix.'/roles')}}" class="btn btn-primary"><i class="icon icon-arrow-left"></i> Kembali</a>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop

