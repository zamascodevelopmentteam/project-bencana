<?php $a =0;?>
@foreach($subModules as $key => $subModule)

    @if($subModule->countChild == 0)
        <input type="hidden" name="data[{{$a}}][sub_module_id]" value="{{$subModule->id}}">
        <input type="hidden" name="data[{{$a}}][module_id]" value="{{$subModule->module_id}}">
        <input type="hidden" name="data[{{$a}}][allow]" value="0">
        <div class="i-checks"><label> <input type="checkbox" value="1" name="data[{{$a}}][allow]"  {{($subModule->allow ? 'checked="checked"' : '')}}> <i></i> {{$subModule->display_name}} </label></div>
    @else
        <input type="hidden" name="data[{{$a}}][sub_module_id]" value="{{$subModule->id}}">
        <input type="hidden" name="data[{{$a}}][module_id]" value="{{$subModule->module_id}}">
        <input type="hidden" name="data[{{$a}}][allow]" value="0">
        <div class="i-checks"><label> <input type="checkbox" value="1" name="data[{{$a}}][allow]" data-id="{{$subModule->id}}"  {{($subModule->allow ? 'checked="checked"' : '')}}> <i></i> {{$subModule->display_name}} </label></div>
        <?php $a = $a +1;?>
        @foreach($subModule->child as $skey => $child)
            <input type="hidden" name="data[{{$a}}][sub_module_id]" value="{{$child->id}}">
            <input type="hidden" name="data[{{$a}}][module_id]" value="{{$child->module_id}}">
            <input type="hidden" name="data[{{$a}}][allow]" value="0">
            <div class="i-checks"><label> <input type="checkbox" value="1" name="data[{{$a}}][allow]" data-parent-id="{{$subModule->id}}" {{($child->allow ? 'checked="checked"' : '')}}> <i></i> ----- {{$child->display_name}} </label></div>
            <?php $a++;?>
        @endforeach
    @endif
    <?php $a++;?>
@endforeach