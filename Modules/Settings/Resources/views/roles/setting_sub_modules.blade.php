@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAJEMEN ROLES</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data roles pada sistem aplikasi</small>
        </p>
    </div>
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>SETTING SUB MODULE ROLE : {{$role->display_name}}</strong>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    @if(!empty($errors->all()))
                        <div class="alert alert-danger">
                        {{ HTML::ul($errors->all()) }}
                        </div>
                    @endif
                    <div class="fake-alert" onclick="this.style.display='none'"></div>
                    {{ Form::open(array('url' => $prefix.'/roles/'.$role->id.'/updateSubModule')) }}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::select('module_id', ['' => 'Pilih module']+$modules,'',['class'=>'form-control','id'=>'module_id']) }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 sub-module-list m-b-30 m-t-30">
                                
                            </div>
                        </div>
                        {{ Form::button('<i class="icon icon-save"></i> Simpan', array('class' => 'btn btn-primary','type'=>'submit')) }}
                        <a href="{{url($prefix.'/roles')}}" class="btn btn-primary"><i class="icon icon-arrow-left"></i> Kembali</a>
                    {{ Form::close() }}
            </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('scriptBlock')
    <script src="{{url('assets/js/plugins/iCheck/icheck.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $("form").on("submit",function(e){
                e.preventDefault();
                var data = $(this).serialize();
                var actionUrl = $(this).attr("action");
                $.ajax({
                    url : actionUrl,
                    data : data,
                    dataType : 'json',
                    type : 'post',
                    beforeSend : function(){
                        $(".fake-alert").hide();
                    },
                    success : function(result){
                        
                        $(".fake-alert").html(result.message);
                        if(result.code == 200){
                            $(".fake-alert").attr('class','fake-alert alert alert-success');
                        }else{
                            $(".fake-alert").attr('class','fake-alert alert alert-danger');
                        }
                        $(".fake-alert").show();
                    }
                })
            })
            $("#module_id").on("change",function(e){
                e.preventDefault();
                var moduleId = $(this).val();
                var roleId = "{{$role->id}}";
                if(moduleId != ""){
                    $(".sub-module-list").load("{{url($prefix.'/roles/get-sub-module')}}" + "/" + roleId + "/" + moduleId )
                }else{
                    $(".sub-module-list").html("")
                }
                
            })
            $("body").on("click",".i-checks input[type='checkbox']",function(e){
                var dataInput = $(this).data();
                var dataId  = dataInput.id;
                var dataParentId = dataInput.parentId;
                var checkboxtype = "";
                var checked = $(this).prop("checked");
                if(dataId == undefined && dataParentId == undefined){
                    checkboxtype = "solo";
                }else if(dataId != undefined && dataParentId == undefined){
                    checkboxtype = "parent";
                }else if(dataId == undefined && dataParentId != undefined){
                    checkboxtype = "child";
                }
                console.log(checkboxtype)
                if(checkboxtype == "parent"){
                    if(checked == true){
                        $("input[data-parent-id=\""+dataId+"\"]").prop("checked",true);
                    }else{
                        $("input[data-parent-id=\""+dataId+"\"]").prop("checked",false);
                    }
                }else if(checkboxtype == "child"){
                    if(checked == true){
                        $("input[data-id=\""+dataParentId+"\"]").prop("checked",true);
                    }else{
                        var numberOfChecked = $("input[data-parent-id=\""+dataParentId+"\"]:checked").length;
                        if(numberOfChecked == 0){
                            $("input[data-id=\""+dataParentId+"\"]").prop("checked",false);
                        }
                        //$("input[data-parent-id=\""+dataId+"\"]").prop("checked",false);
                    }
                }
                
            })
        });
    </script>

@stop
