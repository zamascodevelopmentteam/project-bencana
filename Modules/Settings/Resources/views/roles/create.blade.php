@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAJEMEN ROLES</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data roles pada sistem aplikasi</small>
        </p>
    </div>
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>TAMBAH DATA ROLES</strong>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    @if(!empty($errors->all()))
                        <div class="alert alert-danger">
                        {{ HTML::ul($errors->all()) }}
                        </div>
                    @endif
                    {{ Form::open(array('url' => $prefix.'/roles')) }}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::label('name', 'NAMA ROLE') }}
                                    {{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::label('display_name', 'NAMA TAMPILAN') }}
                                    {{ Form::text('display_name', Input::old('display_name'), array('class' => 'form-control')) }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    {{ Form::label('description', 'DESKRIPSI') }}
                                    {{ Form::textarea('description', Input::old('description'), array('class' => 'form-control')) }}
                                </div>
                            </div>
                        </div>
                        {{ Form::button('<i class="icon icon-save"></i> Simpan', array('class' => 'btn btn-primary','type'=>'submit')) }}
                        <a href="{{url($prefix.'/roles')}}" class="btn btn-primary"><i class="icon icon-arrow-left"></i> Kembali</a>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop

