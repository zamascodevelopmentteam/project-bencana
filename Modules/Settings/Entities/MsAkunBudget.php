<?php

namespace Modules\Settings\Entities;

use Illuminate\Database\Eloquent\Model;

class MsAkunBudget extends Model
{
    protected $table = 'ms_akun_budget';
    protected $fillable = ['sdm_ms_bagian_id', 'kode_akun', 'deskripsi', 'parent_id', 'position', 'created_by', 'updated_by'];
}
