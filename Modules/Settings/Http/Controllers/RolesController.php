<?php

namespace Modules\Settings\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use App\Roles;
use App\Modules;
use App\ModulePermissions;
use App\SubModules;
use App\SubModulePermissions;
use DB;
use Validator;
use Session;
use Redirect;
use View;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Roles::select(DB::raw('*, @rownum := @rownum +1 as rownum'));
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                 '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                    <li><a href="roles/'.$data->id.'/edit">Edit</a></li>
                                    <li><a href="roles/'.$data->id.'/setting-module">Setting Module</a></li>
                                    <li><a href="roles/'.$data->id.'/setting-sub-module">Setting Sub Module</a></li>
                                    <li><a href="roles/'.$data->id.'" data-id="" class="btn-delete-on-table">Delete</a></li>
                                  </ul><div>'
                        ;})
            ->make(true);
        }
        return view('settings::roles.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('settings::roles.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'name'       => 'required',
            'display_name'       => 'required',
        );
        $message = [
            'name.required' => 'Harap masukan nama role',
            'display_name.required' => 'Harap masukan nama tampilan'
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('settings/roles/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $role = new Roles;
            $data_input = $request->input();
            unset($data_input['_method']);
            unset($data_input['_token']);
            foreach($data_input as $key => $val){
                $role->$key = $val;
            }
            $role->save();

            // redirect
            Session::flash('message', 'Data role berhasil ditambahkan');
            return Redirect::to('settings/roles');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('settings::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $role = Roles::find($id);
        return view('settings::roles.edit')->with('role',$role);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'name'       => 'required',
            'display_name'       => 'required',
        );
        $message = [
            'name.required' => 'Harap masukan nama role',
            'display_name.required' => 'Harap masukan nama tampilan'
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('settings/roles/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $role = Roles::find($id);
            $data_input = $request->input();
            unset($data_input['_method']);
            unset($data_input['_token']);
            foreach($data_input as $key => $val){
                $role->$key = $val;
            }
            $role->save();

            // redirect
            Session::flash('message', 'Data role berhasil diupdate');
            return Redirect::to('settings/roles');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $role = Roles::find($id);
        
        if($role->delete()){
            return response()
            ->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()
            ->json(['code' => '50', 'message' => 'Data gagal dihapus']);
        }
    }
    
    public function settingModule($id)
    {
        $role = Roles::find($id);
        $modules = Modules::get();
        foreach($modules as $key=>$module){
            $modulePermission = ModulePermissions::where('role_id','=',$id)->where('module_id','=',$module->id);
            if($modulePermission->count()){
                $modulePermission = $modulePermission->first();
                $module->allow = $modulePermission->allow;
            }else{
                $module->allow = false;
            }
        }
        return view('settings::roles.setting_modules')->with('role',$role)->with('modules',$modules);
    }
    
    public function updateModule(Request $request,$id)
    {
        $data_input = $request->input();
        $data_module = $data_input['data'];
        foreach($data_module as $key => $module){
            $modulePermission = ModulePermissions::where('role_id','=',$id)->where('module_id','=',$module['module_id']);
            if($modulePermission->count()){
                $modulePermission = $modulePermission->first();
            }else{
                $modulePermission = new ModulePermissions;
            }
            $modulePermission->module_id = $module['module_id'];
            $modulePermission->role_id = $id;
            $modulePermission->allow = $module['allow'];
            $modulePermission->save();
        }
        
        Session::flash('message', 'Setting role module berhasil disimpan');
        return Redirect::to('settings/roles');
    }
    
    public function settingSubModule($id)
    {
        $role = Roles::find($id);
        $modulePermissions = ModulePermissions::where('role_id','=',$id)->get();
        $modules = [];
        foreach($modulePermissions as $key => $md){
            if(!empty($md->module))
            {
               $modules[$md->module_id] = $md->module->display_name; 
            }
            
        }
        return view('settings::roles.setting_sub_modules')->with('role',$role)->with('modules',$modules);
    }
    
    public function getSubModule($id,$module_id)
    {
        $role = Roles::find($id);
        $subModules = SubModules::where('module_id','=',$module_id)->where('sub_module_id','=','0')->get();
        foreach($subModules as $key=>$subModule){
            $child = SubModules::where('module_id','=',$module_id)->where('sub_module_id','=',$subModule->id);
            $subModule->countChild = $child->count();
            $subModule->child = $child->get();
            $modulePermission = SubModulePermissions::where('role_id','=',$id)->where('sub_module_id','=',$subModule->id);
            if($modulePermission->count()){
                $modulePermission = $modulePermission->first();
                $subModule->allow = $modulePermission->allow;
            }else{
                $subModule->allow = false;
            }
            foreach($subModule->child as $skey => $schild){
                $modulePermission = SubModulePermissions::where('role_id','=',$id)->where('sub_module_id','=',$subModule->id);
                if($modulePermission->count()){
                    $modulePermission = $modulePermission->first();
                    $schild->allow = $modulePermission->allow;
                }else{
                    $schild->allow = false;
                }
            }
        }
        //dd($subModules);
        return view('settings::roles.get_sub_modules')->with('role',$role)->with('subModules',$subModules);
    }
    
    public function updateSubModule(Request $request,$id)
    {
        $data_input = $request->input();
        $data_module_id = $data_input['module_id'];
        if(!empty($data_module_id)){
            if(empty($data_input['data'])){
                return response()
                ->json(['code' => '50', 'message' => 'Sub module tidak tersedia']); 
            }else{
                $data_module = $data_input['data'];
                foreach($data_module as $key => $module){
                    $modulePermission = SubModulePermissions::where('role_id','=',$id)->where('sub_module_id','=',$module['sub_module_id']);
                    if($modulePermission->count()){
                        $modulePermission = $modulePermission->first();
                    }else{
                        $modulePermission = new SubModulePermissions;
                    }
                    $modulePermission->module_id = $module['module_id'];
                    $modulePermission->sub_module_id = $module['sub_module_id'];
                    $modulePermission->role_id = $id;
                    $modulePermission->allow = $module['allow'];
                    $modulePermission->save();
                }
                return response()
                ->json(['code' => '200', 'message' => 'Setting role sub module berhasil disimpan']);
            }
        }else{
           return response()
            ->json(['code' => '50', 'message' => 'Harap pilih module']); 
        }
        //dd($data_module);

//        
//        Session::flash('message', 'Setting role module berhasil disimpan');
//        return Redirect::to('settings/roles');
    }
    
}
