<?php

namespace Modules\Settings\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use App\User;
use App\Roles;
use DB;
use Hash;
use Validator;
use Session;
use Redirect;
use View;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = User::select(DB::raw('*, @rownum := @rownum +1 as rownum'));
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                 '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                    <li><a href="users/'.$data->id.'/edit">Edit</a></li>
                                    <li><a href="users/'.$data->id.'" data-id="" class="btn-delete-on-table">Delete</a></li>
                                  </ul><div>'
                        ;})
            ->make(true);
        }
        return view('settings::users.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $roles = Roles::all();
        $roles_list = [];
        foreach($roles as $key => $value){
            //dd($value->type->name);
            $roles_list[$value['id']] = $value->display_name ;
        }
        return view('settings::users.create')->with('roles',$roles_list);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
           'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
            'role_id' => 'required',
        );
        $message = [
            'name.required' => 'Harap masukan nama user',
            'email.required' => 'Harap masukan email',
            'email.email' => 'Email tidak valid',
            'email.unique' => 'Email telah terdaftar',
            'password.required' => 'Harap masukan password',
            'password.min' => 'Harap masukan password minimal 6 digit',
            'role_id.required' => 'Harap pilih role',
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('settings/users/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $user = new User;
            $data_input = $request->input();
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            $data_input['password'] = Hash::make($data_input['password']);
            foreach($data_input as $key => $val){
                $user->$key = $val;
            }
            $user->save();

            // redirect
            Session::flash('message', 'Data user berhasil ditambahkan');
            return Redirect::to('settings/users');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('settings::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $roles = Roles::all();
        $roles_list = [];
        foreach($roles as $key => $value){
            //dd($value->type->name);
            $roles_list[$value['id']] = $value->display_name ;
        }
        $user = User::find($id);
        return view('settings::users.edit')->with('roles',$roles_list)->with('user',$user);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
           'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,'.$id,
            'password' => 'required|min:6',
            'role_id' => 'required',
        );
        $dataRequest = $request->all();
        if(empty($dataRequest['password'])){
            unset($rules['password']);
            unset($dataRequest['password']);
        }
        $message = [
            'name.required' => 'Harap masukan nama user',
            'email.required' => 'Harap masukan email',
            'email.email' => 'Email tidak valid',
            'email.unique' => 'Email telah terdaftar',
            'password.required' => 'Harap masukan password',
            'password.min' => 'Harap masukan password minimal 6 digit',
            'role_id.required' => 'Harap pilih role',
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('settings/users/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $user = User::find($id);
            $data_input = $dataRequest;
            unset($data_input['_method']);
            unset($data_input['_token']);
            // update password hash
            if(!empty($data_input['password'])) $data_input['password'] = Hash::make($data_input['password']);
            foreach($data_input as $key => $val){
                $user->$key = $val;
            }
            $user->save();

            // redirect
            Session::flash('message', 'Data user berhasil diupdate');
            return Redirect::to('settings/users');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $user = User::find($id);
        
        if($user->delete()){
            return response()
            ->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()
            ->json(['code' => '50', 'message' => 'Data gagal dihapus']);
        }
    }
}
