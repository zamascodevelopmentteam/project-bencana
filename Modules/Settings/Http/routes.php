<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'settings', 'namespace' => 'Modules\Settings\Http\Controllers'], function()
{
    Route::get('/', 'DashboardController@index');
    Route::get('/dashboard', 'DashboardController@index');
    Route::resource('/roles', 'RolesController');
    Route::resource('/users', 'UsersController');
    Route::get('/roles/{id}/setting-module', 'RolesController@settingModule');
    Route::post('/roles/{id}/updateModule', 'RolesController@updateModule');
    Route::get('/roles/{id}/setting-sub-module', 'RolesController@settingSubModule');
    Route::get('/roles/get-sub-module/{id}/{module_id}', 'RolesController@getSubModule');
    Route::post('/roles/{id}/updateSubModule', 'RolesController@updateSubModule');
    //Route::post('/roles/{id}/updateModule', 'RolesController@updateModule');

    // Kelompok Budget
    Route::resource('/kelompok-budget', 'KelompokBudgetController');
    // SubKelompok Budget
    Route::resource('/sub-kelompok-budget', 'SubKelompokBudgetController');
    // Akun Budget
    Route::resource('/akun', 'AkunController');
    Route::get('/akun/sub-budget/{id}', 'AkunController@getSubKelompokBudget');
    // Budgeting
    Route::resource('/budget', 'BudgetController');
    Route::get('/budget/list-akun/{id}', 'BudgetController@listAkun');
});
