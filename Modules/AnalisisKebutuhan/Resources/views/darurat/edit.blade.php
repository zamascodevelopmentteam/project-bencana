@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAJEMEN DARURAT</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data darurat pada sistem aplikasi</small>
        </p>
    </div>
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>EDIT DATA DARURAT</strong>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    @if(!empty($errors->all()))
                        <div class="alert alert-danger">
                        {{ HTML::ul($errors->all()) }}
                        </div>
                    @endif
                    {{ Form::model($darurat, ['route' => array('darurat.update', $darurat->id), 'method' => 'PUT', 'files' => true]) }}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('province_id', 'PROVINSI') }}
                                        {{ Form::select('province_id',$propinsi, Input::old('province_id'), array('class' => 'form-control','placeholder'=>'Pilih Provinsi','id'=>'province_id')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('district_id', 'KABUPATEN / KOTA') }}
                                        {{ Form::select('district_id',$districts, Input::old('district_id'), array('class' => 'form-control','placeholder'=>'Pilih Kabupaten / Kota','id'=>'district_id')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('kajian', 'KAJIAN') }}
                                        {{ Form::text('kajian', Input::old('kajian'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('nomer_dokumen', 'NOMOR DOKUMEN') }}
                                        {{ Form::text('nomer_dokumen', Input::old('nomer_dokumen'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {{ Form::label('proposal', 'PROPOSAL (MAX 10 MB)') }}
                                        {{ Form::file('proposal', array('class' => 'form-control')) }}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {{ Form::label('sk', 'SK TANGGAP DARURAT (MAX 10 MB)') }}
                                        {{ Form::file('sk', array('class' => 'form-control')) }}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {{ Form::label('memo', 'MEMO (MAX 10 MB)') }}
                                        {{ Form::file('memo', array('class' => 'form-control')) }}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {{ Form::label('rapat', 'FILE RAPAT (MAX 10 MB)') }}
                                        {{ Form::file('rapat', array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                            
                            {{ Form::button('<i class="icon icon-save"></i> Simpan', array('class' => 'btn btn-primary','type'=>'submit')) }}
                            <a href="{{url($prefix.'/darurat')}}" class="btn btn-primary"><i class="icon icon-arrow-left"></i> Kembali</a>
                        {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop

@section('scriptBlock')
    <script>
        $("#province_id").select2().on('select2:select',function(e,item){
            var valus = $(this).val();
            $.ajax({
                url : "{{url($prefix.'/darurat/api/cari-kota')}}/"+valus,
                dataType : "json",
                beforeSend : function(e){
                    $(".card").append('<div class="blockUI" style="display:none"></div><div class="blockUI blockOverlay" style="z-index: 1000; border: none; margin: 0px; padding: 0px; width: 100%; height: 100%; top: 0px; left: 0px; background-color: rgb(255, 255, 255); opacity: 0.8; cursor: wait; position: absolute;"></div><div class="blockUI blockMsg blockElement" style="z-index: 1011; position: absolute; padding: 0px; margin: 0px; width: 30%; top: 194px; left: 382px; text-align: center; color: rgb(0, 0, 0); border: none; cursor: wait;"><div class="spinner spinner-primary"></div></div></div>');
                },
                success : function(result){
                    $(".card .blockUI").remove();
                    var options = "<option value=''>Pilih Kabupaten / Kota</option>";
                    $.each(result.data,function(e,item){
                        options += "<option value='"+e+"'>"+item+"</option>"
                    })
                    $("#district_id").html(options).select2('refresh');
                    console.log(options);
                },
                error:function(e){
                    $(".card .blockUI").remove();
                }
            })
        });
        $("#district_id").select2();
    </script>
@stop

