<?php

namespace Modules\AnalisisKebutuhan\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use App\Penguatan;
use App\Propinsi;
use App\Kota;
use App\Roles;
use DB;
use Hash;
use Validator;
use Session;
use Redirect;
use View;

class PenguatanController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
         if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Penguatan::select(
                DB::raw('@rownum := @rownum +1 as rownum'),
                'rf_penguatan.id',
                'provinces.nama_propinsi',
                'districts.nama_kabupaten',
                'kajian',
                'nomor_dokumen',
                'attachment_file'
            )
            ->join('provinces','provinces.kode_propinsi','=','rf_penguatan.province_id')
            ->leftJoin('districts','districts.kode_kabupaten','=','rf_penguatan.district_id');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                 '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                    <li><a href="penguatan/'.$data->id.'/edit">Edit</a></li>
                                    <li><a href="penguatan/'.$data->id.'" data-id="" class="btn-delete-on-table">Delete</a></li>
                                  </ul><div>'
                        ;})
            ->make(true);
        }
        return view('analisiskebutuhan::penguatan.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $propinsi = Propinsi::orderBy('nama_propinsi','asc')->get()->pluck('nama_propinsi','kode_propinsi');
        return view('analisiskebutuhan::penguatan.create')->with('propinsi',$propinsi);
    }

    public function getDistricts($kode_propinsi)
    {
        $districts = Kota::orderBy('nama_kabupaten','asc')->where('kode_propinsi','=',$kode_propinsi)->get()->pluck('nama_kabupaten','kode_kabupaten');
        return response()
        ->json(['code' => '200', 'message' => 'Data berhasil','data'=>$districts]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */

    public function index_download(\Illuminate\Http\Request $request)
    {
         if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Penguatan::select(
                DB::raw('@rownum := @rownum +1 as rownum'),
                'rf_penguatan.id',
                'provinces.nama_propinsi',
                'districts.nama_kabupaten',
                'kajian',
                'nomor_dokumen',
                'attachment_file'
            )
            ->join('provinces','provinces.kode_propinsi','=','rf_penguatan.province_id')
            ->leftJoin('districts','districts.kode_kabupaten','=','rf_penguatan.district_id');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                 '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                    <li><a href="penguatan/download/'.$data->id.'">Download</a></li>
                                    
                                  </ul><div>'
                        ;})
            ->make(true);
        }
        return view('analisiskebutuhan::download.index');
    }

    public function download($id){
        $find = Penguatan::find($id);
        return response()->download('uploadsPenguatan/'.$find->attachment_file);
    
    }

    public function store(Request $request)
    {
        $rules = array(
            'province_id' => 'required|max:100',
            'kajian' => 'required|max:100',
            'nomor_dokumen' => 'required|max:100',
            'attachment_file' => 'required|max:10000',
        );
        $message = [
            'province_id.required' => 'Harap pilih provinsi',
            'kajian.required' => 'Harap masukan kajian',
            'nomor_dokumen.required' => 'Harap masukkan nomor kontrak',
            'attachment_file.max' => 'Kolom file tidak boleh lebih dari 10 mb',
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('analisiskebutuhan/penguatan/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $penguatan = new Penguatan;
            $data_input = $request->input();
            $file= Input::file('attachment_file');
            $data_input['attachment_file']=$file->getClientOriginalName();
            $file->move('uploadsPenguatan', $file->getClientOriginalName());
            unset($data_input['_method']);
            unset($data_input['_token']);
           

            // make password hash for login
            foreach($data_input as $key => $val){
                $penguatan->$key = $val;
            }
            $penguatan->save();

            // redirect
            Session::flash('message', 'Data penguatan berhasil disimpan');
            return Redirect::to('analisiskebutuhan/penguatan');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('analisiskebutuhan::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $penguatan = Penguatan::find($id);
        $propinsi = Propinsi::orderBy('nama_propinsi','asc')->get()->pluck('nama_propinsi','kode_propinsi');
        $districts = Kota::orderBy('nama_kabupaten','asc')->where('kode_propinsi','=',$penguatan->province_id)->get()->pluck('nama_kabupaten','kode_kabupaten');
        return view('analisiskebutuhan::penguatan/edit',compact('penguatan','id', 'propinsi','districts'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $rules = array(
            'province_id' => 'required|max:100',
            'kajian' => 'required|max:100',
            'nomor_dokumen' => 'required|max:100',
            'attachment_file' => 'sometimes|max:10000',
           
        );
        $message = [
            'province_id.required' => 'Harap masukan provinsi',
            'kajian.required' => 'Harap masukan kajian',
            'nomor_dokumen.required' => 'Harap masukkan nomor kontrak',
            'attachment_file' => 'Kolom file tidak boleh lebih dari 10 mb',
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('analisiskebutuhan/penguatan/'. $id .'/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            // update
            $penguatan = Penguatan::find($id);
            $data_input = $request->input();
            $file= Input::file('attachment_file');
            if(!empty($file)){
                $file->move('uploadsPenguatan', $file->getClientOriginalName());
                $data_input['attachment_file']=$file->getClientOriginalName();    
            }
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $penguatan->$key = $val;
            }
            $penguatan->save();

            // redirect
            Session::flash('message', 'Data penguatan berhasil diupdate');
            return Redirect::to('analisiskebutuhan/penguatan');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $penguatan = Penguatan::find($id);
        
        if($penguatan->delete()){
            return response()
            ->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()
            ->json(['code' => '50', 'message' => 'Data gagal dihapus']);
        }
    }
}
