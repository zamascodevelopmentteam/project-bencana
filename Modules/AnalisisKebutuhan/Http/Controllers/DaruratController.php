<?php

namespace Modules\AnalisisKebutuhan\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use App\Darurat;
use App\Roles;
use App\Propinsi;
use App\Kota;
use DB;
use Hash;
use Validator;
use Session;
use Redirect;
use View;
use Zip;

class DaruratController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
         if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Darurat::select(
                DB::raw('@rownum := @rownum +1 as rownum'),
                'rf_darurat.id',
                'provinces.nama_propinsi',
                'districts.nama_kabupaten',
                'kajian',
                'nomer_dokumen'
            )
            ->join('provinces','provinces.kode_propinsi','=','rf_darurat.province_id')
            ->leftJoin('districts','districts.kode_kabupaten','=','rf_darurat.district_id');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                 '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                    <li><a href="darurat/'.$data->id.'/edit">Edit</a></li>
                                    <li><a href="darurat/'.$data->id.'" data-id="" class="btn-delete-on-table">Delete</a></li>
                                  </ul><div>'
                        ;})
            ->make(true);
        }
        return view('analisiskebutuhan::darurat.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        $propinsi = Propinsi::orderBy('nama_propinsi','asc')->get()->pluck('nama_propinsi','kode_propinsi');
        $districts = [];
        //dd($request);
        return view('analisiskebutuhan::darurat.create')->with('propinsi',$propinsi)->with('districts',$districts);
    }

    public function getDistricts($kode_propinsi)
    {
        $districts = Kota::orderBy('nama_kabupaten','asc')->where('kode_propinsi','=',$kode_propinsi)->get()->pluck('nama_kabupaten','kode_kabupaten');
        return response()
        ->json(['code' => '200', 'message' => 'Data berhasil','data'=>$districts]);
    }

    public function index_download(\Illuminate\Http\Request $request)
    {
         if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Darurat::select(
                DB::raw('@rownum := @rownum +1 as rownum'),
                'rf_darurat.id',
                'provinces.nama_propinsi',
                'districts.nama_kabupaten',
                'kajian',
                'nomer_dokumen'
            )
            ->join('provinces','provinces.kode_propinsi','=','rf_darurat.province_id')
            ->leftJoin('districts','districts.kode_kabupaten','=','rf_darurat.district_id');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                 '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                    <li><a href="darurat/download/'.$data->id.'">Download</a></li>
                                    
                                  </ul><div>'
                        ;})
            ->make(true);
        }
        return view('analisiskebutuhan::downloadDarurat.index');
    }

    public function download(Request $request,$id){
        
        $query = $request->query('download');
        if($query == false){
            $find = Darurat::find($id);
            $rand = rand(2000,33920931);
            $path = 'tmp'.DIRECTORY_SEPARATOR.$rand.".zip";
            $pathDw = 'tmp'.'/'.$rand.".zip";
            $zip =  Zip::create($path);
            if(!empty($find->proposal)){
                if(file_exists('uploadsDarurat'.DIRECTORY_SEPARATOR.$find->id.DIRECTORY_SEPARATOR.$find->proposal)){
                    $zip->add('uploadsDarurat'.DIRECTORY_SEPARATOR.$find->id.DIRECTORY_SEPARATOR.$find->proposal);
                }
            }
            if(!empty($find->sk)){
                if(file_exists('uploadsDarurat'.DIRECTORY_SEPARATOR.$find->id.DIRECTORY_SEPARATOR.$find->sk)){
                    $zip->add('uploadsDarurat'.DIRECTORY_SEPARATOR.$find->id.DIRECTORY_SEPARATOR.$find->sk);
                }
            }
            if(!empty($find->memo)){
                if(file_exists('uploadsDarurat'.DIRECTORY_SEPARATOR.$find->id.DIRECTORY_SEPARATOR.$find->memo)){
                    $zip->add('uploadsDarurat'.DIRECTORY_SEPARATOR.$find->id.DIRECTORY_SEPARATOR.$find->memo);
                }
            }
            if(!empty($find->rapat)){
                if(file_exists('uploadsDarurat'.DIRECTORY_SEPARATOR.$find->id.DIRECTORY_SEPARATOR.$find->rapat)){
                    $zip->add('uploadsDarurat'.DIRECTORY_SEPARATOR.$find->id.DIRECTORY_SEPARATOR.$find->rapat);
                }
            }
            return Redirect::to('analisiskebutuhan/darurat/download/'.$rand.'?download=true');
        }else{
            $pathDw = 'tmp'.'/'.$id.".zip";
            return response()->download($pathDw);
        }
    
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'province_id' => 'required|max:100',
            'kajian' => 'required|max:100',
            'nomer_dokumen' => 'required|max:100',
            'proposal' => 'required|max:10000',
            'sk' => 'required|max:10000',
            'memo' => 'required|max:10000',
            'rapat' => 'required|max:10000',

        );
        $message = [
            'province_id.required' => 'Harap pilih provinsi',
            'kajian.required' => 'Harap masukan kajian',
            'nomer_dokumen.required' => 'Harap masukkan nomor kontrak',
            'proposal.max' => 'Kolom proposal tidak boleh lebih dari 10 mb',
            'sk.max' => 'Kolom SK tidak boleh lebih dari 10 mb',
            'memo.max' => 'Kolom memo tidak boleh lebih dari 10 mb',
            'rapat.max' => 'Kolom file rapat tidak boleh lebih dari 10 mb',
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('analisiskebutuhan/darurat/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $darurat = new Darurat;
            $data_input = $request->input();

            $proposal   = Input::file('proposal');
            if(!empty($proposal)){
                $data_input['proposal']= $proposal->getClientOriginalName();
            }
            $sk   = Input::file('sk');
            if(!empty($sk)){
                $data_input['sk']= $sk->getClientOriginalName();
            }
            $memo   = Input::file('memo');
            if(!empty($memo)){
                $data_input['memo']= $memo->getClientOriginalName();
            }
            $rapat   = Input::file('rapat');
            if(!empty($rapat)){
                $data_input['rapat']= $rapat->getClientOriginalName();
            }
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $darurat->$key = $val;
            }
            $darurat->save();
            if(!empty($proposal)){
                $proposal->move('uploadsDarurat'.DIRECTORY_SEPARATOR.$darurat->id, $proposal->getClientOriginalName());
            }

            if(!empty($sk)){
                $sk->move('uploadsDarurat'.DIRECTORY_SEPARATOR.$darurat->id, $sk->getClientOriginalName());
            }

            if(!empty($memo)){
                $memo->move('uploadsDarurat'.DIRECTORY_SEPARATOR.$darurat->id, $memo->getClientOriginalName());
            }

            if(!empty($rapat)){
                $rapat->move('uploadsDarurat'.DIRECTORY_SEPARATOR.$darurat->id, $rapat->getClientOriginalName());
            }
            
            // redirect
            Session::flash('message', 'Data darurat berhasil disimpan');
            return Redirect::to('analisiskebutuhan/darurat');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('analisiskebutuhan::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $darurat = Darurat::find($id);
        $propinsi = Propinsi::orderBy('nama_propinsi','asc')->get()->pluck('nama_propinsi','kode_propinsi');
        $districts = Kota::orderBy('nama_kabupaten','asc')->where('kode_propinsi','=',$darurat->province_id)->get()->pluck('nama_kabupaten','kode_kabupaten');
        return view('analisiskebutuhan::darurat/edit',compact('darurat','id', 'propinsi','districts'));

    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $rules = array(
            'province_id' => 'required|max:100',
            'kajian' => 'required|max:100',
            'nomer_dokumen' => 'required|max:100',
            'proposal' => 'sometimes|max:10000',
            'sk' => 'sometimes|max:10000',
            'memo' => 'sometimes|max:10000',
            'rapat' => 'sometimes|max:10000',

        );
        $message = [
            'province_id.required' => 'Harap pilih provinsi',
            'kajian.required' => 'Harap masukan kajian',
            'nomer_dokumen.required' => 'Harap masukkan nomor kontrak',
            'proposal.max' => 'Kolom proposal tidak boleh lebih dari 10 mb',
            'sk.max' => 'Kolom SK tidak boleh lebih dari 10 mb',
            'memo.max' => 'Kolom memo tidak boleh lebih dari 10 mb',
            'rapat.max' => 'Kolom file rapat tidak boleh lebih dari 10 mb',

        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('analisiskebutuhan/darurat/'. $id .'/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            // update
            $darurat = Darurat::find($id);
            $proposal_old = $darurat->proposal;
            $sk_old = $darurat->sk;
            $memo_old = $darurat->memo;
            $rapat_old = $darurat->rapat;
            $data_input = $request->input();
            $proposal   = Input::file('proposal');
            if(!empty($proposal)){
                $data_input['proposal']= $proposal->getClientOriginalName();
            }
            $sk   = Input::file('sk');
            if(!empty($sk)){
                $data_input['sk']= $sk->getClientOriginalName();
            }
            $memo   = Input::file('memo');
            if(!empty($memo)){
                $data_input['memo']= $memo->getClientOriginalName();
            }
            $rapat   = Input::file('rapat');
            if(!empty($rapat)){
                $data_input['rapat']= $rapat->getClientOriginalName();
            }
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $darurat->$key = $val;
            }
            $darurat->save();
            if(!empty($proposal)){
                File::delete('uploadsDarurat'.DIRECTORY_SEPARATOR.$darurat->id.DIRECTORY_SEPARATOR.$proposal_old);
                $proposal->move('uploadsDarurat'.DIRECTORY_SEPARATOR.$darurat->id, $proposal->getClientOriginalName());
            }

            if(!empty($sk)){
                File::delete('uploadsDarurat'.DIRECTORY_SEPARATOR.$darurat->id.DIRECTORY_SEPARATOR.$sk_old);
                $sk->move('uploadsDarurat'.DIRECTORY_SEPARATOR.$darurat->id, $sk->getClientOriginalName());
            }

            if(!empty($memo)){
                File::delete('uploadsDarurat'.DIRECTORY_SEPARATOR.$darurat->id.DIRECTORY_SEPARATOR.$memo_old);
                $memo->move('uploadsDarurat'.DIRECTORY_SEPARATOR.$darurat->id, $memo->getClientOriginalName());
            }

            if(!empty($rapat)){
                File::delete('uploadsDarurat'.DIRECTORY_SEPARATOR.$darurat->id.DIRECTORY_SEPARATOR.$rapat_old);
                $rapat->move('uploadsDarurat'.DIRECTORY_SEPARATOR.$darurat->id, $rapat->getClientOriginalName());
            }
            // redirect
            Session::flash('message', 'Data propinsi berhasil diupdate');
            return Redirect::to('analisiskebutuhan/darurat');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $darurat = Darurat::find($id);
        
        if($darurat->delete()){
            File::deleteDirectory('uploadsDarurat'.DIRECTORY_SEPARATOR.$darurat->id);
            return response()
            ->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()
            ->json(['code' => '50', 'message' => 'Data gagal dihapus']);
        }
    }
}
