<?php

Route::group(['middleware' => 'web', 'prefix' => 'analisiskebutuhan', 'namespace' => 'Modules\AnalisisKebutuhan\Http\Controllers'], function()
{
    Route::resource('penguatan', 'PenguatanController');   
    Route::get('penguatan/api/cari-kota/{id}', 'PenguatanController@getDistricts');   
    Route::get('download', 'PenguatanController@index_download');
    Route::get('penguatan/download/{id}/', 'PenguatanController@download')->name('penguatan.download');
    Route::resource('darurat', 'DaruratController');   
    Route::get('darurat/api/cari-kota/{id}', 'DaruratController@getDistricts'); 
    Route::get('downloadDarurat', 'DaruratController@index_download');
    Route::get('darurat/download/{id}/', 'DaruratController@download')->name('darurat.download');

});
