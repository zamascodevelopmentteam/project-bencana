<?php

Route::group(['middleware' => 'web', 'prefix' => 'pengadaan', 'namespace' => 'Modules\Pengadaan\Http\Controllers'], function()
{
    Route::resource('paket', 'PaketController');
    Route::resource('satuan', 'SatuanController');
	Route::resource('vendor', 'VendorController');
	Route::resource('kontrak', 'KontrakController');
	Route::get('download', 'KontrakController@index_download');
    Route::get('kontrak/download/{id}/', 'KontrakController@download')->name('kontrak.download');
});
