<?php

namespace Modules\Pengadaan\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use App\Satuan;
use App\Roles;
use DB;
use Hash;
use Validator;
use Session;
use Redirect;
use View;

class SatuanController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
         if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Satuan::select(DB::raw('*, @rownum := @rownum +1 as rownum'));
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                 '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                    <li><a href="satuan/'.$data->id.'/edit">Edit</a></li>
                                    <li><a href="satuan/'.$data->id.'" data-id="" class="btn-delete-on-table">Delete</a></li>
                                  </ul><div>'
                        ;})
            ->make(true);
        }
        return view('pengadaan::satuan.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        
        return view('pengadaan::satuan.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
         $rules = array(
            'ur_satuan' => 'required|max:100',
           
        );
        $message = [
            'ur_satuan.required' => 'Harap masukan satuan barang',
            
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('pengadaan/satuan/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $satuan = new Satuan;
            $data_input = $request->input();
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $satuan->$key = $val;
            }
            $satuan->save();

            // redirect
            Session::flash('message', 'Data satuan berhasil disimpan');
            return Redirect::to('pengadaan/satuan');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('pengadaan::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
       
        $satuan = Satuan::find($id);
        return view('pengadaan::satuan.edit', compact('satuan','id'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $rules = array(
            'ur_satuan' => 'required|max:100',
           
        );
        $message = [
            'ur_satuan.required' => 'Harap masukan satuan barang',
            
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('pengadaan/satuan/'. $id .'/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            // update
            $satuan = Satuan::find($id);
            $data_input = $request->input();
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $satuan->$key = $val;
            }
            $satuan->save();

            // redirect
            Session::flash('message', 'Data satuan berhasil diupdate');
            return Redirect::to('pengadaan/satuan');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $satuan = Satuan::find($id);
        
        if($satuan->delete()){
            return response()
            ->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()
            ->json(['code' => '50', 'message' => 'Data gagal dihapus']);
        }
    }
}
