<?php

namespace Modules\Pengadaan\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use App\Kontrak;
use App\Vendor;
use App\Paket;
use App\DetailKontrak;
use DB;
use Hash;
use App\Roles;
use Validator;
use Session;
use Redirect;
use View;

class KontrakController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
     public function index(\Illuminate\Http\Request $request)
    {
         if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Kontrak::select(DB::raw('*, @rownum := @rownum +1 as rownum'),'rf_kontrak.id','rf_kontrak.nomor_kontrak','rf_kontrak.nama_pekerjaan','rf_kontrak.jangka_waktu','rf_kontrak.tanggal_kontrak','rf_kontrak.nilai_kontrak','rf_kontrak.attachment_file','rf_vendor.nama_vendor as vendor_name')
            ->join('rf_vendor', 'rf_vendor.id', '=', 'rf_kontrak.vendor');
            return Datatables::of($datas)
            ->addColumn('jangka_waktu', function ($data) { return $data->jangka_waktu.' '.'hari'; })    
            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                 '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                    <li><a href="kontrak/'.$data->id.'/edit">Edit</a></li>
                                    <li><a href="kontrak/'.$data->id.'" data-id="" class="btn-delete-on-table">Delete</a></li>
                                  </ul><div>'
                        ;})
            ->make(true);
        }
        return view('pengadaan::kontrak.index');
    }

    public function index_download(\Illuminate\Http\Request $request)
    {
         if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Kontrak::select(DB::raw('*, @rownum := @rownum +1 as rownum'),'rf_kontrak.id','rf_kontrak.nomor_kontrak','rf_kontrak.nama_pekerjaan','rf_kontrak.jangka_waktu','rf_kontrak.tanggal_kontrak','rf_kontrak.nilai_kontrak','rf_kontrak.attachment_file','rf_vendor.nama_vendor as vendor_name')
                ->join('rf_vendor', 'rf_vendor.id', '=', 'rf_kontrak.vendor');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                 '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                    <li><a href="kontrak/download/'.$data->id.'">Download</a></li>
                                    
                                  </ul><div>'
                        ;})
            ->make(true);
        }
        return view('pengadaan::download.index');
    }

    public function download($id){
        $find = kontrak::find($id);
        return response()->download('uploadsKontrak'.DIRECTORY_SEPARATOR.$find->id.DIRECTORY_SEPARATOR.$find->attachment_file);
    
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $vendor = Vendor::orderBy('nama_vendor','asc')->get();
        $vendor = $vendor->pluck('nama_vendor','id');
        $paket = Paket::orderBy('nama_paket','asc')->get();
        $paket = $paket->pluck('nama_paket', 'kode_paket');
        return view('pengadaan::kontrak.create', compact('vendor','paket'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'vendor' => 'required|max:255',
            'nomor_kontrak' => 'required|max:255',
            'nama_pekerjaan' => 'required|max:255',
            'jangka_waktu' => 'required|max:255',
            'tanggal_kontrak' => 'required|max:255',
            'nilai_kontrak' => 'required|max:255',
            'masa_start' => 'required|max:255',
            'masa_end' => 'required|max:255',
            'detail_kontrak' => 'present|array',
            'attachment_file' => 'required|max:10000',
        );
        $message = [
            'vendor.required' => 'Harap pilih Kode Paket',
            'nomor_kontrak.required' => 'Harap masukan Nama Paket',
            'nama_pekerjaan.required' => 'Harap masukan Isi Per Box',
            'jangka_waktu.required' => 'Harap masukan jangka waktu pekerjaan',
            'tanggal_kontrak.required' => 'Harap masukan tanggal kontrak',
            'nilai_kontrak.required' => 'Harap masukan nilai kontrak',
            'masa_start.required' => 'Harap masukan masa kontrak mulai',
            'masa_end.required' => 'Harap masukan masa kontrak berakhir',
            'detail_kontrak.present' => 'Harap masukan detail barang',
            'detail_kontrak.array' => 'Harap masukan detail barang',
            'attachment_file' => 'Kolom file tidak boleh lebih dari 10 mb',
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('pengadaan/kontrak/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $kontrak = new Kontrak;
            $data_input = $request->input();
            $data_input['nilai_kontrak'] = str_replace( "," , "" , $data_input['nilai_kontrak']);
            $data_detail = $data_input['detail_kontrak'];
            $file= Input::file('attachment_file');
            $data_input['attachment_file']=$file->getClientOriginalName();
            unset($data_input['_method']);
            unset($data_input['_token']);
            unset($data_input['detail_kontrak'],$data_input['qty'],$data_input['kode_paket']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $kontrak->$key = $val;
            }
            $kontrak->save();
            
            $file->move('uploadsKontrak'.DIRECTORY_SEPARATOR.$kontrak->id, $file->getClientOriginalName());
            //SAVE DETAIL KONTRAK//
            foreach ($data_detail as $skey => $row) {
                $kontrakDetail = new DetailKontrak;
                $kontrakDetail->rf_kontrak_id = $kontrak->id;
                foreach ($row as $skey => $val) {
                    $kontrakDetail->$skey = $val;
                }
                $kontrakDetail->save();
            }

            // redirect
            Session::flash('message', 'Data pengadaan berhasil disimpan');
            return Redirect::to('pengadaan/kontrak');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $kontrak = Kontrak::find($id);
        $paket = Paket::orderBy('nama_paket','asc')->get();
        $paket = $paket->pluck('nama_paket', 'kode_paket');

        $kontrakDetail = DetailKontrak::where('rf_kontrak_id','=',$id)
        ->join('rf_paket','rf_paket.kode_paket','rf_detail_kontrak.kode_paket')
        ->orderBy('rf_detail_kontrak.id','ASC')
        ->select(
            'rf_paket.kode_paket',
            'rf_detail_kontrak.id',
            'rf_paket.nama_paket',
            'rf_detail_kontrak.qty'
        )
        ->get();
        
        $vendor = Vendor::orderBy('nama_vendor','asc')->get();
        $vendor = $vendor->pluck('nama_vendor','id');
        return view('pengadaan::kontrak.edit', compact('vendor','kontrak','id','kontrakDetail','paket'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
         $rules = array(
            'vendor' => 'required|max:255',
            'nomor_kontrak' => 'required|max:255',
            'nama_pekerjaan' => 'required|max:255',
            'jangka_waktu' => 'required|max:255',
            'tanggal_kontrak' => 'required|max:255',
            'nilai_kontrak' => 'required|max:255',
            'attachment_file' => 'sometimes|max:10000',
            'masa_start' => 'required|max:255',
            'masa_end' => 'required|max:255',
            'detail_kontrak' => 'present|array',
        );
        $message = [
            'vendor.required' => 'Harap pilih Kode Paket',
            'nomor_kontrak.required' => 'Harap masukan Nama Paket',
            'nama_pekerjaan.required' => 'Harap masukan Isi Per Box',
            'jangka_waktu.required' => 'Harap masukan jangka waktu pekerjaan',
            'tanggal_kontrak.required' => 'Harap masukan tanggal kontrak',
            'nilai_kontrak.required' => 'Harap masukan nilai kontrak',
            'masa_start.required' => 'Harap masukan masa kontrak mulai',
            'masa_end.required' => 'Harap masukan masa kontrak berakhir',
            'detail_kontrak.present' => 'Harap masukan detail barang',
            'detail_kontrak.array' => 'Harap masukan detail barang',
            'attachment_file' => 'Kolom file tidak boleh lebih dari 10 mb',
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('pengadaan/kontrak/'. $id .'/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            // update
            $kontrak = Kontrak::find($id);
            $data_input = $request->input();
            $data_input['nilai_kontrak'] = str_replace( "," , "" , $data_input['nilai_kontrak']);

            $data_detail = $data_input['detail_kontrak'];
            $file= Input::file('attachment_file');
            if(!empty($file)){
                $file->move('uploadsKontrak'.DIRECTORY_SEPARATOR.$kontrak->id, $file->getClientOriginalName());
                $data_input['attachment_file']=$file->getClientOriginalName();    
            }
            
            unset($data_input['_method']);
            unset($data_input['_token']);
            unset($data_input['detail_kontrak'],$data_input['qty'],$data_input['kode_paket']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $kontrak->$key = $val;
            }
            $kontrak->save();

            //DELETE OLD DATA//
            $kontrakDetailOLD = DetailKontrak::where('rf_kontrak_id','=',$id);
            $kontrakDetailOLD->delete();
            ///////////////////

            foreach ($data_detail as $skey => $row) {
                $kontrakDetail = new DetailKontrak;
                $kontrakDetail->rf_kontrak_id = $kontrak->id;
                foreach ($row as $skey => $val) {
                    $kontrakDetail->$skey = $val;
                }
                $kontrakDetail->save();
            }

            // redirect
            Session::flash('message', 'Data paket berhasil diupdate');
            return Redirect::to('pengadaan/kontrak');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
         $kontrak = Kontrak::find($id);
        
        if($kontrak->delete()){
            //DELETE OLD DATA//
            $kontrakDetailOLD = DetailKontrak::where('rf_kontrak_id','=',$id);
            $kontrakDetailOLD->delete();
            ///////////////////
            return response()
            ->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()
            ->json(['code' => '50', 'message' => 'Data gagal dihapus']);
        }
    }
}
