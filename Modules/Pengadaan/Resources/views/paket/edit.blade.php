@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAJEMEN BARANG</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data barang pada sistem aplikasi</small>
        </p>
    </div>
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>EDIT DATA BARANG</strong>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    @if(!empty($errors->all()))
                        <div class="alert alert-danger">
                        {{ HTML::ul($errors->all()) }}
                        </div>
                    @endif
                   
                    {{ Form::model($paket, ['url' => array('administrator/paket', $paket->id), 'method' => 'PUT']) }}
                         <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('kode_paket', 'KODE BARANG') }}
                                        {{ Form::text('kode_paket', Input::old('kode_paket'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('nama_paket', 'NAMA BARANG') }}
                                        {{ Form::text('nama_paket', Input::old('nama_paket'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('rf_satuan_id', 'SATUAN BARANG') }}
                                        {{ Form::select('rf_satuan_id', $satuan,Input::old('rf_satuan_id'), array('class' => 'form-control rf_satuan_id', 'placeholder' => 'Pilih Satuan Barang')) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('isi_per_box', 'ISI PER BOX') }}
                                        {{ Form::text('isi_per_box', Input::old('isi_per_box'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                 <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('berat_per_box', 'BERAT PER KOLI (Kg)') }}
                                        {{ Form::text('berat_per_box', Input::old('berat_per_box'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('berat_per_box_gram', 'BERAT PER KOLI (Gr)') }}
                                        {{ Form::text('berat_per_box_gram', Input::old('berat_per_box_gram'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('berat', 'BERAT (Kg)') }}
                                        {{ Form::text('berat', Input::old('berat'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('berat_gram', 'BERAT (Gr)') }}
                                        {{ Form::text('berat_gram', Input::old('berat_gram'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('panjang', 'PANJANG (M)') }}
                                        {{ Form::text('panjang', Input::old('panjang'), array('class' => 'form-control','id' =>'numPanjangM','onkeyup' => 'calculateSumM()')) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('panjang_cm', 'PANJANG (CM)') }}
                                        {{ Form::text('panjang_cm', Input::old('panjang_cm'), array('class' => 'form-control','id' =>'numPanjangCM','onkeyup' => 'calculateSumCM()')) }}
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('lebar', 'LEBAR (M)') }}
                                        {{ Form::text('lebar', Input::old('lebar'), array('class' => 'form-control', 'id' => 'numLebarM','onkeyup' => 'calculateSumM()')) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('lebar_cm', 'LEBAR (CM)') }}
                                        {{ Form::text('lebar_cm', Input::old('lebar_cm'), array('class' => 'form-control', 'id' => 'numLebarCM','onkeyup' => 'calculateSumCM()')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('tinggi', 'TINGGI (M)') }}
                                        {{ Form::text('tinggi', Input::old('tinggi'), array('class' => 'form-control','id'=>'numTinggiM','onkeyup' => 'calculateSumM()')) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('tinggi_cm', 'TINGGI (CM)') }}
                                        {{ Form::text('tinggi_cm', Input::old('tinggi_cm'), array('class' => 'form-control','id'=>'numTinggiCM','onkeyup' => 'calculateSumCM()')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('volume', 'VOLUME (M3)') }}
                                        {{ Form::text('volume', Input::old('volume'), array('class' => 'form-control','id'=>'sumM','readonly')) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('volume_cm', 'VOLUME (CM3)') }}
                                        {{ Form::text('volume_cm', Input::old('volume_cm'), array('class' => 'form-control','id'=>'sumCM','readonly')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('rf_kelompok_barang_id', 'KATEGORI BARANG') }}
                                        {{ Form::select('rf_kelompok_barang_id', $kelompok,Input::old('rf_kelompok_barang_id'), array('class' => 'form-control rf_kelompok_barang_id', 'placeholder' => 'Pilih Kategori Barang')) }}
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('harga_total', 'HARGA TOTAL') }}
                                        {{ Form::text('harga_total', Input::old('harga_total'), array('class' => 'form-control onlyNumberComma')) }}
                                    </div>
                                </div>
                            </div>
                                {{ Form::button('<i class="icon icon-save"></i> Simpan', array('class' => 'btn btn-primary','type'=>'submit')) }}
                            <a href="{{url($prefix.'/paket')}}" class="btn btn-primary"><i class="icon icon-arrow-left"></i> Kembali</a>
                        {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop
@section('scriptBlock')
<script type="text/javascript">
     $(".rf_satuan_id").select2();
    $(".rf_kelompok_barang_id").select2();
  function calculateSumM()
    {
      var numPanjangM = parseInt(document.getElementById("numPanjangM").value);
      var numLebarM = parseInt(document.getElementById("numLebarM").value);
      var numTinggiM = parseInt(document.getElementById("numTinggiM").value);
     
      // var numBeratbox = parseInt(document.getElementById("numBeratbox").value);
      // var numBerat = parseInt(document.getElementById("numBerat").value);
      document.getElementById("sumM").value = numPanjangM * numLebarM * numTinggiM;
       
     
    }

     function calculateSumCM()
    {
      
      var numPanjangCM= parseInt(document.getElementById("numPanjangCM").value);
      var numLebarCM = parseInt(document.getElementById("numLebarCM").value);
      var numTinggiCM = parseInt(document.getElementById("numTinggiCM").value);
      // var numBeratbox = parseInt(document.getElementById("numBeratbox").value);
      // var numBerat = parseInt(document.getElementById("numBerat").value);
      document.getElementById("sumCM").value = numPanjangCM * numLebarCM * numTinggiCM;
  }
</script>
@stop
