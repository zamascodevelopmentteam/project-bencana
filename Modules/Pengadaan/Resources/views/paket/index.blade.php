@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAJEMEN BARANG</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data barang pada sistem aplikasi</small>
        </p>
    </div>
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>DATA BARANG</strong>
                </div>
                <div class="card-body">
                  <a href="{{url(strtolower(config('pengadaan.name')).'/paket/create')}}" class="btn btn-primary"><i class="icon icon-plus-square"></i> Tambah Data</a>
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    <table class="table table-striped table-bordered table-hover dataTables-data table-responsive" width="100%">
                        <thead>
                            <tr>
                                <th>Aksi</th>
                                <th>No.</th>
                                <th>Kode Barang</th>
                                <th>Nama Barang</th>
                                <th>Satuan</th>
                                <th>Isi Per Box</th>
                                <th>Berat Per Koli (Kg)</th>
                                <th>Berat Per Koli (Gr)</th>
                                <th>Berat (Kg)</th>
                                <th>Berat (Gr)</th>
                                <th>Panjang (M)</th>
                                <th>Panjang (CM)</th>
                                <th>Lebar (M)</th>
                                <th>Lebar (CM)</th>
                                <th>Tinggi (M)</th>
                                <th>Tinggi (CM)</th>
                                <th>Volume (M3)</th>
                                <th>Volume (CM3)</th>
                                <th>Kategori Barang</th>
                                <th>Harga Total</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scriptBlock')
    <script>
            var table = $(".dataTables-data");
            var dataTable = table.DataTable({
            responsive:!0,
            "serverSide":true,
            "processing":true,
            "ajax":{
                url : "{{url($prefix.'/paket')}}"
            },
            dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
            language:{
                paginate:{
                    previous:"&laquo;",
                    next:"&raquo;"
                },search:"_INPUT_",
                searchPlaceholder:"Search..."
            },
            "columns":[
                {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "30"},
                {"data":"rownum","name":"rownum","searchable":false,"orderable":true},
                {"data":"kode_paket","name":"kode_paket","searchable":true,"orderable":true},                
                {"data":"nama_paket","name":"nama_paket","searchable":true,"orderable":true},
                {"data":"satuan","name":"rf_satuan_id","searchable":true,"orderable":true},
                {"data":"isi_per_box","name":"isi_per_box","searchable":true,"orderable":true},
                {"data":"berat_per_box","name":"berat_per_box","searchable":true,"orderable":true},
                {"data":"berat_per_box_gram","name":"berat_per_box_gram","searchable":true,"orderable":true},
                {"data":"berat","name":"berat","searchable":true,"orderable":true},
                {"data":"berat_gram","name":"berat_gram","searchable":true,"orderable":true},
                {"data":"panjang","name":"panjang","searchable":true,"orderable":true},
                {"data":"panjang_cm","name":"panjang_cm","searchable":true,"orderable":true},
                {"data":"lebar","name":"lebar","searchable":true,"orderable":true},
                {"data":"lebar_cm","name":"lebar_cm","searchable":true,"orderable":true},
                {"data":"tinggi","name":"tinggi","searchable":true,"orderable":true},
                {"data":"tinggi_cm","name":"tinggi_cm","searchable":true,"orderable":true},
                {"data":"volume","name":"volume","searchable":true,"orderable":true},
                {"data":"volume_cm","name":"volume_cm","searchable":true,"orderable":true},
                {"data":"kelompok_barang","name":"rf_kelompok_barang_id","searchable":true,"orderable":true},
                {"data":"harga_total","name":"harga_total","searchable":true,"orderable":true},
                // {"data":"created_at","name":"created_at","searchable":true,"orderable":true},
            ],
            order:[[1,"asc"]]
        })

    </script>

@stop
