@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAJEMEN VENDOR</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data vendor sistem aplikasi</small>
        </p>
    </div>
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>TAMBAH DATA VENDOR</strong>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    @if(!empty($errors->all()))
                        <div class="alert alert-danger">
                        {{ HTML::ul($errors->all()) }}
                        </div>
                    @endif
                    {{ Form::open(array('url' => $prefix.'/vendor')) }}
                           <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('nama_vendor', 'NAMA VENDOR') }}
                                        {{ Form::text('nama_vendor', Input::old('nama_vendor'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                         
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('kode_propinsi', 'NAMA PROVINSI') }}
                                        {!! Form::select('kode_propinsi', $propinsi, null, ['class' => 'form-control kode_propinsi', 'id' => 'kode_propinsi', 'placeholder' => 'Pilih Provinsi']) !!}{{-- 
                                        {{ Form::select('kode_propinsi', $propinsi, Input::old('kode_propinsi'), array('class' => 'form-control', 'placeholder' => 'Pilih Propinsi', 'id' => 'kode_propinsi')) }} --}}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                         {{ Form::label('kode_kabupaten', 'NAMA KABUPATEN') }}

                                        {!! Form::select('kode_kabupaten', [], null, ['class' => 'form-control kode_kabupaten', 'id' => 'kode_kabupaten', 'placeholder' => 'Pilih Kabupaten']) !!}

                                       {{--  {{ Form::select('kode_kabupaten', $kabupaten, Input::old('kode_kabupaten'), array('class' => 'form-control', 'placeholder' => 'Pilih Propinsi','id'=>'kode_kabupaten')) }} --}}
                                    </div>
                                </div>
                           
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('alamat', 'ALAMAT') }}
                                        {{ Form::text('alamat', Input::old('alamat'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                           </div>
                           <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('telepon', 'TELEPON') }}
                                        {{ Form::text('telepon', Input::old('telepon'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('pic', 'PIC') }}
                                        {{ Form::text('pic', Input::old('pic'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                         
                            {{ Form::button('<i class="icon icon-save"></i> Simpan', array('class' => 'btn btn-primary','type'=>'submit')) }}
                            <a href="{{url($prefix.'/vendor')}}" class="btn btn-primary"><i class="icon icon-arrow-left"></i> Kembali</a>
                        {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop
@section('scriptBlock')
<script type="text/javascript">
     $(".kode_propinsi").select2();
    $(".kode_kabupaten").select2();
    $("#kode_propinsi").on("change",function(e) {
          var idKodeProvince = $(this).val();//ID KODE PROVINSI SELECTBOX
          if(idKodeProvince.length != 0){
            $.ajax({
              url : "{{url('administrator/vendor/get-districts')}}/" + idKodeProvince,
              dataType : "json",
              success : function(result){
                var html = "<option value=''>Pilih Kabupaten</option>";
                $.each(result,function(e,item){
                  html += "<option value='"+item.kode_kabupaten+"'>"+item.nama_kabupaten+"</option>"
                })
                $("#kode_kabupaten").html(html);
                
              }
            })
          }else{
            $("#kode_kabupaten").html("<option value=''>Pilih Kota</option>");
          }
        })
</script>
@stop



