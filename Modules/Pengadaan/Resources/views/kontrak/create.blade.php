@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAJEMEN KONTRAK</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data kontrak pada sistem aplikasi</small>
        </p>
    </div>
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>DATA KONTRAK</strong>
{{--  --}}
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    @if(!empty($errors->all()))
                        <div class="alert alert-danger">
                        {{ HTML::ul($errors->all()) }}
                        </div>
                    @endif
                    {{ Form::open(array('url' => $prefix.'/kontrak','files' => true)) }}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('vendor', 'NAMA VENDOR') }}
                                        {{ Form::select('vendor', $vendor,Input::old('vendor'), array('class' => 'form-control', 'placeholder' => 'Pilih Vendor', 'id' => 'vendor')) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('nomor_kontrak', 'NOMOR KONTRAK') }}
                                        {{ Form::text('nomor_kontrak', Input::old('nomor_kontrak'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                          	<div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('nama_pekerjaan', 'NAMA PEKERJAAN') }}
                                        {{ Form::text('nama_pekerjaan', Input::old('nama_pekerjaan'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('jangka_waktu', 'JANGKA WAKTU PEKERJAAN') }}
                                        {{ Form::text('jangka_waktu', Input::old('jangka_waktu'), array('class' => 'form-control calculated', 'id'=>'calculated', 'readonly' )) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('tanggal_kontrak', 'TANGGAL KONTRAK') }}
                                        {{ Form::text('tanggal_kontrak', Input::old('tanggal_kontrak'), array('class' => 'form-control datepick')) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('nilai_kontrak', 'NILAI KONTRAK') }}
                                        {{ Form::text('nilai_kontrak', Input::old('nilai_kontrak'), array('class' => 'form-control onlyNumberComma')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        {{ Form::label('TANGGAL PERIODE PENGERJAAN') }}
                                        <div class="input-daterange input-group" id="datepicker">
                                            {{ Form::text('masa_start', Input::old('masa_start'), array('class' => 'form-control fromdate', 'id' => 'date1')) }}
                                            <span class="input-group-addon">-</span>
                                            {{ Form::text('masa_end', Input::old('masa_end'), array('class' => 'form-control todate','id'=>'date2')) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('attachment_file', 'ATTACHMENT FILE (MAX 10 MB)') }}
                                        {{ Form::file('attachment_file', Input::old('attachment_file'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-input">
                                            <thead>
                                                <tr>
                                                    <th width="10%">NO</th>
                                                    <th>JENIS BARANG</th>
                                                    <th width="15%">QTY</th>
                                                    <th width="5%"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="tr-input">
                                                    <td colspan="2">
                                                        {{ Form::select('kode_paket', $paket,Input::old('paket'), array('class' => 'form-control','placeholder' => 'Pilih Jenis Barang','id' => 'kode_paket')) }}
                                                    </td>
                                                    <td>
                                                        {{ Form::text('qty', Input::old('qty'), array('class' => 'form-control onlyNumber','id' => 'qty')) }}
                                                    </td>
                                                    <td>
                                                        <a href="#" class="btn btn-info btn-add-detail" >TAMBAH</a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            {{ Form::button('<i class="icon icon-save"></i> Simpan', array('class' => 'btn btn-primary','type'=>'submit')) }}
                            <a href="{{url($prefix.'/kontrak')}}" class="btn btn-primary"><i class="icon icon-arrow-left"></i> Kembali</a>
                        {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop
@section('scriptBlock')
    <script type="text/javascript">
        var countDetail = 0;
        $("#vendor").select2();
        $("#kode_paket").select2();
        $(".btn-add-detail").on("click",function(e){
            e.preventDefault();
            var kode_paket = $("#kode_paket");
            var qty = $("#qty");
            if(kode_paket.val() == "" || qty.val() == '')
            {
                alert('masukan paket dan qty');
                return false;
            }

            var paket = $("#kode_paket option[value='"+kode_paket.val()+"']").html();
            var no = countDetail +1;
            var html = "<tr>"
                      +"<td>"
                      +no  
                      +"</td>"
                      +"<td>"
                      +paket
                      +"</td>"
                      +"<td>"
                      +$.number(qty.val(),0)
                      +"</td>"
                      +"<td>"
                      +"<a href='#' class='btn btn-danger btn-remove-detail'>HAPUS</a>"
                      +"<input type='hidden' value='"+kode_paket.val()+"' name='detail_kontrak["+countDetail+"][kode_paket]' class='detailKodePaket'>"
                      +"<input type='hidden' value='"+qty.val()+"' name='detail_kontrak["+countDetail+"][qty]' class='detailQty'>" 
                      +"</td>"
                      +"</tr>";
            countDetail = countDetail +1;
            kode_paket.val('').trigger('change');;
            qty.val('');
            $(".table-input tbody").append(html);
        })

        $("body").on("click",".btn-remove-detail",function(e){
            e.preventDefault();
            var tr = $(this).closest('tr');
            tr.remove();
            countDetail = countDetail -1;

            $.each($(".table-input tbody tr:not(.tr-input)"),function(i,item){
                console.log(i);
                var paket = $(this).find('.detailKodePaket');
                var no = $(this).find('td').first();
                var qty = $(this).find('.detailQty');
                var numer = i + 1 *1;
                no.html(numer);
                paket.attr('name',"detail_kontrak["+i+"][kode_paket]");
                qty.attr('name',"detail_kontrak["+i+"][qty]");
                console.log(no);

            })
        })
        $('.input-daterange').datepicker({
           format:"yyyy-mm-dd", 
        });


        $(document).ready(function () {
            var selector = function (dateStr) {
                    var d1 = $('.fromdate').datepicker('getDate');
                    var d2 = $('.todate').datepicker('getDate');
                    var diff = 0;
                    if (d1 && d2) {
                        diff = Math.floor(((d2.getTime() - d1.getTime()) / 86400000)+1); // ms per day
                    }
                    $('.calculated').val(diff);
                }
            $(".fromdate").datepicker({
                minDate: new Date(2012, 7 - 1, 8),
                maxDate: new Date(2012, 7 - 1, 28)
            });
            $('.todate').datepicker({ 
                minDate: new Date(2012, 7 - 1, 9),
                maxDate: new Date(2012, 7 - 1, 28)
            });
            $('.fromdate,.todate').change(selector)
        });
    </script>
@stop