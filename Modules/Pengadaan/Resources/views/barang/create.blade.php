@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAGEMENT BARANG</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data barang pada sistem aplikasi</small>
        </p>
    </div>
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>TAMBAH DATA BARANG</strong>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    @if(!empty($errors->all()))
                        <div class="alert alert-danger">
                        {{ HTML::ul($errors->all()) }}
                        </div>
                    @endif
                    {{ Form::open(array('url' => $prefix.'/barang')) }}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('kode_isi_paket', 'Kode Isi Paket') }}
                                        {{ Form::text('kode_isi_paket', Input::old('kode_isi_paket'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('kode_paket', 'Nama Paket') }}
                                        {{ Form::select('kode_paket', $paket,Input::old('kode_paket'), array('class' => 'form-control', 'placeholder' => 'Pilih Paket')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('ur_isi_paket', 'Nama Isi Paket') }}
                                        {{ Form::text('ur_isi_paket', Input::old('ur_isi_paket'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('satuan', 'Satuan') }}
                                        {{ Form::text('satuan', Input::old('satuan'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                            
                            {{ Form::button('<i class="icon icon-save"></i> Simpan', array('class' => 'btn btn-primary','type'=>'submit')) }}
                            <a href="{{url($prefix.'/barang')}}" class="btn btn-primary"><i class="icon icon-arrow-left"></i> Kembali</a>
                        {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop

