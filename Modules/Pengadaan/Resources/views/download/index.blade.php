@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAGEMENT DOWNLOAD KONTRAK</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data download kontrak pada sistem aplikasi</small>
        </p>
    </div>
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>DATA KONTRAK</strong>
                </div>
                <div class="card-body">{{-- 
                    <a href="{{url(strtolower(config('pengadaan.name')).'/kontrak/create')}}" class="btn btn-primary"><i class="icon icon-plus-square"></i> Tambah Data</a>
 --}}                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    <table class="table table-striped table-bordered table-hover dataTables-data" width="100%">
                        <thead>
                             <tr>
                                <th>Aksi</th>
                                <th>No.</th>
                                <th>Pilih Vendor</th>
                                <th>Nomor Kontrak</th>
                                <th>Nama Pekerjaan</th>
                                <th>Jangka Waktu Pekerjaan</th>
                                <th>Tanggal Kontrak</th>
                                <th>Nilai Kontrak</th>
                                <th>Upload File</th>
                                
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scriptBlock')
    <script>
          var table = $(".dataTables-data");
            var dataTable = table.DataTable({
            responsive:!0,
            "serverSide":true,
            "processing":true,
            "ajax":{
                url : "{{url($prefix.'/download')}}"
            },
            dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
            language:{
                paginate:{
                    previous:"&laquo;",
                    next:"&raquo;"
                },search:"_INPUT_",
                searchPlaceholder:"Search..."
            },
            "columns":[
                {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "30"},
                {"data":"rownum","name":"rownum","searchable":false,"orderable":true},
                {"data":"vendor_name","name":"vendor","searchable":true,"orderable":true},
                {"data":"nomor_kontrak","name":"nomor_kontrak","searchable":true,"orderable":true},
                {"data":"nama_pekerjaan","name":"nama_pekerjaan","searchable":true,"orderable":true},
                {"data":"jangka_waktu","name":"jangka_waktu","searchable":true,"orderable":true},
                {"data":"tanggal_kontrak","name":"tanggal_kontrak","searchable":true,"orderable":true},
                {"data":"nilai_kontrak","name":"nilai_kontrak","searchable":true,"orderable":true},
                {"data":"attachment_file","name":"attachment_file","searchable":true,"orderable":true},

                // {"data":"created_at","name":"created_at","searchable":true,"orderable":true},
            ],
            order:[[1,"asc"]]
        })

    </script>

@stop
