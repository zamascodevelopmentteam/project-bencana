@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAJEMEN PENGELUARAN BARANG</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data pengeluaran barang pada sistem aplikasi</small>
        </p>
    </div>
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>EDIT DATA PENGELUARAN BARANG</strong>
                    </div>
                <div class="card-body">
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    @if(!empty($errors->all()))
                        <div class="alert alert-danger">
                        {{ HTML::ul($errors->all()) }}
                        </div>
                    @endif
                    {{ Form::model($pengiriman, ['route' => array('pengiriman.update', $pengiriman->id), 'method' => 'PUT', 'files' => true]) }}
                           <div class="row" id="kontrak">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('rf_permintaan_id', 'NOMOR PERMINTAAN') }}
                                        {{ Form::select('rf_permintaan_id',$permintaan, Input::old('rf_permintaan_id'), array('class' => 'form-control', 'placeholder' => 'Pilih Nomor Kontrak', 'id' => 'text_permintaan')) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('rf_gudang_id','NAMA GUDANG') }}
                                        {{ Form::select('rf_gudang_id',$gudang, Input::old('rf_gudang_id'), array('class' => 'form-control', 'placeholder' => 'Pilih Gudang','id' => 'rf_gudang_id')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('tanggal_kirim', 'TANGGAL PENGELUARAN') }}
                                        {{ Form::text('tanggal_kirim', Input::old('tanggal_kirim'), array('class' => 'form-control datepick')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="bantuan">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('nomor_pengiriman', 'NOMOR PENGELUARAN') }}
                                        {{ Form::text('nomor_pengiriman', Input::old('nomor_pengiriman'), array('class' => 'form-control','id'=>'text_bantuan')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="table_permintaan">
                                <div class="col-md-12">
                                    <div class="table-responsive" style="margin-bottom:10px;">
                                        <table class="table table-input">
                                            <thead>
                                                <tr>
                                                    <th width="5%%">NO</th>
                                                    <th width="20%">JENIS BARANG</th>
                                                    <th width="5%">QTY KONTRAK</th>
                                                    <th width="5%">QTY TELAH DIKIRIM</th>
                                                    <th width="5%">SISA</th>
                                                    <th width="10%">QTY KIRIM</th>
                                                </tr>
                                            </thead>
                                            <thead>
                                                @foreach($pengirimanDetail as $key => $detail)
                                                    <tr>
                                                      <td>{{$key + 1}}</td>
                                                      <td>{{$detail->paket_name}}</td>
                                                      <td>{{$detail->qty}}</td>
                                                      <td>{{$detail->total_diterima}}</td>
                                                      <td>{{$detail->sisa}}</td>
                                                      <td>
                                                        <input type='hidden' value='{{$detail->kode_paket}}' name='detail_permintaan[{{$key}}][kode_paket]' class='form-control detailKodePaket'>
                                                        <input type='hidden' value='{{$detail->id}}' name='detail_permintaan[{{$key}}][rf_permintaan_detail_id]' class='detailIdKontrakDetail'>
                                                        <input type='text' value='{{$detail->qty_diterima}}' name='detail_permintaan[{{$key}}][qty_kirim]' class='form-control detailQty'></td>
                                                    
                                                    </tr>
                                                @endforeach
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            {{ Form::button('<i class="icon icon-save"></i> Simpan', array('class' => 'btn btn-primary','type'=>'submit')) }}
                            <a href="{{url($prefix.'/pengiriman')}}" class="btn btn-primary"><i class="icon icon-arrow-left"></i> Kembali</a>
                        {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop
@section('scriptBlock')
<script type="text/javascript">
	var countDetail = 0;
  $("#text_permintaan").on("change",function(e) {
          var idPermintaan = $(this).val();//ID KODE PROVINSI SELECTBOX
          $("#table_permintaan tbody").html('');
          if(idPermintaan.length != 0){
            $.ajax({
              url : "{{url('penyimpanan/pengiriman/get-permintaan')}}/" + idPermintaan + "/" + {{$id}},
              dataType : "json",
              success : function(result){
                var html = "";
                var no = 1;
                $.each(result,function(e,item){
                  html += "<tr>"
                          +"<td>"
                          +no
                          +"</td>"
                          +"<td>"
                          +item.paket_name
                          +"</td>"
                          +"<td>"
                          +item.qty
                          +"</td>"
                          +"<td>"
                          +item.total_diterima
                          +"</td>"
                          +"<td>"
                          +item.sisa
                          +"</td>"
                          +"<td>"
                          +"<input type='hidden' value='"+item.kode_paket+"' name='detail_permintaan["+countDetail+"][kode_paket]' class='form-control detailKodePaket'>" 
                          +"<input type='hidden' value='"+item.id+"' name='detail_permintaan["+countDetail+"][rf_permintaan_detail_id]' class='form-control detailIdPermintaanDetail'>" 
                          +"<input type='text' value='"+item.sisa+"' name='detail_permintaan["+countDetail+"][qty_kirim]' class='form-control detailQty'>" 
                          +"</td>"
                          +"<tr>";

                  no = no +1;
                  countDetail = countDetail + 1;
                })
                $("#table_permintaan tbody").html(html);              }
            })
          }else{
            $("#table_permintaan tbody").html('');
          }
        })
</script>
@stop


