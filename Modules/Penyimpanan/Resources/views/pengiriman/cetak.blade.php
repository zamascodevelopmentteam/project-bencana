<!DOCTYPE html>
<html>
<head>
{{--     <link rel="stylesheet" type="text/css" href="../../../../assets/css/elephant.min.css">
 --}}<style>
    body{
        background-color: #0000;
    }


    .table-detail{
        width:100%;
        border:1px solid #000;
        margin:20px 0px;
        border-spacing: 0;
        border-collapse: collapse;
    }
    
    .table-detail thead tr th{
        padding:10px 5px;
        border-right:1px solid #000;
        border-bottom:1px solid #000;
    }
    .table-detail thead tr th:last-child{
        border-right:none;
    }
    .table-detail tbody tr td{
        padding:5px 5px;
        border-right:1px solid #000;
        border-bottom:1px solid #000;
    }
    .table-detail tbody tr td:last-child{
        border-right:none;
    }
    .table-detail tbody tr:last-child td{
        border-bottom:none;
    }
    .text-center{
        text-align:center;
    }
    .text-right{
        text-align:right;
    }
    .signature{
        display:inline-block;
        margin-top:30px;
        text-align:left;
        padding:0px 30px;
    }

    .table-striped tr th{
    font-family: Arial, sans-serif;
    }


    .table-striped tr td{
    font-family: Arial, sans-serif;
    }
</style>


</head>
<body>
<div class="container">
    <div class="row">
     <table width="90%" class="table-striped" border="" style="margin-top:17px;">
        <tr>
            <td width="20%" rowspan="3"  >
                <img src="{{ asset('assets/img/logo_bnpb.jpg') }}" width="220" height="120" />
            </td>
            <td width="93%" valign="top"><strong>&nbsp;Laporan Pengeluaran Barang</strong></td>
        </tr>
        <tr>
            <td valign="top">&nbsp;Kepada Yth.
            Kepala {{$data->gudang_name}}<br></td>
        </tr>
        <br>
        <tr >
            <td valign="top">&nbsp;{{$data->alamat_name}} <div class="text-right">Jakarta, {{tanggal_indo($data->tanggal_kirim)}}</div></td>
        </tr>
    </table>
    <hr>
   <table  style="" class="table table-striped table-bordered table-hover" border="">
      <tr>
        <th width="15%">Nomor</th>
        <th width="3%" class="text-center">:</th>
        <td width="19%">{{$data->nomor_pengiriman}}</td>
        <td width="30%">&nbsp;</td>
        <th width="15%"></th>
        <th width="3%"></th>
        <td width="19%"></td>
    </tr>
    <tr>
        <th width="15%">Sifat</th>
        <th width="3%" class="text-center">:</th>
        <td width="19%">Segera</td>
        <td width="30%">&nbsp;</td>
        <th width="15%"></th>
        <th width="3%"></th>
        <td width="19%"></td>
    </tr>
    <tr>
        <th width="15%">Perihal</th>
        <th width="3%" class="text-center">:</th>
        <td width="19%">Pengeluaran Barang (PB)</td>
        <td width="30%">&nbsp;</td>
        <th width="15%"></th>
        <th width="3%"></th>
        <td width="19%"></td>
    </tr>
   </table>
   <br>
    <table class="table table-striped table-bordered table-hover" border=""  >
    <tr>
       
        <td colspan="5" style="">
        Dalam rangka penyerahan bantuan Logistik untuk {{$data->keterangan}}, BNPB akan memberikan bantuan berupa:
        </td>
    </tr>
    <tr>
        <td colspan="6">
             <table class="table-detail">
                <thead>
                    <tr>
                        <th width="10%">No</th>
                        <th>Jenis Barang Bantuan</th>
                        <th width="15%">Jumlah Kirim</th>
                        <th width="15%">Satuan</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($details as $key => $r)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$r->nama_paket}}</td>
                        <td>{{$r->qty}}</td>
                        <td>{{$r->ur_satuan}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            Dengan ketentuan, sebagai berikut :
            <ol>
                <li>Surat PB ini berlaku selama 7 (tujuh) hari kalender sejak tanggal dikeluarkan;</li>
                <li>Apabila telah melebihi 7 (tujuh) hari sejak tanggal dikeluarkan, maka surat PB ini tidak berlaku dan akan diterbitkan surat PB yang baru.;</li>
                <li>Biaya handling out dan lembur dari ;</li>
            </ol>
            <br>
            <p class="text-left">
                Sehubungan dengan hal tersebut, kiranya Saudara dapat mengeluarkan barang bantuan dimaksud dari gudang.<br>
                Demikian atas kerjasamanya disampaikan terima kasih.
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="6">
            <div>
                <p id="jabatan" style="margin-bottom:100px;">Direktur Logistik,</p>
                <p id="namapejabat" style="">Dra. Prasinta Dewi, MAP.</p>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="6">
            <p>Tembusan :<br>
                Yth. Deputi Bidang Logistik dan Peralatan.
            </p>
        </td> 
    </tr>
</table>

    </div>
</div>

</body>
</html>
