<!DOCTYPE html>
<html>
<head>
{{--     <link rel="stylesheet" type="text/css" href="../../../../assets/css/elephant.min.css">
 --}}<style>
    body{
        background-color: #0000;
    }


    .table-detail{
        width:100%;
        border:1px solid #000;
        margin:20px 0px;
        border-spacing: 0;
        border-collapse: collapse;
    }
    
    .table-detail thead tr th{
        padding:10px 5px;
        border-right:1px solid #000;
        border-bottom:1px solid #000;
    }
    .table-detail thead tr th:last-child{
        border-right:none;
    }
    .table-detail tbody tr td{
        padding:5px 5px;
        border-right:1px solid #000;
        border-bottom:1px solid #000;
    }
    .table-detail tbody tr td:last-child{
        border-right:none;
    }
    .table-detail tbody tr:last-child td{
        border-bottom:none;
    }
    .text-center{
        text-align:center;
    }
    .text-right{
        text-align:right;
    }
    .signature{
        display:inline-block;
        margin-top:30px;
        text-align:left;
        padding:0px 30px;
    }

    .table-striped tr th{
    font-family: Arial, sans-serif;
    }


    .table-striped tr td{
    font-family: Arial, sans-serif;
    }
</style>


</head>
<body>
<div class="container">
    <div class="row">
     <table width="90%" class="table-striped" border="" style="margin-top:17px;">
        <tr>
            <td width="20%" rowspan="3"  >
                <img src="{{ asset('assets/img/logo_bnpb.jpg') }}" width="220" height="120" />
            </td>
            <td width="93%" valign="top"><strong>&nbsp;Laporan Penerimaan Barang</strong></td>
        </tr>
        <tr>
            <td valign="top">&nbsp;Kepada Yth.
            Kepala {{$data->gudang_name}}<br></td>
        </tr>
        <br>
        <tr >
            <td valign="top">&nbsp;{{$data->alamat_name}} <div class="text-right">{{ tanggal_indo($data->tanggal_penerimaan) }}</div></td>
        </tr>
    </table>
    <hr>
   <table  style="" class="table table-striped table-bordered table-hover" border="">
        <tr>
            <th width="">Type</th>
            <th width="3%" class="text-center">:</th>
            <td width="19%">{{$data->type}}</td>
            <th width="15%">Nomor Penerimaan</th>
            <th width="3%" class="text-center">:</th>
            <td width="19%">{{$data->nomor_penerimaan}}</td>
        </tr>
        <tr>
            <th width="15%">Nomor Kontrak</th>
            <th width="3%" class="text-center">:</th>
            <td width="19%">{{ ($data->kontrak_name )? $data->kontrak_name : '-' }}</td>
            <th width="15%">Jam Datang</th>
            <th width="3%" class="text-center">:</th>
            <td width="19%">{{$data->jam_datang}}</td>
        </tr>
        <tr>
            <th width="15%">Jam Bongkar</th>
            <th width="3%" class="text-center">:</th>
            <td width="19%">{{$data->jam_bongkar}}</td>
            <th width="15%">Jenis Kendaraan</th>
            <th width="3%" class="text-center">:</th>
            <td width="19%">{{$data->jenis_kendaraan}}</td>
        </tr>

       
        <tr>
            <th width="15%">No Kendaraan</th>
            <th width="3%" class="text-center">:</th>
            <td width="19%">{{$data->no_kendaraan}}</td>
            <th width="15%">Nama Pengemudi</th>
            <th width="3%" class="text-center">:</th>
            <td width="19%">{{$data->pengemudi}}</td>
        </tr>
        <tr>
            <th width="15%">No Hp Pengemudi</th>
            <th width="3%" class="text-center">:</th>
            <td width="19%">{{$data->no_hp_pengemudi}}</td>
            <th width="15%">No Tally Sheet</th>
            <th width="3%" class="text-center">:</th>
            <td width="19%">{{$data->no_tally_sheet}}</td>
        </tr>
        <tr>
            <th width="15%">Nama Penyedia</th>
            <th width="3%" class="text-center">:</th>
            <td width="19%">{{ ($data->nama_penyedia )? $data->nama_penyedia : '-' }}</td>
            <th width="15%">Nama Penjabat</th>
            <th width="3%" class="text-center">:</th>
            <td width="19%">{{$data->nama_pejabat}}</td>
        </tr>
        <tr>
            <th width="15%">Penanggung Biaya Handling</th>
            <th width="3%" class="text-center">:</th>
            <td width="19%">{{$data->penanggung_biaya_handling}}</td>
            <th width="15%">Tahun Pengadaan</th>
            <th width="3%" class="text-center">:</th>
            <td width="19%">{{$data->tahun_pengadaan}}</td>
        </tr>
        <tr>
            <th width="15%">Asal Surat</th>
            <th width="3%" class="text-center">:</th>
            <td width="19%">{{$data->surat_dari}}</td>
            <th width="15%">No Surat</th>
            <th width="3%" class="text-center">:</th>
            <td width="19%">{{$data->no_surat}}</td>
        </tr>
        <tr>
            <th width="15%">Tanggal Surat</th>
            <th width="3%" class="text-center">:</th>
            <td width="19%">{{$data->tanggal_surat}}</td>
            <th width="15%">Nama Pengemudi</th>
            <th width="3%" class="text-center">:</th>
            <td width="19%">{{$data->pengemudi}}</td>
        </tr>
        <tr>
            <th width="15%">Nama Vendor</th>
            <th width="3%" class="text-center">:</th>
            <td width="19%">{{ ($data->nama_vendor )? $data->nama_vendor : '-' }}</td>
            <th width="15%">Perihal</th>
            <th width="3%" class="text-center">:</th>
            <td width="1%">{{$data->perihal}}</td>
        </tr>
   </table>
   <br>
    <table class="table table-striped table-bordered table-hover" border=""  >
    <tr>
       
        <td colspan="5" style="">
        Dalam rangka penerimaan bantuan Logistik, BNPB akan menerima bantuan berupa &nbsp;:
        </td>
    </tr>
    <tr>
        <td colspan="6">
            <table class="table-detail">
                <thead>
                    <tr>
                        <th width="10%">No</th>
                        <th>Jenis Barang Bantuan</th>
                        <th width="15%">Satuan</th>
                        <th width="15%">Jumlah Penerimaan</th>
                        <th width="15%">Sumber</th>
                        <th width="15%">Keterangan</th>
                        <th width="15%">Expired</th>
                        
                    </tr>
                </thead>
                <tbody>
                    @foreach($penerimaDetail as $key => $r)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$r->nama_paket}}</td>
                        <td>{{$r->ur_satuan}}</td>
                        <td>{{$r->qty}}</td>
                        <td>{{$r->sumber}}</td>
                        <td>{{$r->keterangan}}</td>
                        <td>{{tanggal_indo($r->expired)}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            Dengan ketentuan, sebagai berikut :
            <ol>
                <li>Surat PB ini berlaku selama 7 (tujuh) hari kalender sejak tanggal dikeluarkan;</li>
                <li>Apabila telah melebihi 7 (tujuh) hari sejak tanggal dikeluarkan, maka surat PB ini tidak berlaku dan akan diterbitkan surat PB yang baru.;</li>
                <li>Biaya handling out dan lembur dari ;</li>
            </ol>
            <br>
            <p class="text-left">
                Sehubungan dengan hal tersebut, kiranya Saudara dapat mengeluarkan barang bantuan dimaksud dari gudang.<br>
                Demikian atas kerjasamanya disampaikan terima kasih.
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="6">
            <div>
                <p id="jabatan" style="margin-bottom:100px;">Direktur Logistik,</p>
                <p id="namapejabat" style="">Dra. Prasinta Dewi, MAP.</p>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="6">
            <p>Tembusan :<br>
                Yth. Deputi Bidang Logistik dan Peralatan.
            </p>
        </td> 
    </tr>
</table>

    </div>
</div>

</body>
</html>
