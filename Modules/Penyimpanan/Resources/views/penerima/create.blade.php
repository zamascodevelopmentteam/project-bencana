@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAJEMEN PENERIMA BARANG</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data penerima barang pada sistem aplikasi</small>
        </p>
    </div>
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>DATA PENERIMA BARANG</strong>
                </div>
                {{-- / --}}
                <div class="card-body">
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    @if(!empty($errors->all()))
                        <div class="alert alert-danger">
                        {{ HTML::ul($errors->all()) }}
                        </div>
                    @endif
                    {{ Form::open(array('url' => $prefix.'/penerima','files' => true)) }}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('type', 'TYPE') }}
                                        {{ Form::select('type',['KONTRAK'=>'KONTRAK','BANTUAN'=>'BANTUAN','TITIPAN'=>'TITIPAN'], Input::old('type'), array('class' => 'form-control', 'placeholder' => 'Pilih type','id'=>'type')) }}
                                    </div>
                                </div>
                                <div class="col-md-4" id="vendor">
                                    <div class="form-group">
                                        {{ Form::label('rf_vendor_id', 'NAMA VENDOR') }}
                                        {{ Form::select('rf_vendor_id',$vendor, Input::old('rf_vendor_id'), array('class' => 'form-control rf_vendor_id', 'placeholder' => 'Pilih Nama Vendor','id'=>'text_vendor')) }}
                                    </div>
                                </div>
                                <div class="col-md-4" id="kontrak">
                                    <div class="form-group">
                                        {{ Form::label('rf_kontrak_id', 'NOMOR KONTRAK') }}
                                        {{ Form::select('rf_kontrak_id', [], Input::old('rf_kontrak_id'), array('class' => 'form-control rf_kontrak_id', 'placeholder' => 'Pilih Nomor Kontrak', 'id' => 'text_kontrak')) }}
                                    </div>
                                </div>
                                <div class="col-md-4" id="bantuan">
                                    <div class="form-group">
                                        {{ Form::label('nama_penyedia', 'NAMA PENYEDIA') }}
                                        {{ Form::text('nama_penyedia', Input::old('nama_penyedia'), array('class' => 'form-control','id'=>'text_bantuan')) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('nomor_penerimaan', 'NOMOR PENERIMAAN') }}
                                        {{ Form::text('nomor_penerimaan', Input::old('nomor_penerimaan'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('rf_gudang_id','NAMA GUDANG') }}
                                        {{ Form::select('rf_gudang_id',$gudang, Input::old('rf_gudang_id'), array('class' => 'form-control rf_gudang_id', 'placeholder' => 'Pilih Gudang','id' => 'rf_gudang_id')) }}
                                    </div>
                                </div>
                          
                                <div class="col-md-4">
                                    <div class="form-group">
                                            {{ Form::label('tanggal_penerimaan', 'TANGGAL PENERIMAAN') }}
                                            {{ Form::text('tanggal_penerimaan', Input::old('tanggal_penerimaan'), array('class' => 'form-control datepick')) }}
                                    </div>
                                </div>
                           		<div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('jam_datang', 'JAM DATANG') }}
                                        {{ Form::text('jam_datang', Input::old('jam_datang'), array('class' => 'form-control datang')) }}
                                    </div>
                                </div>
                           
                           		<div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('jam_bongkar', 'JAM BONGKAR') }}
                                        {{ Form::text('jam_bongkar', Input::old('jam_bongkar'), array('class' => 'form-control bongkar')) }}
                                    </div>
                                </div>
                           		<div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('jenis_kendaraan', 'JENIS KENDARAAN') }}
                                        {{ Form::text('jenis_kendaraan', Input::old('jenis_kendaraan'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                         
                           		<div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('no_kendaraan', 'NO KENDARAAN') }}
                                        {{ Form::text('no_kendaraan', Input::old('no_kendaraan'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                           		<div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('pengemudi', 'NAMA PENGEMUDI') }}
                                        {{ Form::text('pengemudi', Input::old('pengemudi'), array('class' => 'form-control')) }}
                                    </div>
                              </div>
                           		<div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('no_hp_pengemudi', 'NO HP PENGEMUDI') }}
                                        {{ Form::text('no_hp_pengemudi', Input::old('no_hp_pengemudi'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                           		<div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('no_tally_sheet', 'NO TALLY SHEET') }}
                                        {{ Form::text('no_tally_sheet', Input::old('no_tally_sheet'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('surat_dari', 'ASAL SURAT') }}
                                        {{ Form::text('surat_dari', Input::old('surat_dari'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                           		<div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('no_surat', 'NO SURAT EKSPEDISI') }}
                                        {{ Form::text('no_surat', Input::old('no_surat'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                           		<div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('tanggal_surat', 'TANGGAL SURAT') }}
                                        {{ Form::text('tanggal_surat', Input::old('tanggal_surat'), array('class' => 'form-control datepick')) }}
                                    </div>
                                </div>
                           		<div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('perihal', 'PERIHAL') }}
                                        {{ Form::text('perihal', Input::old('perihal'), array('class' => 'form-control')) }}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('nama_pejabat', 'NAMA PEJABAT') }}
                                        {{ Form::text('nama_pejabat', Input::old('nama_pejabat'), array('class' => 'form-control')) }}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('tahun_pengadaan', 'TAHUN PENGADAAN') }}
                                        {{ Form::text('tahun_pengadaan', Input::old('tahun_pengadaan'), array('class' => 'form-control year')) }}
                                    </div>
                                </div>
                           		
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('penanggung_biaya_handling', 'PENANGGUNG BIAYA HANDLING') }}
                                        {{ Form::text('penanggung_biaya_handling', Input::old('penanggung_biaya_handling'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('attachment_file', 'ATTACHMENT FILE (MAX 10 MB)') }}
                                        {{ Form::file('attachment_file', array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="table_bantuan">
                                <div class="col-md-12">
                                    <div class="table-responsive" style="margin-bottom:10px;">
                                        <table class="table table-input">
                                            <thead>
                                                <tr>
                                                    <th width="10%">NO</th>
                                                    <th>JENIS BARANG</th>
                                                    <th width="15%">QTY</th>
                                                    <th width="17%">SUMBER</th>
                                                    <th width="20%">KETERANGAN</th>
                                                    <th width="10%">EXPIRED</th>
                                                    <th width="5%"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="tr-input">
                                                    <td colspan="2">
                                                        {{ Form::select('kode_paket', $paket,Input::old('paket'), array('class' => 'form-control','placeholder' => 'Pilih Paket','id' => 'kode_paket')) }} 
                                                    </td>
                                                    <td>
                                                        {{ Form::text('qty', Input::old('qty'), array('class' => 'form-control','id' => 'qty')) }}
                                                    </td>
                                                    <td>
                                                        {{ Form::text('sumber', Input::old('sumber'), array('class' => 'form-control','id' => 'sumber')) }}
                                                    </td>
                                                    <td>
                                                        {{ Form::text('keterangan', Input::old('keterangan'), array('class' => 'form-control','id' => 'keterangan')) }}
                                                    </td>
                                                    <td>
                                                        {{ Form::text('expired', Input::old('expired'), array('class' => 'form-control datepick','id' => 'expired')) }}
                                                    </td>
                                                    <td>
                                                        <a href="#" class="btn btn-info btn-add-detail" >TAMBAH</a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="table_kontrak">
                                <div class="col-md-12">
                                    <div class="table-responsive" style="margin-bottom:10px;">
                                        <table class="table table-input">
                                            <thead>
                                                <tr>
                                                    <th width="10%">NO</th>
                                                    <th>JENIS BARANG</th>
                                                    <th width="5%">QTY KONTRAK</th>
                                                    <th width="5%">QTY TELAH DITERIMA</th>
                                                    <th width="5%">SISA</th>
                                                    <th width="10%">QTY</th>
                                                    <th width="17%">SUMBER</th>
                                                    <th width="20%">KETERANGAN</th>
                                                    <th width="10%">EXPIRED</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    {{ Form::button('<i class="icon icon-save"></i> Simpan', array('class' => 'btn btn-primary','type'=>'submit')) }}
                                    <a href="{{url($prefix.'/penerima')}}" class="btn btn-primary"><i class="icon icon-arrow-left"></i> Kembali</a>
                                </div>
                            </div>
                        {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop
@section('scriptBlock')
    <script type="text/javascript">  
        $(".rf_gudang_id").select2();
        $(".rf_vendor_id").select2();
        $(".rf_kontrak_id").select2();

        var oldVendor = "{{Input::old('rf_vendor_id')}}";
        if(oldVendor != ''){
            findVendor(oldVendor)
        }
        
        function findVendor(value){
            var idKodeVendor= value;//ID KODE PROVINSI SELECTBOX
            var oldKontrakId= "{{Input::old('rf_kontrak_id')}}";;//ID KODE PROVINSI SELECTBOX
            if(idKodeVendor.length != 0){
                $.ajax({
                url : "{{url('penyimpanan/penerima/get-vendor')}}/" + idKodeVendor,
                dataType : "json",
                success : function(result){

                    var html = "<option value=''>Pilih Nomor Kontrak</option>";
                    $.each(result,function(e,item){
                    var selected = '';
                    if(oldKontrakId == item.id){
                        selected = "selected='selected'";
                    }
                    html += "<option value='"+item.id+"' "+selected+">"+item.nomor_kontrak+"</option>"
                    console.log(item);
                    })
                    $("#text_kontrak").html(html);
                    
                }
                })
            }else{
                $("#text_kontrak").html("<option value=''>Pilih Nomor Kontrak</option>");
            }
        }
        $("#text_vendor").on("change",function(e) {
            findVendor($(this).val())
        })
    </script>
    <script type="text/javascript">
        var countDetail = 0;
        $(".btn-add-detail").on("click",function(e){
            e.preventDefault();
            var kode_paket = $("#kode_paket");
            var qty = $("#qty");
            var sumber = $("#sumber");
            var keterangan = $("#keterangan");
            var expired = $("#expired");

            if(kode_paket.val() == "" || qty.val() == '' || sumber.val() == '' || keterangan.val() == '')
            {
                alert('masukan paket,qty,sumber,keterangan');
                return false;
            }

            var paket = $("#kode_paket option[value='"+kode_paket.val()+"']").html();
            var no = countDetail +1;
            var html = "<tr>"
                      +"<td>"
                      +no  
                      +"</td>"
                      +"<td>"
                      +paket
                      +"</td>"
                      +"<td>"
                      +qty.val()
                      +"</td>"
                      +"<td>"
                      +sumber.val()
                      +"</td>"
                      +"<td>"
                      +keterangan.val()
                      +"</td>"
                      +"<td>"
                      +expired.val()
                      +"</td>"
                      +"<td>"
                      +"<a href='#' class='btn btn-danger btn-remove-detail'>HAPUS</a>"
                      +"<input type='hidden' value='"+kode_paket.val()+"' name='detail_penerima["+countDetail+"][kode_paket]' class='detailKodePaket'>"
                      +"<input type='hidden' value='"+qty.val()+"' name='detail_penerima["+countDetail+"][qty]' class='detailQty'>" 
                      +"<input type='hidden' value='"+sumber.val()+"' name='detail_penerima["+countDetail+"][sumber]' class='detailSumber'>"
                      +"<input type='hidden' value='"+keterangan.val()+"' name='detail_penerima["+countDetail+"][keterangan]' class='detailKeterangan'>"
                      +"<input type='hidden' value='"+expired.val()+"' name='detail_penerima["+countDetail+"][expired]' class='detailExpired'>"
                      +"</td>"
                      +"</tr>";
            countDetail = countDetail +1;
            kode_paket.val('').trigger('change');;;
            qty.val('');
            sumber.val('');
            keterangan.val('');
            expired.val('');
            $("#table_bantuan .table-input tbody").append(html);
        })

        $("body").on("click",".btn-remove-detail",function(e){
            e.preventDefault();
            var tr = $(this).closest('tr');
            tr.remove();
            countDetail = countDetail -1;

            $.each($("#table_bantuan .table-input tbody tr:not(.tr-input)"),function(i,item){
                console.log(i);
                var paket = $(this).find('.detailKodePaket');
                var no = $(this).find('td').first();
                var qty = $(this).find('.detailQty');
                var sumber =$(this).find('.detailSumber');
                var keterangan = $(this).find('.detailKeterangan');
                var expired = $(this).find('.detailExpired');
                var numer = i + 1 *1;
                no.html(numer);
                paket.attr('name',"detail_penerima["+i+"][kode_paket]");
                qty.attr('name',"detail_penerima["+i+"][qty]");
                sumber.attr('name',"detail_penerima["+i+"][sumber]");
                keterangan.attr('name',"detail_penerima["+i+"][keterangan]");
                expired.attr('name',"detail_penerima["+i+"][expired]");
                console.log(no);

            })
        })
    </script>



<script type="text/javascript">
    $('.year').datepicker({
        format: 'yyyy',
        viewMode: "years", //this
        minViewMode: "years",//and this
        autoClose:true
    });

    $('.datang').datetimepicker({
        formatViewType: 'time',
        fontAwesome: true,
        autoclose: true,
        startView: 1,
        maxView: 1,
        minView: 0,
        minuteStep: 5,
        format: 'hh:ii'
    });



    $('.bongkar').datetimepicker({
        formatViewType: 'time',
        fontAwesome: true,
        autoclose: true,
        startView: 1,
        maxView: 1,
        minView: 0,
        minuteStep: 5,
        format: 'hh:ii'
    });

    $('#kode_paket').select2();
    
    var oldKontrakDetail = "{{Input::old('rf_kontrak_id')}}";
    if(oldKontrakDetail != ''){
        findKontrak(oldKontrakDetail)
    }
    function findKontrak(value)
    {
        var idkontrakPenerima = value;//ID KODE PROVINSI SELECTBOX
        $("#table_kontrak tbody").html('');
        $("#table_bantuan tbody tr:not(.tr-input)").remove();
        if(idkontrakPenerima.length != 0){
        $.ajax({
                url : "{{url('penyimpanan/penerima/get-kontrak')}}/" + idkontrakPenerima,
                dataType : "json",
                success : function(result){
                var html = "";
                var no = 1;
                $.each(result,function(e,item){
                    html += "<tr>"
                            +"<td>"
                            +no
                            +"</td>"
                            +"<td>"
                            +item.paket_name
                            +"</td>"
                            +"<td>"
                            +item.qty
                            +"</td>"
                            +"<td>"
                            +item.total_diterima
                            +"</td>"
                            +"<td>"
                            +item.sisa
                            +"</td>"
                            +"<td>"
                            +"<input type='hidden' value='"+item.kode_paket+"' name='detail_penerima["+countDetail+"][kode_paket]' class='form-control detailKodePaket'>" 
                            +"<input type='hidden' value='"+item.id+"' name='detail_penerima["+countDetail+"][rf_detail_kontrak_id]' class='form-control detailIdKontrakDetail'>" 
                            +"<input type='text' value='"+item.sisa+"' name='detail_penerima["+countDetail+"][qty]' class='form-control detailQty'>" 
                            +"</td>"
                            +"<td>"
                            +"<input type='text' value='' name='detail_penerima["+countDetail+"][sumber]' class='form-control detailSumber'>" 
                            +"</td>"
                            +"<td>"
                            +"<input type='text' value='' name='detail_penerima["+countDetail+"][keterangan]' class='form-control detailKeterangan'>" 
                            +"</td>"
                            +"<td>"
                            +"<input type='text' value='' name='detail_penerima["+countDetail+"][expired]' class='form-control detailExpired datepickdetail'>" 
                            +"</td>"
                            +"<tr>";

                    no = no +1;
                    countDetail = countDetail + 1;
                })
                $("#table_kontrak tbody").html(html);
                $('.datepickdetail' ).datepicker( "destroy" );
                $(".datepickdetail").datepicker({
                    "format" : "yyyy-mm-dd",
                    autoclose : true,
                    todayBtn : true,
                    todayHighlight : true
                })

                }
            })
        }else{
            $("#table_kontrak tbody").html('');
        }
    }
    $("#text_kontrak").on("change",function(e) {
        findKontrak($(this).val())
    })
</script>

<script type="text/javascript">
    $('#bantuan').hide();
    $('#kontrak').hide();
    $('#vendor').hide();
    $('#table_bantuan').hide();
    $('#table_kontrak').hide();
    var oldType = "{{Input::old('type')}}";
    var first = 1;
    changeType(oldType,first)
    function changeType(value,first)
    {
        countDetail = 0;
        if (value == 'KONTRAK') {
            $('#bantuan').hide();   
            $('#kontrak').show();
            $('#vendor').show();
            $('#table_kontrak').show();
            $('#table_bantuan').hide();
            $("#table_kontrak tbody").html('');
            $("#table_bantuan tbody tr:not(.tr-input)").remove();
            if(first == 0){
                $('#text_bantuan').val('');
                $('#text_kontrak').val('');
            }
        }else if(value == 'BANTUAN'){
            $('#kontrak').hide();
            $('#vendor').hide();
            $('#bantuan').show();
            $('#table_bantuan').show();
            $('#table_kontrak').hide();
            $("#table_kontrak tbody").html('');
            if(first == 0){
                $('#text_bantuan').val('');
                $('#text_kontrak').val('');
                $("#table_bantuan tbody tr:not(.tr-input)").remove();
            }
        }else if (value == "TITIPAN"){
            $('#kontrak').hide();
            $('#vendor').hide();
            $('#bantuan').show();
            $('#table_bantuan').show();
            $('#table_kontrak').hide();
            $("#table_kontrak tbody").html('');
            if(first == 0){
                $('#text_bantuan').val('');
                $('#text_kontrak').val('');
                $("#table_bantuan tbody tr:not(.tr-input)").remove();
            }
            
        }else{
            $('#bantuan').hide();
            $('#kontrak').hide();
            $('#vendor').hide(); 
            $('#table_bantuan').hide();
            $('#table_kontrak').hide();
            $("#table_kontrak tbody").html('');
            $("#table_bantuan tbody tr:not(.tr-input)").remove();
        }
    }
    $('#type').change(function(e){
        changeType($(this).val(),0)
    });

</script>
@stop