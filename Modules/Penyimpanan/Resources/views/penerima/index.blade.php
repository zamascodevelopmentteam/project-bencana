@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAJEMEN PENERIMA BARANG</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data penerima barang pada sistem aplikasi</small>
        </p>
    </div>
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>DATA PENERIMA BARANG</strong>
                </div>
                <div class="card-body">
                    <a href="{{url(strtolower(config('penyimpanan.name')).'/penerima/create')}}" class="btn btn-primary"><i class="icon icon-plus-square"></i> Tambah Data</a>
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    <table class="table table-striped table-bordered table-hover dataTables-data" width="100%">
                        <thead>
                            <tr>
                                <th>Aksi</th>
                                <th>No.</th>
                                <th>Tipe</th>
                                <th>Nama Vendor</th>
                                <th>Nomor Kontrak</th>
                                <th>Nama Penyedia</th>
                                <th>Nomor Penerimaan</th>
                                <th>Nama Gudang</th>
                                <th>Alamat Gudang</th>
                                <th>Tanggal Penerimaan</th>
                                <th>Jam Datang</th>
                                <th>Jam Bongkar</th>
                                <th>Jenis Kendaraan</th>
                                <th>No Kendaraan</th>
                                <th>Nama Pengemudi</th>
                                <th>No Hp Pengemudi</th>
                                <th>No Tally Sheet</th>
                                <th>Asal Surat</th>
                                <th>No Surat Ekspedisi</th>
                                <th>Tanggal Surat</th>
                                <th>Perihal</th>
                                <th>Nama Pejabat</th>
                                <th>Tahun Pengadaan</th>
                                <th>Penanggung Biaya Handling</th> 
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scriptBlock')
    <script>
            var table = $(".dataTables-data");
            var dataTable = table.DataTable({
            responsive:!0,
            "serverSide":true,
            "processing":true,
            "ajax":{
                url : "{{url($prefix.'/penerima')}}"
            },
            dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
            language:{
                paginate:{
                    previous:"&laquo;",
                    next:"&raquo;"
                },search:"_INPUT_",
                searchPlaceholder:"Search..."
            },
            "columns":[
                {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "30"},
                {"data":"rownum","name":"rownum","searchable":false,"orderable":true},
                {"data":"type","name":"type","searchable":true,"orderable":true},
                {"data":"nama_vendor","name":"rf_vendor_id","searchable":true,"orderable":true},
                {"data":"kontrak_name","name":"rf_kontrak_id","searchable":true,"orderable":true},
                {"data":"nama_penyedia","name":"nama_penyedia","searchable":true,"orderable":true},
                {"data":"nomor_penerimaan","name":"nomor_penerimaan","searchable":true,"orderable":true},
                {"data":"gudang_name","name":"rf_gudang_id","searchable":true,"orderable":true},
                {"data":"alamat_name","name":"rf_gudang_id","searchable":true,"orderable":true},
                {"data":"tanggal_penerimaan","name":"tanggal_penerimaan","searchable":true,"orderable":true},
                {"data":"jam_datang","name":"jam_datang","searchable":true,"orderable":true},
                {"data":"jam_bongkar","name":"jam_bongkar","searchable":true,"orderable":true},
                {"data":"jenis_kendaraan","name":"jenis_kendaraan","searchable":true,"orderable":true},
                {"data":"no_kendaraan","name":"no_kendaraan","searchable":true,"orderable":true},
                {"data":"pengemudi","name":"pengemudi","searchable":true,"orderable":true},
                {"data":"no_hp_pengemudi","name":"no_hp_pengemudi","searchable":true,"orderable":true},
                {"data":"no_tally_sheet","name":"no_tally_sheet","searchable":true,"orderable":true},
                {"data":"surat_dari","name":"surat_dari","searchable":true,"orderable":true},
                {"data":"no_surat","name":"no_surat","searchable":true,"orderable":true},
                {"data":"tanggal_surat","name":"tanggal_surat","searchable":true,"orderable":true},
                {"data":"perihal","name":"perihal","searchable":true,"orderable":true},
                {"data":"nama_pejabat","name":"nama_pejabat","searchable":true,"orderable":true},                
                {"data":"tahun_pengadaan","name":"tahun_pengadaan","searchable":true,"orderable":true},
                {"data":"penanggung_biaya_handling","name":"penanggung_biaya_handling","searchable":true,"orderable":true},
            ],
            order:[[1,"asc"]]
        })

    </script>
    
@stop