<?php
Route::group(['middleware' => 'web', 'prefix' => 'penyimpanan', 'namespace' => 'Modules\Penyimpanan\Http\Controllers'], function()
{
    Route::resource('penerima', 'PenerimaController');

    Route::get('penerima/{id}/cetak', 'PenerimaController@show');

    Route::get('/penerima/get-vendor/{id}', 'PenerimaController@getVendor');
    Route::get('/penerima/get-vendor/{id}/{idPenerima}', 'PenerimaController@getVendorEdit');
    Route::get('/penerima/get-gudang/{id}', 'PenerimaController@getGudang');
    Route::get('/penerima/get-kontrak/{id}', 'PenerimaController@getKontrak');
    Route::get('/penerima/get-kontrak/{id}/{idPenerima}', 'PenerimaController@getKontrakEdit');
    Route::resource('pengiriman', 'PengirimanController');
    Route::get('pengiriman/{id}/cetak', 'PengirimanController@show');

    //Route::get('pengiriman/{id}/cetak', 'PengirimanController@getCetak');
    Route::get('pengiriman/get-permintaan/{id}', 'PengirimanController@getPermintaan');
    Route::get('pengiriman/get-permintaan/{id}/{idPermintaan}', 'PengirimanController@getPermintaanEdit');
    // Route::get('pengiriman/cetak')
    
    Route::resource('order', 'OrderController');
	Route::get('order/update/edit-status/{id}/', 'OrderController@editStatus');

    // Route::get('/penerima','PenerimaController@show');
});
