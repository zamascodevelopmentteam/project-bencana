<?php

namespace Modules\Penyimpanan\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use App\Permintaan;
use App\Roles;
use App\Paket;
use App\DetailPermintaan;
use DB;
use Hash;
use Validator;
use Session;
use Redirect;
use View;


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
         if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Permintaan::select(DB::raw('*, @rownum := @rownum +1 as rownum'))->where('rf_permintaan.status', '=', 0);
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return '<a href="order/update/edit-status/'.$data->id.'" class="icon icon-check" style=""></a>'
                        ;})
            ->make(true);
        }
        return view('penyimpanan::order.index');
    }

    public function editStatus($id)
    {
        $order = Permintaan::findOrfail($id);
        $order->status=1;
        $order->save();
        return redirect('penyimpanan/order');
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('penyimpanan::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('penyimpanan::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('penyimpanan::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
