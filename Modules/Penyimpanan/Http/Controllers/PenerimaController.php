<?php

namespace Modules\Penyimpanan\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use App\DetailPenerima;
use App\Gudang;
use App\Penerima;
use App\Kontrak;
use App\Vendor;
use App\Paket;
use App\DetailKontrak;
use DB;
use Hash;
use App\Roles;
use Validator;
use Session;
use Redirect;
use View;
use PDF;

class PenerimaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
         if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Penerima::select(DB::raw('*, @rownum := @rownum +1 as rownum'),
                'tr_penerimaan.type',
                'tr_penerimaan.id',
                'tr_penerimaan.nomor_penerimaan', 
                'tr_penerimaan.tanggal_penerimaan',     
                'tr_penerimaan.jam_datang',
                'tr_penerimaan.jam_bongkar',
                'tr_penerimaan.jenis_kendaraan',
                'tr_penerimaan.no_kendaraan',
                'tr_penerimaan.pengemudi',
                'tr_penerimaan.no_hp_pengemudi',
                
                'tr_penerimaan.no_tally_sheet',
                'tr_penerimaan.surat_dari',
                'tr_penerimaan.no_surat',
                'tr_penerimaan.tanggal_surat',
                'tr_penerimaan.perihal',
                'tr_penerimaan.nama_penyedia',
                'tr_penerimaan.penanggung_biaya_handling',
                'tr_penerimaan.tahun_pengadaan',
                'tr_penerimaan.nama_pejabat',
                'rf_kontrak.nomor_kontrak as kontrak_name',
                'rf_gudang.nama_gudang as gudang_name',
                'rf_gudang.alamat as alamat_name',
                'rf_vendor.nama_vendor as nama_vendor')
                ->leftJoin('rf_kontrak', 'rf_kontrak.id', '=','tr_penerimaan.rf_kontrak_id')
                ->leftJoin('rf_gudang', 'rf_gudang.id', '=','tr_penerimaan.rf_gudang_id')
                ->leftJoin('rf_vendor', 'rf_vendor.id', '=', 'tr_penerimaan.rf_vendor_id');
            return Datatables::of($datas)
            ->addColumn('nama_penyedia', function ($data) { return ($data->nama_penyedia) ? $data->nama_penyedia : "-" ; }) 
            ->addColumn('kontrak_name', function ($data) { return ($data->kontrak_name) ? $data->kontrak_name : "-" ; })   
            ->addColumn('nama_vendor', function ($data) { return ($data->nama_vendor) ? $data->nama_vendor : "-" ; })    

            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                 '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                                                        
                                    <li><a href="penerima/'.$data->id.'" data-id=""  class="btn-delete-on-table">Delete</a></li>
                                    <li><a href="penerima/'.$data->id.'/cetak" target="_blank">cetak</a></li>
                                  </ul><div>'
                        ;
                    })
            ->make(true);
        }
        return view('penyimpanan::penerima.index');

            // <li><a href="penerima/'.$data->id.'/edit">Edit</a></li>
    }

    public function getVendor(\Illuminate\Http\Request $request,$id)
    {
        $kontrak = Kontrak::where('vendor','=',$id)->get();
        foreach ($kontrak as $key => $r) {
            $dkontrak = DetailKontrak::where('rf_kontrak_id','=',$r->id)
            ->select(
                'rf_detail_kontrak.id',
                'rf_detail_kontrak.qty',
                'rf_detail_kontrak.kode_paket',
                'rf_paket.nama_paket as paket_name',
                DB::raw('IFNULL(SUM(tr_penerimaan_detail.qty),0) as total_diterima'),
                DB::raw('(IFNULL((rf_detail_kontrak.qty),0) - IFNULL(SUM(tr_penerimaan_detail.qty),0)) as sisa')
            )
            ->join('rf_paket', 'rf_paket.kode_paket', '=', 'rf_detail_kontrak.kode_paket')
            ->leftJoin('tr_penerimaan_detail', 'rf_detail_kontrak.id', '=', 'tr_penerimaan_detail.rf_detail_kontrak_id')
            ->groupBy('rf_detail_kontrak.id','rf_detail_kontrak.qty','rf_detail_kontrak.kode_paket','rf_paket.nama_paket')
            ->having('sisa','>',0)
            ->orderBy('rf_paket.nama_paket','asc')
            ->get();
            $dkontrak = count($dkontrak);
            if(empty($dkontrak))
            {
                unset($kontrak[$key]);
            }
        }
        return response()
            ->json($kontrak);
    }

    public function getVendorEdit(\Illuminate\Http\Request $request,$id,$idPenerima)
    {
        $kontrak = Kontrak::where('vendor','=',$id)->get();
        foreach ($kontrak as $key => $r) {
           $dkontrak = DetailKontrak::where('rf_kontrak_id','=',$id)
            ->select(
                'rf_detail_kontrak.id',
                'rf_detail_kontrak.qty',
                'rf_detail_kontrak.kode_paket',
                'rf_paket.nama_paket as paket_name',
                DB::raw('IFNULL(SUM(tr_penerimaan_detail.qty),0) as total_diterima'),
                DB::raw('(IFNULL((rf_detail_kontrak.qty),0) - IFNULL(SUM(tr_penerimaan_detail.qty),0)) as sisa')
            )
            ->join('rf_paket', 'rf_paket.kode_paket', '=', 'rf_detail_kontrak.kode_paket')
            ->leftJoin('tr_penerimaan_detail', function($join) use ($idPenerima){
                $join->on('rf_detail_kontrak.id', '=', 'tr_penerimaan_detail.rf_detail_kontrak_id');
                $join->on('tr_penerimaan_detail.tr_penerimaan_id', '!=', DB::raw($idPenerima));
            })
            ->groupBy('rf_detail_kontrak.id','rf_detail_kontrak.qty','rf_detail_kontrak.kode_paket','rf_paket.nama_paket')
            ->having('sisa','>',0)
            ->get();

            $dkontrak = count($dkontrak);
            if(empty($dkontrak))
            {
                unset($kontrak[$key]);
            }
        }
        return response()
            ->json($kontrak);
    }

    
    public function getGudang(\Illuminate\Http\Request $request,$id)
    {
        $gudang = Gudang::where('id','=',$id)->first();
        return response()
            ->json($gudang);
    }

    public function getKontrak(\Illuminate\Http\Request $request,$id)
    {
        $kontrak = DetailKontrak::where('rf_kontrak_id','=',$id)
        ->select(
            'rf_detail_kontrak.id',
            'rf_detail_kontrak.qty',
            'rf_detail_kontrak.kode_paket',
            'rf_paket.nama_paket as paket_name',
            DB::raw('IFNULL(SUM(tr_penerimaan_detail.qty),0) as total_diterima'),
            DB::raw('(IFNULL((rf_detail_kontrak.qty),0) - IFNULL(SUM(tr_penerimaan_detail.qty),0)) as sisa')
        )
        ->join('rf_paket', 'rf_paket.kode_paket', '=', 'rf_detail_kontrak.kode_paket')
        ->leftJoin('tr_penerimaan_detail', 'rf_detail_kontrak.id', '=', 'tr_penerimaan_detail.rf_detail_kontrak_id')
        ->groupBy('rf_detail_kontrak.id','rf_detail_kontrak.qty','rf_detail_kontrak.kode_paket','rf_paket.nama_paket')
        ->having('sisa','>',0)
        ->get();
        return response()
            ->json($kontrak);
    }

    public function getKontrakEdit(\Illuminate\Http\Request $request,$id,$idPenerima)
    {
        $kontrak = DetailKontrak::where('rf_kontrak_id','=',$id)
        ->select(
            'rf_detail_kontrak.id',
            'rf_detail_kontrak.qty',
            'rf_detail_kontrak.kode_paket',
            'rf_paket.nama_paket as paket_name',
            DB::raw('IFNULL(SUM(tr_penerimaan_detail.qty),0) as total_diterima'),
            DB::raw('(IFNULL((rf_detail_kontrak.qty),0) - IFNULL(SUM(tr_penerimaan_detail.qty),0)) as sisa')
        )
        ->join('rf_paket', 'rf_paket.kode_paket', '=', 'rf_detail_kontrak.kode_paket')
        ->leftJoin('tr_penerimaan_detail', function($join) use ($idPenerima){
            $join->on('rf_detail_kontrak.id', '=', 'tr_penerimaan_detail.rf_detail_kontrak_id');
            $join->on('tr_penerimaan_detail.tr_penerimaan_id', '!=', DB::raw($idPenerima));
        })
        ->groupBy('rf_detail_kontrak.id','rf_detail_kontrak.qty','rf_detail_kontrak.kode_paket','rf_paket.nama_paket')
        ->having('sisa','>',0)
        ->get();
        dd($kontrak);
        return response()
            ->json($kontrak);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $roles = Roles::all();
        $roles_list = [];
        foreach($roles as $key => $value){
            //dd($value->type->name);
            $roles_list[$value['id']] = $value->display_name ;
        }
        $paket = Paket::orderBy('nama_paket','asc')->get();
        $paket = $paket->pluck('nama_paket','kode_paket');
        $kontrak = Kontrak::orderBy('nomor_kontrak','asc')->get();
        $kontrak=$kontrak->pluck('nomor_kontrak', 'id');
        $vendor = Vendor::orderBy('nama_vendor','asc')->get();
        $vendor=$vendor->pluck('nama_vendor', 'id');
        $gudang = Gudang::orderBy('nama_gudang','asc')->get(); 
        $gudang = $gudang->pluck('nama_gudang', 'id');

        return view('penyimpanan::penerima.create',compact('kontrak','gudang','paket','vendor'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        
        $data_input = $request->input();
        $rules = array(
            'type' => 'required|max:255',
            'rf_vendor_id' => 'required',
            'rf_kontrak_id' => 'required',
            'penanggung_biaya_handling' => 'required|max:255',
            'tahun_pengadaan' => 'required|max:20',
            'nama_pejabat' => 'required|max:255',
            'tahun_pengadaan' => 'required|max:10',
            'nomor_penerimaan' => 'required|max:255',
            'rf_gudang_id' => 'required|max:255',
            'tanggal_penerimaan' => 'required|max:255',
            'jam_datang' => 'required|max:255',
            'jam_bongkar' => 'required|max:255',
            'jenis_kendaraan'=> 'required|max:255',
            'no_kendaraan'=> 'required|max:255',
            'pengemudi'=> 'required|max:255',
            'no_hp_pengemudi'=> 'required|max:255',
            'no_tally_sheet'=> 'required|max:255',
            'surat_dari'=> 'required|max:255',
            'no_surat'=> 'required|max:255',            
            'tanggal_surat'=> 'required|max:255',
            'perihal'=> 'required|max:255',
            'detail_penerima' => 'present|array',
            'attachment_file' => 'required|max:10000',
        );
        
        $message = [
            'type.required' => 'Harap pilih type',
            'rf_vendor_id.required' => 'Harap masukan nama vendor',
            'rf_kontrak_id.required' => 'Harap masukan nomor kontrak',
            'penanggung_biaya_handling.required' => 'Harap masukan penanggung biaya handling',
            'tahun_pengadaan.required' => 'Harap masukan tahun pengadaan',
            'nomor_penerimaan.required' => 'Harap masukan nomor penerimaan',
            'rf_gudang_id.required' => 'Harap masukan Nama Gudang',
            'tanggal_penerimaan.required' => 'Harap masukan tanggal penerimaan',
            'jam_datang.required' => 'Harap masukan jam datang',
            'jam_bongkar.required' => 'Harap masukan jam bongkar',
            'jenis_kendaraan.required' => 'Harap masukan jenis kendaraan',
            'no_kendaraan.required' => 'Harap masukan no kendaraan',
            'pengemudi.required' => 'Harap masukan pengemudi',
            'no_tally_sheet.required' => 'Harap masukan no tally sheet',
            'surat_dari.required' => 'Harap masukan asal surat',
            'no_surat.required' => 'Harap masukan jenis no surat ',
            'tanggal_surat.required' => 'Harap masukan tanggal surat',
            'perihal.required' => 'Harap masukan perihal',
            'no_hp_pengemudi.required' => 'Harap masukan no hp pengemudi',
            'detail_penerima.array' => 'Harap masukan detail penerima barang',
            'attachment_file.max' => 'File tidak boleh lebih dari 10 mb',
            'nama_pejabat.required' => 'Harap masukan nama pejabat',
            'tahun_pengadaan.required' => 'Harap masukkan tahun pengadaan',
        ];

        if($data_input['type'] == "BANTUAN"){
            unset($rules['rf_vendor_id'],$rules['rf_kontrak_id']);
            $rules['nama_penyedia'] = "required";
            $message['nama_penyedia.required'] = "Harap masukan nama penyedia"; 
        }

        if($data_input['type'] == "TITIPAN"){
            unset($rules['rf_vendor_id'],$rules['rf_kontrak_id']);
            $rules['nama_penyedia'] = "required";
            $message['nama_penyedia.required'] = "Harap masukan nama penyedia"; 
        }


        $validator = Validator::make($request->all(), $rules,$message);
        // process the login
        if ($validator->fails()) {
            return Redirect::to('penyimpanan/penerima/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $penerima = new Penerima;
            $file= Input::file('attachment_file');
            if(!empty($file)){
                $data_input['attachment_file']=$file->getClientOriginalName();
            }
            
            $data_detail = $data_input['detail_penerima'];

            unset($data_input['_method']);
            unset($data_input['_token']);
            unset($data_input['detail_penerima'],$data_input['qty'],$data_input['kode_paket'],$data_input['sumber'], $data_input['keterangan'], $data_input['expired']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $penerima->$key = $val;
            }
            $penerima->save();
            if(!empty($file)){
                $file->move('uploadsPenerimaanSJ'.DIRECTORY_SEPARATOR.$penerima->id, $file->getClientOriginalName());
            }
            //SAVE DETAIL KONTRAK//
            foreach ($data_detail as $skey => $row) {
                $penerimaDetail = new DetailPenerima;
                $penerimaDetail->tr_penerimaan_id = $penerima->id;
                foreach ($row as $skey => $val) {
                    $penerimaDetail->$skey = $val;
                }
                $penerimaDetail->save();
            }

            // redirect
            Session::flash('message', 'Data penerima barang berhasil disimpan');
            return Redirect::to('penyimpanan/penerima');
        }

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $data = Penerima::select(DB::raw('*, @rownum := @rownum +1 as rownum'),
        'tr_penerimaan.type',
        'tr_penerimaan.id',
        'tr_penerimaan.nomor_penerimaan', 
        'tr_penerimaan.tanggal_penerimaan',     
        'tr_penerimaan.jam_datang',
        'tr_penerimaan.jam_bongkar',
        'tr_penerimaan.jenis_kendaraan',
        'tr_penerimaan.no_kendaraan',
        'tr_penerimaan.pengemudi',
        'tr_penerimaan.no_hp_pengemudi',
        'tr_penerimaan.nama_penyedia',
        'tr_penerimaan.surat_dari',
        'tr_penerimaan.no_surat',
        'tr_penerimaan.tanggal_surat',
        'tr_penerimaan.perihal',
        'tr_penerimaan.nama_pejabat',
        'tr_penerimaan.no_tally_sheet',
        'tr_penerimaan.penanggung_biaya_handling',
        'rf_kontrak.nomor_kontrak as kontrak_name',
        'rf_gudang.nama_gudang as gudang_name',
        'rf_gudang.alamat as alamat_name',
        'rf_vendor.nama_vendor as nama_vendor')
        ->leftJoin('rf_kontrak', 'rf_kontrak.id', '=','tr_penerimaan.rf_kontrak_id')
        ->leftJoin('rf_gudang', 'rf_gudang.id', '=','tr_penerimaan.rf_gudang_id')
        ->leftJoin('rf_vendor', 'rf_vendor.id', '=', 'tr_penerimaan.rf_vendor_id')
        ->where('tr_penerimaan.id','=',$id)
        ->first();
        // dd($data);
     
        $penerimaDetail = DetailPenerima::where('tr_penerimaan_id','=',$id)
        ->join('rf_paket','rf_paket.kode_paket','tr_penerimaan_detail.kode_paket')
        ->join('rf_satuan','rf_satuan.id','rf_paket.rf_satuan_id')
        ->orderBy('tr_penerimaan_detail.id','ASC')
        ->select(
            'rf_paket.kode_paket',
            'tr_penerimaan_detail.id',
            'rf_satuan.ur_satuan',
            'rf_paket.nama_paket',
            'tr_penerimaan_detail.qty',
            'tr_penerimaan_detail.sumber',
            'tr_penerimaan_detail.keterangan',
            'tr_penerimaan_detail.expired'
        )
        ->get();
        
        $pdf = PDF::loadView('penyimpanan::penerima.cetak',compact('data','penerimaDetail'));
        $pdf->setPaper('a4');
        return $pdf->stream('Penerimaan'.rand(2,32012).'.pdf');
        // // dd($penerimaDetail);
        // return view('penyimpanan::penerima.cetak',compact());
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    // $datas1 = Penerima::where('rf_paket_id', '=', $id)
    //     ->join('rf_kontrak', 'rf_kontrak.id', '=','tr_penerimaan.rf_kontrak_id')
    //     ->join('rf_gudang', 'rf_gudang.id', '=','tr_penerimaan.rf_gudang_id')
    //     ->orderBy('rf_paket_id','ASC')
    //     ->select(
    //     'tr_penerimaan.type',
    //     'tr_penerimaan.id',
    //     'tr_penerimaan.nomor_penerimaan',   
    //     'tr_penerimaan.jam_datang',
    //     'tr_penerimaan.jam_bongkar',
    //     'tr_penerimaan.jenis_kendaraan',
    //     'tr_penerimaan.no_kendaraan',
    //     'tr_penerimaan.pengemudi',
    //     'tr_penerimaan.no_hp_pengemudi',
    //     'tr_penerimaan.nama_penyedia',
    //     'rf_kontrak.nomor_kontrak as kontrak_name',
    //     'rf_gudang.nama_gudang as gudang_name',
    //     'rf_gudang.alamat as alamat_name');
    public function edit($id)
    { 
        $penerima = Penerima::find($id);
        $penerima['jam_datang'] = substr($penerima['jam_datang'],0,5);
        $penerima['jam_bongkar'] = substr($penerima['jam_bongkar'],0,5);
        $paket = Paket::get();
        $paket = $paket->pluck('nama_paket', 'kode_paket');
        if($penerima->type == "BANTUAN")
        {
            $penerimaDetail = DetailPenerima::where('tr_penerimaan_id','=',$id)
            ->join('rf_paket','rf_paket.kode_paket','tr_penerimaan_detail.kode_paket')
            ->orderBy('tr_penerimaan_detail.id','ASC')
            ->select(
                'rf_paket.kode_paket',
                'tr_penerimaan_detail.id',
                'rf_paket.nama_paket',
                'tr_penerimaan_detail.qty',
                'tr_penerimaan_detail.sumber',
                'tr_penerimaan_detail.keterangan',
                'tr_penerimaan_detail.expired'
            )
            ->get();
        }else if($penerima->type == "TITIPAN"){
            $penerimaDetail = DetailPenerima::where('tr_penerimaan_id','=',$id)
            ->join('rf_paket','rf_paket.kode_paket','tr_penerimaan_detail.kode_paket')
            ->orderBy('tr_penerimaan_detail.id','ASC')
            ->select(
                'rf_paket.kode_paket',
                'tr_penerimaan_detail.id',
                'rf_paket.nama_paket',
                'tr_penerimaan_detail.qty',
                'tr_penerimaan_detail.sumber',
                'tr_penerimaan_detail.keterangan',
                'tr_penerimaan_detail.expired'
            )
            ->get();
        }else {
                $penerimaDetail = DetailKontrak::where('rf_kontrak_id','=',$penerima->rf_kontrak_id)
                ->select(
                    'rf_detail_kontrak.id',
                    'rf_detail_kontrak.qty',
                    'rf_detail_kontrak.kode_paket',
                    'rf_paket.nama_paket as paket_name'
                )
                ->join('rf_paket', 'rf_paket.kode_paket', '=', 'rf_detail_kontrak.kode_paket')
                ->orderBy('rf_paket.nama_paket','asc')
                ->get();
                foreach($penerimaDetail as $r)
                {
                    $penerima_detail = DetailPenerima::where('rf_detail_kontrak_id','=',$r->id)
                                        ->where('tr_penerimaan_detail.tr_penerimaan_id','=',$id)
                                        ->select('tr_penerimaan_detail.qty','tr_penerimaan_detail.sumber','tr_penerimaan_detail.keterangan','tr_penerimaan_detail.expired')
                                        ->first();
                    $r->qty_diterima = $penerima_detail->qty;
                    $r->expired = $penerima_detail->expired;
                    $r->sumber = $penerima_detail->sumber;
                    $r->keterangan = $penerima_detail->keterangan;

                    $total_diterima = DetailPenerima::where('rf_detail_kontrak_id','=',$r->id)
                                        ->where('tr_penerimaan_detail.tr_penerimaan_id','!=',$id)
                                        ->select(DB::raw('IFNULL(SUM(tr_penerimaan_detail.qty),0) as total_diterima'))
                                        ->first();
                    $r->total_diterima = $total_diterima->total_diterima;
                    $r->sisa = $r->qty - $r->total_diterima;
                }
        }
        
        
        $kontrak = Kontrak::get();
        $kontrak=$kontrak->pluck('nomor_kontrak', 'id');
        $gudang = Gudang::get();
        $gudang = $gudang->pluck('nama_gudang','id');
        $gudang1 = Gudang::get();
        $gudang1 = $gudang1->pluck('alamat', 'id');
        $vendor = Vendor::get();
        $vendor=$vendor->pluck('nama_vendor', 'id');
        return view('penyimpanan::penerima.edit', compact('vendor','gudang','penerima','id','kontrak','penerimaDetail','paket','gudang1'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data_input = $request->input();
        $rules = array(
            'type' => 'required|max:255',
            'rf_vendor_id' => 'required',
            'rf_kontrak_id' => 'required',
            'penanggung_biaya_handling' => 'required|max:255',
            'tahun_pengadaan' => 'required|max:20',
            'nama_pejabat' => 'required|max:255',
            'tahun_pengadaan' => 'required|max:10',
            'nomor_penerimaan' => 'required|max:255',
            'rf_gudang_id' => 'required|max:255',
            'tanggal_penerimaan' => 'required|max:255',
            'jam_datang' => 'required|max:255',
            'jam_bongkar' => 'required|max:255',
            'jenis_kendaraan'=> 'required|max:255',
            'no_kendaraan'=> 'required|max:255',
            'pengemudi'=> 'required|max:255',
            'no_hp_pengemudi'=> 'required|max:255',
            'no_tally_sheet'=> 'required|max:255',
            'surat_dari'=> 'required|max:255',
            'no_surat'=> 'required|max:255',            
            'tanggal_surat'=> 'required|max:255',
            'perihal'=> 'required|max:255',
            'detail_penerima' => 'present|array',
            'attachment_file' => 'sometimes|max:10000',
        );
        
        $message = [
            'type.required' => 'Harap pilih type',
            'rf_vendor_id.required' => 'Harap masukan nama vendor',
            'rf_kontrak_id.required' => 'Harap masukan nomor kontrak',
            'penanggung_biaya_handling.required' => 'Harap masukan penanggung biaya handling',
            'tahun_pengadaan.required' => 'Harap masukan tahun pengadaan',
            'nomor_penerimaan.required' => 'Harap masukan nomor penerimaan',
            'rf_gudang_id.required' => 'Harap masukan Nama Gudang',
            'tanggal_penerimaan.required' => 'Harap masukan tanggal penerimaan',
            'jam_datang.required' => 'Harap masukan jam datang',
            'jam_bongkar.required' => 'Harap masukan jam bongkar',
            'jenis_kendaraan.required' => 'Harap masukan jenis kendaraan',
            'no_kendaraan.required' => 'Harap masukan no kendaraan',
            'pengemudi.required' => 'Harap masukan pengemudi',
            'no_tally_sheet.required' => 'Harap masukan no tally sheet',
            'surat_dari.required' => 'Harap masukan asal surat',
            'no_surat.required' => 'Harap masukan jenis no surat ',
            'tanggal_surat.required' => 'Harap masukan tanggal surat',
            'perihal.required' => 'Harap masukan perihal',
            'no_hp_pengemudi.required' => 'Harap masukan no hp pengemudi',
            'detail_penerima.array' => 'Harap masukan detail penerima barang',
            'attachment_file' => 'File tidak boleh lebih dari 10 mb',
            'nama_pejabat.required' => 'Harap masukan nama pejabat',
            'tahun_pengadaan.required' => 'Harap masukkan tahun pengadaan',
        ];
        if($data_input['type'] == "BANTUAN"){
            unset($rules['rf_vendor_id'],$rules['rf_kontrak_id']);
            $rules['nama_penyedia'] = "required";
            $message['nama_penyedia.required'] = "Harap masukan nama penyedia"; 
        }else if($data_input['type'] == "TITIPAN"){
            unset($rules['rf_vendor_id'],$rules['rf_kontrak_id']);
            $rules['nama_penyedia'] = "required";
            $message['nama_penyedia.required'] = "Harap masukan nama penyedia"; 
        }

        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('penyimpanan/penerima/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            // update
            $penerima = Penerima::find($id);
            
            $data_detail = $data_input['detail_penerima'];
            $file= Input::file('attachment_file');
            if(!empty($file)){
                $data_input['attachment_file']=$file->getClientOriginalName();
            }
            
            unset($data_input['_method']);
            unset($data_input['_token']);
            unset($data_input['detail_penerima'],$data_input['qty'],$data_input['kode_paket'],$data_input['sumber'], $data_input['keterangan'],$data_input['expired']);
            // make password hash for login
            if($data_input['type'] == "BANTUAN")
            {
                $data_input['rf_vendor_id'] = 0;
                $data_input['rf_kontrak_id'] = 0;
            }elseif ($data_input['type'] == "TITIPAN") {
                $data_input['rf_vendor_id'] = 0;
                $data_input['rf_kontrak_id'] = 0;
            }
            else
            {
                $data_input['nama_penyedia'] = '';
            }
            foreach($data_input as $key => $val){
                $penerima->$key = $val;
            }
            $penerima->save();
            if(!empty($file)){
                $file->move('uploadsPenerimaanSJ'.DIRECTORY_SEPARATOR.$penerima->id, $file->getClientOriginalName());
            }
            //DELETE OLD DATA//
            $penerimaDetailOLD = DetailPenerima::where('tr_penerimaan_id','=',$id);
            $penerimaDetailOLD->delete();
            ///////////////////

            foreach ($data_detail as $skey => $row) {
                $penerimaDetail = new DetailPenerima;
                $penerimaDetail->tr_penerimaan_id = $penerima->id;
                foreach ($row as $skey => $val) {
                    $penerimaDetail->$skey = $val;
                }
                $penerimaDetail->save();
            }

            // redirect
            Session::flash('message', 'Data penerima barang berhasil diupdate');
            return Redirect::to('penyimpanan/penerima');
        }

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
         $penerima = Penerima::find($id);
        
        if($penerima->delete()){
            //DELETE OLD DATA//
            $penerimaDetailOLD = DetailPenerima::where('tr_penerimaan_id','=',$id);
            $penerimaDetailOLD->delete();
            ///////////////////
            return response()
            ->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()
            ->json(['code' => '50', 'message' => 'Data gagal dihapus']);
        }
    }
}
