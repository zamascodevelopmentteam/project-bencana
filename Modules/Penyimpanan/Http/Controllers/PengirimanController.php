<?php

namespace Modules\Penyimpanan\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use App\Pengiriman;
use App\Roles;
use App\Paket;
use App\Gudang;
use App\DetailPermintaan;
use App\Permintaan;
use App\DetailPengiriman;
use DB;
use Hash;
use Validator;
use Session;
use Redirect;
use View;
use PDF;

class PengirimanController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
         if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Pengiriman::select(DB::raw('*, @rownum := @rownum +1 as rownum'),
                'rf_pengiriman.id',
                'rf_pengiriman.tanggal_kirim',
                'rf_pengiriman.nomor_pengiriman',
                'rf_permintaan.nomor_permintaan as nomor_permintaan')
                ->join('rf_permintaan', 'rf_permintaan.id', '=','rf_pengiriman.rf_permintaan_id');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                    <li><a href="pengiriman/'.$data->id.'/edit">Edit</a></li>
                                    <li><a href="pengiriman/'.$data->id.'" data-id="" class="btn-delete-on-table">Delete</a></li>
                                    <li><a href="pengiriman/'.$data->id.'/cetak" target="_blank" >Cetak</a></li>
                                  </ul><div>'
                        ;})
            ->make(true);
        }
        return view('penyimpanan::pengiriman.index');
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */

    // public function getCetak($id)
    // {   
        // $cetaks=DetailPengiriman::orderBy('id','asc')
        //         ->join('rf_paket','rf_paket.kode_paket','=','rf_pengiriman_detail.kode_paket')
        //         ->join('rf_pengiriman','rf_pengiriman.id','=','rf_pengiriman_detail.rf_pengiriman_id')
        //         ->select('rf_pengiriman_detail.id','rf_pengiriman_detail.qty_kirim','rf_pengiriman.tanggal_kirim as tanggal_kirim','rf_paket.nama_paket as nama_paket','rf_paket.rf_satuan_id as satuan')->get();
               
    //     return view('penyimpanan::pengiriman.cetak', compact('cetaks'));
    // }

    public function create()
    {
        $permintaan = Permintaan::get(); 
        $permintaan = $permintaan->pluck('nomor_permintaan', 'id');
        $gudang = Gudang::orderBy('nama_gudang','asc')->get(); 
        $gudang = $gudang->pluck('nama_gudang', 'id');
        return view('penyimpanan::pengiriman.create', compact('permintaan','gudang'));
    }

    public function getPermintaan(\Illuminate\Http\Request $request,$id)
    {
       $permintaan = DetailPermintaan::where('rf_permintaan_id','=',$id)
        ->select(
            'rf_permintaan_detail.id',
            'rf_permintaan_detail.qty',
            'rf_permintaan_detail.kode_paket',
            'rf_paket.nama_paket as paket_name',
            DB::raw('IFNULL(SUM(rf_pengiriman_detail.qty_kirim),0) as total_diterima'),
            DB::raw('(IFNULL((rf_permintaan_detail.qty),0) - IFNULL(SUM(rf_pengiriman_detail.qty_kirim),0)) as sisa')
        )
        ->join('rf_paket', 'rf_paket.kode_paket', '=', 'rf_permintaan_detail.kode_paket')
        ->leftJoin('rf_pengiriman_detail', 'rf_permintaan_detail.id', '=', 'rf_pengiriman_detail.rf_permintaan_detail_id')
        ->groupBy('rf_permintaan_detail.id','rf_permintaan_detail.qty','rf_permintaan_detail.kode_paket','rf_paket.nama_paket')
        ->having('sisa','>',0)
        ->get();
        return response()
            ->json($permintaan);
    }

    public function getPermintaanEdit(\Illuminate\Http\Request $request,$id,$idPermintaan)
    {
        $permintaan = DetailPermintaan::where('rf_permintaan_id','=',$id)
        ->select(
            'rf_permintaan_detail.id',
            'rf_permintaan_detail.qty',
            'rf_permintaan_detail.kode_paket',
            'rf_paket.nama_paket as paket_name',
            DB::raw('IFNULL(SUM(rf_pengiriman_detail.qty_kirim),0) as total_diterima'),
            DB::raw('(IFNULL((rf_permintaan_detail.qty),0) - IFNULL(SUM(rf_pengiriman_detail.qty_kirim),0)) as sisa')
        )
        ->join('rf_paket', 'rf_paket.kode_paket', '=', 'rf_permintaan_detail.kode_paket')
        ->leftJoin('tr_pengiriman_detail', function($join) use ($idPermintaan){
            $join->on('rf_permintaan_detail.id', '=', 'rf_pengiriman_detail.rf_permintaan_detail_id');
            $join->on('rf_pengiriman_detail.rf_pengiriman_id', '!=', DB::raw($idPermintaan));
        })
        ->groupBy('rf_permintaan_detail.id','rf_permintaan_detail.qty','rf_permintaan_detail.kode_paket','rf_paket.nama_paket')
        ->having('sisa','>',0)
        ->get();
        return response()
            ->json($permintaan);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'rf_permintaan_id' => 'required|max:255',
            'tanggal_kirim' => 'required|max:255',
            'nomor_pengiriman' => 'required|max:255',
            'detail_permintaan' => 'present|array',

        );
        $message = [
            'rf_permintaan_id.required' => 'Harap masukan nomor penerimaan',
            'tanggal_kirim.required' => 'Harap masukan tanggal pengeluaran',
            'nomor_pengiriman.required' => 'Harap masukan nomor pengeluaran',
            'detail_permintaan.array' => 'Harap masukan detail pengeluaran barang',
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('penyimpanan/pengiriman/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $pengiriman = new Pengiriman;
            $data_input = $request->input();
            $data_detail = $data_input['detail_permintaan'];

            unset($data_input['_method']);
            unset($data_input['_token']);
            unset($data_input['detail_permintaan'],$data_input['qty_kirim'],$data_input['kode_paket']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $pengiriman->$key = $val;
            }
            $pengiriman->save();

            //SAVE DETAIL KONTRAK//
            foreach ($data_detail as $skey => $row) {
                $permintaanDetail = new DetailPengiriman;
                $permintaanDetail->rf_pengiriman_id = $pengiriman->id;
                foreach ($row as $skey => $val) {
                    $permintaanDetail->$skey = $val;
                }
                $permintaanDetail->save();
            }

            // redirect
            Session::flash('message', 'Data pengiriman barang berhasil disimpan');
            return Redirect::to('penyimpanan/pengiriman');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $data = Pengiriman::select(DB::raw('*, @rownum := @rownum +1 as rownum'),
        'rf_pengiriman.id',
        'rf_pengiriman.tanggal_kirim',
        'rf_pengiriman.nomor_pengiriman',
        'rf_gudang.nama_gudang as gudang_name',
        'rf_gudang.alamat as alamat_name',
        'rf_permintaan.nomor_permintaan as nomor_permintaan',
        'rf_permintaan.keterangan')
        ->join('rf_permintaan', 'rf_permintaan.id', '=','rf_pengiriman.rf_permintaan_id')
        ->leftJoin('rf_gudang', 'rf_gudang.id', '=','rf_pengiriman.rf_gudang_id')
        ->where('rf_pengiriman.id','=',$id)
        ->first();
        $details = DetailPengiriman::where('rf_pengiriman_id','=',$id)
        ->join('rf_paket','rf_paket.kode_paket','rf_pengiriman_detail.kode_paket')
        ->join('rf_satuan','rf_satuan.id','rf_paket.rf_satuan_id')
        ->orderBy('rf_pengiriman_detail.id','ASC')
        ->select(
            'rf_paket.kode_paket',
            'rf_pengiriman_detail.id',
            'rf_satuan.ur_satuan',
            'rf_paket.nama_paket',
            'rf_pengiriman_detail.qty_kirim as qty'
        )
        ->get();
        $pdf = PDF::loadView('penyimpanan::pengiriman.cetak',compact('data','details'));
        $pdf->setPaper('a4');
        return $pdf->stream('Pengeluaran'.rand(2,32012).'.pdf');
        // return view('penyimpanan::pengiriman.cetak',compact('data','details'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $pengiriman = Pengiriman::find($id);
        $paket = Paket::get();
        $paket = $paket->pluck('nama_paket', 'kode_paket');
        
        $gudang = Gudang::orderBy('nama_gudang','asc')->get(); 
        $gudang = $gudang->pluck('nama_gudang', 'id');
        
          $pengirimanDetail = DetailPermintaan::where('rf_permintaan_id','=',$pengiriman->rf_permintaan_id)
            ->select(
                'rf_permintaan_detail.id',
                'rf_permintaan_detail.qty',
                'rf_permintaan_detail.kode_paket',
                'rf_paket.nama_paket as paket_name'
            )
            ->join('rf_paket', 'rf_paket.kode_paket', '=', 'rf_permintaan_detail.kode_paket')
            ->get();
            foreach($pengirimanDetail as $r)
            {
                $pengiriman_detail = DetailPengiriman::where('rf_permintaan_detail_id','=',$r->id)
                                    ->where('rf_pengiriman_detail.rf_pengiriman_id','=',$id)
                                    ->select('rf_pengiriman_detail.qty_kirim')
                                    ->first();
                $r->qty_diterima = $pengiriman_detail->qty_kirim;

                $total_diterima = DetailPengiriman::where('rf_permintaan_detail_id','=',$r->id)
                                    ->where('rf_pengiriman_detail.rf_pengiriman_id','!=',$id)
                                    ->select(DB::raw('IFNULL(SUM(rf_pengiriman_detail.qty_kirim),0) as total_diterima'))
                                    ->first();
                $r->total_diterima = $total_diterima->total_diterima;
                $r->sisa = $r->qty - $r->total_diterima;
            }
            //dd($pengirimanDetail);
            $permintaan = Permintaan::get(); 
            $permintaan = $permintaan->pluck('nomor_permintaan', 'id');
            return view('penyimpanan::pengiriman.edit', compact('pengiriman','id','pengirimanDetail','permintaan','gudang'));
        }
    
    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'rf_permintaan_id' => 'required|max:255',
            'tanggal_kirim' => 'required|max:255',
            'nomor_pengiriman' => 'required|max:255',
            'detail_permintaan' => 'present|array',

        );
        $message = [
            'rf_permintaan_id.required' => 'Harap masukan nomor penerimaan',
            'tanggal_kirim.required' => 'Harap masukan tanggal pengeluaran',
            'nomor_pengiriman.required' => 'Harap masukan nomor pengeluaran',
            'detail_permintaan.array' => 'Harap masukan detail pengeluaran barang',
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('penyimpanan/pengiriman/'. $id .'/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // update
            $pengiriman = Pengiriman::find($id);
            $data_input = $request->input();
            $data_detail = $data_input['detail_permintaan'];
            
            unset($data_input['_method']);
            unset($data_input['_token']);
            unset($data_input['detail_permintaan'],$data_input['qty_kirim'],$data_input['kode_paket']);
            // make password hash for login
            $pengiriman->save();    


            $pengirimanDetailOLD = DetailPengiriman::where('rf_pengiriman_id','=',$id);
            $pengirimanDetailOLD->delete();

            foreach ($data_detail as $skey => $row) {
                $pengirimanDetail = new DetailPengiriman;
                $pengirimanDetail->rf_pengiriman_id = $pengiriman->id;
                foreach ($row as $skey => $val) {
                    $pengirimanDetail->$skey = $val;
                }
                $pengirimanDetail->save();
            }

            // redirect
            Session::flash('message', 'Data pengiriman barang berhasil diupdate');
            return Redirect::to('penyimpanan/pengiriman');
        }

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $pengiriman = Pengiriman::find($id);
        
        if($pengiriman->delete()){
            //DELETE OLD DATA//
            $pengirimanDetailOLD = DetailPengiriman::where('rf_pengiriman_id','=',$id);
            $pengirimanDetailOLD->delete();
            ///////////////////
            return response()
            ->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()
            ->json(['code' => '50', 'message' => 'Data gagal dihapus']);
        }
    }
}
