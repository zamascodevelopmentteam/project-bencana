@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAJEMEN BARANG</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data barang pada sistem aplikasi</small>
        </p>
    </div>
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>DATA BARANG</strong>
                </div>
                <div class="card-body">
                    
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    <table class="table table-striped table-bordered table-hover dataTables-data table-responsive" width="100%">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Barang</th>
                                <th>Saldo Awal</th>
                                <th>Penerimaan</th>
                                <th>Pengeluaran</th>
                                <th>Stock Opname</th>
                                <th>Saldo Akhir</th>
                            </tr>
                        </thead>
                        <tbody>
                      
                        <?php $i = 0; ?>
                        @foreach($datas as $data)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $data->nama_paket }}</td>
                                <td>{{$data->saldo_awal}}</td>
                                <td>{{ $data->penerimaan_now }}</td>
                                <td>{{ $data->pengiriman_now }}</td>
                                <td>{{ $data->selisih }}</td>
                                <td>{{ $data->stock_fisik_now }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scriptBlock')
@stop

