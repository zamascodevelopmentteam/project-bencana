@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAJEMEN STOCK OPNAME</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data stock opname pada sistem aplikasi</small>
        </p>
    </div>
   <div class="row gutter-xs">
        <div class="col-xs-12">
        <div class="card ">
            <div class="card-header">
              <div class="card-actions">
                <button type="button" class="card-action card-toggler" title="Collapse"></button>
                <button type="button" class="card-action card-reload" title="Reload"></button>
                <button type="button" class="card-action card-remove" title="Remove"></button>
              </div>
              <strong>DATA STOCK OPNAME</strong>
            </div>
            <div class="card-body">
                <a href="{{url(strtolower(config('mutasistok.name')).'/stok/create')}}" class="btn btn-primary"><i class="icon icon-plus-square"></i> Tambah Data</a>
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                <table class="table table-striped table-bordered table-hover dataTables-data" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Aksi</th>
                            <th >No.</th>
                            <th >Nama Paket</th>
                            <th >Saldo Awal</th>
                            <th >Penerimaan</th>
                            <th >Pengeluaran</th>
                            <th >Stock System</th>
                            <th>Baik</th>
                            <th>Kurang Baik</th>
                            <th>Rusak</th>
                            <th>Hilang</th>
                            <th >Stock Fisik</th>
                            <th >Selisih</th>
                            
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>
@stop
@section('scriptBlock')
    <script>
            var table = $(".dataTables-data");
            var dataTable = table.DataTable({
            responsive:!0,
            "serverSide":true,
            "processing":true,
            "ajax":{
                url : "{{url($prefix.'/stok')}}"
            },
            dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
            language:{
                paginate:{
                    previous:"&laquo;",
                    next:"&raquo;"
                },search:"_INPUT_",
                searchPlaceholder:"Search..."
            },
            "columns":[
                {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "30"},
                {"data":"rownum","name":"rownum","searchable":false,"orderable":true},
                {"data":"paket_name","name":"kode_paket","searchable":true,"orderable":true},
                {"data":"saldo_awal","name":"saldo_awal","searchable":true,"orderable":true},
                {"data":"penerimaan","name":"penerimaan","searchable":true,"orderable":true},
                {"data":"pengeluaran","name":"pengeluaran","searchable":true,"orderable":true},
                {"data":"stock_system","name":"stock_system","searchable":true,"orderable":true},
                {"data":"baik","name":"baik","searchable":true,"orderable":true},
                {"data":"kurang_baik","name":"kurang_baik","searchable":true,"orderable":true},
                {"data":"rusak","name":"rusak","searchable":true,"orderable":true},
                {"data":"hilang","name":"hilang","searchable":true,"orderable":true},
                {"data":"stock_fisik","name":"stock_fisik","searchable":true,"orderable":true},
                {"data":"selisih","name":"selisih","searchable":true,"orderable":true},

                // {"data":"created_at","name":"created_at","searchable":true,"orderable":true},
            ],
            order:[[1,"asc"]]
        })

    </script>

@stop

