@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAJEMEN KARTU STOK</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data kartu stok pada sistem aplikasi</small>
        </p>
    </div>
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>DATA KARTU STOK</strong>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    @if(!empty($errors->all()))
                        <div class="alert alert-danger">
                        {{ HTML::ul($errors->all()) }}
                        </div>
                    @endif

                       <form method="get" action="{{url($prefix.'/stok/create')}}">
                       <div class="row">
                            <div class="col-md-4 col-xs-4">
                                <div class="form-group">
                                    {{ Form::label('kode_paket', 'PILIH PAKET') }}
                                    {{ Form::select('kode_paket', $paket,Input::old('kode_paket'), array('class' => 'form-control', 'placeholder' => 'Pilih Paket','id' => 'text_penerima')) }}
                                </div>

                            </div>
                            <div class="col-md-2" style="margin-top: 24px;">
                                <div class="form-group">
                                    <button class="btn btn-primary"><i class="icon icon-search"></i> Cari Data</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    <br>
                  {{--   </div> --}}
                   {{--  <a href="{{url(strtolower(config('mutasistok.name')).'/kartu/create')}}" class="btn btn-primary"><i class="icon icon-plus-square"></i> Tambah Data</a>
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif --}}
                    {{ Form::open(array('url' => $prefix.'/stok')) }}
                    <input type="hidden" name="kode_paket" value="{{ \Request::input('kode_paket') }}">
                   
                    <table class="table" style="width: 31%;">
                        @php
                            $saldo_awal = $peneriman_before->qty_penerimaan_before -$pengiriman_before->qty_kirim_before - $selisih_before->qty_selisih;
                            $stock_system = $saldo_awal + $penerimaan_now->qty_penerimaan_now - $pengiriman_now->qty_kirim_now;
                            $stock_fisik =  + $penerimaan_now->qty_penerimaan_now - $pengiriman_now->qty_kirim_now;

                        @endphp    
                        <tr>
                            <th width="20">SALDO AWAL</th>
                            <td>
                                <div class="col-md-8">
                                    <input id="form-control-1" name="saldo_awal" value="{{ $saldo_awal }}" class="form-control" type="text">
                                </div>
                            </td>
                        </tr>
                       
                        <tr>
                            <th width="20">PENERIMAAN</th>
                            <td>
                                <div class="col-md-8">
                                    <input id="form-control-1" name="penerimaan" value="{{$penerimaan_now->qty_penerimaan_now}}" class="form-control" type="text">
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <th width="20">PENGELUARAN</th>
                            <td>
                                <div class="col-md-8">
                                    <input id="form-control-1" name="pengeluaran" class="form-control" value="{{ $pengiriman_now->qty_kirim_now }}" type="text">
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <th width="20">STOCK SYSTEM</th>
                            <td>
                                <div class="col-md-8">
                                    <input  class="form-control" value="{{ $stock_system}}"  type="text" name="stock_system" id="num1" onkeyup="calculateSum();">
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <th width="20">BAIK</th>
                            <td>
                                <div class="col-md-8">
                                    <input  id="numBaik" name="baik" onkeyup="calculateSum()" class="form-control" type="text">
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <th width="20">KURANG BAIK</th>
                            <td>
                                <div class="col-md-8">
                                    <input  id="numKurangBaik" onkeyup="calculateSum()" name="kurang_baik" class="form-control" type="text">
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <th width="20">RUSAK</th>
                            <td>
                                <div class="col-md-8">
                                    <input  id="numRusak" onkeyup="calculateSum()" name="rusak" class="form-control" type="text">
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <th width="20">HILANG</th>
                            <td>
                                <div class="col-md-8">
                                    <input  id="numHilang" onkeyup="calculateSum()" name="hilang" class="form-control" type="text">
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <th width="20">STOK FISIK</th>
                            <td>
                                <div class="col-md-8">
                                    <input class="form-control" name="stock_fisik" id="num2" onkeyup="calculateSum()"  readonly=""  type="text">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th width="20">SELISIH</th>
                            <td>
                                <div class="col-md-8">
                                    <input  class="form-control" name="selisih" onkeyup="calculateSum()" readonly="" type="text" id="sum">
                                </div>
                            </td>
                        </tr> 
                        <tr>
                            <th>      
                            </th>
                            <td>
                                <div class="col-md-8">
                                    {{ Form::button('<i class="icon icon-save"></i> Simpan', array('class' => 'btn btn-primary','type'=>'submit')) }}
                                </div>
                            </td>
                        </tr>    
                    </table>
                    {{ Form::close() }}{{-- 
                    @include('mutasistok::stok.index') --}}
                </div>
            </div>
        </div>
    </div>
@stop
@section('scriptBlock')
    <script type="text/javascript">
       function calculateSum()
        {
          var numBaik = parseInt(document.getElementById("numBaik").value);
          var numKurangBaik = parseInt(document.getElementById("numKurangBaik").value);
          var numRusak = parseInt(document.getElementById("numRusak").value);
          var numHilang = parseInt(document.getElementById("numHilang").value);
          var num1 = parseInt(document.getElementById("num1").value);
          var num2 = parseInt(document.getElementById("num2").value);

          document.getElementById("num2").value = numBaik+numKurangBaik+numRusak-numHilang;   
          document.getElementById("sum").value = num1-num2;   

        }
    </script>

    {{-- 
    var num1 = parseInt($("#num1").val());
          var num2 = parseInt($("#num2").val());
          document.getElementById("sum").value = num1 - num2;     
          console.log(num1+'\n'+num2); --}}
@stop
