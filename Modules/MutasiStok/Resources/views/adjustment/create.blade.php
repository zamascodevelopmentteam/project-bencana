@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAJEMEN ADJUSTMENT STOCK</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data adjustment stock pada sistem aplikasi</small>
        </p>
    </div>
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>DATA ADJUSTMENT STOCK</strong>
                </div>
                <div class="card-body">
                       <form method="get" action="{{url($prefix.'/adjustment/create')}}">
                       <div class="row">
                            <div class="col-md-4 col-xs-4">
                                <div class="form-group">
                                    {{ Form::label('kode_paket', 'Pilih Paket') }}
                                    {{ Form::select('kode_paket', $paket,Input::old('kode_paket'), array('class' => 'form-control', 'placeholder' => 'Pilih Paket','id' => 'text_penerima')) }}
                                </div>

                            </div>
                            <div class="col-md-2" style="margin-top: 24px;">
                                <div class="form-group">
                                    <button class="btn btn-primary"><i class="icon icon-search"></i> Cari Data</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    <br>
                  {{--   </div> --}}
                   {{--  <a href="{{url(strtolower(config('mutasistok.name')).'/kartu/create')}}" class="btn btn-primary"><i class="icon icon-plus-square"></i> Tambah Data</a>
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif --}}
                    {{ Form::open(array('url' => $prefix.'/adjustment')) }}
                    <input type="hidden" name="kode_paket" value="{{ \Request::input('kode_paket') }}">
                    <table class="table" style="width: 31%;">
                            
                       {{--  <tr>
                            <th width="20">Saldo Awal</th>
                            <td>
                                <div class="col-md-8">
                                    <input id="form-control-1" name="saldo_awal" value="{{ $peneriman_before->qty_penerimaan_before-$pengiriman_before->qty_kirim_before }}" class="form-control" type="text">
                                </div>
                            </td>
                        </tr> --}}
                       
                        <tr>
                            <th width="20">Penerimaan</th>
                            <td>
                                <div class="col-md-8">
                                    <input id="form-control-1" name="penerimaan" value="{{$penerimaan_now->qty_penerimaan_now}}" class="form-control" type="text">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th width="20">Pengeluaran</th>
                            <td>
                                <div class="col-md-8">
                                    <input id="form-control-1" name="pengeluaran" class="form-control" value="{{ $pengiriman_now->qty_kirim_now }}" type="text">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th width="20">Saldo</th>
                            <td>
                                <div class="col-md-8">
                                    <input  class="form-control" value="{{ $penerimaan_now->qty_penerimaan_now-$pengiriman_now->qty_kirim_now }}" type="text" name="saldo" id="num1" onkeyup="calculateSum();">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th width="20">Stok Fisik</th>
                            <td>
                                <div class="col-md-8">
                                    <input class="form-control" name="stock_fisik" value="{{ $stock->stock_fisik }}" onkeyup="calculateSum();" id="num2"  type="text">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th width="20">selisih</th>
                            <td>
                                <div class="col-md-8">
                                    <input  class="form-control" value="{{ $penerimaan_now->qty_penerimaan_now-$pengiriman_now->qty_kirim_now-$stock->stock_fisik}}" name="selisih" type="text" id="sum">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th width="20">Adjustment Stock</th>
                            <td>
                                <div class="col-md-8">
                                    <input  class="form-control" name="adjustment" type="text" id="sum">
                                </div>
                            </td>
                        </tr> 
                        <tr>
                            <th>      
                            </th>
                            <td>
                                <div class="col-md-8">
                                    {{ Form::button('<i class="icon icon-save"></i> Simpan', array('class' => 'btn btn-primary','type'=>'submit')) }}
                                </div>
                            </td>
                        </tr>    
                    </table>
                    {{ Form::close() }}{{-- 
                    @include('mutasistok::stok.index') --}}
                </div>
            </div>
        </div>
    </div>
@stop
@section('scriptBlock')
    <script type="text/javascript">
       function calculateSum()
        {
          var num1 = parseInt(document.getElementById("num1").value);
          var num2 = parseInt(document.getElementById("num2").value);
          document.getElementById("sum").value = num1 - num2;     
        }
    </script>{{-- 
    var num1 = parseInt($("#num1").val());
          var num2 = parseInt($("#num2").val());
          document.getElementById("sum").value = num1 - num2;     
          console.log(num1+'\n'+num2); --}}
@stop
