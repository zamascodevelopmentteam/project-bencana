<style>
    .table-header{
        border-spacing: 0;
        border-collapse: collapse;
        width:100%;
    }
    .header{
        font-size:18px;
        font-weight:bold;
    }

    .table-list{
        margin-top:20px;
        margin-bottom:20px;
        border-spacing: 0;
        border-collapse: collapse;
        width:100%;
        border:1px solid #000;
    }
    .table-list thead > tr th,.table-list tbody > tr td{
        border-right:1px solid #000;
        border-bottom:1px solid #000;
    }
    .table-list thead > tr th:last-child,.table-list tbody > tr td:last-child{
        border-right:none;
    }
    .table-list tbody > tr:last-child td{
        border-bottom:none;
    }
    .text-center{
        text-align:center;
    }
</style>
<table class="table-header">
    <tr>
        <td colspan="2">
            <p class="header">LAPORAN REKAPITULASI BARANG LOGISTIK<br>
            PERIODE {{$date_start}} - {{$date_end}}
            </p>
        </td>
    <tr>
    <tr>
        <td colspan="2">
        <table class="table-list">
            <thead>
                <tr>
                    <th rowspan="2" width="5%">No</th>
                    <th rowspan="2" width="10%">Kode</th>
                    <th rowspan="2">Jenis</th>
                    <th rowspan="2" width="10%">Satuan</th>
                    <th rowspan="2" width="10%">Stok Per Tgl<br> {{$date_start}}</th>
                    <th colspan="2" width="20%">Mutasi</th>
                    <th rowspan="2" width="10%">Stok Per Tgl<br> {{$date_end}}</th>
                    <th rowspan="2" width="10%">Keterangan</th>
                </tr>
                <tr>
                    <th>Masuk</th>
                    <th>Keluar</th>
                </tr>
                <tr>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>5</th>
                    <th>6</th>
                    <th>7</th>
                    <th>8</th>
                    <th>9</th>
                </tr>
            </thead>
            <tbody>
            @foreach($paket as $key=> $detail)
            <?php 
                $saldoawal = ($detail->saldo_awal_in - $detail->saldo_awal_out - $detail->saldo_awal_adjustment) *1;
                $saldoakhir = ($saldoawal + $detail->saldo_in - $detail->saldo_out ) *1;
            ?>
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$detail->kode_paket}}</td>
                    <td>{{$detail->nama_paket}}</td>
                    <td>{{$detail->ur_satuan}}</td>
                    <td>{{$saldoawal}}</td>
                    <td>{{$detail->saldo_in*1}}</td>
                    <td>{{$detail->saldo_out*1}}</td>
                    <td>{{$saldoakhir}}</td>
                    <td>&nbsp;</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        </td>
    </tr>
    <tr>
        <td></td>
        <td width="50%" class="text-center">Jakarta,{{tanggal_indo(date('Y-m-d'))}}</td>
    </tr>
    <tr>
        <td  class="text-center">Mengetahui</td>
        <td width="50%"></td>
    </tr>
    <tr>
        <td class="text-center">Kasubdit Penyimpanan dan Distribusi Logistik</td>
        <td width="50%" class="text-center">Kepala Seksi Penyimpanan</td>
    </tr>
    <tr>
        <td class="text-center">&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<br></td>
    </tr>
    <tr>
        <td class="text-center">............................................................</td>
        <td class="text-center" width="50%">............................................................</td>
    </tr>
    <tr>
        <td class="text-center">NIP:............................................................</td>
        <td class="text-center" width="50%">NIP:............................................................</td>
    </tr>
</table>