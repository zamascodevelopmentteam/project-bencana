@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAJEMEN KARTU STOK</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data kartu stok pada sistem aplikasi</small>
        </p>
    </div>
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>DATA KARTU STOK</strong>
                </div>
                <div class="card-body">
                   <form method="get" action="{{url($prefix.'/lap-rekap-barang')}}">
                   <div class="row">
                        <div class="col-md-3 col-xs-4">
                            <div class="form-group">
                                {{ Form::label('periode', 'Periode') }}
                                <div class="input-group ">
                                    {{ Form::text('date_start',Input::old('date_start'), array('class' => 'form-control datepick')) }}
                                    <span class="input-group-addon">
                                        -
                                    </span>
                                    {{ Form::text('date_end',Input::old('date_end'), array('class' => 'form-control datepick')) }}
                                </div>
                            </div>

                        </div>
                        <div class="col-md-2" style="margin-top: 24px;">
                            <div class="form-group">
                                <button class="btn btn-primary"><i class="icon icon-search"></i> GO</button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

