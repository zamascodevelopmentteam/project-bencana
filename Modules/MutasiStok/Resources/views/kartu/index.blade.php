@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAJEMEN KARTU STOK</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data kartu stok pada sistem aplikasi</small>
        </p>
    </div>
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>DATA KARTU STOK</strong>
                </div>
                <div class="card-body">
                   <form method="get" action="{{url($prefix.'/kartu')}}">
                   <div class="row">
                        <div class="col-md-4 col-xs-4">
                            <div class="form-group">
                                {{ Form::label('kode_paket', 'PILIH PAKET') }}
                                {{ Form::select('kode_paket', $paket,Input::old('kode_paket'), array('class' => 'form-control', 'placeholder' => 'Pilih Paket','id' => 'text_penerima')) }}
                            </div>

                        </div>
                        <div class="col-md-2" style="margin-top: 24px;">
                            <div class="form-group">
                                <button class="btn btn-primary"><i class="icon icon-search"></i> Cari Data</button>
                            </div>
                        </div>
                    </div>
                    </form>
                    <br>
                  {{--   </div> --}}
                   {{--  <a href="{{url(strtolower(config('mutasistok.name')).'/kartu/create')}}" class="btn btn-primary"><i class="icon icon-plus-square"></i> Tambah Data</a>
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif --}}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <p>STOK MASUK :</p>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover dataTables-data" id="table-penerima" width="100%">
                        <thead>
                            <tr>
                                <th width="10">No.</th>
                                <th>Tanggal</th>
                                <th>Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=0; $jml=0; ?>
                            @foreach($penerimaan_list as $peneriman)
                            <?php $jml = $jml+$peneriman->qty;?>
                                <tr>
                                    <td>{{ ++$no }}</td>
                                    <td>{{ $peneriman->tanggal_penerimaan }}</td>
                                    <td>{{ $peneriman->qty }}</td>
                                
                                </tr>
                            @endforeach()
                                <tr rowspan=2>
                                    <td></td>
                                    <td></td>
                                    <td colspan="2">TOTAL MASUK :&nbsp;&nbsp;{{ $jml}}</td>
                                </tr>
                        </tbody>
                        
                    </table>
                    <br>

                    <p>STOK KELUAR :</p>
                    <table class="table table-striped table-bordered table-hover dataTables-data" width="100%">
                        <thead>
                            <tr>
                                <th width="10">No.</th>
                                <th>Tanggal</th>
                                <th>Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=0; $jml1=0;?>
                            @foreach($pengiriman_list as $pengiriman)
                            <?php $jml1 = $jml1+$pengiriman->qty_kirim;?>
                                <tr>
                                    <td>{{ ++$no }}</td>
                                    <td>{{ $pengiriman->tanggal_kirim }}</td>
                                    <td>{{ $pengiriman->qty_kirim }}</td>
                                </tr>
                            @endforeach()
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td colspan="2">Total Keluar :&nbsp;&nbsp;{{ $jml1 }}</td>
                                </tr>                    
                               
                        </tbody>
                    </table>

                    <br>
                    <table class="table table-striped table-bordered table-hover dataTables-data" style="width: 200px;">
                        <thead>
                            <tr>
                                <th>SISA STOK :</td>
                                <td>{{ $jml-$jml1 }}</td>

                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

