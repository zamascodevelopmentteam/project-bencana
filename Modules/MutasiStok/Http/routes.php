<?php

Route::group(['middleware' => 'web', 'prefix' => 'mutasistok', 'namespace' => 'Modules\MutasiStok\Http\Controllers'], function()
{
    Route::resource('kartu', 'KartuController');
    Route::resource('stok', 'StokController'); 
    Route::get('stok/update/edit-status/{id}/', 'StokController@editStatus');
    Route::get('adjustment/update/hapus-status/{id}/', 'AdjustmentController@hapusStatus');
    Route::resource('adjustment', 'AdjustmentController');
    Route::resource('monitoring', 'MonitoringController');
    Route::resource('lap-rekap-barang', 'LapRekapBarangController');

    // Route::get('stok/create', 'StokController@create');
    // Route::post('stok', 'StokController@store');
    // Route::get('stok/{id}/edit', 'StokController@edit');
    // Route::put('stok/{id}', 'StokController@update')->name('stok.update');
    // Route::get('stok/{id}','StokController@destroy');
    // Route::post('stok/{id}','StokController@store');
});
