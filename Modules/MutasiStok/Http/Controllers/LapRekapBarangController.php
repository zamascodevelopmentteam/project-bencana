<?php

namespace Modules\MutasiStok\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use App\Pengiriman;
use App\DetailPengiriman;
use  App\DetailPenerima;
use App\Paket;
use App\Roles;
use DB;
use Hash;
use Validator;
use Session;
use Redirect;
use View;

class LapRekapbarangController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {   
        if(!empty($request->query)){
            $date_start = $request->query('date_start');
            $date_end = $request->query('date_end');
            $paket = Paket::select(DB::raw(
                "
                rf_paket.kode_paket, 
                rf_paket.nama_paket,
                rf_satuan.ur_satuan,
                rf_kelompok_barang.ur_kelompok_barang,
                (
                    (
                        SELECT SUM(qty) as qty FROM tr_penerimaan_detail 
                        INNER JOIN tr_penerimaan on tr_penerimaan.id = tr_penerimaan_detail.tr_penerimaan_id 
                        WHERE DATE(tr_penerimaan.tanggal_penerimaan) < '".$date_start."'  AND tr_penerimaan_detail.kode_paket = rf_paket.kode_paket 
                    )
                ) as saldo_awal_in,
                (
                    (
                        SELECT SUM(qty_kirim) as qty FROM rf_pengiriman_detail 
                        INNER JOIN rf_pengiriman on rf_pengiriman.id = rf_pengiriman_detail.rf_pengiriman_id 
                        WHERE DATE(rf_pengiriman.tanggal_kirim) < '".$date_start."'  AND rf_pengiriman_detail.kode_paket = rf_paket.kode_paket 
                    )
                ) as saldo_awal_out,
                (
                    (
                        SELECT SUM(selisih) as qty FROM rf_stock_opname 
                        WHERE DATE(rf_stock_opname.created_at) < '".$date_start."'  AND rf_stock_opname.kode_paket = rf_paket.kode_paket AND rf_stock_opname.status_adjusment = 1
                    )
                ) as saldo_awal_adjustment,
                (
                    (
                        SELECT SUM(qty) as qty FROM tr_penerimaan_detail 
                        INNER JOIN tr_penerimaan on tr_penerimaan.id = tr_penerimaan_detail.tr_penerimaan_id 
                        WHERE DATE(tr_penerimaan.tanggal_penerimaan) BETWEEN '".$date_start."' AND '".$date_end."'  AND tr_penerimaan_detail.kode_paket = rf_paket.kode_paket 
                    )
                ) as saldo_in,
                (
                    (
                        SELECT SUM(qty_kirim) as qty FROM rf_pengiriman_detail 
                        INNER JOIN rf_pengiriman on rf_pengiriman.id = rf_pengiriman_detail.rf_pengiriman_id 
                        WHERE DATE(rf_pengiriman.tanggal_kirim) BETWEEN '".$date_start."' AND '".$date_end."'  AND rf_pengiriman_detail.kode_paket = rf_paket.kode_paket 
                    )
                ) as saldo_out"
                )
            )
            ->join('rf_satuan','rf_satuan.id','=','rf_paket.rf_satuan_id')
            ->join('rf_kelompok_barang','rf_kelompok_barang.id','=','rf_paket.rf_kelompok_barang_id')
            ->get();
            return view('mutasistok::laporan.rekap.show',compact('paket','date_start','date_end'));
        }
        return view('mutasistok::laporan.rekap.index');
    }

    

}
