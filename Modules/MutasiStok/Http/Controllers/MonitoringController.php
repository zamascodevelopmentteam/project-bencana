<?php

namespace Modules\MutasiStok\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use App\Paket;
use App\Stock; 
use DB;
use App\Satuan;
use Hash;
use App\DetailPenerima;
use App\DetailPengiriman;
use App\Roles;
use Validator;
use Session;
use Redirect;
use View;

class MonitoringController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */


    public function index(Request $request)
    {
        $datas = Paket::orderBy('id','asc')->get();
        foreach($datas as $data)
        {
            $kode_paket = $data->kode_paket;
            $monthNow = date('m');
            //$monthNow = date('m')+1;
            $yearNow = date('Y');
            $peneriman_before = DetailPenerima::orderBy('tr_penerimaan_detail.id','desc')//penerimaan
                               ->select(DB::raw('IFNULL(SUM(qty),0) as qty_penerimaan_before'))
                               ->join('tr_penerimaan', 'tr_penerimaan.id', '=', 'tr_penerimaan_detail.tr_penerimaan_id')
                               ->where('kode_paket','=', $data->kode_paket)
                               ->where(DB::raw('MONTH(tanggal_penerimaan)'),'<',$monthNow)
                               ->where(DB::raw('YEAR(tanggal_penerimaan)'),'=',$yearNow)       
                               ->first();
            $data->peneriman_before = $peneriman_before->qty_penerimaan_before;

            $pengiriman_before = DetailPengiriman::orderBy('rf_pengiriman_detail.id','desc')
                               ->select(DB::raw('IFNULL(SUM(qty_kirim),0) as qty_kirim_before'))
                               ->join('rf_pengiriman', 'rf_pengiriman.id', '=', 'rf_pengiriman_detail.rf_pengiriman_id')
                               ->where('kode_paket','=', $data->kode_paket)
                               ->where(DB::raw('MONTH(tanggal_kirim)'), '<', $monthNow)
                               ->where(DB::raw('YEAR(tanggal_kirim)'), '=', $yearNow)
                               ->first();
            $data->pengiriman_before = $pengiriman_before->qty_kirim_before;

            $selisih_before = Stock::orderBy('rf_stock_opname.id','desc')
                               ->select(DB::raw('IFNULL(SUM(selisih),0) as qty_selisih'))
                               ->where(DB::raw('MONTH(created_at)'), '<', $monthNow)
                               ->where('status_adjusment', '=', 1)
                               ->where('kode_paket','=', $data->kode_paket)
                               ->first(); 

            $data->selisih_before = $selisih_before->qty_selisih;


            $penerimaan_now = DetailPenerima::orderBy('tr_penerimaan_detail.id','desc')
                            ->select(DB::raw('IFNULL(SUM(qty),0) as qty_penerimaan_now'))  
                            ->join('tr_penerimaan','tr_penerimaan.id', '=', 'tr_penerimaan_detail.tr_penerimaan_id')
                            ->where('kode_paket', '=', $data->kode_paket)
                            ->where(DB::raw('MONTH(tanggal_penerimaan)'), '=', $monthNow)
                            ->where(DB::raw('YEAR(tanggal_penerimaan)'), '=', $yearNow)
                            ->first();

            $data->penerimaan_now= $penerimaan_now->qty_penerimaan_now;

            $pengiriman_now = DetailPengiriman::orderBy('rf_pengiriman_detail.id','desc')
                            ->select(DB::raw('IFNULL(SUM(qty_kirim),0) as qty_kirim_now'))  
                            ->join('rf_pengiriman','rf_pengiriman.id', '=', 'rf_pengiriman_detail.rf_pengiriman_id')
                            ->where('kode_paket', '=', $data->kode_paket)
                            ->where(DB::raw('MONTH(tanggal_kirim)'), '=', $monthNow)
                            ->where(DB::raw('YEAR(tanggal_kirim)'), '=', $yearNow)
                            ->where('kode_paket','=', $data->kode_paket)
                            ->first();

            $stock_fisik_now = Stock::orderBy('rf_stock_opname.id','desc')
                            ->select(DB::raw('IFNULL(SUM(stock_fisik),0) as stock_fisik'))  
                            // ->join('rf_pengiriman','rf_pengiriman.id', '=', 'rf_pengiriman_detail.rf_pengiriman_id')
                            ->where('kode_paket', '=', $data->kode_paket)
                            // ->where(DB::raw('MONTH(tanggal_kirim)'), '=', $monthNow)
                            // ->where(DB::raw('YEAR(tanggal_kirim)'), '=', $yearNow)
                            ->where('kode_paket','=', $data->kode_paket)
                            ->first();
                             
            $data->stock_fisik_now= $stock_fisik_now->stock_fisik;


            $data->pengiriman_now= $pengiriman_now->qty_kirim_now;
            $data->saldo_awal = $data->peneriman_before - $data->pengiriman_before - $data->selisih_before*1;
            $data->stock_system = $data->saldo_awal+$data->penerimaan_now-$data->pengiriman_now*1;
            $data->selisih = $data->stock_system-$data->stock_fisik_now*1;
        }
        return view('mutasistok::monitoring.index', compact('datas'));
    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('mutasistok::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('mutasistok::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('mutasistok::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
