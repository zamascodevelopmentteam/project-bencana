<?php

namespace Modules\MutasiStok\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use App\Pengiriman;
use App\DetailPengiriman;
use App\DetailPenerima;
use App\Paket;
use App\Adjustment;
use App\Roles;
use App\Stock;
use DB;
use Hash;
use Validator;
use Session;
use Redirect;
use View;



class AdjustmentController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Stock::select(DB::raw('*, @rownum := @rownum +1 as rownum'),
                'rf_stock_opname.id',
                'rf_stock_opname.saldo_awal',
                'rf_stock_opname.penerimaan',
                'rf_stock_opname.kondisi_barang',
                'rf_stock_opname.pengeluaran',
                'rf_stock_opname.stock_system',
                'rf_stock_opname.stock_fisik',
                'rf_stock_opname.selisih',
                'rf_paket.nama_paket as paket_name')
                ->leftJoin('rf_paket', 'rf_paket.kode_paket', '=','rf_stock_opname.kode_paket')->where('rf_stock_opname.status_adjusment','=', 1);
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                    <li><a href="adjustment/update/hapus-status/'.$data->id.'">Hapus</a></li>
                                  </ul><div>'
                        ;})
            ->make(true);
        }
        return view('mutasistok::adjustment.index');
    }

    public function hapusStatus($id)
    {
        $status = Stock::findOrfail($id);
        $status->status_adjusment=0;
        $status->save();
        return Redirect::to('mutasistok/adjustment');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('mutasistok::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('mutasistok::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $adjustment = Stock::find($id);
        if($adjustment->delete()){
            return response()
            ->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()
            ->json(['code' => '50', 'message' => 'Data gagal dihapus']);
        }
    }
}
