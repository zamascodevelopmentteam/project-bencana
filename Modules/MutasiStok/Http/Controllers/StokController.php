<?php

namespace Modules\MutasiStok\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use App\Pengiriman;
use App\DetailPengiriman;
use App\DetailPenerima;
use App\Paket;
use App\Stock;
use App\Roles;
use DB;
use Hash;
use Validator;
use Session;
use Redirect;
use View;



class StokController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
         if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Stock::select(DB::raw('*, @rownum := @rownum +1 as rownum'),
                'rf_stock_opname.id',
                'rf_stock_opname.saldo_awal',
                'rf_stock_opname.penerimaan',
                'rf_stock_opname.pengeluaran',
                'rf_stock_opname.stock_system',
                'rf_stock_opname.baik',
                'rf_stock_opname.kurang_baik',
                'rf_stock_opname.rusak',
                'rf_stock_opname.hilang',
                'rf_stock_opname.stock_fisik',
                'rf_stock_opname.selisih',
                'rf_paket.nama_paket as paket_name')
                ->leftJoin('rf_paket', 'rf_paket.kode_paket', '=','rf_stock_opname.kode_paket');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                    <li><a href="stok/'.$data->id.'" data-id="" class="btn-delete-on-table">Delete</a></li>
                                    <li><a href="stok/update/edit-status/'.$data->id.'">Update</a></li>
                                  </ul><div>'
                        ;})
            ->make(true);
        }
        return view('mutasistok::stok.index');
    }

    public function editStatus($id)
    {
        $status = Stock::findOrfail($id);
        $status->status_adjusment=1;
        $status->save();
        return Redirect::to('mutasistok/stok');
    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
     public function create(Request $request)
    {   
        $kode_paket = $request->query('kode_paket');
        $monthNow = date('m');
        //$monthNow = date('m')+1;
        $yearNow = date('Y');
        $peneriman_before = DetailPenerima::orderBy('tr_penerimaan_detail.id','desc')//penerimaan
                           ->select(DB::raw('IFNULL(SUM(qty),0) as qty_penerimaan_before'))
                           ->join('tr_penerimaan', 'tr_penerimaan.id', '=', 'tr_penerimaan_detail.tr_penerimaan_id')
                           ->where('kode_paket','=',$kode_paket) 
                           ->where(DB::raw('MONTH(tanggal_penerimaan)'),'<',$monthNow)
                           ->where(DB::raw('YEAR(tanggal_penerimaan)'),'=',$yearNow)       
                           ->first();

        $pengiriman_before = DetailPengiriman::orderBy('rf_pengiriman_detail.id','desc')
                           ->select(DB::raw('IFNULL(SUM(qty_kirim),0) as qty_kirim_before'))
                           ->join('rf_pengiriman', 'rf_pengiriman.id', '=', 'rf_pengiriman_detail.rf_pengiriman_id')
                           ->where('kode_paket','=',$kode_paket)
                           ->where(DB::raw('MONTH(tanggal_kirim)'), '<', $monthNow)
                           ->where(DB::raw('YEAR(tanggal_kirim)'), '=', $yearNow)
                           ->first();

        $selisih_before = Stock::orderBy('rf_stock_opname.id','desc')
                           ->select(DB::raw('IFNULL(SUM(selisih),0) as qty_selisih'))
                           ->where('kode_paket','=',$kode_paket)
                           ->where(DB::raw('MONTH(created_at)'), '<', $monthNow)
                           ->where('status_adjusment', '=', 1)
                           ->first(); 

        $penerimaan_now = DetailPenerima::orderBy('tr_penerimaan_detail.id','desc')
                        ->select(DB::raw('IFNULL(SUM(qty),0) as qty_penerimaan_now'))  
                        ->join('tr_penerimaan','tr_penerimaan.id', '=', 'tr_penerimaan_detail.tr_penerimaan_id')
                        ->where('kode_paket', '=', $kode_paket)
                        ->where(DB::raw('MONTH(tanggal_penerimaan)'), '=', $monthNow)
                        ->where(DB::raw('YEAR(tanggal_penerimaan)'), '=', $yearNow)
                        ->first();

        $pengiriman_now = DetailPengiriman::orderBy('rf_pengiriman_detail.id','desc')
                        ->select(DB::raw('IFNULL(SUM(qty_kirim),0) as qty_kirim_now'))  
                        ->join('rf_pengiriman','rf_pengiriman.id', '=', 'rf_pengiriman_detail.rf_pengiriman_id')
                        ->where('kode_paket', '=', $kode_paket)
                        ->where(DB::raw('MONTH(tanggal_kirim)'), '=', $monthNow)
                        ->where(DB::raw('YEAR(tanggal_kirim)'), '=', $yearNow)
                        ->first();  

        $paket= Paket::get();
        $paket=$paket->pluck('nama_paket','kode_paket');
        return view('mutasistok::stok.create', compact('paket','pengiriman_before','peneriman_before','penerimaan_now','pengiriman_now','selisih_before'));
    }


    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'kode_paket' => 'required|max:255',
            'saldo_awal' => 'required|max:15',
            'penerimaan' => 'required|max:11',
            'pengeluaran' => 'required|max:11',
            'stock_system' => 'required|max:11',
            'baik' => 'required|max:255',
            'kurang_baik' => 'required|max:255',
            'rusak' => 'required|max:255',
            'hilang' => 'required|max:255',
            'stock_fisik' => 'required|max:11',
            'selisih' => 'required|max:11',
        );
        $message = [
            'kode_propinsi.required' => 'Harap masukan kode propinsi',
            'saldo_awal.required' => 'Harap masukan saldo awal',
            'penerimaan.required' => 'Harap masukan penerimaan',
            'pengeluaran.required' => 'Harap masukan pengeluaran',
            'stock_system.required' => 'Harap masukan stok system',
            'baik.required' => 'Harap masukan Kondisi baik',
            'kurang_baik.required' => 'Harap masukan kondisi kurang baik',
            'rusak.required' => 'Harap masukan kondisi rusak',
            'hilang.required' => 'Harap masukan kondisi hilang',
            'stock_fisik.required' => 'Harap masukan stock fisik',
            'selisih.required' => 'Harap masukan selisih',            
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('mutasistok/stok/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $stock = new Stock;
            $data_input = $request->input();
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $stock->$key = $val;
            }
            $stock->save();

            // redirect
            Session::flash('message', 'Data stock berhasil disimpan');
            return Redirect::to('mutasistok/stok');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(\Illuminate\Http\Request $request)
    {
      
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Request $request ,$id)
    {
        $kode_paket = $request->query('kode_paket');
        $stok =Stock::find($id);
        $monthNow = date('m');
        $peneriman_before = DetailPenerima::orderBy('tr_penerimaan_detail.id','desc')//penerimaan
                           ->select(DB::raw('IFNULL(SUM(qty),0) as qty_penerimaan_before'))
                           ->join('tr_penerimaan', 'tr_penerimaan.id', '=', 'tr_penerimaan_detail.tr_penerimaan_id')
                           ->where('kode_paket','=',$kode_paket) 
                           ->where(DB::raw('MONTH(tanggal_penerimaan)'),'<',$monthNow)   
                           ->first();

        $pengiriman_before = DetailPengiriman::orderBy('rf_pengiriman_detail.id','desc')
                           ->select(DB::raw('IFNULL(SUM(qty_kirim),0) as qty_kirim_before'))
                           ->join('rf_pengiriman', 'rf_pengiriman.id', '=', 'rf_pengiriman_detail.rf_pengiriman_id')
                           ->where('kode_paket','=',$kode_paket)
                           ->where(DB::raw('MONTH(tanggal_kirim)'), '<', $monthNow)
                           ->first(); 

        $penerimaan_now = DetailPenerima::orderBy('tr_penerimaan_detail.id','desc')
                        ->select(DB::raw('IFNULL(SUM(qty),0) as qty_penerimaan_now'))  
                        ->join('tr_penerimaan','tr_penerimaan.id', '=', 'tr_penerimaan_detail.tr_penerimaan_id')
                        ->where('kode_paket', '=', $kode_paket)
                        ->where(DB::raw('MONTH(tanggal_penerimaan)'), '=', $monthNow)
                        ->first();

        $pengiriman_now = DetailPengiriman::orderBy('rf_pengiriman_detail.id','desc')
                        ->select(DB::raw('IFNULL(SUM(qty_kirim),0) as qty_kirim_now'))  
                        ->join('rf_pengiriman','rf_pengiriman.id', '=', 'rf_pengiriman_detail.rf_pengiriman_id')
                        ->where('kode_paket', '=', $kode_paket)
                        ->where(DB::raw('MONTH(tanggal_kirim)'), '=', $monthNow)
                        ->first();  

        $paket= Paket::get();
        $paket=$paket->pluck('nama_paket','kode_paket');
        return view('mutasistok::stok.edit', compact('stok','id','paket','pengiriman_before','peneriman_before','penerimaan_now','pengiriman_now'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
         $rules = array(
            'kode_paket' => 'required|max:255',
            'saldo_awal' => 'required|max:15',
            'penerimaan' => 'required|max:11',
            'pengeluaran' => 'required|max:11',
            'stock_system' => 'required|max:11',
            'stock_fisik' => 'required|max:11',
            'selisih' => 'required|max:11',
        );
        $message = [
            'kode_propinsi.required' => 'Harap masukan kode propinsi',
            'saldo_awal.required' => 'Harap masukan saldo awal',
            'penerimaan.required' => 'Harap masukan penerimaan',
            'pengeluaran.required' => 'Harap masukan pengeluaran',
            'stock_system.required' => 'Harap masukan stok system',
            'stock_fisik.required' => 'Harap masukan stock fisik',
            'selisih.required' => 'Harap masukan selisih',            
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('mutasistok/stok/'. $id .'/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            // update
            $stock = Stock::find($id);
            $data_input = $request->input();
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $stock->$key = $val;
            }
            $stock->save();

            // redirect
            Session::flash('message', 'Data stock berhasil diupdate');
            return Redirect::to('mutasistok/stok');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $stok = Stock::find($id);
        if($stok->delete()){
            return response()
            ->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()
            ->json(['code' => '50', 'message' => 'Data gagal dihapus']);
        }
    }
}
