<?php

namespace Modules\MutasiStok\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use App\Pengiriman;
use App\DetailPengiriman;
use  App\DetailPenerima;
use App\Paket;
use App\Roles;
use DB;
use Hash;
use Validator;
use Session;
use Redirect;
use View;

class KartuController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {   
        $kode_paket = $request->query('kode_paket');

        $penerimaan_list = DetailPenerima::orderBy('id','desc')//penerimaan
                           ->select('tr_penerimaan_detail.id','tr_penerimaan_detail.qty', 'tr_penerimaan.tanggal_penerimaan as tanggal_penerimaan')
                           ->join('tr_penerimaan', 'tr_penerimaan.id', '=', 'tr_penerimaan_detail.tr_penerimaan_id')
                           ->where('kode_paket','=',$kode_paket)     
                           ->get();

        $pengiriman_list = DetailPengiriman::orderBy('id','desc')
                           ->select('qty_kirim','tanggal_kirim')
                           ->select('rf_pengiriman_detail.id','rf_pengiriman_detail.qty_kirim', 'rf_pengiriman.tanggal_kirim as tanggal_kirim')
                           ->join('rf_pengiriman', 'rf_pengiriman.id', '=', 'rf_pengiriman_detail.rf_pengiriman_id')
                           ->where('kode_paket','=',$kode_paket)
                           ->get();

       
        $paket= Paket::get();
        $paket=$paket->pluck('nama_paket','kode_paket');
        return view('mutasistok::kartu.index', compact('paket','penerimaan_list','pengiriman_list'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
   
    public function create()
    {
        return view('mutasistok::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('mutasistok::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('mutasistok::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
