<?php

Route::group(['middleware' => 'web', 'prefix' => 'administrator', 'namespace' => 'Modules\Administrator\Http\Controllers'], function()
{
    Route::resource('propinsi', 'PropinsiController');
    Route::resource('kota', 'KotaController');
    Route::resource('gudang', 'GudangController');
    Route::get('gudang/get-districts/{id}', 'GudangController@getDistricts');
    Route::resource('paket', 'PaketController');
    Route::resource('bencana', 'BencanaController');
    Route::resource('vendor', 'VendorController');
    Route::get('vendor/get-districts/{id}', 'VendorController@getDistricts');
    Route::resource('satuan', 'SatuanController');
    Route::resource('kelompok', 'KelompokController');
    Route::resource('perolehan', 'PerolehanController');

    // Route::get('/propinsi', 'PropinsiController@index');
    // Route::get('/propinsi/create', 'PropinsiController@create');
    // Route::post('/propinsi', 'PropinsiController@store');
    // Route::get('/propinsi/edit', 'PropinsiController@edit');
});
