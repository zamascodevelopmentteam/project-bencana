<?php

namespace Modules\Administrator\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use App\Propinsi;
use App\Kota;
use App\Vendor;
use App\Roles;
use DB;
use Hash;
use Validator;
use Session;
use Redirect;
use View;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
     public function index(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Vendor::select(DB::raw('*, @rownum := @rownum +1 as rownum'),'rf_vendor.nama_vendor','rf_vendor.id','rf_vendor.alamat','rf_vendor.telepon','rf_vendor.pic', 'provinces.nama_propinsi as province_name', 'districts.nama_kabupaten as districts_name')
                ->join('provinces', 'provinces.kode_propinsi', '=', 'rf_vendor.kode_propinsi')
                ->join('districts','districts.kode_kabupaten', '=','rf_vendor.kode_kabupaten');
           
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                 '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                    <li><a href="vendor/'.$data->id.'/edit">Edit</a></li>
                                    <li><a href="vendor/'.$data->id.'" data-id="" class="btn-delete-on-table">Delete</a></li>
                                  </ul><div>'
                        ;})
            ->make(true);
        }
        return view('administrator::vendor.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $roles = Roles::all();
        $roles_list = [];
        foreach($roles as $key => $value){
            //dd($value->type->name);
            $roles_list[$value['id']] = $value->display_name ;
        }
        $propinsi = Propinsi::get();
        $propinsi = $propinsi->pluck('nama_propinsi', 'kode_propinsi');
        $kabupaten = Kota::get();
        $kabupaten = $kabupaten->pluck('nama_kabupaten', 'kode_kabupaten');
        return view('administrator::vendor.create',compact('propinsi','kabupaten'))->with('roles',$roles_list);
    }

    public function getDistricts(\Illuminate\Http\Request $request,$id)
    {
        $vendor= Kota::where('kode_propinsi','=',$id)->get();
        return response()
            ->json($vendor);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'nama_vendor' => 'required|max:255',
            'kode_propinsi' => 'required|max:15',
            'kode_kabupaten' => 'required|max:15',
            'alamat' => 'required',
            'telepon' => 'required|max:100|unique:rf_gudang',
            'pic' => 'required|max:255',

           
        );
        $message = [
            'nama_gudang.required' => 'Harap masukan nama gudang',
            'kode_propinsi.required' => 'Harap pilih nama provinsi',
            'kode_kabupaten.required' => 'Harap masukan nama kabupaten',
            'telepon.unique' => 'Nomor telepon telah terdaftar',
            'telepon.required' => 'Harap masukan nomor telepon',
            'pic.required' => 'Harap masukan nama pic',

            
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('administrator/vendor/'. $id .'/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $vendor = new Vendor;
            $data_input = $request->input();
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $vendor->$key = $val;
            }
            $vendor->save();

            // redirect
            Session::flash('message', 'Data Vendor berhasil ditambahkan');
            return Redirect::to('administrator/vendor');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('administrator::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $roles = Roles::all();
        $roles_list = [];
        foreach($roles as $key => $value){
            //dd($value->type->name);
            $roles_list[$value['id']] = $value->display_name ;
        }
        $vendor = vendor::find($id);
        $propinsi = Propinsi::get();
        $propinsi = $propinsi->pluck('nama_propinsi', 'kode_propinsi');
        $kabupaten = Kota::get();
        $kabupaten = $kabupaten->pluck('nama_kabupaten', 'kode_kabupaten');
        return view('administrator::vendor/edit',compact('propinsi','id','kabupaten','vendor'))->with('roles',$roles_list)->with('vendor',$vendor);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $rules = array(
            'nama_vendor' => 'required|max:255',
            'kode_propinsi' => 'required|max:15',
            'kode_kabupaten' => 'required|max:15',
            'alamat' => 'required',
            'telepon' => 'required|max:100|unique:rf_gudang',
            'pic' => 'required|max:255',

           
        );
        $message = [
            'nama_gudang.required' => 'Harap masukan nama gudang',
            'kode_propinsi.required' => 'Harap pilih nama provinsi',
            'kode_kabupaten.required' => 'Harap masukan nama kabupaten',
            'telepon.unique' => 'Nomor telepon telah terdaftar',
            'telepon.required' => 'Harap masukan nomor telepon',
            'pic.required' => 'Harap masukan nama pic',

            
            
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('administrator/vendor/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            // update
            $vendor = Vendor::find($id);
            $data_input = $request->input();
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $vendor->$key = $val;
            }
            $vendor->save();

            // redirect
            Session::flash('message', 'Data vendor berhasil diupdate');
            return Redirect::to('administrator/vendor');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
         $vendor = Vendor::find($id);
        
        if($vendor->delete()){
            return response()
            ->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()
            ->json(['code' => '50', 'message' => 'Data gagal dihapus']);
        }
    }
}
