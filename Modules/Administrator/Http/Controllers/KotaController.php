<?php

namespace Modules\Administrator\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use App\Propinsi;
use App\Kota;
use App\Roles;
use DB;
use Hash;
use Validator;
use Session;
use Redirect;
use View;


class KotaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Kota::select(DB::raw('*, @rownum := @rownum +1 as rownum'),'districts.kode_kabupaten','districts.id','districts.nama_kabupaten', 'provinces.nama_propinsi as province_name')
                ->join('provinces', 'provinces.kode_propinsi', '=', 'districts.kode_propinsi');
           
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                 '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                    <li><a href="kota/'.$data->id.'/edit">Edit</a></li>
                                    <li><a href="kota/'.$data->id.'" data-id="" class="btn-delete-on-table">Delete</a></li>
                                  </ul><div>'
                        ;})
            ->make(true);
        }
        return view('administrator::kota.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $roles = Roles::all();
        $roles_list = [];
        foreach($roles as $key => $value){
            //dd($value->type->name);
            $roles_list[$value['id']] = $value->display_name ;
        }
        $propinsi = Propinsi::get();
        $propinsi = $propinsi->pluck('nama_propinsi', 'kode_propinsi');
        return view('administrator::kota.create', compact('propinsi'))->with('roles',$roles_list);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'kode_kabupaten' => 'required|max:15',
            'kode_propinsi' => 'required|max:15',
            'nama_kabupaten' => 'required|max:25',
           
        );
        $message = [
            'kode_kabupaten.required' => 'Harap masukan kode kabupaten',
            'nama_propinsi.required' => 'Harap pilih nama propinsi',
            'nama_kabupaten.required' => 'Harap masukkan nama kabupaten'
            
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('administrator/kota/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $kota = new Kota;
            $data_input = $request->input();
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $kota->$key = $val;
            }
            $kota->save();

            // redirect
            Session::flash('message', 'Data kota berhasil ditambahkan');
            return Redirect::to('administrator/kota');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $roles = Roles::all();
        $roles_list = [];
        foreach($roles as $key => $value){
            //dd($value->type->name);
            $roles_list[$value['id']] = $value->display_name ;
        }
        $kota = Kota::find($id);
        $propinsi = Propinsi::get();
        $propinsi = $propinsi->pluck('nama_propinsi', 'kode_propinsi');
        return view('administrator::kota/edit',compact('propinsi','id','kota'))->with('roles',$roles_list)->with('kota',$kota);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'kode_kabupaten' => 'required|max:15',
            'kode_propinsi' => 'required|max:15',
            'nama_kabupaten' => 'required|max:25',
           
        );
        $message = [
            'kode_kabupaten.required' => 'Harap masukan kode kabupaten',
            'nama_propinsi.required' => 'Harap pilih nama propinsi',
            'nama_kabupaten.required' => 'Harap masukkan nama kabupaten'
            
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('administrator/kota/'. $id .'/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            // update
            $kota = Kota::find($id);
            $data_input = $request->input();
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $kota->$key = $val;
            }
            $kota->save();

            // redirect
            Session::flash('message', 'Data kota berhasil diupdate');
            return Redirect::to('administrator/kota');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $kota = Kota::find($id);
        
        if($kota->delete()){
            return response()
            ->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()
            ->json(['code' => '50', 'message' => 'Data gagal dihapus']);
        }
    }
}
