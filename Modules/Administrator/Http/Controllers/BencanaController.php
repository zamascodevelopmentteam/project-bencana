<?php

namespace Modules\Administrator\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use App\Bencana;
use App\Roles;
use DB;
use Hash;
use Validator;
use Session;
use Redirect;
use View;

class BencanaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
         if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Bencana::select(DB::raw('*, @rownum := @rownum +1 as rownum'));
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                 '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                    <li><a href="bencana/'.$data->id.'/edit">Edit</a></li>
                                    <li><a href="bencana/'.$data->id.'" data-id="" class="btn-delete-on-table">Delete</a></li>
                                  </ul><div>'
                        ;})
            ->make(true);
        }
        return view('administrator::bencana.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $roles = Roles::all();
        $roles_list = [];
        foreach($roles as $key => $value){
            //dd($value->type->name);
            $roles_list[$value['id']] = $value->display_name ;
        }
        return view('administrator::bencana.create')->with('roles',$roles_list);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'kd_bencana' => 'required|max:15',
            'ur_bencana' => 'required|max:25',
           
        );
        $message = [
            'kd_bencana.required' => 'Harap masukan kode bencana',
            'ur_bencana.required' => 'Harap masukan jenis bencana',
            
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('settings/bencana/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $bencana = new Bencana;
            $data_input = $request->input();
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $bencana->$key = $val;
            }
            $bencana->save();

            // redirect
            Session::flash('message', 'Data bencana berhasil disimpan');
            return Redirect::to('administrator/bencana');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('administrator::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $roles = Roles::all();
        $roles_list = [];
        foreach($roles as $key => $value){
            //dd($value->type->name);
            $roles_list[$value['id']] = $value->display_name ;
        }
        $bencana = Bencana::find($id);
        return view('administrator::bencana.edit', compact('bencana','id', 'roles_list','roles'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'kd_bencana' => 'required|max:15',
            'ur_bencana' => 'required|max:25',
           
        );
        $message = [
            'kd_bencana.required' => 'Harap masukan kode bencana',
            'ur_bencana.required' => 'Harap masukan jenis bencana',
            
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('administrator/bencana/'. $id .'/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            // update
            $bencana = Bencana::find($id);
            $data_input = $request->input();
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $bencana->$key = $val;
            }
            $bencana->save();

            // redirect
            Session::flash('message', 'Data bencana berhasil diupdate');
            return Redirect::to('administrator/bencana');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
         $bencana = bencana::find($id);
        
        if($bencana->delete()){
            return response()
            ->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()
            ->json(['code' => '50', 'message' => 'Data gagal dihapus']);
        }
    }
}
