<?php

namespace Modules\Administrator\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use App\Paket;
use App\Barang;
use App\Roles;
use DB;
use Hash;
use Validator;
use Session;
use Redirect;
use View;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Barang::select(DB::raw('*, @rownum := @rownum +1 as rownum'),'rf_isi_paket.kode_isi_paket','rf_isi_paket.id','rf_isi_paket.ur_isi_paket','rf_isi_paket.satuan','rf_paket.nama_paket as paket_name')
                ->join('rf_paket', 'rf_paket.kode_paket', '=', 'rf_isi_paket.kode_paket');
             
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                 '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                    <li><a href="barang/'.$data->id.'/edit">Edit</a></li>
                                    <li><a href="barang/'.$data->id.'" data-id="" class="btn-delete-on-table">Delete</a></li>
                                  </ul><div>'
                        ;})
            ->make(true);
        }
        return view('administrator::barang.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
   public function create()
    {
        $roles = Roles::all();
        $roles_list = [];
        foreach($roles as $key => $value){
            //dd($value->type->name);
            $roles_list[$value['id']] = $value->display_name ;
        }
        $paket = Paket::get();
        $paket = $paket->pluck('nama_paket', 'kode_paket');
        return view('administrator::barang.create', compact('paket'))->with('roles',$roles_list);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'kode_isi_paket' => 'required|max:25',
            'kode_paket' => 'required|max:25',
            'ur_isi_paket' => 'required|max:25',
            'satuan' => 'required|max:100',
           
        );
        $message = [

            'kode_isi_paket.required' => 'Harap masukan kode isi paket',
            'kode_paket.required' => 'Harap pilih nama paket',
            'ur_isi_paket.required' => 'Harap masukkan nama isi paket',
            'satuan.required' =>'Harap masukkan nama satuan',
            
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('administrator/barang/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $barang = new barang;
            $data_input = $request->input();
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $barang->$key = $val;
            }
            $barang->save();

            // redirect
            Session::flash('message', 'Data barang berhasil ditambahkan');
            return Redirect::to('administrator/barang');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('administrator::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $roles = Roles::all();
        $roles_list = [];
        foreach($roles as $key => $value){
            //dd($value->type->name);
            $roles_list[$value['id']] = $value->display_name ;
        }
        $barang = Barang::find($id);
        $paket = Paket::get();
        $paket = $paket->pluck('nama_paket', 'kode_paket');
        return view('administrator::barang/edit',compact('paket','id','barang'))->with('roles',$roles_list)->with('barang',$barang);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'kode_isi_paket' => 'required|max:25',
            'kode_paket' => 'required|max:25',
            'ur_isi_paket' => 'required|max:25',
            'satuan' => 'required|max:100',
           
        );
        $message = [
            'kode_isi_paket.required' => 'Harap masukan kode isi paket',
            'kode_paket.required' => 'Harap pilih nama paket',
            'ur_isi_paket.required' => 'Harap masukkan nama isi paket',
            'satuan.required' =>'Harap masukkan nama satuan',
            
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('administrator/barang/'. $id .'/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            // update
            $barang = Barang::find($id);
            $data_input = $request->input();
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $barang->$key = $val;
            }
            $barang->save();

            // redirect
            Session::flash('message', 'Data barang berhasil diupdate');
            return Redirect::to('administrator/barang');
        }
    }


    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $barang = Barang::find($id);
        
        if($barang->delete()){
            return response()
            ->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()
            ->json(['code' => '50', 'message' => 'Data gagal dihapus']);
        }
    }
}
