<?php

namespace Modules\Administrator\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use App\Perolehan;
use App\Roles;
use DB;
use Hash;
use Validator;
use Session;
use Redirect;
use View;

class PerolehanController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
         if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Perolehan::select(DB::raw('*, @rownum := @rownum +1 as rownum'));
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                 '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                    <li><a href="perolehan/'.$data->id.'/edit">Edit</a></li>
                                    <li><a href="perolehan/'.$data->id.'" data-id="" class="btn-delete-on-table">Delete</a></li>
                                  </ul><div>'
                        ;})
            ->make(true);
        }
        return view('administrator::perolehan.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        
        return view('administrator::perolehan.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'nama_asal_perolehan' => 'required|max:255',
            'unsur' => 'required|max:100',
           
        );
        $message = [
            'nama_asal_perolehan.required' => 'Harap masukan nama asal perolehan',
            'unsur.required' => 'Harap masukan unsur',
            
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('administrator/perolehan/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $perolehan = new Perolehan;
            $data_input = $request->input();
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $perolehan->$key = $val;
            }
            $perolehan->save();

            // redirect
            Session::flash('message', 'Data perolehan berhasil disimpan');
            return Redirect::to('administrator/perolehan');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('administrator::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
       
        $perolehan = perolehan::find($id);
        return view('administrator::perolehan.edit', compact('perolehan','id'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $rules = array(
            'nama_asal_perolehan' => 'required|max:255',
            'unsur' => 'required|max:100',
           
        );
        $message = [
            'nama_asal_perolehan.required' => 'Harap masukan nama asal perolehan',
            'unsur.required' => 'Harap masukan unsur',
            
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('administrator/perolehan/'. $id .'/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            // update
            $perolehan = Perolehan::find($id);
            $data_input = $request->input();
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $perolehan->$key = $val;
            }
            $perolehan->save();

            // redirect
            Session::flash('message', 'Data perolehan berhasil diupdate');
            return Redirect::to('administrator/perolehan');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $perolehan = Perolehan::find($id);
        
        if($perolehan->delete()){
            return response()
            ->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()
            ->json(['code' => '50', 'message' => 'Data gagal dihapus']);
        }
    }
}
