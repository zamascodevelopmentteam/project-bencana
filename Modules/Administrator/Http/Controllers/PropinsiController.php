<?php

namespace Modules\Administrator\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use App\Propinsi;
use App\Roles;
use DB;
use Hash;
use Validator;
use Session;
use Redirect;
use View;

class PropinsiController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
         if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Propinsi::select(DB::raw('*, @rownum := @rownum +1 as rownum'));
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                 '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                    <li><a href="propinsi/'.$data->id.'/edit">Edit</a></li>
                                    <li><a href="propinsi/'.$data->id.'" data-id="" class="btn-delete-on-table">Delete</a></li>
                                  </ul><div>'
                        ;})
            ->make(true);
        }
        return view('administrator::propinsi.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $roles = Roles::all();
        $roles_list = [];
        foreach($roles as $key => $value){
            //dd($value->type->name);
            $roles_list[$value['id']] = $value->display_name ;
        }
        return view('administrator::propinsi.create')->with('roles',$roles_list);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'kode_propinsi' => 'required|max:15',
            'nama_propinsi' => 'required|max:25',
           
        );
        $message = [
            'kode_propinsi.required' => 'Harap masukan kode provinsi',
            'nama_propinsi.required' => 'Harap masukan nama provinsi',
            
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('administrator/propinsi/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $propinsi = new Propinsi;
            $data_input = $request->input();
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $propinsi->$key = $val;
            }
            $propinsi->save();

            // redirect
            Session::flash('message', 'Data provinsi berhasil disimpan');
            return Redirect::to('administrator/propinsi');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('administrator::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $roles = Roles::all();
        $roles_list = [];
        foreach($roles as $key => $value){
            //dd($value->type->name);
            $roles_list[$value['id']] = $value->display_name ;
        }
        $propinsi = Propinsi::find($id);
        return view('administrator::propinsi.edit', compact('propinsi','id', 'roles_list','roles'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'kode_propinsi' => 'required|max:15',
            'nama_propinsi' => 'required|max:25',
           
        );
        $message = [
            'kode_propinsi.required' => 'Harap masukan kode provinsi',
            'nama_propinsi.required' => 'Harap masukan nama provinsi',
            
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('administrator/propinsi/'. $id .'/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            // update
            $propinsi = Propinsi::find($id);
            $data_input = $request->input();
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $propinsi->$key = $val;
            }
            $propinsi->save();

            // redirect
            Session::flash('message', 'Data provinsi berhasil diupdate');
            return Redirect::to('administrator/propinsi');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $propinsi = Propinsi::find($id);
        
        if($propinsi->delete()){
            return response()
            ->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()
            ->json(['code' => '50', 'message' => 'Data gagal dihapus']);
        }
    }
}