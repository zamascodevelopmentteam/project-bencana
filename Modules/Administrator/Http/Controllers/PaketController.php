<?php

namespace Modules\Administrator\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use App\Paket;
use App\Kelompok;
use DB;
use App\Satuan;
use Hash;
use App\Roles;
use Validator;
use Session;
use Redirect;
use View;

class PaketController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
     public function index(\Illuminate\Http\Request $request)
    {
         if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Paket::select(DB::raw('*, @rownum := @rownum +1 as rownum'),'rf_paket.nama_paket','rf_paket.kode_paket','rf_paket.isi_per_box','rf_paket.id','rf_paket.berat','rf_paket.berat_gram','rf_paket.berat_per_box','rf_paket.berat_per_box_gram','rf_paket.volume','rf_paket.volume_cm','rf_paket.panjang','rf_paket.panjang_cm','rf_paket.lebar','rf_paket.lebar_cm','rf_paket.tinggi','rf_paket.tinggi_cm','rf_paket.harga_total','rf_satuan.ur_satuan as satuan','rf_kelompok_barang.ur_kelompok_barang as kelompok_barang')
            ->join('rf_satuan', 'rf_satuan.id', '=', 'rf_paket.rf_satuan_id')
            ->join('rf_kelompok_barang', 'rf_kelompok_barang.id', '=', 'rf_paket.rf_kelompok_barang_id');
            return Datatables::of($datas)
            ->addColumn('isi_per_box', function ($data) { return ($data->isi_per_box) ? $data->isi_per_box : "-" ; })    

            ->addColumn('berat_per_box_gram', function ($data) { return ($data->berat_per_box_gram) ? $data->berat_per_box_gram.' '.'gr' : "-" ; })
            ->addColumn('berat_per_box', function ($data) { return ($data->berat_per_box) ? $data->berat_per_box.' '.'kg' : "-" ; })

            ->addColumn('berat', function ($data) { return ($data->berat) ? $data->berat.' '.'kg' : "-"; })
            ->addColumn('berat_gram', function ($data) { return ($data->berat_gram) ? $data->berat_gram .' '.'gr' : "-" ; })

            ->addColumn('lebar', function ($data) { return ($data->lebar) ? $data->lebar.' '.'m' : "-"; })
            ->addColumn('lebar_cm', function ($data) { return ($data->lebar_cm) ? $data->lebar_cm .' '.'cm' : "-" ; })

            ->addColumn('panjang_cm', function ($data) { return ($data->panjang_cm) ? $data->panjang_cm .' '. 'cm' : "-" ; })
            ->addColumn('panjang', function ($data) { return ($data->panjang) ? $data->panjang .' '.'m' : "-"; })

            ->addColumn('tinggi', function ($data) { return ($data->tinggi) ? $data->tinggi .' '.'m' : "-" ; })
            ->addColumn('tinggi_cm', function ($data) { return ($data->tinggi_cm) ? $data->tinggi_cm.' '.'cm' : "-" ; })

            ->addColumn('volume', function ($data) { return ($data->volume) ? $data->volume .' '.'m' : "-"; })
            ->addColumn('volume_cm', function ($data) { return ($data->volume_cm) ? $data->volume_cm .' '.'cm' : "-" ; })
            
            // ->addColumn('harga_total', function ($data) { return  $data->harga_total; })

            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                 '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                    <li><a href="paket/'.$data->id.'/edit">Edit</a></li>
                                    <li><a href="paket/'.$data->id.'" data-id="" class="btn-delete-on-table">Delete</a></li>
                                  </ul><div>'
                        ;})
            ->make(true);
        }
        return view('administrator::paket.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
       $satuan = Satuan::get();
       $satuan=$satuan->pluck('ur_satuan','id');
       $kelompok = Kelompok::get();
       $kelompok=$kelompok->pluck('ur_kelompok_barang','id');
        return view('administrator::paket.create', compact('kelompok','satuan'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'kode_paket' => 'required|max:25',
            'nama_paket' => 'required|max:255',
            'isi_per_box' => 'required|max:20',
            'rf_satuan_id' => 'required|max:20',
            // 'berat' => 'required|max:20',
            // 'berat_per_box' => 'required|max:25',
            // 'volume' => 'required|max:255',
            // 'panjang' => 'required|max:20',
            // 'lebar' => 'required|max:20',
            // 'tinggi' => 'required|max:20',
            'rf_kelompok_barang_id' => 'required|max:20',
            // 'harga_total' => 'required|max:20',
        
        );
        $message = [
            'kode_paket.required' => 'Harap masukan Kode Barang',
            'nama_paket.required' => 'Harap masukan Nama Barang',
            'isi_per_box.required' => 'Harap masukan Isi Per Box',
            'rf_satuan_id.required' => 'Harap masukan satuan barang',
            // 'berat.required' => 'Harap masukan berat',
            // 'berat_per_box.required' => 'Harap masukan Berat Per Koli',
            // 'volume.required' => 'Harap masukan volume',
            // 'panjang.required' => 'Harap masukan panjang',
            // 'lebar.required' => 'Harap masukan lebar',
            // 'tinggi.required' => 'Harap masukan tinggi',
            'rf_kelompok_barang_id.required' => 'Harap masukan kelompok',
            // 'harga_total.required' => 'Harap masukan harga total',
            
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('administrator/paket/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $paket = new Paket;
            $data_input = $request->input();
            $data_input['harga_total'] = str_replace( "," , "" , $data_input['harga_total']);
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $paket->$key = $val;
            }
            $paket->save();

            // redirect
            Session::flash('message', 'Data paket berhasil disimpan');
            return Redirect::to('administrator/paket');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $paket = Paket::find($id);
        $satuan = Satuan::get();
        $satuan = $satuan->pluck('ur_satuan', 'id');
        $kelompok = Kelompok::get();
        $kelompok = $kelompok->pluck('ur_kelompok_barang', 'id');
        return view('administrator::paket.edit', compact('satuan','paket','kelompok','id'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
       $rules = array(
            'kode_paket' => 'required|max:25',
            'nama_paket' => 'required|max:255',
            'isi_per_box' => 'required|max:20',
            'rf_satuan_id' => 'required|max:20',
            // 'berat' => 'required|max:20',
            // 'berat_per_box' => 'required|max:25',
            // 'volume' => 'required|max:255',
            // 'panjang' => 'required|max:20',
            // 'lebar' => 'required|max:20',
            // 'tinggi' => 'required|max:20',
            'rf_kelompok_barang_id' => 'required|max:20',
            // 'harga_total' => 'required|max:20',
        
        );
        $message = [
            'kode_paket.required' => 'Harap masukan Kode Barang',
            'nama_paket.required' => 'Harap masukan Nama Barang',
            'isi_per_box.required' => 'Harap masukan Isi Per Box',
            'rf_satuan_id.required' => 'Harap masukan satuan barang',
            // 'berat.required' => 'Harap masukan berat',
            // 'berat_per_box.required' => 'Harap masukan berat per box',
            // 'volume.required' => 'Harap masukan volume',
            // 'panjang.required' => 'Harap masukan panjang',
            // 'lebar.required' => 'Harap masukan lebar',
            // 'tinggi.required' => 'Harap masukan tinggi',
            'rf_kelompok_barang_id.required' => 'Harap masukan kelompok',
            // 'harga_total.required' => 'Harap masukan harga total',
            
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('administrator/paket/'. $id .'/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            // update
            $paket = Paket::find($id);
            $data_input = $request->input();
            $data_input['harga_total'] = str_replace( "," , "" , $data_input['harga_total']);
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $paket->$key = $val;
            }
            $paket->save();

            // redirect
            Session::flash('message', 'Data paket berhasil diupdate');
            return Redirect::to('administrator/paket');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
         $paket = Paket::findOrFail($id);        
        if($paket->delete()){
            return response()
            ->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()
            ->json(['code' => '50', 'message' => 'Data gagal dihapus']);
        }
    }
}
