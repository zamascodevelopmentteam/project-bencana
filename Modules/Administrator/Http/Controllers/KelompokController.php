<?php

namespace Modules\Administrator\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use App\Kelompok;
use App\Roles;
use DB;
use Hash;
use Validator;
use Session;
use Redirect;
use View;

class KelompokController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
         if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Kelompok::select(DB::raw('*, @rownum := @rownum +1 as rownum'));
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                 '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                    <li><a href="kelompok/'.$data->id.'/edit">Edit</a></li>
                                    <li><a href="kelompok/'.$data->id.'" data-id="" class="btn-delete-on-table">Delete</a></li>
                                  </ul><div>'
                        ;})
            ->make(true);
        } 
        return view('administrator::kelompok.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        
        return view('administrator::kelompok.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
         $rules = array(
            'ur_kelompok_barang' => 'required|max:100',
           
        );
        $message = [
            'ur_kelompok_barang.required' => 'Harap masukan kelompok barang',
            
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('administrator/kelompok/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $kelompok = new Kelompok;
            $data_input = $request->input();
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $kelompok->$key = $val;
            }
            $kelompok->save();

            // redirect
            Session::flash('message', 'Data kelompok berhasil disimpan');
            return Redirect::to('administrator/kelompok');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('administrator::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
       
        $kelompok = Kelompok::find($id);
        return view('administrator::kelompok.edit', compact('kelompok','id'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $rules = array(
            'ur_kelompok_barang' => 'required|max:100',
           
        );
        $message = [
            'ur_kelompok_barang.required' => 'Harap masukan kelompok barang',
            
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('administrator/kelompok/'. $id .'/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            // update
            $kelompok = kelompok::find($id);
            $data_input = $request->input();
            unset($data_input['_method']);
            unset($data_input['_token']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $kelompok->$key = $val;
            }
            $kelompok->save();

            // redirect
            Session::flash('message', 'Data kelompok berhasil diupdate');
            return Redirect::to('administrator/kelompok');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $kelompok = Kelompok::find($id);
        
        if($kelompok->delete()){
            return response()
            ->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()
            ->json(['code' => '50', 'message' => 'Data gagal dihapus']);
        }
    }
}
