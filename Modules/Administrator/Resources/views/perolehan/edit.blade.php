@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAJEMEN ASAL PEROLEHAN</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data asal perolehan pada sistem aplikasi</small>
        </p>
    </div>
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>EDIT DATA ASAL PEROLEHAN</strong>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    @if(!empty($errors->all()))
                        <div class="alert alert-danger">
                        {{ HTML::ul($errors->all()) }}
                        </div>
                    @endif
                    {{ Form::model($perolehan, ['url' => array('administrator/perolehan', $perolehan->id), 'method' => 'PUT']) }}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('nama_asal_perolehan', 'NAMA ASAL PEROLEHAN') }}
                                        {{ Form::text('nama_asal_perolehan', Input::old('nama_asal_perolehan'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('unsur', 'UNSUR') }}
                                       <select name="unsur" id="type" class="form-control" placeholder="Pilih level">
                                           <option value="">Pilih</option>
                                           <option value="MASYARAKAT" {{ ($perolehan->unsur == 'MASYARAKAT') ? 'selected' : '' }}>MASYARAKAT</option>
                                           <option value="PEMERINTAH" {{ ($perolehan->unsur == 'PEMERINTAH') ? 'selected' : '' }}>PEMERINTAH</option>
                                           <option value="DUNIA USAHA" {{ ($perolehan->unsur == 'DUNIA USAHA') ? 'selected' : '' }}>DUNIA USAHA</option>          
                                        </select> 
                                    </div>
                                </div>
                            </div>


                            {{ Form::button('<i class="icon icon-save"></i> Simpan', array('class' => 'btn btn-primary','type'=>'submit')) }}
                            <a href="{{url($prefix.'/perolehan')}}" class="btn btn-primary"><i class="icon icon-arrow-left"></i> Kembali</a>
                        {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop
