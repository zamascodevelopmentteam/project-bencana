@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAJEMEN KOTA/KABUPATEN</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data kota/kabupaten pada sistem aplikasi</small>
        </p>
    </div>
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>EDIT DATA KOTA/KABUPATEN</strong>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    @if(!empty($errors->all()))
                        <div class="alert alert-danger">
                        {{ HTML::ul($errors->all()) }}
                        </div>
                    @endif
                    {{ Form::model($kota, ['route' => array('kota.update', $kota->id), 'method' => 'PUT']) }}
                           <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('kode_kabupaten', 'KODE KOTA/KABUPATEN') }}
                                        {{ Form::text('kode_kabupaten', Input::old('kode_kabupaten'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('kode_propinsi', 'NAMA PROVINSI') }}
                                        {{ Form::select('kode_propinsi', $propinsi,Input::old('nama_propinsi'), array('class' => 'form-control kode_propinsi', 'placeholder' => 'Pilih Propinsi')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('nama_kabupaten', 'NAMA KOTA/KABUPATEN') }}
                                        {{ Form::text('nama_kabupaten', Input::old('nama_kabupaten'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                            {{ Form::button('<i class="icon icon-save"></i> Simpan', array('class' => 'btn btn-primary','type'=>'submit')) }}
                            <a href="{{url($prefix.'/kota')}}" class="btn btn-primary"><i class="icon icon-arrow-left"></i> Kembali</a>
                        {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop

@section('scriptBlock')
    <script type="text/javascript">
        $(".kode_propinsi").select2();
    </script>
@stop