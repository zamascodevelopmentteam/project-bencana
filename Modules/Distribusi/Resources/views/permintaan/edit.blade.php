@extends('layouts.master')
@section('content')
    <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">MANAJEMEN PERMINTAAN BARANG</span>
        </h1>
        <p class="title-bar-description">
          <small>Mengelola data permintaan barang pada sistem aplikasi</small>
        </p>
    </div>
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>EDIT DATA PERMINTAAN BARANG</strong>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    @if(!empty($errors->all()))
                        <div class="alert alert-danger">
                        {{ HTML::ul($errors->all()) }}
                        </div>
                    @endif
                    {{ Form::model($permintaan, ['route' => array('permintaan.update', $permintaan->id), 'method' => 'PUT', 'files' => true]) }}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('nama', 'NAMA') }}
                                        {{ Form::text('nama', Input::old('nama'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('nomor_permintaan', 'NOMOR PERMINTAAN') }}
                                        {{ Form::text('nomor_permintaan', Input::old('nomor_permintaan'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('tanggal', 'TANGGAL') }}
                                        {{ Form::text('tanggal', Input::old('tanggal'), array('class' => 'form-control datepick')) }}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('attachment_file', 'ATTACHMENT FILE (MAX 10 MB)') }}
                                        {{ Form::file('attachment_file', Input::old('attachment_file'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                             <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('keterangan', 'KETERANGAN') }}
                                        {{ Form::text('keterangan', Input::old('keterangan'), array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-input">
                                            <thead>
                                                <tr>
                                                    <th width="10%">NO</th>
                                                    <th>PAKET</th>
                                                    <th width="15%">QTY</th>
                                                    <th width="5%"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="tr-input">
                                                    <td colspan="2">
                                                        {{ Form::select('kode_paket', $paket,Input::old('paket'), array('class' => 'form-control kode','placeholder' => 'Pilih Paket','id' => 'kode_paket')) }}
                                                    </td>
                                                    <td>
                                                        {{ Form::text('qty', Input::old('qty'), array('class' => 'form-control','id' => 'qty')) }}
                                                    </td>
                                                    <td>
                                                        <a href="#" class="btn btn-info btn-add-detail" >TAMBAH</a>
                                                    </td>
                                                </tr>
                                                @foreach($permintaanDetail as $key => $detail)
                                                	<tr>
                                                		<td>{{$key + 1}}</td>
                                                		<td>{{$detail->nama_paket}}</td>
                                                		<td>{{$detail->qty}}</td>
                                                		<td>
                                                			<a href="#" class="btn btn-danger btn-remove-detail">HAPUS</a>
                                                			<input type='hidden' value='{{$detail->kode_paket}}' name='detail_permintaan[{{$key}}][kode_paket]' class='detailKodePaket'>
                                                			<input type='hidden' value='{{$detail->qty}}' name='detail_permintaan[{{$key}}][qty]' class='detailQty'>
                                                		</td>
                                                	</tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            {{ Form::button('<i class="icon icon-save"></i> Simpan', array('class' => 'btn btn-primary','type'=>'submit')) }}
                            <a href="{{url($prefix.'/permintaan')}}" class="btn btn-primary"><i class="icon icon-arrow-left"></i> Kembali</a>
                        {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop
@section('scriptBlock')
    <script type="text/javascript">
        $(".kode").select2();
        var countDetail = $(".table-input tbody tr:not(.tr-input)").length ;
        console.log(countDetail);
        $(".btn-add-detail").on("click",function(e){
            e.preventDefault();
            var kode_paket = $("#kode_paket");
            var qty = $("#qty");
            if(kode_paket.val() == "" || qty.val() == '')
            {
                alert('masukan paket dan qty');
                return false;
            }

            var paket = $("#kode_paket option[value='"+kode_paket.val()+"']").html();
            var no = countDetail +1;
            var html = "<tr>"
                      +"<td>"
                      +no  
                      +"</td>"
                      +"<td>"
                      +paket
                      +"</td>"
                      +"<td>"
                      +qty.val()
                      +"</td>"
                      +"<td>"
                      +"<a href='#' class='btn btn-danger btn-remove-detail'>HAPUS</a>"
                      +"<input type='hidden' value='"+kode_paket.val()+"' name='detail_permintaan["+countDetail+"][kode_paket]' class='detailKodePaket'>"
                      +"<input type='hidden' value='"+qty.val()+"' name='detail_permintaan["+countDetail+"][qty]' class='detailQty'>" 
                      +"</td>"
                      +"</tr>";
            countDetail = countDetail +1;
            kode_paket.val('');
            qty.val('');
            $(".table-input tbody").append(html);
        })

        $("body").on("click",".btn-remove-detail",function(e){
            e.preventDefault();
            var tr = $(this).closest('tr');
            tr.remove();
            countDetail = countDetail -1;

            $.each($(".table-input tbody tr:not(.tr-input)"),function(i,item){
                console.log(i);
                var paket = $(this).find('.detailKodePaket');
                var no = $(this).find('td').first();
                var qty = $(this).find('.detailQty');
                var numer = i + 1 *1;
                no.html(numer);
                paket.attr('name',"detail_permintaan["+i+"][kode_paket]");
                qty.attr('name',"detail_permintaan["+i+"][qty]");
                console.log(no);

            })
        })
    </script>
@stop