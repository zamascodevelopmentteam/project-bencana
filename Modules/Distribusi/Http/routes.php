<?php

Route::group(['middleware' => 'web', 'prefix' => 'distribusi', 'namespace' => 'Modules\Distribusi\Http\Controllers'], function()
{
    Route::resource('permintaan', 'PermintaanController');
    Route::get('download', 'PermintaanController@index_download');
    Route::get('permintaan/download/{id}/', 'PermintaanController@download')->name('permintaan.download');
});
