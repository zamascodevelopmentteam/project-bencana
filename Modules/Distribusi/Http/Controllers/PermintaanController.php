<?php

namespace Modules\Distribusi\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use App\Permintaan;
use App\Roles;
use App\Paket;
use App\DetailPermintaan;
use DB;
use Hash;
use Validator;
use Session;
use Redirect;
use View;

class PermintaanController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
         if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Permintaan::select(DB::raw('*, @rownum := @rownum +1 as rownum'));
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                 '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                    <li><a href="permintaan/'.$data->id.'/edit">Edit</a></li>
                                    <li><a href="permintaan/'.$data->id.'" data-id="" class="btn-delete-on-table">Delete</a></li>
                                  </ul><div>'
                        ;})
            ->make(true);
        }
        return view('distribusi::permintaan.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $paket = Paket::get();
        $paket = $paket->pluck('nama_paket', 'kode_paket');
        return view('distribusi::permintaan.create', compact('paket'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function index_download(\Illuminate\Http\Request $request)
    {
         if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Permintaan::select(DB::raw('*, @rownum := @rownum +1 as rownum'));
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "<div class=\"dropdown\"><a href='#' id='dropdownMenu".$data->id."' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-sm btn-info\"><i class='icon icon-bars'></i></a>".
                                 '<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$data->id.'">
                                    <li><a href="permintaan/download/'.$data->id.'">Download</a></li>
                                    
                                  </ul><div>'
                        ;})
            ->make(true);
        }
        return view('distribusi::download.index');
    }

    public function download($id){
        $find = Permintaan::find($id);
        return response()->download('uploadsPermintaan/'.$find->attachment_file);
    
    }


    public function store(Request $request)
    {
        $rules = array(
            'nama' => 'required|max:255',
            'nomor_permintaan' => 'required|max:255',
            'tanggal' => 'required|date|max:255',
            'attachment_file' => 'required|max:10000',
            'keterangan' => 'required|max:255',
            'detail_permintaan' => 'present|array',
        );
        $message = [
            'nama.required' => 'Harap masukkan nama',
            'nomor_permintaan.required' => 'Harap masukan nomor permintaan',
            'tanggal.required' => 'Harap masukan tanggal',
            'keterangan' => 'Harap masukan berat',
            'detail_permintaan.array' => 'Harap masukan detail barang',
            'attachment_file' => 'File tidak boleh lebih dari 10 mb',
        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('distribusi/permintaan/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $permintaan = new Permintaan;
            $data_input = $request->input();
            $data_detail = $data_input['detail_permintaan'];
            $file= Input::file('attachment_file');
            $data_input['attachment_file']=$file->getClientOriginalName();
            $file->move('uploadsPermintaan', $file->getClientOriginalName());
            $data_input['status'] = 0;

            unset($data_input['_method']);
            unset($data_input['_token']);
            unset($data_input['detail_permintaan'],$data_input['qty'],$data_input['kode_paket']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $permintaan->$key = $val;
            }
            $permintaan->save();

            //SAVE DETAIL Permintaan//
            foreach ($data_detail as $skey => $row) {
                $permintaanDetail = new DetailPermintaan;
                $permintaanDetail->rf_permintaan_id = $permintaan->id;
                foreach ($row as $skey => $val) {
                    $permintaanDetail->$skey = $val;
                }
                $permintaanDetail->save();
            }

            // redirect
            Session::flash('message', 'Data permintaan barang berhasil disimpan');
            return Redirect::to('distribusi/permintaan');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('distribusi::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $permintaan = Permintaan::find($id);
        $paket = Paket::get();
        $paket = $paket->pluck('nama_paket', 'kode_paket');

        $permintaanDetail = DetailPermintaan::where('rf_permintaan_id','=',$id)
        ->join('rf_paket','rf_paket.kode_paket','rf_permintaan_detail.kode_paket')
        ->orderBy('rf_permintaan_detail.id','ASC')
        ->select(
            'rf_paket.kode_paket',
            'rf_permintaan_detail.id',
            'rf_paket.nama_paket',
            'rf_permintaan_detail.qty'
        )
        ->get();
        
        return view('distribusi::permintaan.edit', compact('permintaan','id','permintaanDetail','paket'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
         $rules = array(
            'nama' => 'required|max:255',
            'nomor_permintaan' => 'required|max:255',
            'tanggal' => 'required|date|max:255',
            'attachment_file' => 'sometimes|max:10000',
            'keterangan' => 'required|max:255',
            'detail_permintaan' => 'present|array',
        );
        $message = [
            'nama.required' => 'Harap masukkan nama',
            'nomor_permintaan.required' => 'Harap masukan nomor permintaan',
            'tanggal.required' => 'Harap masukan tanggal',
            'keterangan' => 'Harap masukan berat',
            'detail_permintaan.array' => 'Harap masukan detail barang',
            'attachment_file' => 'File tidak boleh lebih dari 10 mb',

        ];
        $validator = Validator::make($request->all(), $rules,$message);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('distribusi/permintaan/'. $id .'/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            // update
            $permintaan = permintaan::find($id);
            $data_input = $request->input();
            $data_detail = $data_input['detail_permintaan'];
            $file= Input::file('attachment_file');
            if(!empty($file)){
                $file->move('uploadsPermintaan', $file->getClientOriginalName());
                $data_input['attachment_file']=$file->getClientOriginalName();    
            }
            
            unset($data_input['_method']);
            unset($data_input['_token']);
            unset($data_input['detail_permintaan'],$data_input['qty'],$data_input['kode_paket']);
            // make password hash for login
            foreach($data_input as $key => $val){
                $permintaan->$key = $val;
            }
            $permintaan->save();

            //DELETE OLD DATA//
            $permintaanDetailOLD = DetailPermintaan::where('rf_permintaan_id','=',$id);
            $permintaanDetailOLD->delete();
            ///////////////////

            foreach ($data_detail as $skey => $row) {
                $permintaanDetail = new DetailPermintaan;
                $permintaanDetail->rf_permintaan_id = $permintaan->id;
                foreach ($row as $skey => $val) {
                    $permintaanDetail->$skey = $val;
                }
                $permintaanDetail->save();
            }

            // redirect
            Session::flash('message', 'Data penerima barang berhasil diupdate');
            return Redirect::to('distribusi/permintaan');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $permintaan = Permintaan::find($id);
        
        if($permintaan->delete()){
            //DELETE OLD DATA//
            $permintaanDetailOLD = DetailPermintaan::where('rf_permintaan_id','=',$id);
            $permintaanDetailOLD->delete();
            ///////////////////
            return response()
            ->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()
            ->json(['code' => '50', 'message' => 'Data gagal dihapus']);
        }
    }
}
