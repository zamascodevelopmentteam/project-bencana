<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penguatan extends Model
{
    protected $table = 'rf_penguatan';
    protected $fillable=[
    	 'bagian',
    	 'kajian',
    	 'nomor_dokumen',
    	 'attachment_file'
    ];
}
