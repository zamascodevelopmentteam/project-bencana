<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailKontrak extends Model
{
    protected $table = 'rf_detail_kontrak';
    protected $fillable=[
    	 'rf_kontrak_id',
    	 'kode_paket',
    	 'qty',
    ];
}
