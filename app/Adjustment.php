<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adjustment extends Model
{
    Protected $table = 'rf_adjustment_stok';
    Protected $fillable =[
    	'kode_paket',
    	'penerimaan',
    	'pengeluaran',
    	'saldo',
    	'stock_fisik',
    	'selisih',
    	'adjustment'
    ];
}
