<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengiriman extends Model
{
    protected $table = 'rf_pengiriman';
    protected $fillable=[
    	 'rf_permintaan_id',
    	 'tanggal_kirim',
    	 'nomor_pengiriman',
    	 'status'
    ];
}
