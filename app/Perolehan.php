<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perolehan extends Model
{
    Protected $table = 'rf_asal_perolehan';
    Protected $fillable =[
    	'nama_asal_perolehan',
    	'unsur',
    ];
}
