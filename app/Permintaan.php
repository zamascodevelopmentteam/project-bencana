<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permintaan extends Model
{
    protected $table = 'rf_permintaan';
    protected $fillable=[
    	'nama',
    	'nomor_permintaan',
        'tanggal',
        'attachment_file',
        'keterangan', 
        'status'
    ];
}
