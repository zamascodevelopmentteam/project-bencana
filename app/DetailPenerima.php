<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPenerima extends Model
{
    protected $table = 'tr_penerimaan_detail';
    protected $fillable=[
    	 'tr_penerimaan_id',
    	 'kode_paket',
    	 'qty',
    	 'sumber',
    	 'keterangan',
    	 'created_at',
    ];

}
