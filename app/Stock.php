<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    Protected $table = 'rf_stock_opname';
    Protected $fillable =[
    	'kode_paket',
    	'saldo_awal',
    	'penerimaan',
    	'pengeluaran',
    	'stock_system',
        'baik',
        'kurang_baik',
        'rusak',
        'hilang',
    	'stock_fisik',
    	'selisih',
        'status_adjustment'
    ];
}
