<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kontrak extends Model
{
   	protected $table = 'rf_kontrak';

    protected $fillable = [
    	'vendor',
    	'nomor_kontrak',
    	'nama_pekerjaan',
    	'jangka_waktu',
    	'tanggal_kontrak',
        'nilai_kontrak',
        'attachment_file',
    ];

}
