<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Auth;
use App\Modules as ModulesDB;
use App\ModulePermissions;
use App\SubModules;
use App\SubModulePermissions;
use Module;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $this->checkModuleAccess();
        require base_path() . '/app/Helpers/Macros.php';
    }
    
    function checkModuleAccess()
    {
        view()->composer('*', function ($view) 
        {
            $requestAction = app('request')->route()->action;
            $prefix = $requestAction['prefix'];
            $prefix = str_replace("/","",$prefix);
            if(!empty($prefix)){
                if(empty(Auth::user()->role_id)){
                    return redirect('login')->send();
                }
                $role_id  = Auth::user()->role_id;
                $module = config($prefix.'.name');
                //check module available on database//
                $getModule = ModulesDB::where('name','=',$module)->first();
                if(empty($getModule)){
                    //redirect if not module not available//
                    return redirect('/not_authorize'.$module)->send();
                }else{
                    //check permission of module//
                    $getModulePermission = ModulePermissions::where('role_id','=',$role_id)
                        ->where('module_id','=',$getModule->id)->where('allow','=','1')->first();
                    if(empty($getModulePermission)){
                        //redirect if module not allowed//
                        return redirect('/not_authorize')->send();
                    }else{
                        //check sub module with controller//
                        $action = app('request')->route()->getAction();
                        $controller = class_basename($action['controller']);
                        list($controller, $action) = explode('@', $controller);
                        $controller = str_replace("Controller","",$controller);
                        $controller = preg_replace('/(?<!\ )[A-Z]/', ' $0', $controller);
                        $controller = strtolower($controller);
                        $getSubModule = SubModules::where('name','=',trim($controller))->where('module_id','=',$getModule->id)->first();
                         if(empty($getSubModule)){
                             //redirect if sub module not available//
                            return redirect('/not_authorize')->send();
                        }else{
                             //check permission of sub module//
                            $getSubModulePermission = SubModulePermissions::where('role_id','=',$role_id)->where('sub_module_id','=',$getSubModule->id)->where('allow','=','1')->first();
                             if(empty($getSubModulePermission)){
                                //redirect if sub module not allowed//
                                return redirect("/".strtolower($module).'/not_authorize')->send();
                            }else{
                                $subModules = SubModules::where('module_id','=',$getModule->id)->where('sub_module_id','=','0')->get();
                                foreach($subModules as $key=>$subModule){
                                    $subModule->name = str_replace(" ","-",$subModule->name);
                                    $child = SubModules::where('module_id','=',$getModule->id)->where('sub_module_id','=',$subModule->id);
                                    $subModule->countChild = $child->count();
                                    $subModule->child = $child->get();
                                    $modulePermission = SubModulePermissions::where('role_id','=',$role_id)->where('sub_module_id','=',$subModule->id);
                                    if($modulePermission->count()){
                                        $modulePermission = $modulePermission->first();
                                        $subModule->allow = $modulePermission->allow;
                                    }else{
                                        $subModule->allow = false;
                                    }
                                    foreach($subModule->child as $skey => $schild){
                                        $schild->name = str_replace(" ","-",$schild->name);
                                        $modulePermission = SubModulePermissions::where('role_id','=',$role_id)->where('sub_module_id','=',$schild->id);
                                        if($modulePermission->count()){
                                            $modulePermission = $modulePermission->first();
                                            $schild->allow = $modulePermission->allow;
                                        }else{
                                            $schild->allow = false;
                                        }
                                    }
                                }
                                $view->with('listSubModules', $subModules)->with('prefix',$prefix)->with('moduleActive',$getModule);
                            }
                         }
                    }
                    //end of check permission module//
                }
                //end check module available on database//   
            }else{
                if(!Auth::guest()){
                    $role_id  = Auth::user()->role_id;
                    $getModulePermission = ModulePermissions::where('role_id','=',$role_id)->where('allow','=','1')
                    ->join('modules', 'modules.id' , '=', 'module_permissions.module_id')->get();
                    foreach($getModulePermission as $gm){
                        $subModuleAllow = SubModules::where('sub_modules.module_id','=',$gm->id)->where('sub_module_permissions.allow','=',1)
                            ->select('sub_modules.*')
                            ->join('sub_module_permissions','sub_module_permissions.sub_module_id','=','sub_modules.id')
                            ->first();
                        if(!empty($subModuleAllow)){
                            $child = $this->checkChild($subModuleAllow);
                            $gm->child = $child;
                        }else{
                            $gm->child = null;
                        }
                        
                    }
                    $view->with('listModules', $getModulePermission);
                    //dd($getModulePermission);
                }
                
                //dd($getModulePermission);
            }
        });  
    }
    
    public function checkChild($data)
    {
        $subModuleAllow = SubModules::where('sub_modules.sub_module_id','=',$data->id);
        if(!empty($subModuleAllow->count())){
            //dd($subModuleAllow->first());
            return $this->checkChild($subModuleAllow->first());
        }else{
            return $data;
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
