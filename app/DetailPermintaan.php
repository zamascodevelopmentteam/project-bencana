<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPermintaan extends Model
{
    protected $table = 'rf_permintaan_detail';
    protected $fillable=[
    	 'rf_permintaan_id',
    	 'kode_paket',
    	 'qty',
    ];

}
