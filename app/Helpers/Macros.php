<?php
\HTML::macro('activeMenu', function($mainUrl, $child = null) {
    $path  = request()->url();
    $url   = url($mainUrl);
    if ($child == null) {
        $class = (strpos($path, $url) !== false)  ? 'active' : '';
        return $class;
    } else {
        foreach ($child as $key=>$child) {
            if ((strpos($path, $url."/".$child->name) !== false)) {
                $class = 'active';
                return $class;
                break;
            }
        }
    }
});