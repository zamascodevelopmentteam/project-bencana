<?php


function Romawi($n){
    $hasil = "";
    $iromawi = array("","I","II","III","IV","V","VI","VII","VIII","IX","X",20=>"XX",30=>"XXX",40=>"XL",50=>"L",
    60=>"LX",70=>"LXX",80=>"LXXX",90=>"XC",100=>"C",200=>"CC",300=>"CCC",400=>"CD",500=>"D",600=>"DC",700=>"DCC",
    800=>"DCCC",900=>"CM",1000=>"M",2000=>"MM",3000=>"MMM");
    if(array_key_exists($n,$iromawi)){
        $hasil = $iromawi[$n];
    }elseif($n >= 11 && $n <= 99){
        $i = $n % 10;
        $hasil = $iromawi[$n-$i] . Romawi($n % 10);
    }elseif($n >= 101 && $n <= 999){
        $i = $n % 100;
        $hasil = $iromawi[$n-$i] . Romawi($n % 100);
    }else{
        $i = $n % 1000;
        $hasil = $iromawi[$n-$i] . Romawi($n % 1000);
    }
    return $hasil;
}

function bulan()
{
    $bulan = array (1 =>   'Januari',
				'Februari',
				'Maret',
				'April',
				'Mei',
				'Juni',
				'Juli',
				'Agustus',
				'September',
				'Oktober',
				'November',
				'Desember'
			);
    return $bulan;
}

function tanggal_indo($tanggal,$time = false)
{
	$bulan = bulan();
    if($time == false){
        $split = explode('-', $tanggal);
        return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
    }else{
        $datepick = explode(" ", $tanggal);
        $split = explode("-", $datepick[0]);
        return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0]. ' ' . $datepick[1];
    }
	
	
}

function periode_tanggal_indo($periode)
{
    $periode_ex = explode(" - ",$periode);
    if(count($periode_ex) == 2){
        $date_start = tanggal_indo($periode_ex[0]);
        $date_end = tanggal_indo($periode_ex[1]);
        return $date_start . ' - '. $date_end;
    }else{
        return null;
    }
}

function getNomorJurnal($tanggal)
{
    $explodeDate = explode("-",$tanggal);
    $tahun         = $explodeDate[2];
    $bulan         = $explodeDate[1];
    
    $formula = "GL/EPS/".romawi($bulan)."/".$tahun."/";
    $getLastJurnalNumber = \App\GLJurnal::where('nomor_jurnal','LIKE',$formula."%")
        ->orderBy('nomor_jurnal','DESC')
        ->groupBy('nomor_jurnal')
        ->first();
    if(empty($getLastJurnalNumber)){
        $nomor_gl = $formula."1";
    }else{
        //dd($getLastJurnalNumber);
        $explodeNomor = explode("/",$getLastJurnalNumber->nomor_jurnal);
        $count = count($explodeNomor);
        $last = $explodeNomor[$count - 1] + 1;
        $nomor_gl = $formula.$last;
    }
    return $nomor_gl;
}

function formatIdr($idr)
{
    return number_format($idr,2);
}

function onlyNumber($idr)
{
    return number_format($idr,0);
}
