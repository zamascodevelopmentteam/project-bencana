<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPengiriman extends Model
{
	protected $table = 'rf_pengiriman_detail';
    protected $fillable=[
    	 'kode_paket',
    	 'rf_pengiriman_id',
    	 'qty_kirim',
    ];
}
