<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bencana extends Model
{
    protected $table='rf_jenis_bencana';
    protected $fillable=[
    	'kd_bencana',
    	'ur_bencana',
    ];
}
