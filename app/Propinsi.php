<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Propinsi extends Model
{
    Protected $table = 'provinces';
    Protected $fillable =[
    	'kode_paket',
    	'nama_propinsi',
    ];

}
