<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    Protected $table = 'rf_isi_paket';
    Protected $fillable =[
    	'kode_paket',
    	'kode_isi_paket',
    	'ur_isi_paket',
    	'satuan'
    ];//
}
