<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $table = 'rf_vendor';

    protected $fillable = [
    	'nama_vendor',
    	'kode_propinsi',
    	'kode_kabupaten',
    	'alamat',
        'telepon',
        'pic', 
    ];
}
