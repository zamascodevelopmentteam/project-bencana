<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
    Protected $table = 'districts';
    Protected $fillable =[
    	'kode_propinsi',
    	'kode_kabupaten',
    	'nama_kabupaten'
    ];
}
