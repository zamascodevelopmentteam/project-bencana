<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelompok extends Model
{
    Protected $table = 'rf_kelompok_barang';
    Protected $fillable =[
    	'ur_kelompok_barang',
    ];
}
