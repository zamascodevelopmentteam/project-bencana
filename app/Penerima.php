<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penerima extends Model
{
    protected $table='tr_penerimaan';
    protected $fillable=[
    	'type',
        'rf_vendor_id',
    	'rf_kontrak_id',
    	'nomor_penerimaan',
    	'rf_gudang_id',
        'tanggal_penerimaan',
    	'jam_datang',
    	'jam_bongkar',
    	'jenis_kendaraan',
    	'no_kendaraan',	
        'no_tally_sheet',
    	'pengemudi',
    	'no_hp_pengemudi',
        'nama_penyedia',
        'penanggung_biaya_handling',
        'tahun_pengadaan',
        'nama_pejabat',
    ];

}
