<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModulePermissions extends Model
{
    protected $table ='module_permissions';
    
    public function module()
    {
        return $this->belongsTo('App\Modules', 'module_id');
    }
}
