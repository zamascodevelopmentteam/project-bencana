<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Darurat extends Model
{
    protected $table = 'rf_darurat';
    protected $fillable=[
    	 'bagian',
    	 'kajian',
    	 'nomer_dokumen',
    	 'attachment_file'
    ];

}
