<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubModulePermissions extends Model
{
    protected $table ='sub_module_permissions';
}
