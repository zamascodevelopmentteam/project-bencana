<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gudang extends Model
{
	protected $table = 'rf_gudang';

    protected $fillable = [
    	'nama_gudang',
    	'kode_propinsi',
    	'kode_kabupaten',
    	'alamat',
        'email',
        'telepon',
        'pic',
        'latitude',
        'longitude' 
    ];
}
