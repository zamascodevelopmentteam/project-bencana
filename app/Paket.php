<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class paket extends Model
{
    protected $table = 'rf_paket';

    protected $fillable = [
    	'nama_paket',
    	'kode_paket',
    	'isi_per_box',
    	'satuan',
    	'berat',
    	'berat_per_box',
    	'volume',
    	'panjang',
    	'lebar',
    	'tinggi',
        'berat_gram',
        'berat_per_box_gram',
        'volume_cm',
        'panjang_cm',
        'lebar_cm',
        'tinggi_cm',
    	'rf_kelompok_barang_id',
        'rf_satuan_ukuran_id',
        'rf_satuan_id',
    	'harga_total',
    ];
}
