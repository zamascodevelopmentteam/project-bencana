<?php

use Illuminate\Database\Seeder;

class HumanResourcesSubModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	DB::insert("INSERT INTO `sub_modules` (`id`, `module_id`, `sub_module_id`, `name`, `display_name`, `description`, `icon`, `created_at`, `updated_at`) VALUES
		(12, 2, 0, 'master', 'Master Data', '', 'fa fa-book', '2017-02-24 02:31:41', null),
		(13, 2, 12, 'pegawai', 'Pegawai', '', 'fa fa-database', '2017-02-24 02:31:41', null),
		(14, 2, 12, 'jabatan', 'Jabatan', '', 'fa fa-group', '2017-02-24 02:33:58', null),
		(15, 2, 12, 'bagian', 'Bagian', '', 'fa fa-dashboard', '2017-02-28 09:48:09', null),
		(16, 2, 12, 'agama', 'Agama', '', 'fa fa-dashboard', '2017-03-02 08:18:04', null),
		(17, 2, 12, 'pendidikan', 'Pendidikan', '', 'fa fa-dashboard', '2017-03-02 08:35:53', null),
		(18, 2, 12, 'perhitungan', 'Perhitungan', '', 'fa fa-dashboard', '2017-03-02 08:35:57', null)
		");



    	DB::insert("INSERT INTO `sub_module_permissions` (`id`, `role_id`, `module_id`, `sub_module_id`, `allow`, `created_at`, `updated_at`) VALUES
			(11, 1, 2, 12, 1, '2017-02-28 00:03:07', '2017-02-28 00:03:07'),
			(12, 1, 2, 13, 1, '2017-02-28 00:03:07', '2017-02-28 00:03:07'),
			(13, 1, 2, 14, 1, '2017-02-28 00:03:07', '2017-02-28 00:03:07'),
			(14, 1, 2, 15, 1, '2017-02-28 00:03:07', '2017-02-28 00:03:07'),
			(15, 1, 2, 16, 1, '2017-02-28 02:51:20', '2017-02-28 02:51:20'),
			(16, 1, 2, 17, 1, '2017-03-02 01:18:59', '2017-03-02 01:18:59'),
			(17, 1, 2, 18, 1, '2017-03-02 01:36:39', '2017-03-02 01:36:39')");
    }
}
