<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <meta name="theme-color" content="#ffffff">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="{{url('assets/css/vendor.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/elephant.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/application.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/custom.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/vegas.min.css') }}">
    <link href="{{url('assets/css/select2.min.css')}}" rel="stylesheet" />

  </head>
      <body class="main-navigation">

        <div id="wrapper">
            <div id="page-wrapper" class="gray-bg">
                <div class="border-bottom white-bg">
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                            <i class="fa fa-reorder"></i>
                        </button>
                        <a href="#" class="navbar-brand">SYSTEM INVENTORY BNPB</a>
                        </div>
                        <div class="navbar-collapse collapse" id="navbar">
                            <ul class="nav navbar-nav navbar-right">
                                @if (!Auth::guest())
                                    <li>
                                        <a href="{{ route('logout') }}"  onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                            <i class="icon icon-sign-out"></i> Log out
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                   </li>
                                @endif
                            </ul>
                        </div>
                    </nav>
                </div>
                <div class="wrapper wrapper-content">
                    @yield('content')
                </div>
            </div>
        </div>

        <script src="{{url('assets/js/vendor.min.js')}}"></script>
        <script src="{{url('assets/js/elephant.min.js')}}"></script>
        <script src="{{url('assets/js/application.min.js')}}"></script>
        <script src="{{url('assets/js/demo.min.js')}}"></script>
        <script src="{{url('assets/js/vegas.min.js')}}"></script>
        <script src="{{url('assets/js/select2.min.js')}}"></script>

        <script type="text/javascript">
       
        var table = $("#table-datatable");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function(){
            $('body').vegas({
                slides: [
                    { src: '{{URL::asset("assets/img/vegas/bg/bnpb/bnpbpic.jpg")}}' },
                    { src: '{{URL::asset("assets/img/vegas/bg/bnpb/IMG_5030.jpg")}}' },
                    { src: '{{URL::asset("assets/img/vegas/bg/bnpb/web-IMG_0020.jpg")}}' },
                    
                ],
                /*overlay: true,*/
                overlay: "{{url('assets/img/vegas/overlays/07.png')}}",
            });
        });
        </script>
            @yield('scriptBlock')
        <script>
        $("tbody").on("click",".btn-delete-on-table",function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            $.ajax({
                url : url,
                type : "DELETE",
                dataType : "json",
                beforeSend : function(e){
                toastr.clear();
                toastr["info"]("Processing")
                },
                success: function(result){
                    toastr.clear();
                    if(result.code == 200){
                    toastr["success"](result.message)
                    dataTable.ajax.reload();
                    }else{
                    toastr["error"](result.message)
                    }
                }
            })
        })

        </script>
  </body>
</html>