
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SYTEM INVENTORY BNPB</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#d9230f">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link href="{{url('assets/css/select2.min.css')}}" rel="stylesheet" />
    <link href="{{url('assets/css/bootstrap-datepicker.css')}}" rel="stylesheet" />
    <link href="{{url('assets/css/bootstrap-datetimepicker.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{url('assets/css/vendor.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/elephant.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/application.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/sweetalert.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/custom.min.css')}}">
{{--     <link rel="stylesheet" href="{{url('assets/css/demo.min.css')}}">
 --}}


  </head>
  <body class="layout layout-header-fixed">
    <div class="layout-header">
      <div class="navbar navbar-default">
        <div class="navbar-header">
          <a class="navbar-brand navbar-brand-center" href="#">
            SYSTEM INVENTORY BNPB
          </a>
          <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#sidenav">
            <span class="sr-only">Toggle navigation</span>
            <span class="bars">
              <span class="bar-line bar-line-1 out"></span>
              <span class="bar-line bar-line-2 out"></span>
              <span class="bar-line bar-line-3 out"></span>
            </span>
            <span class="bars bars-x">
              <span class="bar-line bar-line-4"></span>
              <span class="bar-line bar-line-5"></span>
            </span>
          </button>
          <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="arrow-up"></span>
            <span class="ellipsis ellipsis-vertical">
              <img class="ellipsis-object" width="32" height="32" src="{{url('assets/img/avatar.png')}}" alt="AVATAR">
            </span>
          </button>
        </div>
        <div class="navbar-toggleable">
          <nav id="navbar" class="navbar-collapse collapse">
            <button class="sidenav-toggler hidden-xs" title="Collapse sidenav ( [ )" aria-expanded="true" type="button">
              <span class="sr-only">Toggle navigation</span>
              <span class="bars">
                <span class="bar-line bar-line-1 out"></span>
                <span class="bar-line bar-line-2 out"></span>
                <span class="bar-line bar-line-3 out"></span>
                <span class="bar-line bar-line-4 in"></span>
                <span class="bar-line bar-line-5 in"></span>
                <span class="bar-line bar-line-6 in"></span>
              </span>
            </button>
            <ul class="nav navbar-nav navbar-right">
              <li class="visible-xs-block">
                <h4 class="navbar-text text-center">Hi, {{ Auth::user()->name }} </h4>
              </li>
              <li class="dropdown hidden-xs">
                <button class="navbar-account-btn" data-toggle="dropdown" aria-haspopup="true">
                  <img class="rounded" width="36" height="36" src="{{url('assets/img/avatar.png')}}" alt="{{ Auth::user()->name }} "> {{ Auth::user()->name }} 
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                  <li><a href="{{ url('/') }}">Main Menu</a></li>
                  <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">Sign out</a>
                                               <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form></li>
                </ul>
              </li>
              <li class="visible-xs-block">
                <a href="{{ url('/') }}">
                  <span class="icon icon-arrow-left"></span>
                  Main Menu
                </a>
              </li>
              <li class="visible-xs-block">
                <a href="{{ route('logout') }}">
                  <span class="icon icon-power-off icon-lg icon-fw"></span>
                  Sign out
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
    <div class="layout-main">
      <div class="layout-sidebar">
        <div class="layout-sidebar-backdrop"></div>
        <div class="layout-sidebar-body">
          <div class="custom-scrollbar">
            <nav id="sidenav" class="sidenav-collapse collapse">
              <ul class="sidenav">
                <li class="sidenav-profile">
                    <h3 class="module-name">Module : {{$moduleActive->display_name}}<small>{{$moduleActive->description}}</small></h3>
                </li>
                <li class="sidenav-heading">Navigation</li>
                @foreach($listSubModules as $key=>$subModules)
                    @if($subModules->allow)
                        @if($subModules->countChild == 0)
                            <li class="sidenav-item {{ HTML::activeMenu($prefix.'/'.$subModules->name) }}">
                                <a href="{{url($prefix.'/'.$subModules->name)}}">
                                  <span class="sidenav-icon {{$subModules->icon}}"></span> 
                                  <span class="sidenav-label">{{$subModules->display_name}}</span>
                                </a>
                            </li>
                        @else
                            <li class="sidenav-item has-subnav {{ HTML::activeMenu($prefix, $subModules->child) }}">
                                <a href="#" aria-haspopup="true">
                                    <span class="sidenav-icon icon {{$subModules->icon}}"></span>
                                    <span class="sidenav-label">{{$subModules->display_name}}</span>
                                </a>
                                <ul class="sidenav-subnav collapse">
                                    <li class="sidenav-subheading">{{$subModules->display_name}}</li>
                                    @foreach($subModules->child as $key=>$child)
                                        @if($child->allow)
                                            <li class="{{ HTML::activeMenu($prefix.'/'.$child->name) }}">
                                                <a href="{{url($prefix.'/'.$child->name)}}">{{$child->display_name}}</a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                        @endif
                    @endif
                @endforeach
              </ul>
            </nav>
          </div>
        </div>
      </div>
      <div class="layout-content">
        <div class="layout-content-body">
          @yield('content')
        </div>
      </div>
      <div class="layout-footer">
        <div class="layout-footer-body">
          <small class="version">Version 1.0</small>
          <small class="copyright">2017 &copy; Zamasco Mitra Solusindo</small>
        </div>
      </div>
    </div>
    <div class="theme">
      <div class="theme-panel theme-panel-collapsed">
        <div class="theme-panel-controls">
          <button class="theme-panel-toggler" title="Expand theme panel ( ] )" type="button">
            <span class="icon icon-cog icon-spin" aria-hidden="true"></span>
          </button>
        </div>
        <div class="theme-panel-body">
          <div class="custom-scrollbar">
            <div class="custom-scrollbar-inner">
              <h5 class="theme-heading">
                
              </h5>
              <ul class="theme-settings">
                <li class="theme-settings-heading">
                  <div class="divider">
                    <div class="divider-content">Theme Settings</div>
                  </div>
                </li>
                <li class="theme-settings-item">
                  <div class="theme-settings-label">Header fixed</div>
                  <div class="theme-settings-switch">
                    <label class="switch switch-primary">
                      <input class="switch-input" type="checkbox" name="layout-header-fixed" data-sync="true">
                      <span class="switch-track"></span>
                      <span class="switch-thumb"></span>
                    </label>
                  </div>
                </li>
                <li class="theme-settings-item">
                  <div class="theme-settings-label">Sidebar fixed</div>
                  <div class="theme-settings-switch">
                    <label class="switch switch-primary">
                      <input class="switch-input" type="checkbox" name="layout-sidebar-fixed" data-sync="true">
                      <span class="switch-track"></span>
                      <span class="switch-thumb"></span>
                    </label>
                  </div>
                </li>
                <li class="theme-settings-item">
                  <div class="theme-settings-label">Sidebar sticky*</div>
                  <div class="theme-settings-switch">
                    <label class="switch switch-primary">
                      <input class="switch-input" type="checkbox" name="layout-sidebar-sticky" data-sync="true">
                      <span class="switch-track"></span>
                      <span class="switch-thumb"></span>
                    </label>
                  </div>
                </li>
                <li class="theme-settings-item">
                  <div class="theme-settings-label">Sidebar collapsed</div>
                  <div class="theme-settings-switch">
                    <label class="switch switch-primary">
                      <input class="switch-input" type="checkbox" name="layout-sidebar-collapsed" data-sync="false">
                      <span class="switch-track"></span>
                      <span class="switch-thumb"></span>
                    </label>
                  </div>
                </li>
                <li class="theme-settings-item">
                  <div class="theme-settings-label">Footer fixed</div>
                  <div class="theme-settings-switch">
                    <label class="switch switch-primary">
                      <input class="switch-input" type="checkbox" name="layout-footer-fixed" data-sync="true">
                      <span class="switch-track"></span>
                      <span class="switch-thumb"></span>
                    </label>
                  </div>
                </li>
              </ul>
              <hr class="theme-divider">
              <ul class="theme-variants">
              </ul>
            </div>
          </div>
        </div>
        <div class="theme-panel-footer">
        </div>
      </div>
    </div>
    <script src="{{url('assets/js/vendor.min.js')}}"></script>
    <script src="{{url('assets/js/bootstrap-datepicker.js')}}"></script> 
    <script src="{{url('assets/js/bootstrap-datetimepicker.js')}}"></script>
    <script src="{{url('assets/js/jquery.number.min.js')}}"></script>
    <script src="{{url('assets/js/elephant.min.js')}}"></script>
    <script src="{{url('assets/js/application.min.js')}}"></script>
    <script src="{{url('assets/js/sweetalert.min.js')}}"></script>
    <script src="{{url('assets/js/demo.min.js')}}"></script>
    <script src="{{url('assets/js/select2.min.js')}}"></script>
    

    <script type="text/javascript">
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });

      $('#select').select2();
      $('.onlyNumber').number(true,0);
      $('.onlyNumberComma').number(true,2);

    </script>
    @yield('scriptBlock')
    <script type="text/javascript">
    $(document).ready(function(){
            $('.users').select2();
    });
    $(".datepick").datepicker({
      "format" : "yyyy-mm-dd",
      autoclose : true,
      todayBtn : true,
      todayHighlight : true
    })
      function removeComa(value)
          {
              var x = parseFloat(value.replace(/,/g, '')) ;
              return x * 1;
          }
          $('.table-responsive').on('show.bs.dropdown', function () {
                 $('.table-responsive').css( "overflow", "inherit" );
            });

            $('.table-responsive').on('hide.bs.dropdown', function () {
                 $('.table-responsive').css( "overflow", "auto" );
            })
           $("body").on("click",".btn-delete-on-table",function(e){
                e.preventDefault();
                var thisUrl = $(this).attr("href");
                swal({
                        title: "Delete Record",
                        text: "Are you sure want to delete this record?",
                        type: "info",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true
                    }, function () {
                        $.ajax({
                            url : thisUrl,
                            type : "delete",
                            dataType : "json",
                            beforeSend : function(){
                                
                            },
                            success : function(result){
                                if(result.code == 200){
                                    swal("Success", "Record has been deleted","success");
                                    dataTable.ajax.reload();
                                }else if(result.code == 99){
                                    swal("Ooopp!!!", result.message,"error");
                                }else{
                                    swal("Failed to deleted record, please try again");
                                }
                            },
                            error : function()
                            {
                                swal("Failed to deleted record, please try again");
                            }
                        })
                    });
            });

      
    </script>
  </body>

</html>