    <html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>@yield('title')</title>
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
        <meta name="theme-color" content="#ffffff">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
        <link rel="stylesheet" href="{{url('assets/css/vendor.min.css')}}">
        <link rel="stylesheet" href="{{url('assets/css/elephant.min.css')}}">
        <link rel="stylesheet" href="{{url('assets/css/login-3.min.css')}}">
        <link rel="stylesheet" href="{{url('assets/css/vegas.min.css') }}">
    </head>
    <body>

        <div class="login">
          <div class="login-body">
            <a class="login-brand" href="index-2.html">
              <img class="img-responsive" src="{{url('assets/img/Logo-BNPB_new.jpg')}}" alt="Elephant">
            </a>
            <h3 class="login-heading">Sign in</h3>
            <div class="login-form">
              <form data-toggle="md-validator" role="form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @if(!empty($errors->all()))
                    <div class="alert alert-danger">
                    {{ HTML::ul($errors->all()) }}
                    </div>
                @endif
                <div class="md-form-group md-label-floating">
                  <input class="md-form-control" type="email" name="email" spellcheck="false" autocomplete="off" data-msg-required="Please enter your email address." required>
                  <label class="md-control-label">Email</label>
                </div>
                <div class="md-form-group md-label-floating">
                  <input class="md-form-control" type="password" name="password" minlength="6" data-msg-minlength="Password must be 6 characters or more." data-msg-required="Please enter your password." required>
                  <label class="md-control-label">Password</label>
                </div>
                <div class="md-form-group md-custom-controls">
                  <label class="custom-control custom-control-primary custom-checkbox">
                    <input class="custom-control-input" type="checkbox" name="remember" checked="checked">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-label">Keep me signed in</span>
                  </label>
                  <span aria-hidden="true"> · </span>
                </div>
                <button class="btn btn-primary btn-block" type="submit">Sign in</button>
              </form>
            </div>
          </div>
        </div>

        <script src="{{url('assets/js/vendor.min.js')}}"></script>
        <script src="{{url('assets/js/elephant.min.js')}}"></script>
        <script src="{{url('assets/js/vegas.min.js')}}"></script>

        <script>
        $(function(){
            $('body').vegas({
                slides: [
                    { src: '{{URL::asset("assets/img/vegas/bg/bnpb/bnpbpic.jpg")}}' },
                    { src: '{{URL::asset("assets/img/vegas/bg/bnpb/IMG_5030.jpg")}}' },
                    { src: '{{URL::asset("assets/img/vegas/bg/bnpb/web-IMG_0020.jpg")}}' },
                    
                ],
                /*overlay: true,*/
                overlay: "{{url('assets/img/vegas/overlays/07.png')}}",
            });
        });
        </script>

    </body>
</html>