@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @foreach($listModules as $key=>$moduleAllowed)
        <div class="col-md-4" style="z-index: 2">
            @if(!empty($moduleAllowed->child))
            <a href="{{url(strtolower($moduleAllowed->name).'/'.$moduleAllowed->child->name)}}" class="widget widget-menu">
            @else
                <a href="{{url(strtolower($moduleAllowed->name).'/')}}" class="widget widget-menu">
            @endif
                <!-- <img src="{{url('assets/img/menus/'.$moduleAllowed->img)}}"> -->
                <div class="icon-name">
                    <i class="{{$moduleAllowed->img}}"></i>
                </div>
                <div class="app-name">
                    <h2 class="font-bold">{{$moduleAllowed->display_name}}</h2>
                    <p>{{$moduleAllowed->description}}</p>
                </div>
            </a>
        </div>
        @endforeach
    </div>
</div>
@endsection
