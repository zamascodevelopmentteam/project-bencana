-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2017 at 03:39 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `logistik-bnpb`
--

-- --------------------------------------------------------

--
-- Table structure for table `rf_gudang`
--

CREATE TABLE `rf_gudang` (
  `id` int(11) NOT NULL,
  `nama_gudang` varchar(255) DEFAULT NULL,
  `kode_propinsi` varchar(15) NOT NULL,
  `kode_kabupaten` varchar(15) NOT NULL,
  `alamat` text,
  `email` varchar(255) DEFAULT NULL,
  `telepon` varchar(255) DEFAULT NULL,
  `pic` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rf_gudang`
--

INSERT INTO `rf_gudang` (`id`, `nama_gudang`, `kode_propinsi`, `kode_kabupaten`, `alamat`, `email`, `telepon`, `pic`, `latitude`, `longitude`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(18, 'Gudang-1', '1700000000', '1701000000', 'JL. Bengkulu', 'Bengkulu@gmail.com', '08787302980', 'Bani', '09099.12312', '090913.12132', '2017-09-15 15:59:53', NULL, '2017-09-15 08:59:53', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rf_gudang`
--
ALTER TABLE `rf_gudang`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rf_gudang`
--
ALTER TABLE `rf_gudang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
