-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 08 Bulan Mei 2018 pada 04.30
-- Versi server: 5.7.19
-- Versi PHP: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zmc_app_persediaan`
--

DELIMITER $$
--
-- Fungsi
--
CREATE DEFINER=`root`@`localhost` FUNCTION `SPLIT_STRING` (`str` VARCHAR(255), `delim` VARCHAR(12), `pos` INT) RETURNS VARCHAR(255) CHARSET latin1 RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(str, delim, pos),
       LENGTH(SUBSTRING_INDEX(str, delim, pos-1)) + 1),
       delim, '')$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `acl_phinxlog`
--

CREATE TABLE `acl_phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `acl_phinxlog`
--

INSERT INTO `acl_phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
(20141229162641, 'CakePhpDbAcl', '2018-02-01 03:04:00', '2018-02-01 03:04:01', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `acos`
--

CREATE TABLE `acos` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(11) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `sort` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `acos`
--

INSERT INTO `acos` (`id`, `parent_id`, `name`, `model`, `foreign_key`, `alias`, `lft`, `rght`, `status`, `sort`) VALUES
(1, NULL, 'controllers', NULL, NULL, 'controllers', 1, 330, 0, 0),
(2, 1, 'Error', NULL, NULL, 'Error', 2, 3, 1, 0),
(3, 1, 'Pages', NULL, NULL, 'Pages', 4, 15, 1, 0),
(19, 1, 'Acl', NULL, NULL, 'Acl', 16, 17, 1, 0),
(20, 1, 'Bake', NULL, NULL, 'Bake', 18, 19, 1, 0),
(21, 1, 'DebugKit', NULL, NULL, 'DebugKit', 20, 47, 1, 0),
(22, 21, 'Composer', NULL, NULL, 'Composer', 21, 24, 1, 0),
(23, 22, 'checkDependencies', NULL, NULL, 'checkDependencies', 22, 23, 1, 0),
(24, 21, 'MailPreview', NULL, NULL, 'MailPreview', 25, 32, 1, 0),
(25, 24, 'index', NULL, NULL, 'index', 26, 27, 1, 0),
(26, 24, 'sent', NULL, NULL, 'sent', 28, 29, 1, 0),
(27, 24, 'email', NULL, NULL, 'email', 30, 31, 1, 0),
(28, 21, 'Panels', NULL, NULL, 'Panels', 33, 38, 1, 0),
(29, 28, 'index', NULL, NULL, 'index', 34, 35, 1, 0),
(30, 28, 'view', NULL, NULL, 'view', 36, 37, 1, 4),
(31, 21, 'Requests', NULL, NULL, 'Requests', 39, 42, 1, 0),
(32, 31, 'view', NULL, NULL, 'view', 40, 41, 1, 4),
(33, 21, 'Toolbar', NULL, NULL, 'Toolbar', 43, 46, 1, 0),
(34, 33, 'clearCache', NULL, NULL, 'clearCache', 44, 45, 1, 0),
(35, 1, 'Migrations', NULL, NULL, 'Migrations', 48, 49, 1, 0),
(56, 1, 'AuditStash', NULL, NULL, 'AuditStash', 50, 51, 1, 0),
(59, 1, 'Josegonzalez\\Upload', NULL, NULL, 'Josegonzalez\\Upload', 52, 53, 1, 0),
(60, 1, 'AppSettings', NULL, NULL, 'AppSettings', 54, 57, 0, 100),
(61, 60, 'index', NULL, NULL, 'index', 55, 56, 0, 0),
(62, 1, 'Dashboard', NULL, NULL, 'Dashboard', 58, 61, 0, 0),
(63, 62, 'index', NULL, NULL, 'index', 59, 60, 0, 0),
(64, 1, 'Errors', NULL, NULL, 'Errors', 62, 65, 1, 0),
(65, 64, 'unauthorized', NULL, NULL, 'unauthorized', 63, 64, 1, 0),
(66, 1, 'Groups', NULL, NULL, 'Groups', 66, 79, 0, 1),
(67, 66, 'index', NULL, NULL, 'index', 67, 68, 0, 0),
(68, 66, 'view', NULL, NULL, 'view', 69, 70, 0, 4),
(69, 66, 'add', NULL, NULL, 'add', 71, 72, 0, 1),
(70, 66, 'edit', NULL, NULL, 'edit', 73, 74, 0, 2),
(71, 66, 'delete', NULL, NULL, 'delete', 75, 76, 0, 3),
(72, 66, 'configure', NULL, NULL, 'configure', 77, 78, 1, 5),
(73, 3, 'index', NULL, NULL, 'index', 5, 6, 1, 0),
(74, 3, 'logout', NULL, NULL, 'logout', 7, 8, 1, 0),
(75, 3, 'editProfile', NULL, NULL, 'editProfile', 9, 10, 1, 0),
(76, 3, 'activitiesLog', NULL, NULL, 'activitiesLog', 11, 12, 1, 0),
(77, 1, 'Users', NULL, NULL, 'Users', 80, 93, 0, 2),
(78, 77, 'index', NULL, NULL, 'index', 81, 82, 0, 0),
(79, 77, 'view', NULL, NULL, 'view', 83, 84, 0, 4),
(80, 77, 'add', NULL, NULL, 'add', 85, 86, 0, 1),
(81, 77, 'edit', NULL, NULL, 'edit', 87, 88, 0, 2),
(82, 77, 'delete', NULL, NULL, 'delete', 89, 90, 0, 3),
(83, 77, 'configure', NULL, NULL, 'configure', 91, 92, 0, 5),
(89, 3, 'uploadMedia', NULL, NULL, 'uploadMedia', 13, 14, 1, 0),
(95, 1, 'InternalOrders', NULL, NULL, 'InternalOrders', 94, 105, 0, 0),
(96, 95, 'index', NULL, NULL, 'index', 95, 96, 0, 0),
(97, 95, 'view', NULL, NULL, 'view', 97, 98, 0, 1),
(98, 95, 'add', NULL, NULL, 'add', 99, 100, 0, 2),
(99, 95, 'edit', NULL, NULL, 'edit', 101, 102, 0, 3),
(100, 95, 'delete', NULL, NULL, 'delete', 103, 104, 0, 4),
(104, 1, NULL, NULL, NULL, 'Apis', 106, 121, 0, 0),
(105, 104, NULL, NULL, NULL, 'getProducts', 107, 108, 0, 0),
(106, 1, 'Categories', NULL, NULL, 'Categories', 122, 133, 0, 0),
(107, 106, 'index', NULL, NULL, 'index', 123, 124, 0, 0),
(108, 106, 'view', NULL, NULL, 'view', 125, 126, 0, 0),
(109, 106, 'add', NULL, NULL, 'add', 127, 128, 0, 0),
(110, 106, 'edit', NULL, NULL, 'edit', 129, 130, 0, 0),
(111, 106, 'delete', NULL, NULL, 'delete', 131, 132, 0, 0),
(112, 1, 'Institutes', NULL, NULL, 'Institutes', 134, 145, 0, 0),
(113, 112, 'index', NULL, NULL, 'index', 135, 136, 0, 0),
(114, 112, 'view', NULL, NULL, 'view', 137, 138, 0, 0),
(115, 112, 'add', NULL, NULL, 'add', 139, 140, 0, 0),
(116, 112, 'edit', NULL, NULL, 'edit', 141, 142, 0, 0),
(117, 112, 'delete', NULL, NULL, 'delete', 143, 144, 0, 0),
(118, 1, 'Institutions', NULL, NULL, 'Institutions', 146, 157, 0, 0),
(119, 118, 'index', NULL, NULL, 'index', 147, 148, 0, 0),
(120, 118, 'view', NULL, NULL, 'view', 149, 150, 0, 0),
(121, 118, 'add', NULL, NULL, 'add', 151, 152, 0, 0),
(122, 118, 'edit', NULL, NULL, 'edit', 153, 154, 0, 0),
(123, 118, 'delete', NULL, NULL, 'delete', 155, 156, 0, 0),
(124, 1, 'Products', NULL, NULL, 'Products', 158, 169, 0, 0),
(125, 124, 'index', NULL, NULL, 'index', 159, 160, 0, 0),
(126, 124, 'view', NULL, NULL, 'view', 161, 162, 0, 0),
(127, 124, 'add', NULL, NULL, 'add', 163, 164, 0, 0),
(128, 124, 'edit', NULL, NULL, 'edit', 165, 166, 0, 0),
(129, 124, 'delete', NULL, NULL, 'delete', 167, 168, 0, 0),
(130, 1, 'Suppliers', NULL, NULL, 'Suppliers', 170, 181, 0, 0),
(131, 130, 'index', NULL, NULL, 'index', 171, 172, 0, 0),
(132, 130, 'view', NULL, NULL, 'view', 173, 174, 0, 0),
(133, 130, 'add', NULL, NULL, 'add', 175, 176, 0, 0),
(134, 130, 'edit', NULL, NULL, 'edit', 177, 178, 0, 0),
(135, 130, 'delete', NULL, NULL, 'delete', 179, 180, 0, 0),
(136, 1, 'ExpendituresGrants', NULL, NULL, 'ExpendituresGrants', 182, 193, 0, 0),
(137, 136, 'index', NULL, NULL, 'index', 183, 184, 0, 0),
(138, 136, 'view', NULL, NULL, 'view', 185, 186, 0, 0),
(139, 136, 'add', NULL, NULL, 'add', 187, 188, 0, 0),
(140, 136, 'edit', NULL, NULL, 'edit', 189, 190, 0, 0),
(141, 136, 'delete', NULL, NULL, 'delete', 191, 192, 0, 0),
(142, 1, 'PurchaseRequests', NULL, NULL, 'PurchaseRequests', 194, 205, 0, 0),
(143, 142, 'index', NULL, NULL, 'index', 195, 196, 0, 0),
(144, 142, 'view', NULL, NULL, 'view', 197, 198, 0, 0),
(145, 142, 'add', NULL, NULL, 'add', 199, 200, 0, 0),
(146, 142, 'edit', NULL, NULL, 'edit', 201, 202, 0, 0),
(147, 142, 'delete', NULL, NULL, 'delete', 203, 204, 0, 0),
(148, 1, 'ExpendituresOthers', NULL, NULL, 'ExpendituresOthers', 206, 217, 0, 0),
(149, 148, 'index', NULL, NULL, 'index', 207, 208, 0, 0),
(150, 148, 'view', NULL, NULL, 'view', 209, 210, 0, 0),
(151, 148, 'add', NULL, NULL, 'add', 211, 212, 0, 0),
(152, 148, 'edit', NULL, NULL, 'edit', 213, 214, 0, 0),
(153, 148, 'delete', NULL, NULL, 'delete', 215, 216, 0, 0),
(154, 1, 'ExpendituresReclarifications', NULL, NULL, 'ExpendituresReclarifications', 218, 229, 0, 0),
(155, 154, 'index', NULL, NULL, 'index', 219, 220, 0, 0),
(156, 154, 'view', NULL, NULL, 'view', 221, 222, 0, 0),
(157, 154, 'add', NULL, NULL, 'add', 223, 224, 0, 0),
(158, 154, 'edit', NULL, NULL, 'edit', 225, 226, 0, 0),
(159, 154, 'delete', NULL, NULL, 'delete', 227, 228, 0, 0),
(160, 1, 'ExpendituresTransfers', NULL, NULL, 'ExpendituresTransfers', 230, 241, 0, 0),
(161, 160, 'index', NULL, NULL, 'index', 231, 232, 0, 0),
(162, 160, 'view', NULL, NULL, 'view', 233, 234, 0, 0),
(163, 160, 'add', NULL, NULL, 'add', 235, 236, 0, 0),
(164, 160, 'edit', NULL, NULL, 'edit', 237, 238, 0, 0),
(165, 160, 'delete', NULL, NULL, 'delete', 239, 240, 0, 0),
(166, 1, 'ExpendituresDistributions', NULL, NULL, 'ExpendituresDistributions', 242, 253, 0, 0),
(167, 166, 'index', NULL, NULL, 'index', 243, 244, 0, 0),
(168, 166, 'view', NULL, NULL, 'view', 245, 246, 0, 0),
(169, 166, 'add', NULL, NULL, 'add', 247, 248, 0, 0),
(170, 166, 'edit', NULL, NULL, 'edit', 249, 250, 0, 0),
(171, 166, 'delete', NULL, NULL, 'delete', 251, 252, 0, 0),
(172, 1, 'PurchaseOrders', NULL, NULL, 'PurchaseOrders', 254, 265, 0, 0),
(173, 172, 'index', NULL, NULL, 'index', 255, 256, 0, 0),
(174, 172, 'view', NULL, NULL, 'view', 257, 258, 0, 0),
(175, 172, 'add', NULL, NULL, 'add', 259, 260, 0, 0),
(176, 172, 'edit', NULL, NULL, 'edit', 261, 262, 0, 0),
(177, 172, 'delete', NULL, NULL, 'delete', 263, 264, 0, 0),
(178, 1, 'ReceiptGrants', NULL, NULL, 'ReceiptGrants', 266, 277, 0, 0),
(179, 178, 'index', NULL, NULL, 'index', 267, 268, 0, 0),
(180, 178, 'view', NULL, NULL, 'view', 269, 270, 0, 0),
(181, 178, 'add', NULL, NULL, 'add', 271, 272, 0, 0),
(182, 178, 'edit', NULL, NULL, 'edit', 273, 274, 0, 0),
(183, 178, 'delete', NULL, NULL, 'delete', 275, 276, 0, 0),
(184, 1, 'ReceiptOthers', NULL, NULL, 'ReceiptOthers', 278, 289, 0, 0),
(185, 184, 'index', NULL, NULL, 'index', 279, 280, 0, 0),
(186, 184, 'view', NULL, NULL, 'view', 281, 282, 0, 0),
(187, 184, 'add', NULL, NULL, 'add', 283, 284, 0, 0),
(188, 184, 'edit', NULL, NULL, 'edit', 285, 286, 0, 0),
(189, 184, 'delete', NULL, NULL, 'delete', 287, 288, 0, 0),
(190, 1, 'ReceiptTransfers', NULL, NULL, 'ReceiptTransfers', 290, 301, 0, 0),
(191, 190, 'index', NULL, NULL, 'index', 291, 292, 0, 0),
(192, 190, 'view', NULL, NULL, 'view', 293, 294, 0, 0),
(193, 190, 'add', NULL, NULL, 'add', 295, 296, 0, 0),
(194, 190, 'edit', NULL, NULL, 'edit', 297, 298, 0, 0),
(195, 190, 'delete', NULL, NULL, 'delete', 299, 300, 0, 0),
(196, 104, 'getPrs', NULL, NULL, 'getPrs', 109, 110, 0, 0),
(197, 104, 'getRps', NULL, NULL, 'getRps', 111, 112, 0, 0),
(198, 104, 'getRpsDetails', NULL, NULL, 'getRpsDetails', 113, 114, 0, 0),
(199, 104, 'getPrsDetails', NULL, NULL, 'getPrsDetails', 115, 116, 0, 0),
(200, 1, 'ReceiptPurchases', NULL, NULL, 'ReceiptPurchases', 302, 313, 0, 0),
(201, 200, 'index', NULL, NULL, 'index', 303, 304, 0, 0),
(202, 200, 'view', NULL, NULL, 'view', 305, 306, 0, 0),
(203, 200, 'add', NULL, NULL, 'add', 307, 308, 0, 0),
(204, 200, 'edit', NULL, NULL, 'edit', 309, 310, 0, 0),
(205, 200, 'delete', NULL, NULL, 'delete', 311, 312, 0, 0),
(206, 1, 'ReceiptReclarifications', NULL, NULL, 'ReceiptReclarifications', 314, 325, 0, 0),
(207, 206, 'index', NULL, NULL, 'index', 315, 316, 0, 0),
(208, 206, 'view', NULL, NULL, 'view', 317, 318, 0, 0),
(209, 206, 'add', NULL, NULL, 'add', 319, 320, 0, 0),
(210, 206, 'edit', NULL, NULL, 'edit', 321, 322, 0, 0),
(211, 206, 'delete', NULL, NULL, 'delete', 323, 324, 0, 0),
(212, 104, 'getRrs', NULL, NULL, 'getRrs', 117, 118, 0, 0),
(213, 104, 'getRrsDetails', NULL, NULL, 'getRrsDetails', 119, 120, 0, 0),
(214, 1, 'ReportMutations', NULL, NULL, 'ReportMutations', 326, 329, 0, 0),
(215, 214, 'index', NULL, NULL, 'index', 327, 328, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `app_settings`
--

CREATE TABLE `app_settings` (
  `id` int(11) NOT NULL,
  `keyField` varchar(225) NOT NULL,
  `valueField` varchar(225) NOT NULL,
  `type` enum('text','long text','image','select') NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `app_settings`
--

INSERT INTO `app_settings` (`id`, `keyField`, `valueField`, `type`, `status`) VALUES
(1, 'App.Name', 'Z-APP', 'text', 0),
(2, 'App.Logo', '/webroot/assets/img/logo.png', 'image', 0),
(3, 'App.Logo.Login', '/webroot/assets/img/logo_login.png', 'image', 0),
(4, 'App.Logo.Width', '160', 'text', 0),
(5, 'App.Logo.Height', '30', 'text', 0),
(6, 'App.Logo.Login.Width', '160', 'text', 0),
(7, 'App.Logo.Login.Height', '160', 'text', 0),
(8, 'App.Login.Cover', '/webroot/assets/img/cover_login.jpg', 'image', 0),
(9, 'App.Description', 'Management application', 'long text', 0),
(10, 'App.Favico', '/webroot/assets/img/favico.png', 'long text', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `aros`
--

CREATE TABLE `aros` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(11) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `aros`
--

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, 'Groups', 1, NULL, 1, 6),
(2, 1, 'Users', 1, NULL, 2, 3),
(3, 5, 'Users', 2, NULL, 8, 9),
(5, NULL, 'Groups', 2, NULL, 7, 10),
(6, 1, 'Users', 4, NULL, 4, 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `aros_acos`
--

CREATE TABLE `aros_acos` (
  `id` int(11) NOT NULL,
  `aro_id` int(11) NOT NULL,
  `aco_id` int(11) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `aros_acos`
--

INSERT INTO `aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`) VALUES
(1, 1, 6, '1', '1', '1', '1'),
(2, 1, 7, '1', '1', '1', '1'),
(3, 1, 8, '1', '1', '1', '1'),
(4, 1, 9, '1', '1', '1', '1'),
(5, 1, 10, '1', '1', '1', '1'),
(6, 1, 11, '1', '1', '1', '1'),
(7, 1, 12, '1', '1', '1', '1'),
(8, 1, 13, '1', '1', '1', '1'),
(9, 1, 14, '1', '1', '1', '1'),
(10, 1, 15, '1', '1', '1', '1'),
(11, 1, 16, '1', '1', '1', '1'),
(12, 1, 17, '1', '1', '1', '1'),
(13, 1, 18, '1', '1', '1', '1'),
(14, 2, 6, '1', '1', '1', '1'),
(15, 2, 7, '1', '1', '1', '1'),
(16, 2, 8, '1', '1', '1', '1'),
(17, 2, 9, '1', '1', '1', '1'),
(18, 2, 13, '1', '1', '1', '1'),
(19, 2, 14, '1', '1', '1', '1'),
(20, 2, 15, '1', '1', '1', '1'),
(21, 2, 16, '1', '1', '1', '1'),
(22, 2, 17, '1', '1', '1', '1'),
(23, 2, 18, '1', '1', '1', '1'),
(24, 2, 10, '1', '1', '1', '1'),
(25, 2, 11, '1', '1', '1', '1'),
(26, 2, 12, '1', '1', '1', '1'),
(57, 1, 36, '1', '1', '1', '1'),
(58, 1, 37, '1', '1', '1', '1'),
(59, 2, 36, '1', '1', '1', '1'),
(60, 1, 40, '1', '1', '1', '1'),
(61, 2, 40, '1', '1', '1', '1'),
(62, 2, 37, '1', '1', '1', '1'),
(63, 1, 47, '1', '1', '1', '1'),
(64, 1, 49, '1', '1', '1', '1'),
(65, 1, 48, '1', '1', '1', '1'),
(66, 1, 52, '1', '1', '1', '1'),
(67, 1, 51, '1', '1', '1', '1'),
(68, 1, 50, '1', '1', '1', '1'),
(69, 1, 41, '1', '1', '1', '1'),
(70, 1, 45, '1', '1', '1', '1'),
(71, 1, 44, '1', '1', '1', '1'),
(72, 1, 43, '1', '1', '1', '1'),
(73, 1, 42, '1', '1', '1', '1'),
(74, 1, 46, '1', '1', '1', '1'),
(75, 2, 47, '1', '1', '1', '1'),
(76, 2, 49, '1', '1', '1', '1'),
(77, 2, 48, '1', '1', '1', '1'),
(78, 2, 52, '1', '1', '1', '1'),
(79, 2, 51, '1', '1', '1', '1'),
(80, 2, 50, '1', '1', '1', '1'),
(81, 2, 41, '1', '1', '1', '1'),
(82, 2, 45, '1', '1', '1', '1'),
(83, 2, 44, '1', '1', '1', '1'),
(84, 2, 43, '1', '1', '1', '1'),
(85, 2, 42, '1', '1', '1', '1'),
(86, 2, 46, '1', '1', '1', '1'),
(87, 1, 57, '1', '1', '1', '1'),
(88, 1, 58, '1', '1', '1', '1'),
(89, 2, 57, '1', '1', '1', '1'),
(90, 2, 58, '1', '1', '1', '1'),
(91, 3, 57, '-1', '-1', '-1', '-1'),
(92, 3, 58, '-1', '-1', '-1', '-1'),
(93, 3, 36, '1', '1', '1', '1'),
(94, 3, 37, '1', '1', '1', '1'),
(95, 3, 47, '1', '1', '1', '1'),
(96, 3, 49, '1', '1', '1', '1'),
(97, 3, 48, '1', '1', '1', '1'),
(98, 3, 52, '1', '1', '1', '1'),
(99, 3, 51, '1', '1', '1', '1'),
(100, 3, 50, '1', '1', '1', '1'),
(101, 3, 41, '1', '1', '1', '1'),
(102, 3, 45, '1', '1', '1', '1'),
(103, 3, 44, '1', '1', '1', '1'),
(104, 3, 43, '1', '1', '1', '1'),
(105, 3, 42, '1', '1', '1', '1'),
(106, 3, 46, '1', '1', '1', '1'),
(107, 3, 6, '1', '1', '1', '1'),
(108, 3, 12, '1', '1', '1', '1'),
(109, 3, 7, '1', '1', '1', '1'),
(110, 3, 11, '1', '1', '1', '1'),
(111, 3, 10, '1', '1', '1', '1'),
(112, 3, 9, '1', '1', '1', '1'),
(113, 3, 8, '1', '1', '1', '1'),
(114, 3, 13, '1', '1', '1', '1'),
(115, 3, 40, '1', '1', '1', '1'),
(116, 3, 16, '1', '1', '1', '1'),
(117, 3, 15, '1', '1', '1', '1'),
(118, 3, 14, '1', '1', '1', '1'),
(119, 3, 18, '1', '1', '1', '1'),
(120, 3, 17, '1', '1', '1', '1'),
(121, 1, 62, '1', '1', '1', '1'),
(122, 1, 63, '1', '1', '1', '1'),
(123, 1, 66, '1', '1', '1', '1'),
(124, 1, 68, '1', '1', '1', '1'),
(125, 1, 67, '1', '1', '1', '1'),
(126, 1, 71, '1', '1', '1', '1'),
(127, 1, 70, '1', '1', '1', '1'),
(128, 1, 69, '1', '1', '1', '1'),
(129, 1, 77, '1', '1', '1', '1'),
(130, 1, 82, '1', '1', '1', '1'),
(131, 1, 81, '1', '1', '1', '1'),
(132, 1, 80, '1', '1', '1', '1'),
(133, 1, 79, '1', '1', '1', '1'),
(134, 1, 83, '1', '1', '1', '1'),
(135, 1, 78, '1', '1', '1', '1'),
(136, 1, 60, '1', '1', '1', '1'),
(137, 1, 61, '1', '1', '1', '1'),
(138, 2, 62, '1', '1', '1', '1'),
(139, 2, 63, '1', '1', '1', '1'),
(140, 2, 66, '1', '1', '1', '1'),
(141, 2, 68, '1', '1', '1', '1'),
(142, 2, 67, '1', '1', '1', '1'),
(143, 2, 71, '1', '1', '1', '1'),
(144, 2, 70, '1', '1', '1', '1'),
(145, 2, 69, '1', '1', '1', '1'),
(146, 2, 77, '1', '1', '1', '1'),
(147, 2, 82, '1', '1', '1', '1'),
(148, 2, 81, '1', '1', '1', '1'),
(149, 2, 80, '1', '1', '1', '1'),
(150, 2, 79, '1', '1', '1', '1'),
(151, 2, 83, '1', '1', '1', '1'),
(152, 2, 78, '1', '1', '1', '1'),
(153, 2, 60, '1', '1', '1', '1'),
(154, 2, 61, '1', '1', '1', '1'),
(155, 3, 62, '-1', '-1', '-1', '-1'),
(156, 3, 63, '-1', '-1', '-1', '-1'),
(157, 3, 66, '-1', '-1', '-1', '-1'),
(158, 3, 68, '-1', '-1', '-1', '-1'),
(159, 3, 67, '-1', '-1', '-1', '-1'),
(160, 3, 71, '-1', '-1', '-1', '-1'),
(161, 3, 70, '-1', '-1', '-1', '-1'),
(162, 3, 69, '-1', '-1', '-1', '-1'),
(163, 3, 77, '1', '1', '1', '1'),
(164, 3, 82, '-1', '-1', '-1', '-1'),
(165, 3, 81, '-1', '-1', '-1', '-1'),
(166, 3, 80, '-1', '-1', '-1', '-1'),
(167, 3, 79, '-1', '-1', '-1', '-1'),
(168, 3, 83, '-1', '-1', '-1', '-1'),
(169, 3, 78, '1', '1', '1', '1'),
(170, 3, 60, '-1', '-1', '-1', '-1'),
(171, 3, 61, '-1', '-1', '-1', '-1'),
(172, 5, 62, '-1', '-1', '-1', '-1'),
(173, 5, 63, '-1', '-1', '-1', '-1'),
(174, 5, 66, '-1', '-1', '-1', '-1'),
(175, 5, 68, '-1', '-1', '-1', '-1'),
(176, 5, 67, '-1', '-1', '-1', '-1'),
(177, 5, 71, '-1', '-1', '-1', '-1'),
(178, 5, 70, '-1', '-1', '-1', '-1'),
(179, 5, 69, '-1', '-1', '-1', '-1'),
(180, 5, 77, '1', '1', '1', '1'),
(181, 5, 82, '-1', '-1', '-1', '-1'),
(182, 5, 81, '-1', '-1', '-1', '-1'),
(183, 5, 80, '-1', '-1', '-1', '-1'),
(184, 5, 79, '-1', '-1', '-1', '-1'),
(185, 5, 83, '-1', '-1', '-1', '-1'),
(186, 5, 78, '1', '1', '1', '1'),
(187, 5, 60, '-1', '-1', '-1', '-1'),
(188, 5, 61, '-1', '-1', '-1', '-1'),
(189, 1, 95, '1', '1', '1', '1'),
(191, 1, 96, '1', '1', '1', '1'),
(192, 1, 97, '1', '1', '1', '1'),
(193, 1, 98, '1', '1', '1', '1'),
(194, 1, 99, '1', '1', '1', '1'),
(195, 1, 100, '1', '1', '1', '1'),
(208, 2, 95, '1', '1', '1', '1'),
(210, 2, 96, '1', '1', '1', '1'),
(211, 2, 97, '1', '1', '1', '1'),
(212, 2, 98, '1', '1', '1', '1'),
(213, 2, 99, '1', '1', '1', '1'),
(214, 2, 100, '1', '1', '1', '1'),
(215, 6, 99, '-1', '-1', '-1', '-1'),
(216, 1, 118, '1', '1', '1', '1'),
(217, 1, 119, '1', '1', '1', '1'),
(218, 1, 123, '1', '1', '1', '1'),
(219, 1, 122, '1', '1', '1', '1'),
(220, 1, 121, '1', '1', '1', '1'),
(221, 1, 120, '1', '1', '1', '1'),
(222, 1, 104, '1', '1', '1', '1'),
(223, 1, 105, '1', '1', '1', '1'),
(224, 1, 112, '1', '1', '1', '1'),
(225, 1, 114, '1', '1', '1', '1'),
(226, 1, 113, '1', '1', '1', '1'),
(227, 1, 117, '1', '1', '1', '1'),
(228, 1, 116, '1', '1', '1', '1'),
(229, 1, 115, '1', '1', '1', '1'),
(230, 1, 130, '1', '1', '1', '1'),
(231, 1, 133, '1', '1', '1', '1'),
(232, 1, 132, '1', '1', '1', '1'),
(233, 1, 131, '1', '1', '1', '1'),
(234, 1, 135, '1', '1', '1', '1'),
(235, 1, 134, '1', '1', '1', '1'),
(236, 1, 106, '1', '1', '1', '1'),
(237, 1, 110, '1', '1', '1', '1'),
(238, 1, 109, '1', '1', '1', '1'),
(239, 1, 108, '1', '1', '1', '1'),
(240, 1, 107, '1', '1', '1', '1'),
(241, 1, 111, '1', '1', '1', '1'),
(242, 1, 124, '1', '1', '1', '1'),
(243, 1, 128, '1', '1', '1', '1'),
(244, 1, 127, '1', '1', '1', '1'),
(245, 1, 126, '1', '1', '1', '1'),
(246, 1, 125, '1', '1', '1', '1'),
(247, 1, 129, '1', '1', '1', '1'),
(248, 2, 118, '1', '1', '1', '1'),
(249, 2, 119, '1', '1', '1', '1'),
(250, 2, 123, '1', '1', '1', '1'),
(251, 2, 122, '1', '1', '1', '1'),
(252, 2, 121, '1', '1', '1', '1'),
(253, 2, 120, '1', '1', '1', '1'),
(254, 2, 104, '1', '1', '1', '1'),
(255, 2, 105, '1', '1', '1', '1'),
(256, 2, 112, '1', '1', '1', '1'),
(257, 2, 114, '1', '1', '1', '1'),
(258, 2, 113, '1', '1', '1', '1'),
(259, 2, 117, '1', '1', '1', '1'),
(260, 2, 116, '1', '1', '1', '1'),
(261, 2, 115, '1', '1', '1', '1'),
(262, 2, 130, '1', '1', '1', '1'),
(263, 2, 133, '1', '1', '1', '1'),
(264, 2, 132, '1', '1', '1', '1'),
(265, 2, 131, '1', '1', '1', '1'),
(266, 2, 135, '1', '1', '1', '1'),
(267, 2, 134, '1', '1', '1', '1'),
(268, 2, 106, '1', '1', '1', '1'),
(269, 2, 110, '1', '1', '1', '1'),
(270, 2, 109, '1', '1', '1', '1'),
(271, 2, 108, '1', '1', '1', '1'),
(272, 2, 107, '1', '1', '1', '1'),
(273, 2, 111, '1', '1', '1', '1'),
(274, 2, 124, '1', '1', '1', '1'),
(275, 2, 128, '1', '1', '1', '1'),
(276, 2, 127, '1', '1', '1', '1'),
(277, 2, 126, '1', '1', '1', '1'),
(278, 2, 125, '1', '1', '1', '1'),
(279, 2, 129, '1', '1', '1', '1'),
(280, 1, 142, '1', '1', '1', '1'),
(281, 1, 146, '1', '1', '1', '1'),
(282, 2, 146, '1', '1', '1', '1'),
(283, 6, 146, '-1', '-1', '-1', '-1'),
(284, 1, 145, '1', '1', '1', '1'),
(285, 2, 145, '1', '1', '1', '1'),
(286, 6, 145, '-1', '-1', '-1', '-1'),
(287, 1, 144, '1', '1', '1', '1'),
(288, 2, 144, '1', '1', '1', '1'),
(289, 6, 144, '-1', '-1', '-1', '-1'),
(290, 1, 143, '1', '1', '1', '1'),
(291, 2, 143, '1', '1', '1', '1'),
(292, 6, 143, '-1', '-1', '-1', '-1'),
(293, 1, 147, '1', '1', '1', '1'),
(294, 2, 147, '1', '1', '1', '1'),
(295, 6, 147, '-1', '-1', '-1', '-1'),
(296, 2, 142, '1', '1', '1', '1'),
(297, 6, 142, '-1', '-1', '-1', '-1'),
(298, 1, 136, '1', '1', '1', '1'),
(299, 1, 137, '1', '1', '1', '1'),
(300, 1, 141, '1', '1', '1', '1'),
(301, 1, 140, '1', '1', '1', '1'),
(302, 1, 139, '1', '1', '1', '1'),
(303, 1, 138, '1', '1', '1', '1'),
(304, 2, 136, '1', '1', '1', '1'),
(305, 2, 137, '1', '1', '1', '1'),
(306, 2, 141, '1', '1', '1', '1'),
(307, 2, 140, '1', '1', '1', '1'),
(308, 2, 139, '1', '1', '1', '1'),
(309, 2, 138, '1', '1', '1', '1'),
(310, 1, 148, '1', '1', '1', '1'),
(311, 1, 151, '1', '1', '1', '1'),
(312, 1, 150, '1', '1', '1', '1'),
(313, 1, 149, '1', '1', '1', '1'),
(314, 1, 153, '1', '1', '1', '1'),
(315, 1, 152, '1', '1', '1', '1'),
(316, 2, 148, '1', '1', '1', '1'),
(317, 2, 152, '1', '1', '1', '1'),
(318, 2, 151, '1', '1', '1', '1'),
(319, 2, 150, '1', '1', '1', '1'),
(320, 2, 149, '1', '1', '1', '1'),
(321, 2, 153, '1', '1', '1', '1'),
(322, 1, 154, '1', '1', '1', '1'),
(323, 1, 155, '1', '1', '1', '1'),
(324, 1, 159, '1', '1', '1', '1'),
(325, 1, 158, '1', '1', '1', '1'),
(326, 1, 157, '1', '1', '1', '1'),
(327, 1, 156, '1', '1', '1', '1'),
(328, 2, 154, '1', '1', '1', '1'),
(329, 2, 157, '1', '1', '1', '1'),
(330, 2, 156, '1', '1', '1', '1'),
(331, 2, 155, '1', '1', '1', '1'),
(332, 2, 159, '1', '1', '1', '1'),
(333, 2, 158, '1', '1', '1', '1'),
(334, 1, 160, '1', '1', '1', '1'),
(335, 1, 165, '1', '1', '1', '1'),
(336, 1, 164, '1', '1', '1', '1'),
(337, 1, 163, '1', '1', '1', '1'),
(338, 1, 162, '1', '1', '1', '1'),
(339, 1, 161, '1', '1', '1', '1'),
(340, 2, 160, '1', '1', '1', '1'),
(341, 2, 161, '1', '1', '1', '1'),
(342, 2, 165, '1', '1', '1', '1'),
(343, 2, 164, '1', '1', '1', '1'),
(344, 2, 163, '1', '1', '1', '1'),
(345, 2, 162, '1', '1', '1', '1'),
(346, 1, 166, '1', '1', '1', '1'),
(347, 1, 169, '1', '1', '1', '1'),
(348, 1, 168, '1', '1', '1', '1'),
(349, 1, 167, '1', '1', '1', '1'),
(350, 1, 171, '1', '1', '1', '1'),
(351, 1, 170, '1', '1', '1', '1'),
(352, 2, 166, '1', '1', '1', '1'),
(353, 2, 171, '1', '1', '1', '1'),
(354, 2, 170, '1', '1', '1', '1'),
(355, 2, 169, '1', '1', '1', '1'),
(356, 2, 168, '1', '1', '1', '1'),
(357, 2, 167, '1', '1', '1', '1'),
(358, 1, 178, '1', '1', '1', '1'),
(359, 1, 183, '1', '1', '1', '1'),
(360, 1, 182, '1', '1', '1', '1'),
(361, 1, 181, '1', '1', '1', '1'),
(362, 1, 180, '1', '1', '1', '1'),
(363, 1, 179, '1', '1', '1', '1'),
(364, 1, 172, '1', '1', '1', '1'),
(365, 1, 174, '1', '1', '1', '1'),
(366, 1, 173, '1', '1', '1', '1'),
(367, 1, 177, '1', '1', '1', '1'),
(368, 1, 176, '1', '1', '1', '1'),
(369, 1, 175, '1', '1', '1', '1'),
(370, 1, 190, '1', '1', '1', '1'),
(371, 1, 192, '1', '1', '1', '1'),
(372, 1, 191, '1', '1', '1', '1'),
(373, 1, 195, '1', '1', '1', '1'),
(374, 1, 194, '1', '1', '1', '1'),
(375, 1, 193, '1', '1', '1', '1'),
(376, 1, 184, '1', '1', '1', '1'),
(377, 1, 187, '1', '1', '1', '1'),
(378, 1, 186, '1', '1', '1', '1'),
(379, 1, 185, '1', '1', '1', '1'),
(380, 1, 189, '1', '1', '1', '1'),
(381, 1, 188, '1', '1', '1', '1'),
(382, 2, 178, '1', '1', '1', '1'),
(383, 2, 183, '1', '1', '1', '1'),
(384, 2, 182, '1', '1', '1', '1'),
(385, 2, 181, '1', '1', '1', '1'),
(386, 2, 180, '1', '1', '1', '1'),
(387, 2, 179, '1', '1', '1', '1'),
(388, 2, 172, '1', '1', '1', '1'),
(389, 2, 174, '1', '1', '1', '1'),
(390, 2, 173, '1', '1', '1', '1'),
(391, 2, 177, '1', '1', '1', '1'),
(392, 2, 176, '1', '1', '1', '1'),
(393, 2, 175, '1', '1', '1', '1'),
(394, 2, 190, '1', '1', '1', '1'),
(395, 2, 192, '1', '1', '1', '1'),
(396, 2, 191, '1', '1', '1', '1'),
(397, 2, 195, '1', '1', '1', '1'),
(398, 2, 194, '1', '1', '1', '1'),
(399, 2, 193, '1', '1', '1', '1'),
(400, 2, 184, '1', '1', '1', '1'),
(401, 2, 187, '1', '1', '1', '1'),
(402, 2, 186, '1', '1', '1', '1'),
(403, 2, 185, '1', '1', '1', '1'),
(404, 2, 189, '1', '1', '1', '1'),
(405, 2, 188, '1', '1', '1', '1'),
(406, 1, 206, '1', '1', '1', '1'),
(407, 1, 210, '1', '1', '1', '1'),
(408, 1, 209, '1', '1', '1', '1'),
(409, 1, 208, '1', '1', '1', '1'),
(410, 1, 207, '1', '1', '1', '1'),
(411, 1, 211, '1', '1', '1', '1'),
(412, 1, 197, '1', '1', '1', '1'),
(413, 1, 196, '1', '1', '1', '1'),
(414, 1, 199, '1', '1', '1', '1'),
(415, 1, 198, '1', '1', '1', '1'),
(416, 1, 200, '1', '1', '1', '1'),
(417, 1, 201, '1', '1', '1', '1'),
(418, 1, 205, '1', '1', '1', '1'),
(419, 1, 204, '1', '1', '1', '1'),
(420, 1, 203, '1', '1', '1', '1'),
(421, 1, 202, '1', '1', '1', '1'),
(422, 2, 206, '1', '1', '1', '1'),
(423, 2, 210, '1', '1', '1', '1'),
(424, 2, 209, '1', '1', '1', '1'),
(425, 2, 208, '1', '1', '1', '1'),
(426, 2, 207, '1', '1', '1', '1'),
(427, 2, 211, '1', '1', '1', '1'),
(428, 2, 197, '1', '1', '1', '1'),
(429, 2, 196, '1', '1', '1', '1'),
(430, 2, 199, '1', '1', '1', '1'),
(431, 2, 198, '1', '1', '1', '1'),
(432, 2, 200, '1', '1', '1', '1'),
(433, 2, 201, '1', '1', '1', '1'),
(434, 2, 205, '1', '1', '1', '1'),
(435, 2, 204, '1', '1', '1', '1'),
(436, 2, 203, '1', '1', '1', '1'),
(437, 2, 202, '1', '1', '1', '1'),
(438, 1, 213, '1', '1', '1', '1'),
(439, 1, 212, '1', '1', '1', '1'),
(440, 1, 214, '1', '1', '1', '1'),
(441, 1, 215, '1', '1', '1', '1'),
(442, 2, 214, '1', '1', '1', '1'),
(443, 2, 215, '1', '1', '1', '1'),
(444, 2, 213, '1', '1', '1', '1'),
(445, 2, 212, '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `audit_logs`
--

CREATE TABLE `audit_logs` (
  `id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `controller` varchar(225) DEFAULT NULL,
  `_action` varchar(225) DEFAULT NULL,
  `type` varchar(250) DEFAULT NULL,
  `primary_key` int(11) DEFAULT NULL,
  `source` varchar(250) DEFAULT NULL,
  `parent_source` varchar(250) DEFAULT NULL,
  `original` json DEFAULT NULL,
  `changed` json DEFAULT NULL,
  `meta` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `audit_logs`
--

INSERT INTO `audit_logs` (`id`, `timestamp`, `user_id`, `controller`, `_action`, `type`, `primary_key`, `source`, `parent_source`, `original`, `changed`, `meta`) VALUES
(1, '2018-02-06 15:08:55', 1, 'users', 'add', 'create', 10, 'users', NULL, '\"{\\\"id\\\":10,\\\"username\\\":\\\"sadsadsad\\\",\\\"name\\\":\\\"KAS PUSAT\\\",\\\"email\\\":\\\"sadsa23dsadsa@gmail.com\\\",\\\"group_id\\\":2,\\\"status\\\":false,\\\"created_by\\\":1}\"', '\"{\\\"id\\\":10,\\\"username\\\":\\\"sadsadsad\\\",\\\"name\\\":\\\"KAS PUSAT\\\",\\\"email\\\":\\\"sadsa23dsadsa@gmail.com\\\",\\\"group_id\\\":2,\\\"status\\\":false,\\\"created_by\\\":1}\"', '\"[]\"'),
(2, '2018-02-06 15:11:34', 1, 'groups', 'delete', 'delete', 10, 'users', NULL, 'null', 'null', '\"[]\"'),
(3, '2018-02-06 15:11:34', 1, 'groups', 'delete', 'delete', 2, 'groups', NULL, 'null', 'null', '\"[]\"'),
(4, '2018-02-08 10:38:04', 1, 'users', 'add', 'create', 2, 'users', NULL, '\"{\\\"id\\\":2,\\\"username\\\":\\\"admin\\\",\\\"name\\\":\\\"dsadsa\\\",\\\"email\\\":\\\"dsadsads@dsadsa.com\\\",\\\"group_id\\\":1,\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"{\\\"id\\\":2,\\\"username\\\":\\\"admin\\\",\\\"name\\\":\\\"dsadsa\\\",\\\"email\\\":\\\"dsadsads@dsadsa.com\\\",\\\"group_id\\\":1,\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"[]\"'),
(5, '2018-02-08 10:38:27', 1, 'users', 'add', 'create', 3, 'users', NULL, '\"{\\\"id\\\":3,\\\"username\\\":\\\"dsadsad\\\",\\\"name\\\":\\\"KAS PUSAT\\\",\\\"email\\\":\\\"321321@gmail.com\\\",\\\"group_id\\\":1,\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"{\\\"id\\\":3,\\\"username\\\":\\\"dsadsad\\\",\\\"name\\\":\\\"KAS PUSAT\\\",\\\"email\\\":\\\"321321@gmail.com\\\",\\\"group_id\\\":1,\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"[]\"'),
(6, '2018-02-09 03:50:34', 1, 'users', 'edit', 'update', 2, 'users', NULL, '\"{\\\"username\\\":\\\"admin\\\",\\\"name\\\":\\\"dsadsa\\\",\\\"email\\\":\\\"dsadsads@dsadsa.com\\\",\\\"modified_by\\\":null}\"', '\"{\\\"username\\\":\\\"rezama\\\",\\\"name\\\":\\\"rezama\\\",\\\"email\\\":\\\"rezama@app.com\\\",\\\"modified_by\\\":1}\"', '\"[]\"'),
(7, '2018-02-09 04:14:55', 1, 'groups', 'add', 'create', 2, 'groups', NULL, '\"{\\\"id\\\":2,\\\"name\\\":\\\"SPV\\\",\\\"status\\\":false,\\\"created_by\\\":1}\"', '\"{\\\"id\\\":2,\\\"name\\\":\\\"SPV\\\",\\\"status\\\":false,\\\"created_by\\\":1}\"', '\"[]\"'),
(8, '2018-02-09 04:15:23', 1, 'users', 'add', 'create', 4, 'users', NULL, '\"{\\\"id\\\":4,\\\"username\\\":\\\"SPV\\\",\\\"name\\\":\\\"spv\\\",\\\"email\\\":\\\"spv@app.com\\\",\\\"group_id\\\":1,\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"{\\\"id\\\":4,\\\"username\\\":\\\"SPV\\\",\\\"name\\\":\\\"spv\\\",\\\"email\\\":\\\"spv@app.com\\\",\\\"group_id\\\":1,\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"[]\"'),
(9, '2018-02-09 04:15:32', 1, 'users', 'edit', 'update', 2, 'users', NULL, '\"{\\\"status\\\":true,\\\"modified_by\\\":1}\"', '\"{\\\"status\\\":false,\\\"modified_by\\\":1}\"', '\"[]\"'),
(10, '2018-03-01 16:49:08', 1, 'edit', '2', 'update', 2, 'users', NULL, '\"{\\\"status\\\":false,\\\"modified_by\\\":1}\"', '\"{\\\"status\\\":true,\\\"modified_by\\\":1}\"', '\"[]\"'),
(11, '2018-03-02 07:10:57', 1, 'edit', '2', 'update', 2, 'users', NULL, '\"{\\\"name\\\":\\\"rezama\\\",\\\"modified_by\\\":1}\"', '\"{\\\"name\\\":\\\"rezama1\\\",\\\"modified_by\\\":1}\"', '\"[]\"'),
(12, '2018-03-02 07:19:29', 2, 'edit', '2', 'update', 2, 'users', NULL, '\"{\\\"name\\\":\\\"rezama1\\\",\\\"modified_by\\\":1}\"', '\"{\\\"name\\\":\\\"rezama123445\\\",\\\"modified_by\\\":2}\"', '\"[]\"'),
(13, '2018-03-02 07:20:27', 1, 'edit', '2', 'update', 2, 'users', NULL, '\"{\\\"email\\\":\\\"rezama@app.com\\\",\\\"modified_by\\\":2}\"', '\"{\\\"email\\\":\\\"rezam3213a@app.com\\\",\\\"modified_by\\\":1}\"', '\"[]\"'),
(14, '2018-03-02 07:24:39', 1, 'edit', '2', 'update', 2, 'users', NULL, '\"{\\\"username\\\":\\\"rezama\\\",\\\"name\\\":\\\"rezama123445\\\",\\\"modified_by\\\":1}\"', '\"{\\\"username\\\":\\\"rezama123\\\",\\\"name\\\":\\\"udinpetot\\\",\\\"modified_by\\\":1}\"', '\"[]\"'),
(15, '2018-03-02 07:28:28', 1, 'edit', '2', 'update', 2, 'users', NULL, '\"{\\\"username\\\":\\\"rezama123\\\",\\\"name\\\":\\\"udinpetot\\\",\\\"modified_by\\\":1}\"', '\"{\\\"username\\\":\\\"rezama\\\",\\\"name\\\":\\\"rezama ryan\\\",\\\"modified_by\\\":1}\"', '\"[]\"'),
(16, '2018-03-02 07:33:55', 1, 'edit', '2', 'update', 2, 'users', NULL, '\"{\\\"group_id\\\":1,\\\"modified_by\\\":1}\"', '\"{\\\"group_id\\\":2,\\\"modified_by\\\":1}\"', '\"[]\"'),
(17, '2018-03-02 17:00:55', 1, 'delete', '3', 'delete', 3, 'users', NULL, 'null', 'null', '\"[]\"');

-- --------------------------------------------------------

--
-- Struktur dari tabel `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `name` varchar(225) NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `categories`
--

INSERT INTO `categories` (`id`, `code`, `name`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(10, 'A0001', 'PRT', 1, '2018-04-24 15:58:42', 1, '2018-04-24 15:58:42', NULL),
(11, 'A0002', 'ART', 1, '2018-04-24 16:02:52', 1, '2018-04-25 08:33:52', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `expenditures_distributions`
--

CREATE TABLE `expenditures_distributions` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `institute_id` int(11) NOT NULL COMMENT 'DIBERIKAN KEPADA',
  `internal_order_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `expenditures_distributions`
--

INSERT INTO `expenditures_distributions` (`id`, `code`, `institute_id`, `internal_order_id`, `date`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'ED/2018/05/000001', 1, 1, '2018-05-07', 0, '2018-05-07 11:02:34', 1, '2018-05-08 10:47:55', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `expenditures_distributions_details`
--

CREATE TABLE `expenditures_distributions_details` (
  `id` int(11) NOT NULL,
  `expenditures_distribution_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `price` decimal(22,2) DEFAULT '0.00',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `expenditures_distributions_details`
--

INSERT INTO `expenditures_distributions_details` (`id`, `expenditures_distribution_id`, `product_id`, `qty`, `price`, `created`, `modified`) VALUES
(11, 1, 1, '12.00', '12.00', '2018-05-08 10:47:55', '2018-05-08 10:47:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `expenditures_grants`
--

CREATE TABLE `expenditures_grants` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `given_to` varchar(225) NOT NULL COMMENT 'DIBERIKAN KEPADA',
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `expenditures_grants`
--

INSERT INTO `expenditures_grants` (`id`, `code`, `given_to`, `date`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(3, 'EG/2018/05/000001', 'sahal', '2018-05-07', 0, '2018-05-07 15:58:52', 1, '2018-05-08 10:51:19', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `expenditures_grants_details`
--

CREATE TABLE `expenditures_grants_details` (
  `id` int(11) NOT NULL,
  `expenditures_grant_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `price` decimal(22,2) DEFAULT '0.00',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `expenditures_grants_details`
--

INSERT INTO `expenditures_grants_details` (`id`, `expenditures_grant_id`, `product_id`, `qty`, `price`, `created`, `modified`) VALUES
(23, 3, 2, '8.00', '1000.00', '2018-05-08 10:51:19', '2018-05-08 10:51:19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `expenditures_others`
--

CREATE TABLE `expenditures_others` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `given_to` varchar(225) NOT NULL COMMENT 'DIBERIKAN KEPADA',
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `expenditures_others`
--

INSERT INTO `expenditures_others` (`id`, `code`, `given_to`, `date`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'EO/2018/05/000001', 'Muhamad Sahal', '2018-05-07', 0, '2018-05-07 11:00:12', 1, '2018-05-07 16:09:07', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `expenditures_others_details`
--

CREATE TABLE `expenditures_others_details` (
  `id` int(11) NOT NULL,
  `expenditures_other_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `price` decimal(22,2) DEFAULT '0.00',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `expenditures_others_details`
--

INSERT INTO `expenditures_others_details` (`id`, `expenditures_other_id`, `product_id`, `qty`, `price`, `created`, `modified`) VALUES
(10, 1, 1, '2.00', '2.00', '2018-05-07 16:09:07', '2018-05-07 16:09:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `expenditures_reclarifications`
--

CREATE TABLE `expenditures_reclarifications` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `expenditures_reclarifications`
--

INSERT INTO `expenditures_reclarifications` (`id`, `code`, `date`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'ER/2018/05/000001', '2018-05-07', 0, '2018-05-07 11:01:14', 1, '2018-05-07 16:37:15', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `expenditures_reclarifications_details`
--

CREATE TABLE `expenditures_reclarifications_details` (
  `id` int(11) NOT NULL,
  `expenditures_reclarification_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) DEFAULT '0',
  `price` decimal(22,2) DEFAULT '0.00',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `expenditures_reclarifications_details`
--

INSERT INTO `expenditures_reclarifications_details` (`id`, `expenditures_reclarification_id`, `product_id`, `qty`, `price`, `status`, `created`, `modified`) VALUES
(3, 1, 2, 2, '2.00', 0, '2018-05-07 16:37:15', '2018-05-07 16:37:15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `expenditures_transfers`
--

CREATE TABLE `expenditures_transfers` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `institution_id` int(11) NOT NULL COMMENT 'Pilih lembaga',
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `expenditures_transfers`
--

INSERT INTO `expenditures_transfers` (`id`, `code`, `institution_id`, `date`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'ET/2018/05/000001', 1, '2018-05-07', 0, '2018-05-07 16:37:42', 1, '2018-05-07 16:38:01', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `expenditures_transfers_details`
--

CREATE TABLE `expenditures_transfers_details` (
  `id` int(11) NOT NULL,
  `expenditures_transfer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `price` decimal(22,2) DEFAULT '0.00',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `expenditures_transfers_details`
--

INSERT INTO `expenditures_transfers_details` (`id`, `expenditures_transfer_id`, `product_id`, `qty`, `price`, `created`, `modified`) VALUES
(2, 1, 2, '3.00', '3.00', '2018-05-07 16:38:01', '2018-05-07 16:38:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `groups`
--

INSERT INTO `groups` (`id`, `name`, `status`, `created_by`, `created`, `modified_by`, `modified`) VALUES
(1, 'administrator', 1, NULL, '2018-02-05 08:03:19', NULL, '2018-02-05 08:03:19'),
(2, 'SPV', 0, 1, '2018-02-09 04:14:55', NULL, '2018-02-09 04:14:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `institutes`
--

CREATE TABLE `institutes` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `head_of_institute` varchar(225) NOT NULL,
  `head_of_institute_id` varchar(225) NOT NULL,
  `position_head_institute` varchar(225) NOT NULL,
  `code_lab` char(50) NOT NULL,
  `code_insititute` char(50) NOT NULL,
  `code_serial` char(50) NOT NULL,
  `time_up` decimal(22,2) NOT NULL,
  `unit_time_up` char(50) NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `institutes`
--

INSERT INTO `institutes` (`id`, `name`, `head_of_institute`, `head_of_institute_id`, `position_head_institute`, `code_lab`, `code_insititute`, `code_serial`, `time_up`, `unit_time_up`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'HEMATOLOGI', '1', '1', '1', '1', '1', '11', '1.00', '1', 1, '2018-04-25 00:00:00', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `institutions`
--

CREATE TABLE `institutions` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `name` varchar(225) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(225) NOT NULL,
  `pic_name` varchar(225) NOT NULL,
  `pic_phone` varchar(15) NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `institutions`
--

INSERT INTO `institutions` (`id`, `code`, `name`, `address`, `phone`, `pic_name`, `pic_phone`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, '0001', 'Penanggung', 'Bogor', '087899000', 'Gugu', '01090909', 0, '2018-04-25 11:44:54', 1, '2018-04-25 11:45:28', 1),
(2, '00021', 'Huut11', 'Bogor1', '0999909091', 'Yuuu1', '090808001', 1, '2018-04-25 11:50:35', 1, '2018-04-25 12:45:17', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `internal_orders`
--

CREATE TABLE `internal_orders` (
  `id` int(11) NOT NULL,
  `institute_id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `approve_date` datetime DEFAULT NULL,
  `approve_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `internal_orders`
--

INSERT INTO `internal_orders` (`id`, `institute_id`, `code`, `date`, `status`, `created`, `modified`, `created_by`, `modified_by`, `approve_date`, `approve_by`) VALUES
(2, 1, 'IO/2018/05/000001', '2018-05-08', 0, '2018-05-08 11:29:05', '2018-05-08 11:29:05', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `internal_orders_details`
--

CREATE TABLE `internal_orders_details` (
  `id` int(11) NOT NULL,
  `internal_order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `internal_orders_details`
--

INSERT INTO `internal_orders_details` (`id`, `internal_order_id`, `product_id`, `qty`, `created`, `modified`) VALUES
(7, 2, 2, '1.00', '2018-05-08 11:29:05', '2018-05-08 11:29:05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `name` varchar(225) NOT NULL,
  `unit` varchar(225) NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `products`
--

INSERT INTO `products` (`id`, `code`, `name`, `unit`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, '100021', 'CHOLATE', 'PCS', 1, '2018-04-24 02:30:00', 1, NULL, NULL),
(2, '21321', 'TABUNG', 'PCS', 1, '2018-04-24 02:30:00', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `purchase_orders`
--

CREATE TABLE `purchase_orders` (
  `id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `nomor_po` varchar(225) NOT NULL,
  `nomor_spk` varchar(225) NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `approve_date` datetime DEFAULT NULL,
  `approve_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `purchase_orders`
--

INSERT INTO `purchase_orders` (`id`, `supplier_id`, `nomor_po`, `nomor_spk`, `date`, `status`, `created`, `created_by`, `modified`, `modified_by`, `approve_date`, `approve_by`) VALUES
(2, 2, '1001', '1000', '2018-05-07', 0, '2018-05-07 11:28:03', 1, '2018-05-07 16:43:44', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `purchase_orders_details`
--

CREATE TABLE `purchase_orders_details` (
  `id` int(11) NOT NULL,
  `purchase_order_id` int(11) NOT NULL,
  `purchase_requests_detail_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `price` decimal(22,2) DEFAULT '0.00',
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `purchase_orders_details`
--

INSERT INTO `purchase_orders_details` (`id`, `purchase_order_id`, `purchase_requests_detail_id`, `product_id`, `qty`, `price`, `created`, `modified`) VALUES
(2, 2, 1, 1, '10.00', '1000.00', '2018-05-07 11:28:03', '2018-05-07 11:28:03'),
(4, 2, 3, 2, '2.00', '2.00', '2018-05-07 16:43:44', '2018-05-07 16:43:44');

-- --------------------------------------------------------

--
-- Struktur dari tabel `purchase_requests`
--

CREATE TABLE `purchase_requests` (
  `id` int(11) NOT NULL,
  `institute_id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `approve_date` datetime DEFAULT NULL,
  `approve_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `purchase_requests`
--

INSERT INTO `purchase_requests` (`id`, `institute_id`, `code`, `date`, `status`, `created`, `created_by`, `modified`, `modified_by`, `approve_date`, `approve_by`) VALUES
(1, 1, 'PR/2018/05/000001', '2018-05-07', 0, '2018-05-07 11:09:19', 1, '2018-05-07 16:39:20', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `purchase_requests_details`
--

CREATE TABLE `purchase_requests_details` (
  `id` int(11) NOT NULL,
  `purchase_request_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `purchase_requests_details`
--

INSERT INTO `purchase_requests_details` (`id`, `purchase_request_id`, `product_id`, `qty`, `status`, `created`, `modified`) VALUES
(3, 1, 2, '5.00', 0, '2018-05-07 16:39:20', '2018-05-07 16:39:20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `receipt_grants`
--

CREATE TABLE `receipt_grants` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `source` varchar(225) NOT NULL COMMENT 'SUMBER HIBAH',
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `receipt_grants`
--

INSERT INTO `receipt_grants` (`id`, `code`, `source`, `date`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'RG/2018/05/000001', 'Muhamad Sahal', '2018-05-07', 0, '2018-05-07 11:14:49', 1, '2018-05-08 10:51:57', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `receipt_grants_details`
--

CREATE TABLE `receipt_grants_details` (
  `id` int(11) NOT NULL,
  `receipt_grant_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `price` decimal(22,2) DEFAULT '0.00',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `receipt_grants_details`
--

INSERT INTO `receipt_grants_details` (`id`, `receipt_grant_id`, `product_id`, `qty`, `price`, `created`, `modified`) VALUES
(5, 1, 2, '10.00', '1000.00', '2018-05-08 10:51:57', '2018-05-08 10:51:57');

-- --------------------------------------------------------

--
-- Struktur dari tabel `receipt_others`
--

CREATE TABLE `receipt_others` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `source` varchar(225) NOT NULL COMMENT 'SUMBER HIBAH',
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `receipt_others`
--

INSERT INTO `receipt_others` (`id`, `code`, `source`, `date`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'RO/2018/05/000001', 'Muhamad Sahal', '2018-05-07', 0, '2018-05-07 11:12:16', 1, '2018-05-07 16:39:45', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `receipt_others_details`
--

CREATE TABLE `receipt_others_details` (
  `id` int(11) NOT NULL,
  `receipt_other_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `price` decimal(22,2) DEFAULT '0.00',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `receipt_others_details`
--

INSERT INTO `receipt_others_details` (`id`, `receipt_other_id`, `product_id`, `qty`, `price`, `created`, `modified`) VALUES
(3, 1, 2, '3.00', '3.00', '2018-05-07 16:39:45', '2018-05-07 16:39:45');

-- --------------------------------------------------------

--
-- Struktur dari tabel `receipt_purchases`
--

CREATE TABLE `receipt_purchases` (
  `id` int(11) NOT NULL,
  `purchase_order_id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `receipt_purchases`
--

INSERT INTO `receipt_purchases` (`id`, `purchase_order_id`, `code`, `date`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 2, 'RP/2018/05/000001', '2018-05-07', 0, '2018-05-07 11:30:08', 1, '2018-05-07 16:42:19', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `receipt_purchases_details`
--

CREATE TABLE `receipt_purchases_details` (
  `id` int(11) NOT NULL,
  `receipt_purchase_id` int(11) NOT NULL,
  `purchase_orders_detail_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `receipt_purchases_details`
--

INSERT INTO `receipt_purchases_details` (`id`, `receipt_purchase_id`, `purchase_orders_detail_id`, `product_id`, `qty`, `created`, `modified`) VALUES
(3, 1, 2, 1, '3.00', '2018-05-07 16:42:19', '2018-05-07 16:42:19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `receipt_reclarifications`
--

CREATE TABLE `receipt_reclarifications` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `expenditures_reclarification_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `receipt_reclarifications`
--

INSERT INTO `receipt_reclarifications` (`id`, `code`, `expenditures_reclarification_id`, `date`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'RT/2018/05/000001', 1, '2018-05-07', 0, '2018-05-07 11:32:56', 1, '2018-05-07 16:42:54', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `receipt_reclarifications_details`
--

CREATE TABLE `receipt_reclarifications_details` (
  `id` int(11) NOT NULL,
  `receipt_reclarification_id` int(11) NOT NULL,
  `expenditures_reclarifications_detail_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `price` decimal(22,2) DEFAULT '0.00',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `receipt_reclarifications_details`
--

INSERT INTO `receipt_reclarifications_details` (`id`, `receipt_reclarification_id`, `expenditures_reclarifications_detail_id`, `product_id`, `qty`, `price`, `created`, `modified`) VALUES
(1, 1, 1, 1, '10.00', '1000.00', '2018-05-07 11:32:56', '2018-05-07 11:32:56'),
(3, 1, 3, 2, '2.00', '1000.00', '2018-05-07 16:42:54', '2018-05-07 16:42:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `receipt_transfers`
--

CREATE TABLE `receipt_transfers` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `institution_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `receipt_transfers`
--

INSERT INTO `receipt_transfers` (`id`, `code`, `institution_id`, `date`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'RT/2018/05/000001', 1, '2018-05-07', 0, '2018-05-07 11:22:50', 1, '2018-05-07 16:41:40', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `receipt_transfers_details`
--

CREATE TABLE `receipt_transfers_details` (
  `id` int(11) NOT NULL,
  `receipt_transfer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `price` decimal(22,2) DEFAULT '0.00',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `receipt_transfers_details`
--

INSERT INTO `receipt_transfers_details` (`id`, `receipt_transfer_id`, `product_id`, `qty`, `price`, `created`, `modified`) VALUES
(4, 1, 2, '3.00', '3.00', '2018-05-07 16:41:40', '2018-05-07 16:41:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `stocks`
--

CREATE TABLE `stocks` (
  `id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `type` enum('IN','OUT') DEFAULT NULL,
  `ref_table` varchar(225) DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `price` decimal(22,2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `stocks`
--

INSERT INTO `stocks` (`id`, `date`, `type`, `ref_table`, `ref_id`, `product_id`, `qty`, `price`, `created`, `modified`) VALUES
(75, '2018-05-07', 'OUT', 'expenditures_distributions_details', 11, 1, 12, '12.00', '2018-05-08 10:47:55', '2018-05-08 10:47:55'),
(76, '2018-05-07', 'OUT', 'expenditures_grants_details', 23, 2, 8, '1000.00', '2018-05-08 10:51:19', '2018-05-08 10:51:19'),
(77, '2018-05-07', 'IN', 'receipt_grants_details', 5, 2, 10, '1000.00', '2018-05-08 10:51:57', '2018-05-08 10:51:57'),
(78, '2018-05-08', 'OUT', 'internal_orders_details', 7, 2, 1, NULL, '2018-05-08 11:29:05', '2018-05-08 11:29:05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(225) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `address`, `phone`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(2, 'DImas', '1', '1', 1, '2018-04-24 16:48:58', 1, '2018-04-26 13:27:36', 1),
(3, 'Lauren', '3', '3', 0, '2018-04-24 16:54:51', 1, '2018-04-26 13:27:26', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` char(60) NOT NULL,
  `name` varchar(225) DEFAULT NULL,
  `email` varchar(225) DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `email`, `group_id`, `status`, `created_by`, `created`, `modified_by`, `modified`) VALUES
(1, 'administrator', '$2y$10$FFIjh73z6y6Yq09cwqWAMuzwZ3eKp8XJ3jwU1ZKlR4Rg0BtV5eogy', 'administrator', 'administrator@app.com', 1, 1, 1, '2018-02-05 08:44:27', 1, '2018-02-06 11:06:20'),
(2, 'rezama', '$2y$10$aKyMf5zgu2JnbxSyAaEM/.sH53CE574SYAkKhobJpCnlfhaH62HEu', 'rezama ryan', 'rezam3213a@app.com', 2, 1, 1, '2018-02-08 10:38:03', 1, '2018-03-02 07:33:55'),
(4, 'SPV', '$2y$10$67VKrPYuvfpXTOD1pibrxOKbqrRUl//tIQUIqsP.s5cX.1psIJcJ6', 'spv', 'spv@app.com', 1, 1, 1, '2018-02-09 04:15:23', NULL, '2018-02-09 04:15:23');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `acl_phinxlog`
--
ALTER TABLE `acl_phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indeks untuk tabel `acos`
--
ALTER TABLE `acos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lft` (`lft`,`rght`),
  ADD KEY `alias` (`alias`);

--
-- Indeks untuk tabel `app_settings`
--
ALTER TABLE `app_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `aros`
--
ALTER TABLE `aros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lft` (`lft`,`rght`),
  ADD KEY `alias` (`alias`);

--
-- Indeks untuk tabel `aros_acos`
--
ALTER TABLE `aros_acos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `aro_id` (`aro_id`,`aco_id`),
  ADD KEY `aco_id` (`aco_id`);

--
-- Indeks untuk tabel `audit_logs`
--
ALTER TABLE `audit_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `expenditures_distributions`
--
ALTER TABLE `expenditures_distributions`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `expenditures_distributions_details`
--
ALTER TABLE `expenditures_distributions_details`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `expenditures_grants`
--
ALTER TABLE `expenditures_grants`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `expenditures_grants_details`
--
ALTER TABLE `expenditures_grants_details`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `expenditures_others`
--
ALTER TABLE `expenditures_others`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `expenditures_others_details`
--
ALTER TABLE `expenditures_others_details`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `expenditures_reclarifications`
--
ALTER TABLE `expenditures_reclarifications`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `expenditures_reclarifications_details`
--
ALTER TABLE `expenditures_reclarifications_details`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `expenditures_transfers`
--
ALTER TABLE `expenditures_transfers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `expenditures_transfers_details`
--
ALTER TABLE `expenditures_transfers_details`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `institutes`
--
ALTER TABLE `institutes`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `institutions`
--
ALTER TABLE `institutions`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `internal_orders`
--
ALTER TABLE `internal_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `internal_orders_details`
--
ALTER TABLE `internal_orders_details`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `purchase_orders`
--
ALTER TABLE `purchase_orders`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `purchase_orders_details`
--
ALTER TABLE `purchase_orders_details`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `purchase_requests`
--
ALTER TABLE `purchase_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `purchase_requests_details`
--
ALTER TABLE `purchase_requests_details`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `receipt_grants`
--
ALTER TABLE `receipt_grants`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `receipt_grants_details`
--
ALTER TABLE `receipt_grants_details`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `receipt_others`
--
ALTER TABLE `receipt_others`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `receipt_others_details`
--
ALTER TABLE `receipt_others_details`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `receipt_purchases`
--
ALTER TABLE `receipt_purchases`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `receipt_purchases_details`
--
ALTER TABLE `receipt_purchases_details`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `receipt_reclarifications`
--
ALTER TABLE `receipt_reclarifications`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `receipt_reclarifications_details`
--
ALTER TABLE `receipt_reclarifications_details`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `receipt_transfers`
--
ALTER TABLE `receipt_transfers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `receipt_transfers_details`
--
ALTER TABLE `receipt_transfers_details`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `username` (`username`) USING BTREE;

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `acos`
--
ALTER TABLE `acos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=216;

--
-- AUTO_INCREMENT untuk tabel `app_settings`
--
ALTER TABLE `app_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `aros`
--
ALTER TABLE `aros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `aros_acos`
--
ALTER TABLE `aros_acos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=446;

--
-- AUTO_INCREMENT untuk tabel `audit_logs`
--
ALTER TABLE `audit_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `expenditures_distributions`
--
ALTER TABLE `expenditures_distributions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `expenditures_distributions_details`
--
ALTER TABLE `expenditures_distributions_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `expenditures_grants`
--
ALTER TABLE `expenditures_grants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `expenditures_grants_details`
--
ALTER TABLE `expenditures_grants_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `expenditures_others`
--
ALTER TABLE `expenditures_others`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `expenditures_others_details`
--
ALTER TABLE `expenditures_others_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `expenditures_reclarifications`
--
ALTER TABLE `expenditures_reclarifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `expenditures_reclarifications_details`
--
ALTER TABLE `expenditures_reclarifications_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `expenditures_transfers`
--
ALTER TABLE `expenditures_transfers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `expenditures_transfers_details`
--
ALTER TABLE `expenditures_transfers_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `institutes`
--
ALTER TABLE `institutes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `institutions`
--
ALTER TABLE `institutions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `internal_orders`
--
ALTER TABLE `internal_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `internal_orders_details`
--
ALTER TABLE `internal_orders_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `purchase_orders`
--
ALTER TABLE `purchase_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `purchase_orders_details`
--
ALTER TABLE `purchase_orders_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `purchase_requests`
--
ALTER TABLE `purchase_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `purchase_requests_details`
--
ALTER TABLE `purchase_requests_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `receipt_grants`
--
ALTER TABLE `receipt_grants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `receipt_grants_details`
--
ALTER TABLE `receipt_grants_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `receipt_others`
--
ALTER TABLE `receipt_others`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `receipt_others_details`
--
ALTER TABLE `receipt_others_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `receipt_purchases`
--
ALTER TABLE `receipt_purchases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `receipt_purchases_details`
--
ALTER TABLE `receipt_purchases_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `receipt_reclarifications`
--
ALTER TABLE `receipt_reclarifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `receipt_reclarifications_details`
--
ALTER TABLE `receipt_reclarifications_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `receipt_transfers`
--
ALTER TABLE `receipt_transfers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `receipt_transfers_details`
--
ALTER TABLE `receipt_transfers_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
