-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 08, 2018 at 10:20 PM
-- Server version: 10.0.31-MariaDB-0ubuntu0.16.04.2
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zmc_era`
--

-- --------------------------------------------------------

--
-- Table structure for table `outsourcing_invoice`
--

DROP TABLE IF EXISTS `outsourcing_invoice`;
CREATE TABLE `outsourcing_invoice` (
  `id` int(11) NOT NULL,
  `invoice` varchar(100) DEFAULT NULL,
  `outsourcing_ms_perusahaan_id` int(11) DEFAULT NULL,
  `bulan` tinyint(4) DEFAULT NULL,
  `tahun` smallint(6) DEFAULT NULL,
  `gaji_pokok` decimal(20,2) DEFAULT NULL,
  `bpjs` decimal(20,2) DEFAULT NULL,
  `lembur_nasional` decimal(20,2) DEFAULT NULL,
  `jumlah` decimal(20,2) DEFAULT NULL,
  `lembur_bebas` decimal(20,2) DEFAULT NULL,
  `uang_makan` decimal(20,2) DEFAULT NULL,
  `fee` decimal(20,2) DEFAULT NULL,
  `jumlah2` decimal(20,2) DEFAULT NULL,
  `ppn` decimal(20,2) DEFAULT NULL,
  `total1` decimal(20,2) DEFAULT NULL,
  `pph23` decimal(20,2) DEFAULT NULL,
  `total2` decimal(20,2) DEFAULT NULL,
  `status` enum('0','1') DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `outsourcing_invoice`
--

INSERT INTO `outsourcing_invoice` (`id`, `invoice`, `outsourcing_ms_perusahaan_id`, `bulan`, `tahun`, `gaji_pokok`, `bpjs`, `lembur_nasional`, `jumlah`, `lembur_bebas`, `uang_makan`, `fee`, `jumlah2`, `ppn`, `total1`, `pph23`, `total2`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(10, 'ERP1712126564', 13, 12, 2017, '3355750.00', '0.00', '0.00', '3355750.00', '0.00', '0.00', '335575.00', '3691325.00', '33557.00', '3724882.00', '6711.00', '3718171.00', '0', '2017-12-12 11:23:49', 19, '2017-12-12 04:23:49', NULL),
(11, 'ERP1712126030', 13, 12, 2017, '3355750.00', '0.00', '0.00', '3355750.00', '0.00', '0.00', '335575.00', '3691325.00', '33557.00', '3724882.00', '6711.00', '3718171.00', '0', '2017-12-12 11:26:52', 19, '2017-12-12 04:26:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `outsourcing_invoice_detail`
--

DROP TABLE IF EXISTS `outsourcing_invoice_detail`;
CREATE TABLE `outsourcing_invoice_detail` (
  `id` int(11) NOT NULL,
  `outsourcing_invoice_id` int(11) DEFAULT NULL,
  `outsourcing_ms_pegawai_nik` varchar(50) DEFAULT NULL,
  `outsourcing_ms_fungsional_id` int(11) DEFAULT NULL,
  `outsourcing_ms_list_fungsional_id` int(11) DEFAULT NULL,
  `gaji_pokok` decimal(20,2) DEFAULT NULL,
  `bpjs` decimal(20,2) DEFAULT NULL,
  `lembur_nasional` decimal(20,2) DEFAULT NULL,
  `jumlah` decimal(20,2) DEFAULT NULL,
  `lembur_bebas` decimal(20,2) DEFAULT NULL,
  `uang_makan` decimal(20,2) DEFAULT NULL,
  `fee` decimal(20,2) DEFAULT NULL,
  `jumlah2` decimal(20,2) DEFAULT NULL,
  `ppn` decimal(20,2) DEFAULT NULL,
  `total1` decimal(20,2) DEFAULT NULL,
  `pph23` decimal(20,2) DEFAULT NULL,
  `total2` decimal(20,2) DEFAULT NULL,
  `status` enum('0','1') DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `outsourcing_invoice_detail`
--

INSERT INTO `outsourcing_invoice_detail` (`id`, `outsourcing_invoice_id`, `outsourcing_ms_pegawai_nik`, `outsourcing_ms_fungsional_id`, `outsourcing_ms_list_fungsional_id`, `gaji_pokok`, `bpjs`, `lembur_nasional`, `jumlah`, `lembur_bebas`, `uang_makan`, `fee`, `jumlah2`, `ppn`, `total1`, `pph23`, `total2`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(20, 10, 'EK17121212', 1, 2, '3355750.00', '0.00', '0.00', '3355750.00', '0.00', '0.00', '335575.00', '3691325.00', '33558.00', '3724883.00', '6712.00', '3718171.00', '0', '2017-12-12 11:23:49', NULL, '2017-12-12 04:23:49', NULL),
(21, 11, 'EK17121212', 1, 2, '3355750.00', '0.00', '0.00', '3355750.00', '0.00', '0.00', '335575.00', '3691325.00', '33558.00', '3724883.00', '6712.00', '3718171.00', '0', '2017-12-12 11:26:52', NULL, '2017-12-12 04:26:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `outsourcing_invoice_fungsional`
--

DROP TABLE IF EXISTS `outsourcing_invoice_fungsional`;
CREATE TABLE `outsourcing_invoice_fungsional` (
  `id` int(11) NOT NULL,
  `outsourcing_invoice_id` int(11) DEFAULT NULL,
  `outsourcing_ms_fungsional_id` int(11) DEFAULT NULL,
  `outsourcing_ms_list_fungsional_id` int(11) DEFAULT NULL,
  `gaji_pokok` decimal(20,2) DEFAULT NULL,
  `bpjs` decimal(20,2) DEFAULT NULL,
  `lembur_nasional` decimal(20,2) DEFAULT NULL,
  `jumlah` decimal(20,2) DEFAULT NULL,
  `lembur_bebas` decimal(20,2) DEFAULT NULL,
  `uang_makan` decimal(20,2) DEFAULT NULL,
  `fee` decimal(20,2) DEFAULT NULL,
  `jumlah2` decimal(20,2) DEFAULT NULL,
  `ppn` decimal(20,2) DEFAULT NULL,
  `total1` decimal(20,2) DEFAULT NULL,
  `pph23` decimal(20,2) DEFAULT NULL,
  `total2` decimal(20,2) DEFAULT NULL,
  `status` enum('0','1') DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `outsourcing_inv_pembayaran_gaji`
--

DROP TABLE IF EXISTS `outsourcing_inv_pembayaran_gaji`;
CREATE TABLE `outsourcing_inv_pembayaran_gaji` (
  `id` int(11) NOT NULL,
  `outsourcing_ms_perusahaan_id` int(11) DEFAULT NULL,
  `jumlah_pegawai` smallint(6) DEFAULT NULL,
  `gapok` decimal(20,2) DEFAULT NULL,
  `tunj_pengelolaan` decimal(20,2) DEFAULT NULL,
  `tunj_jabatan` decimal(20,2) DEFAULT NULL,
  `tunj_insentif` decimal(20,2) DEFAULT NULL,
  `tunj_lain` decimal(20,2) DEFAULT NULL,
  `jamsostek` decimal(20,2) DEFAULT NULL,
  `hutang_jamsostek` decimal(20,2) DEFAULT NULL,
  `potongan_kmg` decimal(20,2) DEFAULT NULL,
  `bunga_kmg` decimal(20,2) DEFAULT NULL,
  `potongan_koperasi` decimal(20,2) DEFAULT NULL,
  `potongan_lain` decimal(20,2) DEFAULT NULL,
  `potongan_absen` decimal(20,2) DEFAULT NULL,
  `total_potongan` decimal(20,2) DEFAULT NULL,
  `pph21` decimal(20,2) DEFAULT NULL,
  `total` decimal(20,2) DEFAULT NULL,
  `status` enum('0','1') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `outsourcing_inv_pembayaran_gaji_detail`
--

DROP TABLE IF EXISTS `outsourcing_inv_pembayaran_gaji_detail`;
CREATE TABLE `outsourcing_inv_pembayaran_gaji_detail` (
  `id` int(11) NOT NULL,
  `outsourcing_inv_pembayaran_gaji_id` int(11) DEFAULT NULL,
  `outsourcing_ms_fungsional_id` int(11) DEFAULT NULL,
  `jumlah` smallint(6) DEFAULT NULL,
  `gapok` decimal(20,2) DEFAULT NULL,
  `tunj_pengelolaan` decimal(20,2) DEFAULT NULL,
  `tunj_jabatan` decimal(20,2) DEFAULT NULL,
  `tunj_insentif` decimal(20,2) DEFAULT NULL,
  `tunj_lain` decimal(20,2) DEFAULT NULL,
  `jamsostek` decimal(20,2) DEFAULT NULL,
  `hutang_jamsostek` decimal(20,2) DEFAULT NULL,
  `potongan_kmg` decimal(20,2) DEFAULT NULL,
  `bunga_kmg` decimal(20,2) DEFAULT NULL,
  `potongan_koperasi` decimal(20,2) DEFAULT NULL,
  `potongan_lain` decimal(20,2) DEFAULT NULL,
  `potongan_absen` decimal(20,2) DEFAULT NULL,
  `total_potongan` decimal(20,2) DEFAULT NULL,
  `pph21` decimal(20,2) DEFAULT NULL,
  `total` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `outsourcing_inv_pembayaran_gaji_pegawai`
--

DROP TABLE IF EXISTS `outsourcing_inv_pembayaran_gaji_pegawai`;
CREATE TABLE `outsourcing_inv_pembayaran_gaji_pegawai` (
  `id` int(11) NOT NULL,
  `outsourcing_inv_pembayaran_gaji_id` int(11) DEFAULT NULL,
  `outsourcing_ms_pegawai_nik` varchar(50) DEFAULT NULL,
  `gapok` decimal(20,2) DEFAULT NULL,
  `tunj_pengelolaan` decimal(20,2) DEFAULT NULL,
  `tunj_jabatan` decimal(20,2) DEFAULT NULL,
  `tunj_insentif` decimal(20,2) DEFAULT NULL,
  `tunj_lain` decimal(20,2) DEFAULT NULL,
  `jamsostek` decimal(20,2) DEFAULT NULL,
  `hutang_jamsostek` decimal(20,2) DEFAULT NULL,
  `potongan_kmg` decimal(20,2) DEFAULT NULL,
  `bunga_kmg` decimal(20,2) DEFAULT NULL,
  `potongan_koperasi` decimal(20,2) DEFAULT NULL,
  `potongan_lain` decimal(20,2) DEFAULT NULL,
  `potongan_absen` decimal(20,2) DEFAULT NULL,
  `pph21` decimal(20,2) DEFAULT NULL,
  `total` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `outsourcing_inv_pembayaran_lembur`
--

DROP TABLE IF EXISTS `outsourcing_inv_pembayaran_lembur`;
CREATE TABLE `outsourcing_inv_pembayaran_lembur` (
  `id` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `outsourcing_ms_perusahaan_id` int(11) DEFAULT NULL,
  `lembur` decimal(20,2) DEFAULT NULL,
  `uang_makan` decimal(20,2) DEFAULT NULL,
  `skpd` decimal(20,2) DEFAULT NULL,
  `total` decimal(20,2) DEFAULT NULL,
  `keterangan` enum('cash','transfer') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `outsourcing_inv_pembayaran_lembur`
--

INSERT INTO `outsourcing_inv_pembayaran_lembur` (`id`, `tanggal`, `outsourcing_ms_perusahaan_id`, `lembur`, `uang_makan`, `skpd`, `total`, `keterangan`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(8, '2017-12-18', 13, '0.00', '0.00', '100000.00', '100000.00', 'cash', '2017-12-11 23:37:55', 2, '2017-12-11 16:37:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `outsourcing_inv_pembayaran_lembur_detail`
--

DROP TABLE IF EXISTS `outsourcing_inv_pembayaran_lembur_detail`;
CREATE TABLE `outsourcing_inv_pembayaran_lembur_detail` (
  `id` int(11) NOT NULL,
  `outsourcing_inv_pembayaran_lembur_id` int(11) DEFAULT NULL,
  `outsourcing_ms_pegawai_nik` varchar(50) DEFAULT NULL,
  `lembur` decimal(20,2) DEFAULT NULL,
  `uang_makan` decimal(20,2) DEFAULT NULL,
  `skpd` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `outsourcing_inv_pembayaran_lembur_detail`
--

INSERT INTO `outsourcing_inv_pembayaran_lembur_detail` (`id`, `outsourcing_inv_pembayaran_lembur_id`, `outsourcing_ms_pegawai_nik`, `lembur`, `uang_makan`, `skpd`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(8, 8, 'EK17121212', NULL, '0.00', NULL, '2017-12-11 23:37:55', NULL, '2017-12-11 16:37:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `outsourcing_inv_pembayaran_lembur_fungsional`
--

DROP TABLE IF EXISTS `outsourcing_inv_pembayaran_lembur_fungsional`;
CREATE TABLE `outsourcing_inv_pembayaran_lembur_fungsional` (
  `id` int(11) NOT NULL,
  `outsourcing_inv_pembayaran_lembur_id` int(11) DEFAULT NULL,
  `outsourcing_ms_fungsional_id` int(11) DEFAULT NULL,
  `lembur` decimal(20,2) DEFAULT NULL,
  `uang_makan` decimal(20,2) DEFAULT NULL,
  `skpd` decimal(20,2) DEFAULT NULL,
  `total` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `outsourcing_inv_pembayaran_thr`
--

DROP TABLE IF EXISTS `outsourcing_inv_pembayaran_thr`;
CREATE TABLE `outsourcing_inv_pembayaran_thr` (
  `id` int(11) NOT NULL,
  `outsourcing_ms_perusahaan_id` int(11) DEFAULT NULL,
  `total_thr` decimal(20,2) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `created_by` int(255) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `outsourcing_inv_pembayaran_thr`
--

INSERT INTO `outsourcing_inv_pembayaran_thr` (`id`, `outsourcing_ms_perusahaan_id`, `total_thr`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(9, 13, '838937.00', '2017-12-11', 2, '2017-12-11 16:41:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `outsourcing_inv_pembayaran_thr_detail`
--

DROP TABLE IF EXISTS `outsourcing_inv_pembayaran_thr_detail`;
CREATE TABLE `outsourcing_inv_pembayaran_thr_detail` (
  `id` int(11) NOT NULL,
  `outsourcing_inv_pembayaran_thr_id` int(11) DEFAULT NULL,
  `outsourcing_ms_pegawai_nik` varchar(50) DEFAULT NULL,
  `total_thr` decimal(20,2) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `created_by` int(255) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `outsourcing_inv_pembayaran_thr_detail`
--

INSERT INTO `outsourcing_inv_pembayaran_thr_detail` (`id`, `outsourcing_inv_pembayaran_thr_id`, `outsourcing_ms_pegawai_nik`, `total_thr`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(16, 9, 'EK17121212', '838938.00', '2017-12-11', NULL, '2017-12-11 16:41:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `outsourcing_inv_pembayaran_thr_fungsional`
--

DROP TABLE IF EXISTS `outsourcing_inv_pembayaran_thr_fungsional`;
CREATE TABLE `outsourcing_inv_pembayaran_thr_fungsional` (
  `id` int(11) NOT NULL,
  `outsourcing_inv_pembayaran_thr_id` int(11) DEFAULT NULL,
  `outsourcing_ms_fungsional_id` int(11) DEFAULT NULL,
  `total_thr` decimal(20,2) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `created_by` int(255) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `outsourcing_inv_pembayaran_tunjangan_pakaian`
--

DROP TABLE IF EXISTS `outsourcing_inv_pembayaran_tunjangan_pakaian`;
CREATE TABLE `outsourcing_inv_pembayaran_tunjangan_pakaian` (
  `id` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `outsourcing_ms_perusahaan_id` int(11) DEFAULT NULL,
  `total` decimal(20,2) DEFAULT NULL,
  `keterangan` enum('cash','transfer') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `outsourcing_inv_pembayaran_tunjangan_pakaian_fungsional`
--

DROP TABLE IF EXISTS `outsourcing_inv_pembayaran_tunjangan_pakaian_fungsional`;
CREATE TABLE `outsourcing_inv_pembayaran_tunjangan_pakaian_fungsional` (
  `id` int(11) NOT NULL,
  `outsourcing_inv_pembayaran_tunjangan_pakaian_id` int(11) DEFAULT NULL,
  `outsourcing_ms_fungsional_id` int(11) DEFAULT NULL,
  `total` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `outsourcing_invoice`
--
ALTER TABLE `outsourcing_invoice`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `outsourcing_invoice_detail`
--
ALTER TABLE `outsourcing_invoice_detail`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `outsourcing_invoice_fungsional`
--
ALTER TABLE `outsourcing_invoice_fungsional`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `outsourcing_inv_pembayaran_gaji`
--
ALTER TABLE `outsourcing_inv_pembayaran_gaji`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `outsourcing_inv_pembayaran_gaji_detail`
--
ALTER TABLE `outsourcing_inv_pembayaran_gaji_detail`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `outsourcing_inv_pembayaran_gaji_pegawai`
--
ALTER TABLE `outsourcing_inv_pembayaran_gaji_pegawai`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `outsourcing_inv_pembayaran_lembur`
--
ALTER TABLE `outsourcing_inv_pembayaran_lembur`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `outsourcing_inv_pembayaran_lembur_detail`
--
ALTER TABLE `outsourcing_inv_pembayaran_lembur_detail`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `outsourcing_inv_pembayaran_lembur_fungsional`
--
ALTER TABLE `outsourcing_inv_pembayaran_lembur_fungsional`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `outsourcing_inv_pembayaran_thr`
--
ALTER TABLE `outsourcing_inv_pembayaran_thr`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `outsourcing_inv_pembayaran_thr_detail`
--
ALTER TABLE `outsourcing_inv_pembayaran_thr_detail`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `outsourcing_inv_pembayaran_thr_fungsional`
--
ALTER TABLE `outsourcing_inv_pembayaran_thr_fungsional`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `outsourcing_inv_pembayaran_tunjangan_pakaian`
--
ALTER TABLE `outsourcing_inv_pembayaran_tunjangan_pakaian`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `outsourcing_inv_pembayaran_tunjangan_pakaian_fungsional`
--
ALTER TABLE `outsourcing_inv_pembayaran_tunjangan_pakaian_fungsional`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `outsourcing_invoice`
--
ALTER TABLE `outsourcing_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `outsourcing_invoice_detail`
--
ALTER TABLE `outsourcing_invoice_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `outsourcing_invoice_fungsional`
--
ALTER TABLE `outsourcing_invoice_fungsional`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `outsourcing_inv_pembayaran_gaji`
--
ALTER TABLE `outsourcing_inv_pembayaran_gaji`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `outsourcing_inv_pembayaran_gaji_detail`
--
ALTER TABLE `outsourcing_inv_pembayaran_gaji_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;
--
-- AUTO_INCREMENT for table `outsourcing_inv_pembayaran_gaji_pegawai`
--
ALTER TABLE `outsourcing_inv_pembayaran_gaji_pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=252;
--
-- AUTO_INCREMENT for table `outsourcing_inv_pembayaran_lembur`
--
ALTER TABLE `outsourcing_inv_pembayaran_lembur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `outsourcing_inv_pembayaran_lembur_detail`
--
ALTER TABLE `outsourcing_inv_pembayaran_lembur_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `outsourcing_inv_pembayaran_lembur_fungsional`
--
ALTER TABLE `outsourcing_inv_pembayaran_lembur_fungsional`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `outsourcing_inv_pembayaran_thr`
--
ALTER TABLE `outsourcing_inv_pembayaran_thr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `outsourcing_inv_pembayaran_thr_detail`
--
ALTER TABLE `outsourcing_inv_pembayaran_thr_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `outsourcing_inv_pembayaran_thr_fungsional`
--
ALTER TABLE `outsourcing_inv_pembayaran_thr_fungsional`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `outsourcing_inv_pembayaran_tunjangan_pakaian`
--
ALTER TABLE `outsourcing_inv_pembayaran_tunjangan_pakaian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `outsourcing_inv_pembayaran_tunjangan_pakaian_fungsional`
--
ALTER TABLE `outsourcing_inv_pembayaran_tunjangan_pakaian_fungsional`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
