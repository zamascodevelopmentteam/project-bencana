-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 09, 2018 at 03:32 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `era_v2`
--

--
-- Dumping data for table `sub_modules`
--

INSERT INTO `sub_modules` (`id`, `module_id`, `sub_module_id`, `name`, `display_name`, `description`, `icon`, `created_at`, `updated_at`) VALUES
(800000, 8, 0, 'Master', 'Master', '', 'fa fa-book', '2017-10-12 18:21:30', '0000-00-00 00:00:00'),
(800001, 8, 800000, 'coa', 'Code of Accounting', '', '', '2017-10-12 18:23:13', '0000-00-00 00:00:00'),
(800002, 8, 0, 'Rental', 'Rental', '', 'fa fa-car', '2017-10-12 18:24:20', '0000-00-00 00:00:00'),
(800003, 8, 800002, 'dp rental', 'Pembayaran DP Rental', '', '', '2017-10-12 18:24:35', '0000-00-00 00:00:00'),
(800004, 8, 800002, 'asuransi rental', 'Pembayaran Asuransi Rental', '', '', '2017-10-12 18:24:55', '0000-00-00 00:00:00'),
(800005, 8, 800002, 'cash rental', 'Pembayaran Rental Tunai', '', '', '2017-10-12 18:25:34', '0000-00-00 00:00:00'),
(800006, 8, 800002, 'pengakuan hutang rental', 'Pengakuan Hutang Rental', '', '', '2017-10-12 18:26:35', '0000-00-00 00:00:00'),
(800007, 8, 0, 'budgeting', 'Budgeting / Anggaran', '', 'fa fa-asterisk', '2017-10-18 16:15:54', '0000-00-00 00:00:00'),
(800008, 8, 800007, 'pengajuan anggaran', 'Pengajuan Anggaran', '', '', '2017-10-18 16:16:00', '0000-00-00 00:00:00'),
(800009, 8, 800007, 'pergeseran anggaran', 'Pergeseran Anggaran', '', '', '2017-10-18 16:16:37', '0000-00-00 00:00:00'),
(800010, 8, 0, 'sistem kasir', 'Sistem Kasir', '', 'fa fa-dollar', '2017-10-18 16:17:25', '0000-00-00 00:00:00'),
(800011, 8, 800010, 'pemasukan', 'Pemasukan', '', '', '2017-10-18 16:18:04', '0000-00-00 00:00:00'),
(800012, 8, 800010, 'pengeluaran', 'Pengeluaran', '', '', '2017-10-18 16:18:13', '0000-00-00 00:00:00'),
(800013, 8, 800010, 'pencairan ump', 'Pencairan UMP', '', '', '2017-10-18 16:18:24', '0000-00-00 00:00:00'),
(800014, 8, 800010, 'pencairan spoa sopa', 'Pencairan SOPA / SPOA', '', '', '2017-10-18 16:18:56', '0000-00-00 00:00:00'),
(800015, 8, 0, 'view jurnal', 'Daftar Jurnal', '', 'fa fa-file', '2017-10-18 16:35:14', '0000-00-00 00:00:00'),
(800016, 8, 800002, 'invoice rental', 'Penerimaan Invoice Rental', '', '', '2017-10-23 03:13:41', '0000-00-00 00:00:00'),
(800017, 8, 800002, 'penyusutan rental', 'Penyusutan Kendaraan Rental', '', '', '2017-10-23 07:25:38', '0000-00-00 00:00:00'),
(800018, 8, 800002, 'penghapusan rental', 'Penghapusan Asset', '', '', '2017-10-26 02:56:00', '0000-00-00 00:00:00'),
(800019, 8, 800002, 'penjualan rental', 'Penjualan Asset Rental', '', '', '2017-10-26 03:57:37', '0000-00-00 00:00:00'),
(800020, 8, 800002, 'pengalihan rental', 'Pengalihan Rental', '', '', '2017-10-26 07:10:16', '0000-00-00 00:00:00'),
(800021, 8, 800002, 'kredit rental', 'Kredit Asset Rental', '', '', '2017-10-26 09:36:07', '0000-00-00 00:00:00'),
(800022, 8, 0, 'ekspedisi', 'Ekspedisi', '', 'fa fa-car', '2017-10-31 05:50:10', '0000-00-00 00:00:00'),
(800023, 8, 800022, 'dp ekspedisi', 'Pembayaran DP Kendaraan Ekspedisi', '', '', '2017-10-31 05:50:29', '0000-00-00 00:00:00'),
(800024, 8, 800022, 'cash ekspedisi', 'Pembayaran Tunai Kendaraan Ekspedisi ', '', '', '2017-10-31 06:00:25', '0000-00-00 00:00:00'),
(800025, 8, 800022, 'pengakuan hutang ekspedisi', 'Pengakuan Hutang Kendaraan Ekspedisi', '', '', '2017-10-31 06:01:38', '0000-00-00 00:00:00'),
(800026, 8, 800022, 'asuransi ekspedisi', 'Pembayaran Asuransi Kendaraan Ekspedisi ', '', '', '2017-10-31 07:27:10', '0000-00-00 00:00:00'),
(800027, 8, 800022, 'penyusutan ekspedisi', 'Penyusutan Kendaraan', '', '', '2017-10-31 07:28:15', '0000-00-00 00:00:00'),
(800028, 8, 800022, 'penjualan ekspedisi', 'Penjualan Kendaraan', '', '', '2017-10-31 07:28:35', '0000-00-00 00:00:00'),
(800029, 8, 800022, 'penghapusan ekspedisi', 'Penghapusan Asset', '', '', '2017-10-31 07:30:58', '0000-00-00 00:00:00'),
(800030, 8, 0, 'Kendaraan Dinas', 'Kendaraan Dinas', '', 'fa fa-car', '2017-10-31 07:33:54', '0000-00-00 00:00:00'),
(800031, 8, 800030, 'penyusutan dinas', 'Penyusutan Kendaraan', '', '', '2017-10-31 07:34:47', '0000-00-00 00:00:00'),
(800032, 8, 800030, 'penjualan dinas', 'Penjualan Kendaraan Dinas', '', '', '2017-10-31 07:35:15', '0000-00-00 00:00:00'),
(800033, 8, 800030, 'penghapusan dinas', 'Penghapusan kendaraan Dinas', '', '', '2017-10-31 07:35:40', '0000-00-00 00:00:00'),
(800034, 8, 800030, 'kredit dinas', 'Penjualan Kredit Kendaraan Dinas', '', '', '2017-10-31 07:36:20', '0000-00-00 00:00:00'),
(800035, 8, 0, 'SDM', 'SDM', '', 'fa fa-users', '2017-11-02 01:50:24', '0000-00-00 00:00:00'),
(800036, 8, 800035, 'pembayaran gaji', 'Pembayaran Gaji', '', '', '2017-11-02 01:50:42', '0000-00-00 00:00:00'),
(800037, 8, 800035, 'pembayaran lembur', 'Pembayaran Lembur', '', '', '2017-11-02 01:51:10', '0000-00-00 00:00:00'),
(800038, 8, 800035, 'pembayaran sppd', 'Pembayaran SPPD', '', '', '2017-11-02 01:51:56', '0000-00-00 00:00:00'),
(800039, 8, 800035, 'pembayaran thr', 'Pembayaran THR', '', '', '2017-11-02 01:52:20', '0000-00-00 00:00:00'),
(800040, 8, 800035, 'pembayaran bonus', 'Pembayaran Bonus', '', '', '2017-11-02 02:06:06', '0000-00-00 00:00:00'),
(800041, 8, 800035, 'pembayaran pakaian', 'Pembayaran Tunjangan Pakaian', '', '', '2017-11-02 02:06:35', '0000-00-00 00:00:00'),
(800042, 8, 800035, 'pembayaran cuti', 'Pembayaran Tunjangan Cuti', '', '', '2017-11-02 02:07:18', '0000-00-00 00:00:00'),
(800043, 8, 800022, 'kredit ekspedisi', 'Penjualan Kredit Kendaraan', '', '', '2017-11-20 04:39:11', '0000-00-00 00:00:00'),
(800044, 8, 800000, 'ms bagian', 'COA Bagian SDM', '', '', '2017-11-21 00:54:06', '0000-00-00 00:00:00'),
(800045, 8, 800010, 'verifikasi ump', 'Verfikasi Penyelesaian UMP', '', '', '2017-11-28 02:22:44', '0000-00-00 00:00:00'),
(800046, 8, 0, 'UMUM', 'UMUM', '', 'fa fa-cubes', '2017-11-29 03:02:23', '0000-00-00 00:00:00'),
(800047, 8, 800046, 'asset inventaris', 'Data Inventaris', '', '', '2017-11-29 03:03:00', '0000-00-00 00:00:00'),
(800048, 8, 800046, 'penyusutan inventaris', 'Penyusutan Inventaris', '', '', '2017-11-29 03:03:17', '0000-00-00 00:00:00'),
(800049, 8, 0, 'pencairan kredit', 'Pencairan Kredit', '', 'fa fa-credit-card', '2017-11-30 02:59:31', '0000-00-00 00:00:00'),
(800050, 8, 800049, 'kredit pendanaan', 'Kredit Pendanaan', '', '', '2017-11-30 02:59:57', '0000-00-00 00:00:00'),
(800051, 8, 800049, 'kredit kendaraan', 'Kredit Kendaraan', '', '', '2017-11-30 03:00:22', '0000-00-00 00:00:00'),
(800052, 8, 800049, 'kredit property', 'Kredit Property', '', '', '2017-11-30 03:19:47', '0000-00-00 00:00:00'),
(800053, 8, 0, 'Usaha Lain', 'Usaha Lain', '', 'fa fa-briefcase', '2017-11-30 03:22:20', '0000-00-00 00:00:00'),
(800054, 8, 800053, 'usla so', 'Penerimaan Tagihan SO', '', '', '2017-11-30 03:22:47', '0000-00-00 00:00:00'),
(800055, 8, 800053, 'usla fotocopy', 'Penerimaan Tagihan Foto Copy', '', '', '2017-11-30 03:23:06', '0000-00-00 00:00:00'),
(800056, 8, 0, 'OutSourcing', 'OutSourcing', '', 'fa fa-users', '2017-11-30 03:23:36', '0000-00-00 00:00:00'),
(800057, 8, 800056, 'outsourcing gaji', 'Pembayaran Gaji', '', '', '2017-11-30 03:24:19', '0000-00-00 00:00:00'),
(800058, 8, 800056, 'outsourcing tunj lain', 'Pembayaran Lembur, Uang Makan dan SKPD', '', '', '2017-11-30 03:24:35', '0000-00-00 00:00:00'),
(800059, 8, 800056, 'outsourcing thr', 'Pembayaran THR', '', '', '2017-11-30 03:26:34', '0000-00-00 00:00:00'),
(800060, 8, 800056, 'outsourcing pakaian', 'Pembayaran Tunj Pakaian', '', '', '2017-11-30 03:27:26', '0000-00-00 00:00:00'),
(800061, 8, 800056, 'outsorcing invoice', 'Penerimaan Invoice', '', '', '2017-11-30 03:27:47', '0000-00-00 00:00:00'),
(800062, 8, 800049, 'kredit top up', 'Top Up', '', '', '2017-12-05 05:16:42', '0000-00-00 00:00:00'),
(800063, 8, 800002, 'penambahan asset rental', 'Penambahan Asset Rental', '', '', '2017-12-05 06:11:09', '0000-00-00 00:00:00'),
(800064, 8, 0, 'buku besar', 'Buku Besar', '', 'fa fa-window-restore', '2017-12-07 14:06:17', '0000-00-00 00:00:00'),
(800065, 8, 0, 'neraca saldo', 'Neraca Saldo', '', 'fa fa-window-restore', '2017-12-07 14:06:49', '0000-00-00 00:00:00'),
(800066, 8, 0, 'arus kas', 'Arus Kas', '', 'fa fa-window-restore', '2017-12-11 16:17:37', '0000-00-00 00:00:00'),
(800067, 8, 0, 'jurnal umum', 'Jurnal Umum', '', 'fa fa-file', '2017-12-11 17:00:32', '0000-00-00 00:00:00'),
(800068, 8, 0, 'neraca', 'Neraca', '', 'fa fa-file', '2018-01-08 11:51:19', '0000-00-00 00:00:00'),
(800069, 8, 0, 'laba rugi', 'Laba Rugi', '', 'fa fa-file', '2018-01-08 11:52:30', '0000-00-00 00:00:00'),
(800070, 8, 0, 'arus kas langsung', 'Arus Kas Langsung', '', '', '2018-01-08 11:53:31', '0000-00-00 00:00:00');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
