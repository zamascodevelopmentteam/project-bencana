-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3307
-- Generation Time: Apr 25, 2018 at 01:34 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zmc_app_persediaan`
--

-- --------------------------------------------------------

--
-- Table structure for table `expenditures_distributions`
--

CREATE TABLE `expenditures_distributions` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `intitute_id` int(11) NOT NULL COMMENT 'DIBERIKAN KEPADA',
  `internal_order_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expenditures_distributions_details`
--

CREATE TABLE `expenditures_distributions_details` (
  `id` int(11) NOT NULL,
  `expenditures_distribution_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `price` decimal(22,2) DEFAULT '0.00',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expenditures_grants`
--

CREATE TABLE `expenditures_grants` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `given_to` varchar(225) NOT NULL COMMENT 'DIBERIKAN KEPADA',
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expenditures_grants_details`
--

CREATE TABLE `expenditures_grants_details` (
  `id` int(11) NOT NULL,
  `expenditures_grant_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `price` decimal(22,2) DEFAULT '0.00',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expenditures_others`
--

CREATE TABLE `expenditures_others` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `given_to` varchar(225) NOT NULL COMMENT 'DIBERIKAN KEPADA',
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expenditures_others_details`
--

CREATE TABLE `expenditures_others_details` (
  `id` int(11) NOT NULL,
  `expenditures_other_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `price` decimal(22,2) DEFAULT '0.00',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expenditures_reclarifications`
--

CREATE TABLE `expenditures_reclarifications` (
  `id` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime NOT NULL,
  `modified_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expenditures_reclarifications_details`
--

CREATE TABLE `expenditures_reclarifications_details` (
  `id` int(11) NOT NULL,
  `expenditures_reclarification_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) DEFAULT '0',
  `price` decimal(22,2) DEFAULT '0.00',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expenditures_transfers`
--

CREATE TABLE `expenditures_transfers` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `institution_id` int(11) NOT NULL COMMENT 'Pilih lembaga',
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expenditures_transfers_details`
--

CREATE TABLE `expenditures_transfers_details` (
  `id` int(11) NOT NULL,
  `expenditures_transfer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `price` decimal(22,2) DEFAULT '0.00',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `expenditures_distributions`
--
ALTER TABLE `expenditures_distributions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenditures_distributions_details`
--
ALTER TABLE `expenditures_distributions_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenditures_grants`
--
ALTER TABLE `expenditures_grants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenditures_grants_details`
--
ALTER TABLE `expenditures_grants_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenditures_others`
--
ALTER TABLE `expenditures_others`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenditures_others_details`
--
ALTER TABLE `expenditures_others_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenditures_reclarifications`
--
ALTER TABLE `expenditures_reclarifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenditures_reclarifications_details`
--
ALTER TABLE `expenditures_reclarifications_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenditures_transfers`
--
ALTER TABLE `expenditures_transfers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenditures_transfers_details`
--
ALTER TABLE `expenditures_transfers_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `expenditures_distributions`
--
ALTER TABLE `expenditures_distributions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expenditures_distributions_details`
--
ALTER TABLE `expenditures_distributions_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expenditures_grants`
--
ALTER TABLE `expenditures_grants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expenditures_grants_details`
--
ALTER TABLE `expenditures_grants_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expenditures_others`
--
ALTER TABLE `expenditures_others`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expenditures_others_details`
--
ALTER TABLE `expenditures_others_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expenditures_reclarifications`
--
ALTER TABLE `expenditures_reclarifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expenditures_reclarifications_details`
--
ALTER TABLE `expenditures_reclarifications_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expenditures_transfers`
--
ALTER TABLE `expenditures_transfers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expenditures_transfers_details`
--
ALTER TABLE `expenditures_transfers_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
