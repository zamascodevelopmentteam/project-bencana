-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3307
-- Generation Time: Apr 25, 2018 at 01:36 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zmc_app_persediaan`
--

-- --------------------------------------------------------

--
-- Table structure for table `purchase_orders`
--

CREATE TABLE `purchase_orders` (
  `id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `nomor_po` varchar(225) NOT NULL,
  `nomor_spk` varchar(225) NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `approve_date` datetime DEFAULT NULL,
  `approve_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_orders_details`
--

CREATE TABLE `purchase_orders_details` (
  `id` int(11) NOT NULL,
  `purchase_order_id` int(11) NOT NULL,
  `purchase_requests_detail_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `price` decimal(22,2) DEFAULT '0.00',
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_requests`
--

CREATE TABLE `purchase_requests` (
  `id` int(11) NOT NULL,
  `institute_id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `approve_date` datetime DEFAULT NULL,
  `approve_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_requests_details`
--

CREATE TABLE `purchase_requests_details` (
  `id` int(11) NOT NULL,
  `purchase_request_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `receipt_grants`
--

CREATE TABLE `receipt_grants` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `source` varchar(225) NOT NULL COMMENT 'SUMBER HIBAH',
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `receipt_grants_details`
--

CREATE TABLE `receipt_grants_details` (
  `id` int(11) NOT NULL,
  `receipt_grant_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `price` decimal(22,2) DEFAULT '0.00',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `receipt_others`
--

CREATE TABLE `receipt_others` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `source` varchar(225) NOT NULL COMMENT 'SUMBER HIBAH',
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `receipt_others_details`
--

CREATE TABLE `receipt_others_details` (
  `id` int(11) NOT NULL,
  `receipt_other_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `price` decimal(22,2) DEFAULT '0.00',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `receipt_purchases`
--

CREATE TABLE `receipt_purchases` (
  `id` int(11) NOT NULL,
  `purchase_order_id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `receipt_purchases_details`
--

CREATE TABLE `receipt_purchases_details` (
  `id` int(11) NOT NULL,
  `receipt_purchase_id` int(11) NOT NULL,
  `purchase_orders_detail_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `receipt_reclarifications`
--

CREATE TABLE `receipt_reclarifications` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `expenditures_reclarification_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `receipt_reclarifications_details`
--

CREATE TABLE `receipt_reclarifications_details` (
  `id` int(11) NOT NULL,
  `receipt_reclarification_id` int(11) NOT NULL,
  `expenditures_reclarifications_detail_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `price` decimal(22,2) DEFAULT '0.00',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `receipt_transfers`
--

CREATE TABLE `receipt_transfers` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `institution_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `receipt_transfers_details`
--

CREATE TABLE `receipt_transfers_details` (
  `id` int(11) NOT NULL,
  `receipt_transfer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(22,2) NOT NULL,
  `price` decimal(22,2) DEFAULT '0.00',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `purchase_orders`
--
ALTER TABLE `purchase_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_orders_details`
--
ALTER TABLE `purchase_orders_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_requests`
--
ALTER TABLE `purchase_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_requests_details`
--
ALTER TABLE `purchase_requests_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt_grants`
--
ALTER TABLE `receipt_grants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt_grants_details`
--
ALTER TABLE `receipt_grants_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt_others`
--
ALTER TABLE `receipt_others`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt_others_details`
--
ALTER TABLE `receipt_others_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt_purchases`
--
ALTER TABLE `receipt_purchases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt_purchases_details`
--
ALTER TABLE `receipt_purchases_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt_reclarifications`
--
ALTER TABLE `receipt_reclarifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt_reclarifications_details`
--
ALTER TABLE `receipt_reclarifications_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt_transfers`
--
ALTER TABLE `receipt_transfers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt_transfers_details`
--
ALTER TABLE `receipt_transfers_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `purchase_orders`
--
ALTER TABLE `purchase_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase_orders_details`
--
ALTER TABLE `purchase_orders_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase_requests`
--
ALTER TABLE `purchase_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase_requests_details`
--
ALTER TABLE `purchase_requests_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `receipt_grants`
--
ALTER TABLE `receipt_grants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `receipt_grants_details`
--
ALTER TABLE `receipt_grants_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `receipt_others`
--
ALTER TABLE `receipt_others`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `receipt_others_details`
--
ALTER TABLE `receipt_others_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `receipt_purchases`
--
ALTER TABLE `receipt_purchases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `receipt_purchases_details`
--
ALTER TABLE `receipt_purchases_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `receipt_reclarifications`
--
ALTER TABLE `receipt_reclarifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `receipt_reclarifications_details`
--
ALTER TABLE `receipt_reclarifications_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `receipt_transfers`
--
ALTER TABLE `receipt_transfers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `receipt_transfers_details`
--
ALTER TABLE `receipt_transfers_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
