-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 07, 2018 at 02:28 PM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.1.16-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zmc_pdplb`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `SPLIT_STRING` (`str` VARCHAR(255), `delim` VARCHAR(12), `pos` INT) RETURNS VARCHAR(255) CHARSET latin1 RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(str, delim, pos),
       LENGTH(SUBSTRING_INDEX(str, delim, pos-1)) + 1),
       delim, '')$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `acl_phinxlog`
--

CREATE TABLE `acl_phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acl_phinxlog`
--

INSERT INTO `acl_phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
(20141229162641, 'CakePhpDbAcl', '2018-02-01 03:04:00', '2018-02-01 03:04:01', 0);

-- --------------------------------------------------------

--
-- Table structure for table `acos`
--

CREATE TABLE `acos` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(11) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `status_aco` tinyint(2) DEFAULT '0',
  `sort` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acos`
--

INSERT INTO `acos` (`id`, `parent_id`, `name`, `model`, `foreign_key`, `alias`, `lft`, `rght`, `status`, `status_aco`, `sort`) VALUES
(1, NULL, 'controllers', NULL, NULL, 'controllers', 1, 300, 0, 0, 0),
(2, 1, 'Adjustment', NULL, NULL, 'Adjustments', 2, 13, 0, 1, 14),
(3, 2, 'index', NULL, NULL, 'index', 3, 4, 0, 1, 0),
(4, 2, 'view', NULL, NULL, 'view', 5, 6, 0, 1, 4),
(5, 2, 'add', NULL, NULL, 'add', 7, 8, 0, 1, 2),
(6, 2, 'edit', NULL, NULL, 'edit', 9, 10, 0, 1, 3),
(7, 2, 'delete', NULL, NULL, 'delete', 11, 12, 0, 1, 0),
(8, 1, 'Apis', NULL, NULL, 'Apis', 14, 27, 1, 0, 0),
(9, 8, 'getCountries', NULL, NULL, 'getCountries', 15, 16, 1, 0, 0),
(10, 8, 'getCurrencies', NULL, NULL, 'getCurrencies', 17, 18, 1, 0, 0),
(11, 8, 'getLocations', NULL, NULL, 'getLocations', 19, 20, 1, 0, 0),
(12, 8, 'getProducts', NULL, NULL, 'getProducts', 21, 22, 1, 0, 0),
(13, 8, 'getStocksPutAways', NULL, NULL, 'getStocksPutAways', 23, 24, 1, 0, 0),
(14, 8, 'getPickingLists', NULL, NULL, 'getPickingLists', 25, 26, 1, 0, 0),
(15, 1, 'App Settings', NULL, NULL, 'AppSettings', 28, 31, 0, 2, 99),
(16, 15, 'index', NULL, NULL, 'index', 29, 30, 0, 2, 0),
(17, 1, 'Master Negara', NULL, NULL, 'Countries', 32, 43, 0, 0, 3),
(18, 17, 'index', NULL, NULL, 'index', 33, 34, 0, 0, 0),
(19, 17, 'view', NULL, NULL, 'view', 35, 36, 0, 0, 4),
(20, 17, 'add', NULL, NULL, 'add', 37, 38, 0, 0, 2),
(21, 17, 'edit', NULL, NULL, 'edit', 39, 40, 0, 0, 3),
(22, 17, 'delete', NULL, NULL, 'delete', 41, 42, 0, 0, 0),
(23, 1, 'Master Mata Uang', NULL, NULL, 'Currencies', 44, 55, 0, 0, 4),
(24, 23, 'index', NULL, NULL, 'index', 45, 46, 0, 0, 0),
(25, 23, 'view', NULL, NULL, 'view', 47, 48, 0, 0, 4),
(26, 23, 'add', NULL, NULL, 'add', 49, 50, 0, 0, 2),
(27, 23, 'edit', NULL, NULL, 'edit', 51, 52, 0, 0, 3),
(28, 23, 'delete', NULL, NULL, 'delete', 53, 54, 0, 0, 0),
(29, 1, 'Master Customer', NULL, NULL, 'Customers', 56, 67, 0, 1, 5),
(30, 29, 'index', NULL, NULL, 'index', 57, 58, 0, 1, 0),
(31, 29, 'view', NULL, NULL, 'view', 59, 60, 0, 1, 4),
(32, 29, 'add', NULL, NULL, 'add', 61, 62, 0, 1, 2),
(33, 29, 'edit', NULL, NULL, 'edit', 63, 64, 0, 1, 3),
(34, 29, 'delete', NULL, NULL, 'delete', 65, 66, 0, 1, 0),
(35, 1, 'Dashboard', NULL, NULL, 'Dashboard', 68, 71, 0, 0, 0),
(36, 35, 'index', NULL, NULL, 'index', 69, 70, 0, 0, 0),
(37, 1, 'Pengeluaran Barang', NULL, NULL, 'Deliveries', 72, 83, 0, 1, 13),
(38, 37, 'index', NULL, NULL, 'index', 73, 74, 0, 1, 0),
(39, 37, 'view', NULL, NULL, 'view', 75, 76, 0, 1, 4),
(40, 37, 'add', NULL, NULL, 'add', 77, 78, 0, 1, 2),
(41, 37, 'edit', NULL, NULL, 'edit', 79, 80, 0, 1, 3),
(42, 37, 'delete', NULL, NULL, 'delete', 81, 82, 0, 1, 0),
(43, 1, 'Error', NULL, NULL, 'Error', 84, 85, 1, 0, 0),
(44, 1, 'Errors', NULL, NULL, 'Errors', 86, 89, 1, 0, 0),
(45, 44, 'unauthorized', NULL, NULL, 'unauthorized', 87, 88, 1, 0, 0),
(46, 1, 'Master Grup', NULL, NULL, 'Groups', 90, 103, 0, 0, 8),
(47, 46, 'index', NULL, NULL, 'index', 91, 92, 0, 0, 0),
(48, 46, 'view', NULL, NULL, 'view', 93, 94, 0, 0, 4),
(49, 46, 'add', NULL, NULL, 'add', 95, 96, 0, 0, 2),
(50, 46, 'edit', NULL, NULL, 'edit', 97, 98, 0, 0, 3),
(51, 46, 'delete', NULL, NULL, 'delete', 99, 100, 0, 0, 0),
(52, 46, 'configure', NULL, NULL, 'configure', 101, 102, 0, 0, 5),
(53, 1, 'Master Lokasi Penyimpanan', NULL, NULL, 'Locations', 104, 115, 0, 1, 7),
(54, 53, 'index', NULL, NULL, 'index', 105, 106, 0, 1, 0),
(55, 53, 'view', NULL, NULL, 'view', 107, 108, 0, 1, 4),
(56, 53, 'add', NULL, NULL, 'add', 109, 110, 0, 1, 2),
(57, 53, 'edit', NULL, NULL, 'edit', 111, 112, 0, 1, 3),
(58, 53, 'delete', NULL, NULL, 'delete', 113, 114, 0, 1, 0),
(59, 1, 'Pages', NULL, NULL, 'Pages', 116, 127, 1, 0, 0),
(60, 59, 'index', NULL, NULL, 'index', 117, 118, 1, 0, 0),
(61, 59, 'logout', NULL, NULL, 'logout', 119, 120, 1, 0, 0),
(62, 59, 'editProfile', NULL, NULL, 'editProfile', 121, 122, 1, 0, 0),
(63, 59, 'activitiesLog', NULL, NULL, 'activitiesLog', 123, 124, 1, 0, 0),
(64, 59, 'uploadMedia', NULL, NULL, 'uploadMedia', 125, 126, 1, 0, 0),
(65, 1, 'Master PDPLB', NULL, NULL, 'Pdplbs', 128, 139, 0, 2, 2),
(66, 65, 'index', NULL, NULL, 'index', 129, 130, 0, 2, 0),
(67, 65, 'view', NULL, NULL, 'view', 131, 132, 0, 2, 4),
(68, 65, 'add', NULL, NULL, 'add', 133, 134, 0, 2, 2),
(69, 65, 'edit', NULL, NULL, 'edit', 135, 136, 0, 2, 3),
(70, 65, 'delete', NULL, NULL, 'delete', 137, 138, 0, 2, 0),
(71, 1, 'Picking List', NULL, NULL, 'PickingLists', 140, 151, 0, 1, 12),
(72, 71, 'index', NULL, NULL, 'index', 141, 142, 0, 1, 0),
(73, 71, 'view', NULL, NULL, 'view', 143, 144, 0, 1, 4),
(74, 71, 'add', NULL, NULL, 'add', 145, 146, 0, 1, 2),
(75, 71, 'edit', NULL, NULL, 'edit', 147, 148, 0, 1, 3),
(76, 71, 'delete', NULL, NULL, 'delete', 149, 150, 0, 1, 0),
(77, 1, 'Master Barang', NULL, NULL, 'Products', 152, 163, 0, 1, 4),
(78, 77, 'index', NULL, NULL, 'index', 153, 154, 0, 1, 0),
(79, 77, 'view', NULL, NULL, 'view', 155, 156, 0, 1, 4),
(80, 77, 'add', NULL, NULL, 'add', 157, 158, 0, 1, 2),
(81, 77, 'edit', NULL, NULL, 'edit', 159, 160, 0, 1, 3),
(82, 77, 'delete', NULL, NULL, 'delete', 161, 162, 0, 1, 0),
(83, 1, 'Put Away', NULL, NULL, 'PutAways', 164, 175, 0, 1, 11),
(84, 83, 'index', NULL, NULL, 'index', 165, 166, 0, 1, 0),
(85, 83, 'view', NULL, NULL, 'view', 167, 168, 0, 1, 4),
(86, 83, 'add', NULL, NULL, 'add', 169, 170, 0, 1, 2),
(87, 83, 'edit', NULL, NULL, 'edit', 171, 172, 0, 1, 3),
(88, 83, 'delete', NULL, NULL, 'delete', 173, 174, 0, 1, 0),
(89, 1, 'Penerimaan Barang', NULL, NULL, 'Receives', 176, 187, 0, 1, 10),
(90, 89, 'index', NULL, NULL, 'index', 177, 178, 0, 1, 0),
(91, 89, 'view', NULL, NULL, 'view', 179, 180, 0, 1, 4),
(92, 89, 'add', NULL, NULL, 'add', 181, 182, 0, 1, 2),
(93, 89, 'edit', NULL, NULL, 'edit', 183, 184, 0, 1, 3),
(94, 89, 'delete', NULL, NULL, 'delete', 185, 186, 0, 1, 0),
(95, 1, 'Stock Opname', NULL, NULL, 'StockOpnames', 188, 199, 0, 1, 13),
(96, 95, 'index', NULL, NULL, 'index', 189, 190, 0, 1, 0),
(97, 95, 'view', NULL, NULL, 'view', 191, 192, 0, 1, 4),
(98, 95, 'add', NULL, NULL, 'add', 193, 194, 0, 1, 2),
(99, 95, 'edit', NULL, NULL, 'edit', 195, 196, 0, 1, 3),
(100, 95, 'delete', NULL, NULL, 'delete', 197, 198, 0, 1, 0),
(101, 1, 'Monitoring Stocks', NULL, NULL, 'Stocks', 200, 203, 0, 0, 0),
(102, 101, 'index', NULL, NULL, 'index', 201, 202, 0, 0, 0),
(103, 1, 'Master Gudang', NULL, NULL, 'Storages', 204, 215, 0, 2, 1),
(104, 103, 'index', NULL, NULL, 'index', 205, 206, 0, 2, 0),
(105, 103, 'view', NULL, NULL, 'view', 207, 208, 0, 2, 4),
(106, 103, 'add', NULL, NULL, 'add', 209, 210, 0, 2, 2),
(107, 103, 'edit', NULL, NULL, 'edit', 211, 212, 0, 2, 3),
(108, 103, 'delete', NULL, NULL, 'delete', 213, 214, 0, 2, 0),
(109, 1, 'Master Supplier', NULL, NULL, 'Suppliers', 216, 227, 0, 1, 6),
(110, 109, 'index', NULL, NULL, 'index', 217, 218, 0, 1, 0),
(111, 109, 'view', NULL, NULL, 'view', 219, 220, 0, 1, 4),
(112, 109, 'add', NULL, NULL, 'add', 221, 222, 0, 1, 2),
(113, 109, 'edit', NULL, NULL, 'edit', 223, 224, 0, 1, 3),
(114, 109, 'delete', NULL, NULL, 'delete', 225, 226, 0, 1, 0),
(115, 1, 'Master User', NULL, NULL, 'Users', 228, 243, 0, 0, 9),
(116, 115, 'index', NULL, NULL, 'index', 229, 230, 0, 0, 0),
(117, 115, 'view', NULL, NULL, 'view', 231, 232, 0, 0, 4),
(118, 115, 'add', NULL, NULL, 'add', 233, 234, 0, 0, 2),
(119, 115, 'getDataGroups', NULL, NULL, 'getDataGroups', 235, 236, 1, 0, 0),
(120, 115, 'edit', NULL, NULL, 'edit', 237, 238, 0, 0, 3),
(121, 115, 'delete', NULL, NULL, 'delete', 239, 240, 0, 0, 0),
(122, 115, 'configure', NULL, NULL, 'configure', 241, 242, 0, 0, 5),
(123, 1, 'Acl', NULL, NULL, 'Acl', 244, 245, 1, 0, 0),
(124, 1, 'AuditStash', NULL, NULL, 'AuditStash', 246, 247, 1, 0, 0),
(125, 1, 'Bake', NULL, NULL, 'Bake', 248, 249, 1, 0, 0),
(126, 1, 'DebugKit', NULL, NULL, 'DebugKit', 250, 277, 1, 0, 0),
(127, 126, 'Composer', NULL, NULL, 'Composer', 251, 254, 1, 0, 0),
(128, 127, 'checkDependencies', NULL, NULL, 'checkDependencies', 252, 253, 1, 0, 0),
(129, 126, 'MailPreview', NULL, NULL, 'MailPreview', 255, 262, 1, 0, 0),
(130, 129, 'index', NULL, NULL, 'index', 256, 257, 1, 0, 0),
(131, 129, 'sent', NULL, NULL, 'sent', 258, 259, 1, 0, 0),
(132, 129, 'email', NULL, NULL, 'email', 260, 261, 1, 0, 0),
(133, 126, 'Panels', NULL, NULL, 'Panels', 263, 268, 1, 0, 0),
(134, 133, 'index', NULL, NULL, 'index', 264, 265, 1, 0, 0),
(135, 133, 'view', NULL, NULL, 'view', 266, 267, 1, 0, 4),
(136, 126, 'Requests', NULL, NULL, 'Requests', 269, 272, 1, 0, 0),
(137, 136, 'view', NULL, NULL, 'view', 270, 271, 1, 0, 4),
(138, 126, 'Toolbar', NULL, NULL, 'Toolbar', 273, 276, 1, 0, 0),
(139, 138, 'clearCache', NULL, NULL, 'clearCache', 274, 275, 1, 0, 0),
(140, 1, 'Josegonzalez\\Upload', NULL, NULL, 'Josegonzalez\\Upload', 278, 279, 1, 0, 0),
(141, 1, 'Migrations', NULL, NULL, 'Migrations', 280, 281, 1, 0, 0),
(142, 1, 'Report Penerimaan', NULL, NULL, 'ReportReceives', 282, 285, 0, 0, 50),
(143, 142, 'index', NULL, NULL, 'index', 283, 284, 0, 0, 0),
(144, 1, NULL, NULL, NULL, 'CakePdf', 286, 287, 1, 0, 0),
(145, 1, 'Report Pengeluaran Barang', NULL, NULL, 'ReportDelivery', 288, 291, 0, 0, 51),
(146, 145, 'index', NULL, NULL, 'index', 289, 290, 0, 0, 0),
(147, 1, 'Report Mutasi Barang', NULL, NULL, 'ReportMutations', 292, 295, 0, 0, 52),
(148, 147, 'index', NULL, NULL, 'index', 293, 294, 0, 0, 0),
(149, 1, 'Report Mutasi Barang Per BC', NULL, NULL, 'ReportMutationsBc', 296, 299, 0, 0, 53),
(150, 149, 'index', NULL, NULL, 'index', 297, 298, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `adjustments`
--

CREATE TABLE `adjustments` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `date` date NOT NULL,
  `storage_id` int(11) DEFAULT NULL,
  `pdplb_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `adjustments_details`
--

CREATE TABLE `adjustments_details` (
  `id` int(11) NOT NULL,
  `adjustment_id` int(11) NOT NULL,
  `stock_opnames_detail_id` int(11) NOT NULL,
  `put_aways_detail_id` int(11) NOT NULL,
  `receives_detail_id` int(11) NOT NULL,
  `qty_system` decimal(65,2) DEFAULT '0.00',
  `qty` decimal(65,2) DEFAULT '0.00',
  `difference` decimal(65,2) DEFAULT '0.00',
  `adjust` decimal(65,2) DEFAULT '0.00',
  `note` varchar(225) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `storage_id` int(11) DEFAULT NULL,
  `pdplb_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `app_settings`
--

CREATE TABLE `app_settings` (
  `id` int(11) NOT NULL,
  `keyField` varchar(225) NOT NULL,
  `valueField` varchar(225) NOT NULL,
  `type` enum('text','long text','image','select') NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_settings`
--

INSERT INTO `app_settings` (`id`, `keyField`, `valueField`, `type`, `status`) VALUES
(1, 'App.Name', 'Z-APP-INVENTORY', 'text', 0),
(2, 'App.Logo', '/webroot/assets/img/logo.png', 'image', 0),
(3, 'App.Logo.Login', '/webroot/assets/img/logo_login.png', 'image', 0),
(4, 'App.Logo.Width', '160', 'text', 0),
(5, 'App.Logo.Height', '30', 'text', 0),
(6, 'App.Logo.Login.Width', '160', 'text', 0),
(7, 'App.Logo.Login.Height', '160', 'text', 0),
(8, 'App.Login.Cover', '/webroot/assets/img/cover_login.png', 'image', 0),
(9, 'App.Description', 'Management application', 'long text', 0),
(10, 'App.Favico', '/webroot/assets/img/favico.png', 'long text', 0);

-- --------------------------------------------------------

--
-- Table structure for table `aros`
--

CREATE TABLE `aros` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(11) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aros`
--

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, 'Groups', 1, NULL, 1, 6),
(2, 1, 'Users', 1, NULL, 2, 3),
(36, NULL, 'Groups', 32, NULL, 7, 10),
(37, 36, 'Users', 6, NULL, 8, 9),
(38, 1, 'Users', 7, NULL, 4, 5),
(39, NULL, 'Groups', 33, NULL, 11, 12);

-- --------------------------------------------------------

--
-- Table structure for table `aros_acos`
--

CREATE TABLE `aros_acos` (
  `id` int(11) NOT NULL,
  `aro_id` int(11) NOT NULL,
  `aco_id` int(11) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aros_acos`
--

INSERT INTO `aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`) VALUES
(1, 1, 1, '0', '0', '0', '0'),
(2, 38, 1, '0', '0', '0', '0'),
(3, 37, 1, '0', '0', '0', '0'),
(4, 36, 1, '0', '0', '0', '0'),
(5, 36, 35, '1', '1', '1', '1'),
(6, 36, 36, '1', '1', '1', '1'),
(7, 36, 101, '1', '1', '1', '1'),
(8, 36, 102, '1', '1', '1', '1'),
(9, 36, 17, '1', '1', '1', '1'),
(10, 36, 21, '1', '1', '1', '1'),
(11, 36, 20, '1', '1', '1', '1'),
(12, 36, 19, '1', '1', '1', '1'),
(13, 36, 18, '1', '1', '1', '1'),
(14, 36, 22, '1', '1', '1', '1'),
(15, 36, 77, '1', '1', '1', '1'),
(16, 36, 78, '1', '1', '1', '1'),
(17, 36, 82, '1', '1', '1', '1'),
(18, 36, 81, '1', '1', '1', '1'),
(19, 36, 80, '1', '1', '1', '1'),
(20, 36, 79, '1', '1', '1', '1'),
(21, 36, 23, '1', '1', '1', '1'),
(22, 36, 26, '1', '1', '1', '1'),
(23, 36, 25, '1', '1', '1', '1'),
(24, 36, 24, '1', '1', '1', '1'),
(25, 36, 28, '1', '1', '1', '1'),
(26, 36, 27, '1', '1', '1', '1'),
(27, 36, 29, '1', '1', '1', '1'),
(28, 36, 31, '1', '1', '1', '1'),
(29, 36, 30, '1', '1', '1', '1'),
(30, 36, 34, '1', '1', '1', '1'),
(31, 36, 33, '1', '1', '1', '1'),
(32, 36, 32, '1', '1', '1', '1'),
(33, 36, 109, '1', '1', '1', '1'),
(34, 36, 111, '1', '1', '1', '1'),
(35, 36, 110, '1', '1', '1', '1'),
(36, 36, 114, '1', '1', '1', '1'),
(37, 36, 113, '1', '1', '1', '1'),
(38, 36, 112, '1', '1', '1', '1'),
(39, 36, 53, '1', '1', '1', '1'),
(40, 36, 56, '1', '1', '1', '1'),
(41, 36, 55, '1', '1', '1', '1'),
(42, 36, 54, '1', '1', '1', '1'),
(43, 36, 58, '1', '1', '1', '1'),
(44, 36, 57, '1', '1', '1', '1'),
(45, 36, 46, '1', '1', '1', '1'),
(46, 36, 52, '1', '1', '1', '1'),
(47, 36, 47, '1', '1', '1', '1'),
(48, 36, 51, '1', '1', '1', '1'),
(49, 36, 50, '1', '1', '1', '1'),
(50, 36, 49, '1', '1', '1', '1'),
(51, 36, 48, '1', '1', '1', '1'),
(52, 36, 115, '1', '1', '1', '1'),
(53, 36, 116, '1', '1', '1', '1'),
(54, 36, 121, '1', '1', '1', '1'),
(55, 36, 120, '1', '1', '1', '1'),
(56, 36, 118, '1', '1', '1', '1'),
(57, 36, 117, '1', '1', '1', '1'),
(58, 36, 122, '1', '1', '1', '1'),
(59, 36, 89, '1', '1', '1', '1'),
(60, 36, 91, '1', '1', '1', '1'),
(61, 36, 90, '1', '1', '1', '1'),
(62, 36, 94, '1', '1', '1', '1'),
(63, 36, 93, '1', '1', '1', '1'),
(64, 36, 92, '1', '1', '1', '1'),
(65, 36, 83, '1', '1', '1', '1'),
(66, 36, 87, '1', '1', '1', '1'),
(67, 36, 86, '1', '1', '1', '1'),
(68, 36, 85, '1', '1', '1', '1'),
(69, 36, 84, '1', '1', '1', '1'),
(70, 36, 88, '1', '1', '1', '1'),
(71, 36, 71, '1', '1', '1', '1'),
(72, 36, 73, '1', '1', '1', '1'),
(73, 36, 72, '1', '1', '1', '1'),
(74, 36, 76, '1', '1', '1', '1'),
(75, 36, 75, '1', '1', '1', '1'),
(76, 36, 74, '1', '1', '1', '1'),
(77, 36, 95, '1', '1', '1', '1'),
(78, 36, 96, '1', '1', '1', '1'),
(79, 36, 100, '1', '1', '1', '1'),
(80, 36, 99, '1', '1', '1', '1'),
(81, 36, 98, '1', '1', '1', '1'),
(82, 36, 97, '1', '1', '1', '1'),
(83, 36, 37, '1', '1', '1', '1'),
(84, 36, 40, '1', '1', '1', '1'),
(85, 36, 39, '1', '1', '1', '1'),
(86, 36, 38, '1', '1', '1', '1'),
(87, 36, 42, '1', '1', '1', '1'),
(88, 36, 41, '1', '1', '1', '1'),
(89, 36, 2, '1', '1', '1', '1'),
(90, 36, 3, '1', '1', '1', '1'),
(91, 36, 7, '1', '1', '1', '1'),
(92, 36, 6, '1', '1', '1', '1'),
(93, 36, 5, '1', '1', '1', '1'),
(94, 36, 4, '1', '1', '1', '1'),
(95, 37, 35, '1', '1', '1', '1'),
(96, 37, 36, '1', '1', '1', '1'),
(97, 37, 101, '1', '1', '1', '1'),
(98, 37, 102, '1', '1', '1', '1'),
(99, 37, 17, '1', '1', '1', '1'),
(100, 37, 21, '1', '1', '1', '1'),
(101, 37, 20, '1', '1', '1', '1'),
(102, 37, 19, '1', '1', '1', '1'),
(103, 37, 18, '1', '1', '1', '1'),
(104, 37, 22, '1', '1', '1', '1'),
(105, 37, 77, '1', '1', '1', '1'),
(106, 37, 78, '1', '1', '1', '1'),
(107, 37, 82, '1', '1', '1', '1'),
(108, 37, 81, '1', '1', '1', '1'),
(109, 37, 80, '1', '1', '1', '1'),
(110, 37, 79, '1', '1', '1', '1'),
(111, 37, 23, '1', '1', '1', '1'),
(112, 37, 26, '1', '1', '1', '1'),
(113, 37, 25, '1', '1', '1', '1'),
(114, 37, 24, '1', '1', '1', '1'),
(115, 37, 28, '1', '1', '1', '1'),
(116, 37, 27, '1', '1', '1', '1'),
(117, 37, 29, '1', '1', '1', '1'),
(118, 37, 31, '1', '1', '1', '1'),
(119, 37, 30, '1', '1', '1', '1'),
(120, 37, 34, '1', '1', '1', '1'),
(121, 37, 33, '1', '1', '1', '1'),
(122, 37, 32, '1', '1', '1', '1'),
(123, 37, 109, '1', '1', '1', '1'),
(124, 37, 111, '1', '1', '1', '1'),
(125, 37, 110, '1', '1', '1', '1'),
(126, 37, 114, '1', '1', '1', '1'),
(127, 37, 113, '1', '1', '1', '1'),
(128, 37, 112, '1', '1', '1', '1'),
(129, 37, 53, '1', '1', '1', '1'),
(130, 37, 56, '1', '1', '1', '1'),
(131, 37, 55, '1', '1', '1', '1'),
(132, 37, 54, '1', '1', '1', '1'),
(133, 37, 58, '1', '1', '1', '1'),
(134, 37, 57, '1', '1', '1', '1'),
(135, 37, 46, '1', '1', '1', '1'),
(136, 37, 52, '1', '1', '1', '1'),
(137, 37, 47, '1', '1', '1', '1'),
(138, 37, 51, '1', '1', '1', '1'),
(139, 37, 50, '1', '1', '1', '1'),
(140, 37, 49, '1', '1', '1', '1'),
(141, 37, 48, '1', '1', '1', '1'),
(142, 37, 115, '1', '1', '1', '1'),
(143, 37, 116, '1', '1', '1', '1'),
(144, 37, 121, '1', '1', '1', '1'),
(145, 37, 120, '1', '1', '1', '1'),
(146, 37, 118, '1', '1', '1', '1'),
(147, 37, 117, '1', '1', '1', '1'),
(148, 37, 122, '1', '1', '1', '1'),
(149, 37, 89, '1', '1', '1', '1'),
(150, 37, 91, '1', '1', '1', '1'),
(151, 37, 90, '1', '1', '1', '1'),
(152, 37, 94, '1', '1', '1', '1'),
(153, 37, 93, '1', '1', '1', '1'),
(154, 37, 92, '1', '1', '1', '1'),
(155, 37, 83, '1', '1', '1', '1'),
(156, 37, 87, '1', '1', '1', '1'),
(157, 37, 86, '1', '1', '1', '1'),
(158, 37, 85, '1', '1', '1', '1'),
(159, 37, 84, '1', '1', '1', '1'),
(160, 37, 88, '1', '1', '1', '1'),
(161, 37, 71, '1', '1', '1', '1'),
(162, 37, 73, '1', '1', '1', '1'),
(163, 37, 72, '1', '1', '1', '1'),
(164, 37, 76, '1', '1', '1', '1'),
(165, 37, 75, '1', '1', '1', '1'),
(166, 37, 74, '1', '1', '1', '1'),
(167, 37, 95, '1', '1', '1', '1'),
(168, 37, 96, '1', '1', '1', '1'),
(169, 37, 100, '1', '1', '1', '1'),
(170, 37, 99, '1', '1', '1', '1'),
(171, 37, 98, '1', '1', '1', '1'),
(172, 37, 97, '1', '1', '1', '1'),
(173, 37, 37, '1', '1', '1', '1'),
(174, 37, 40, '1', '1', '1', '1'),
(175, 37, 39, '1', '1', '1', '1'),
(176, 37, 38, '1', '1', '1', '1'),
(177, 37, 42, '1', '1', '1', '1'),
(178, 37, 41, '1', '1', '1', '1'),
(179, 37, 2, '1', '1', '1', '1'),
(180, 37, 3, '1', '1', '1', '1'),
(181, 37, 7, '1', '1', '1', '1'),
(182, 37, 6, '1', '1', '1', '1'),
(183, 37, 5, '1', '1', '1', '1'),
(184, 37, 4, '1', '1', '1', '1'),
(185, 1, 35, '1', '1', '1', '1'),
(186, 1, 36, '1', '1', '1', '1'),
(187, 1, 103, '1', '1', '1', '1'),
(188, 1, 107, '1', '1', '1', '1'),
(189, 1, 106, '1', '1', '1', '1'),
(190, 1, 105, '1', '1', '1', '1'),
(191, 1, 104, '1', '1', '1', '1'),
(192, 1, 108, '1', '1', '1', '1'),
(193, 1, 65, '1', '1', '1', '1'),
(194, 1, 66, '1', '1', '1', '1'),
(195, 1, 70, '1', '1', '1', '1'),
(196, 1, 69, '1', '1', '1', '1'),
(197, 1, 68, '1', '1', '1', '1'),
(198, 1, 67, '1', '1', '1', '1'),
(199, 1, 17, '1', '1', '1', '1'),
(200, 1, 21, '1', '1', '1', '1'),
(201, 1, 20, '1', '1', '1', '1'),
(202, 1, 19, '1', '1', '1', '1'),
(203, 1, 18, '1', '1', '1', '1'),
(204, 1, 22, '1', '1', '1', '1'),
(205, 1, 23, '1', '1', '1', '1'),
(206, 1, 25, '1', '1', '1', '1'),
(207, 1, 24, '1', '1', '1', '1'),
(208, 1, 28, '1', '1', '1', '1'),
(209, 1, 27, '1', '1', '1', '1'),
(210, 1, 26, '1', '1', '1', '1'),
(211, 1, 46, '1', '1', '1', '1'),
(212, 1, 50, '1', '1', '1', '1'),
(213, 1, 49, '1', '1', '1', '1'),
(214, 1, 48, '1', '1', '1', '1'),
(215, 1, 52, '1', '1', '1', '1'),
(216, 1, 47, '1', '1', '1', '1'),
(217, 1, 51, '1', '1', '1', '1'),
(218, 1, 115, '1', '1', '1', '1'),
(219, 1, 118, '1', '1', '1', '1'),
(220, 1, 117, '1', '1', '1', '1'),
(221, 1, 122, '1', '1', '1', '1'),
(222, 1, 116, '1', '1', '1', '1'),
(223, 1, 121, '1', '1', '1', '1'),
(224, 1, 120, '1', '1', '1', '1'),
(225, 1, 15, '1', '1', '1', '1'),
(226, 1, 16, '1', '1', '1', '1'),
(227, 2, 35, '1', '1', '1', '1'),
(228, 2, 36, '1', '1', '1', '1'),
(229, 2, 103, '1', '1', '1', '1'),
(230, 2, 107, '1', '1', '1', '1'),
(231, 2, 106, '1', '1', '1', '1'),
(232, 2, 105, '1', '1', '1', '1'),
(233, 2, 104, '1', '1', '1', '1'),
(234, 2, 108, '1', '1', '1', '1'),
(235, 2, 65, '1', '1', '1', '1'),
(236, 2, 66, '1', '1', '1', '1'),
(237, 2, 70, '1', '1', '1', '1'),
(238, 2, 69, '1', '1', '1', '1'),
(239, 2, 68, '1', '1', '1', '1'),
(240, 2, 67, '1', '1', '1', '1'),
(241, 2, 17, '1', '1', '1', '1'),
(242, 2, 21, '1', '1', '1', '1'),
(243, 2, 20, '1', '1', '1', '1'),
(244, 2, 19, '1', '1', '1', '1'),
(245, 2, 18, '1', '1', '1', '1'),
(246, 2, 22, '1', '1', '1', '1'),
(247, 2, 23, '1', '1', '1', '1'),
(248, 2, 25, '1', '1', '1', '1'),
(249, 2, 24, '1', '1', '1', '1'),
(250, 2, 28, '1', '1', '1', '1'),
(251, 2, 27, '1', '1', '1', '1'),
(252, 2, 26, '1', '1', '1', '1'),
(253, 2, 46, '1', '1', '1', '1'),
(254, 2, 50, '1', '1', '1', '1'),
(255, 2, 49, '1', '1', '1', '1'),
(256, 2, 48, '1', '1', '1', '1'),
(257, 2, 52, '1', '1', '1', '1'),
(258, 2, 47, '1', '1', '1', '1'),
(259, 2, 51, '1', '1', '1', '1'),
(260, 2, 115, '1', '1', '1', '1'),
(261, 2, 118, '1', '1', '1', '1'),
(262, 2, 117, '1', '1', '1', '1'),
(263, 2, 122, '1', '1', '1', '1'),
(264, 2, 116, '1', '1', '1', '1'),
(265, 2, 121, '1', '1', '1', '1'),
(266, 2, 120, '1', '1', '1', '1'),
(267, 2, 15, '1', '1', '1', '1'),
(268, 2, 16, '1', '1', '1', '1'),
(269, 36, 142, '1', '1', '1', '1'),
(270, 36, 143, '1', '1', '1', '1'),
(271, 37, 142, '1', '1', '1', '1'),
(272, 37, 143, '1', '1', '1', '1'),
(273, 36, 145, '1', '1', '1', '1'),
(274, 36, 146, '1', '1', '1', '1'),
(275, 37, 145, '1', '1', '1', '1'),
(276, 37, 146, '1', '1', '1', '1'),
(277, 36, 147, '1', '1', '1', '1'),
(278, 36, 148, '1', '1', '1', '1'),
(279, 37, 147, '1', '1', '1', '1'),
(280, 37, 148, '1', '1', '1', '1'),
(281, 36, 149, '1', '1', '1', '1'),
(282, 36, 150, '1', '1', '1', '1'),
(283, 37, 149, '1', '1', '1', '1'),
(284, 37, 150, '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `audit_logs`
--

CREATE TABLE `audit_logs` (
  `id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  `controller` varchar(225) DEFAULT NULL,
  `_action` varchar(225) DEFAULT NULL,
  `type` varchar(250) DEFAULT NULL,
  `primary_key` int(11) DEFAULT NULL,
  `source` varchar(250) DEFAULT NULL,
  `parent_source` varchar(250) DEFAULT NULL,
  `original` json DEFAULT NULL,
  `changed` json DEFAULT NULL,
  `meta` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `audit_logs`
--

INSERT INTO `audit_logs` (`id`, `timestamp`, `user_id`, `controller`, `_action`, `type`, `primary_key`, `source`, `parent_source`, `original`, `changed`, `meta`) VALUES
(1, '2018-02-06 15:08:55', 1, 'users', 'add', 'create', 10, 'users', NULL, '"{\\"id\\":10,\\"username\\":\\"sadsadsad\\",\\"name\\":\\"KAS PUSAT\\",\\"email\\":\\"sadsa23dsadsa@gmail.com\\",\\"group_id\\":2,\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":10,\\"username\\":\\"sadsadsad\\",\\"name\\":\\"KAS PUSAT\\",\\"email\\":\\"sadsa23dsadsa@gmail.com\\",\\"group_id\\":2,\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(2, '2018-02-06 15:11:34', 1, 'groups', 'delete', 'delete', 10, 'users', NULL, 'null', 'null', '"[]"'),
(3, '2018-02-06 15:11:34', 1, 'groups', 'delete', 'delete', 2, 'groups', NULL, 'null', 'null', '"[]"'),
(4, '2018-02-08 10:38:04', 1, 'users', 'add', 'create', 2, 'users', NULL, '"{\\"id\\":2,\\"username\\":\\"admin\\",\\"name\\":\\"dsadsa\\",\\"email\\":\\"dsadsads@dsadsa.com\\",\\"group_id\\":1,\\"status\\":true,\\"created_by\\":1}"', '"{\\"id\\":2,\\"username\\":\\"admin\\",\\"name\\":\\"dsadsa\\",\\"email\\":\\"dsadsads@dsadsa.com\\",\\"group_id\\":1,\\"status\\":true,\\"created_by\\":1}"', '"[]"'),
(5, '2018-02-08 10:38:27', 1, 'users', 'add', 'create', 3, 'users', NULL, '"{\\"id\\":3,\\"username\\":\\"dsadsad\\",\\"name\\":\\"KAS PUSAT\\",\\"email\\":\\"321321@gmail.com\\",\\"group_id\\":1,\\"status\\":true,\\"created_by\\":1}"', '"{\\"id\\":3,\\"username\\":\\"dsadsad\\",\\"name\\":\\"KAS PUSAT\\",\\"email\\":\\"321321@gmail.com\\",\\"group_id\\":1,\\"status\\":true,\\"created_by\\":1}"', '"[]"'),
(6, '2018-02-09 03:50:34', 1, 'users', 'edit', 'update', 2, 'users', NULL, '"{\\"username\\":\\"admin\\",\\"name\\":\\"dsadsa\\",\\"email\\":\\"dsadsads@dsadsa.com\\",\\"modified_by\\":null}"', '"{\\"username\\":\\"rezama\\",\\"name\\":\\"rezama\\",\\"email\\":\\"rezama@app.com\\",\\"modified_by\\":1}"', '"[]"'),
(7, '2018-02-09 04:14:55', 1, 'groups', 'add', 'create', 2, 'groups', NULL, '"{\\"id\\":2,\\"name\\":\\"SPV\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":2,\\"name\\":\\"SPV\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(8, '2018-02-09 04:15:23', 1, 'users', 'add', 'create', 4, 'users', NULL, '"{\\"id\\":4,\\"username\\":\\"SPV\\",\\"name\\":\\"spv\\",\\"email\\":\\"spv@app.com\\",\\"group_id\\":1,\\"status\\":true,\\"created_by\\":1}"', '"{\\"id\\":4,\\"username\\":\\"SPV\\",\\"name\\":\\"spv\\",\\"email\\":\\"spv@app.com\\",\\"group_id\\":1,\\"status\\":true,\\"created_by\\":1}"', '"[]"'),
(9, '2018-02-09 04:15:32', 1, 'users', 'edit', 'update', 2, 'users', NULL, '"{\\"status\\":true,\\"modified_by\\":1}"', '"{\\"status\\":false,\\"modified_by\\":1}"', '"[]"'),
(10, '2018-02-20 03:19:18', 1, 'delete', '2', 'delete', 2, 'groups', NULL, 'null', 'null', '"[]"'),
(11, '2018-02-20 03:19:31', 1, 'delete', '4', 'delete', 4, 'users', NULL, 'null', 'null', '"[]"'),
(12, '2018-02-20 03:19:37', 1, 'delete', '3', 'delete', 3, 'users', NULL, 'null', 'null', '"[]"'),
(13, '2018-02-20 03:19:44', 1, 'delete', '2', 'delete', 2, 'users', NULL, 'null', 'null', '"[]"'),
(14, '2018-02-20 04:05:01', 1, 'edit', '1', 'update', 1, 'pdplbs', NULL, '"{\\"logo\\":null,\\"logo_dir\\":null,\\"modified_by\\":null}"', '"{\\"logo\\":\\"logo.png\\",\\"logo_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\pdplb\\\\\\\\201802\\\\\\\\logo\\\\\\\\\\",\\"modified_by\\":1}"', '"[]"'),
(15, '2018-02-20 04:19:59', 1, 'edit', '2', 'update', 2, 'pdplbs', NULL, '"{\\"logo\\":null,\\"logo_dir\\":null,\\"modified_by\\":null}"', '"{\\"logo\\":\\"logo Zapps-02-1-01 (2).png\\",\\"logo_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\pdplb\\\\\\\\201802\\\\\\\\logo\\\\\\\\\\",\\"modified_by\\":1}"', '"[]"'),
(16, '2018-02-20 04:26:49', 1, 'add', NULL, 'create', 1, 'storages', NULL, '"{\\"id\\":1,\\"name\\":\\"GUDANG JONGGOL\\",\\"address\\":\\"JONGGOL\\",\\"created_by\\":1}"', '"{\\"id\\":1,\\"name\\":\\"GUDANG JONGGOL\\",\\"address\\":\\"JONGGOL\\",\\"created_by\\":1}"', '"[]"'),
(17, '2018-02-20 05:22:05', 1, 'edit', '11', 'update', 11, 'countries', NULL, '"{\\"name\\":\\"Suriyah\\",\\"modified_by\\":null}"', '"{\\"name\\":\\"Beijing\\",\\"modified_by\\":1}"', '"[]"'),
(18, '2018-02-20 05:58:51', 1, 'add', NULL, 'create', 3, 'groups', NULL, '"{\\"id\\":3,\\"name\\":\\"SPV\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":3,\\"name\\":\\"SPV\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(19, '2018-02-20 05:59:10', 1, 'add', NULL, 'create', 4, 'groups', NULL, '"{\\"id\\":4,\\"pdplb_id\\":1,\\"name\\":\\"SPV GUDANG\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":4,\\"pdplb_id\\":1,\\"name\\":\\"SPV GUDANG\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(20, '2018-02-20 05:59:52', 1, 'delete', '4', 'delete', 4, 'groups', NULL, 'null', 'null', '"[]"'),
(21, '2018-02-20 05:59:58', 1, 'delete', '3', 'delete', 3, 'groups', NULL, 'null', 'null', '"[]"'),
(22, '2018-02-20 06:00:13', 1, 'add', NULL, 'create', 5, 'groups', NULL, '"{\\"id\\":5,\\"name\\":\\"KAS PUSAT\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":5,\\"name\\":\\"KAS PUSAT\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(23, '2018-02-20 06:00:36', 1, 'add', NULL, 'create', 6, 'groups', NULL, '"{\\"id\\":6,\\"storage_id\\":1,\\"pdplb_id\\":1,\\"name\\":\\"KAS PUSATs\\",\\"status\\":true,\\"created_by\\":1}"', '"{\\"id\\":6,\\"storage_id\\":1,\\"pdplb_id\\":1,\\"name\\":\\"KAS PUSATs\\",\\"status\\":true,\\"created_by\\":1}"', '"[]"'),
(24, '2018-02-20 06:00:49', 1, 'delete', '6', 'delete', 6, 'groups', NULL, 'null', 'null', '"[]"'),
(25, '2018-02-20 06:00:54', 1, 'delete', '5', 'delete', 5, 'groups', NULL, 'null', 'null', '"[]"'),
(26, '2018-02-20 06:03:35', 1, 'add', NULL, 'create', 7, 'groups', NULL, '"{\\"id\\":7,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":7,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(27, '2018-02-20 06:03:42', 1, 'delete', '7', 'delete', 7, 'groups', NULL, 'null', 'null', '"[]"'),
(28, '2018-02-20 06:04:03', 1, 'add', NULL, 'create', 8, 'groups', NULL, '"{\\"id\\":8,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":8,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(29, '2018-02-20 06:04:14', 1, 'delete', '8', 'delete', 8, 'groups', NULL, 'null', 'null', '"[]"'),
(30, '2018-02-20 06:04:45', 1, 'add', NULL, 'create', 9, 'groups', NULL, '"{\\"id\\":9,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":9,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(31, '2018-02-20 06:05:04', 1, 'add', NULL, 'create', 10, 'groups', NULL, '"{\\"id\\":10,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":10,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(32, '2018-02-20 06:06:06', 1, 'add', NULL, 'create', 11, 'groups', NULL, '"{\\"id\\":11,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":11,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(33, '2018-02-20 06:06:20', 1, 'delete', '9', 'delete', 9, 'groups', NULL, 'null', 'null', '"[]"'),
(34, '2018-02-20 06:06:25', 1, 'delete', '10', 'delete', 10, 'groups', NULL, 'null', 'null', '"[]"'),
(35, '2018-02-20 06:06:29', 1, 'delete', '11', 'delete', 11, 'groups', NULL, 'null', 'null', '"[]"'),
(36, '2018-02-20 06:07:05', 1, 'add', NULL, 'create', 12, 'groups', NULL, '"{\\"id\\":12,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":12,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(37, '2018-02-20 06:07:45', 1, 'delete', '12', 'delete', 12, 'groups', NULL, 'null', 'null', '"[]"'),
(38, '2018-02-20 06:07:52', 1, 'add', NULL, 'create', 13, 'groups', NULL, '"{\\"id\\":13,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":13,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(39, '2018-02-20 06:08:10', 1, 'delete', '13', 'delete', 13, 'groups', NULL, 'null', 'null', '"[]"'),
(40, '2018-02-20 06:10:13', 1, 'add', NULL, 'create', 14, 'groups', NULL, '"{\\"id\\":14,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":14,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(41, '2018-02-20 06:10:24', 1, 'delete', '14', 'delete', 14, 'groups', NULL, 'null', 'null', '"[]"'),
(42, '2018-02-20 06:10:53', 1, 'add', NULL, 'create', 15, 'groups', NULL, '"{\\"id\\":15,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":15,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(43, '2018-02-20 06:11:38', 1, 'delete', '15', 'delete', 15, 'groups', NULL, 'null', 'null', '"[]"'),
(44, '2018-02-20 06:19:42', 1, 'add', NULL, 'create', 16, 'groups', NULL, '"{\\"id\\":16,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":16,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(45, '2018-02-20 06:20:14', 1, 'delete', '16', 'delete', 16, 'groups', NULL, 'null', 'null', '"[]"'),
(46, '2018-02-20 06:20:50', 1, 'add', NULL, 'create', 17, 'groups', NULL, '"{\\"id\\":17,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":17,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(47, '2018-02-20 06:21:03', 1, 'delete', '17', 'delete', 17, 'groups', NULL, 'null', 'null', '"[]"'),
(48, '2018-02-20 06:22:11', 1, 'add', NULL, 'create', 18, 'groups', NULL, '"{\\"id\\":18,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":18,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"administrator\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(49, '2018-02-20 06:22:21', 1, 'delete', '18', 'delete', 18, 'groups', NULL, 'null', 'null', '"[]"'),
(50, '2018-02-20 06:23:03', 1, 'add', NULL, 'create', 19, 'groups', NULL, '"{\\"id\\":19,\\"storage_id\\":0,\\"pdplb_id\\":0,\\"name\\":\\"dsadasdadsa\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":19,\\"storage_id\\":0,\\"pdplb_id\\":0,\\"name\\":\\"dsadasdadsa\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(51, '2018-02-20 06:23:15', 1, 'add', NULL, 'create', 20, 'groups', NULL, '"{\\"id\\":20,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"dsadasdadsa\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":20,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"dsadasdadsa\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(52, '2018-02-20 06:23:24', 1, 'delete', '19', 'delete', 19, 'groups', NULL, 'null', 'null', '"[]"'),
(53, '2018-02-20 06:23:29', 1, 'delete', '20', 'delete', 20, 'groups', NULL, 'null', 'null', '"[]"'),
(54, '2018-02-20 06:24:20', 1, 'add', NULL, 'create', 21, 'groups', NULL, '"{\\"id\\":21,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"dsadasdadsa\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":21,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"dsadasdadsa\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(55, '2018-02-20 06:24:33', 1, 'delete', '21', 'delete', 21, 'groups', NULL, 'null', 'null', '"[]"'),
(56, '2018-02-20 06:25:20', 1, 'add', NULL, 'create', 22, 'groups', NULL, '"{\\"id\\":22,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"dsadsadsadsa\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":22,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"dsadsadsadsa\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(57, '2018-02-20 06:25:52', 1, 'delete', '22', 'delete', 22, 'groups', NULL, 'null', 'null', '"[]"'),
(58, '2018-02-20 06:26:24', 1, 'add', NULL, 'create', 23, 'groups', NULL, '"{\\"id\\":23,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"dsadsadsadsa\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":23,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"dsadsadsadsa\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(59, '2018-02-20 06:26:44', 1, 'delete', '23', 'delete', 23, 'groups', NULL, 'null', 'null', '"[]"'),
(60, '2018-02-20 06:26:52', 1, 'add', NULL, 'create', 24, 'groups', NULL, '"{\\"id\\":24,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"aaaa\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":24,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"aaaa\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(61, '2018-02-20 06:27:27', 1, 'delete', '24', 'delete', 24, 'groups', NULL, 'null', 'null', '"[]"'),
(62, '2018-02-20 06:27:44', 1, 'add', NULL, 'create', 25, 'groups', NULL, '"{\\"id\\":25,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"aaaa\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":25,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"aaaa\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(63, '2018-02-20 06:28:57', 1, 'delete', '25', 'delete', 25, 'groups', NULL, 'null', 'null', '"[]"'),
(64, '2018-02-20 06:29:10', 1, 'add', NULL, 'create', 26, 'groups', NULL, '"{\\"id\\":26,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"aaaa\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":26,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"aaaa\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(65, '2018-02-20 06:29:41', 1, 'delete', '26', 'delete', 26, 'groups', NULL, 'null', 'null', '"[]"'),
(66, '2018-02-20 06:29:45', 1, 'add', NULL, 'create', 27, 'groups', NULL, '"{\\"id\\":27,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"aaaa\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":27,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"aaaa\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(67, '2018-02-20 06:29:54', 1, 'delete', '27', 'delete', 27, 'groups', NULL, 'null', 'null', '"[]"'),
(68, '2018-02-20 06:30:14', 1, 'add', NULL, 'create', 28, 'groups', NULL, '"{\\"id\\":28,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"dadsadsad\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":28,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"dadsadsad\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(69, '2018-02-20 06:32:14', 1, 'delete', '28', 'delete', 28, 'groups', NULL, 'null', 'null', '"[]"'),
(70, '2018-02-20 07:01:32', 1, 'add', NULL, 'create', 29, 'groups', NULL, '"{\\"id\\":29,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"dadsadsad\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":29,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"dadsadsad\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(71, '2018-02-20 07:01:43', 1, 'delete', '29', 'delete', 29, 'groups', NULL, 'null', 'null', '"[]"'),
(72, '2018-02-20 07:06:44', 1, 'add', NULL, 'create', 30, 'groups', NULL, '"{\\"id\\":30,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"dadsadsad\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":30,\\"storage_id\\":0,\\"pdplb_id\\":1,\\"name\\":\\"dadsadsad\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(73, '2018-02-20 07:06:53', 1, 'delete', '30', 'delete', 30, 'groups', NULL, 'null', 'null', '"[]"'),
(74, '2018-02-20 07:14:48', 1, 'add', NULL, 'create', 31, 'groups', NULL, '"{\\"id\\":31,\\"storage_id\\":0,\\"pdplb_id\\":0,\\"name\\":\\"dadsadsad\\",\\"status\\":false,\\"created_by\\":1}"', '"{\\"id\\":31,\\"storage_id\\":0,\\"pdplb_id\\":0,\\"name\\":\\"dadsadsad\\",\\"status\\":false,\\"created_by\\":1}"', '"[]"'),
(75, '2018-02-20 07:15:09', 1, 'delete', '31', 'delete', 31, 'groups', NULL, 'null', 'null', '"[]"'),
(76, '2018-02-20 07:18:35', 1, 'add', NULL, 'create', 32, 'groups', NULL, '"{\\"id\\":32,\\"storage_id\\":1,\\"pdplb_id\\":1,\\"name\\":\\"administrator\\",\\"status\\":true,\\"created_by\\":1}"', '"{\\"id\\":32,\\"storage_id\\":1,\\"pdplb_id\\":1,\\"name\\":\\"administrator\\",\\"status\\":true,\\"created_by\\":1}"', '"[]"'),
(77, '2018-02-20 08:10:45', 1, 'add', NULL, 'create', 6, 'users', NULL, '"{\\"id\\":6,\\"username\\":\\"administrator\\",\\"name\\":\\"administrator\\",\\"email\\":\\"superadministrator@gmail.com\\",\\"group_id\\":32,\\"status\\":true,\\"created_by\\":1,\\"pdplb_id\\":1,\\"storage_id\\":1}"', '"{\\"id\\":6,\\"username\\":\\"administrator\\",\\"name\\":\\"administrator\\",\\"email\\":\\"superadministrator@gmail.com\\",\\"group_id\\":32,\\"status\\":true,\\"created_by\\":1,\\"pdplb_id\\":1,\\"storage_id\\":1}"', '"[]"'),
(78, '2018-02-21 02:17:10', 1, 'edit', '6', 'update', 6, 'users', NULL, '"{\\"modified_by\\":null}"', '"{\\"modified_by\\":1}"', '"[]"'),
(79, '2018-02-21 03:18:08', 6, 'add', NULL, 'create', 7, 'users', NULL, '"{\\"id\\":7,\\"username\\":\\"gudangmanager\\",\\"name\\":\\"gudangmanager\\",\\"email\\":\\"gudang@gmail.com\\",\\"group_id\\":1,\\"status\\":true,\\"created_by\\":6,\\"pdplb_id\\":1,\\"storage_id\\":1}"', '"{\\"id\\":7,\\"username\\":\\"gudangmanager\\",\\"name\\":\\"gudangmanager\\",\\"email\\":\\"gudang@gmail.com\\",\\"group_id\\":1,\\"status\\":true,\\"created_by\\":6,\\"pdplb_id\\":1,\\"storage_id\\":1}"', '"[]"'),
(80, '2018-02-21 03:39:30', 6, 'add', NULL, 'create', 33, 'groups', NULL, '"{\\"id\\":33,\\"storage_id\\":1,\\"pdplb_id\\":1,\\"name\\":\\"GUDANG\\",\\"status\\":true,\\"created_by\\":6}"', '"{\\"id\\":33,\\"storage_id\\":1,\\"pdplb_id\\":1,\\"name\\":\\"GUDANG\\",\\"status\\":true,\\"created_by\\":6}"', '"[]"'),
(81, '2018-02-26 09:18:35', 6, 'add', NULL, 'create', 11, 'receives', NULL, '"{\\"id\\":11,\\"code\\":10000,\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"dsad\\",\\"bc_date\\":\\"2018-02-26T00:00:00+00:00\\",\\"date\\":\\"2018-02-26T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpF654.tmp\\",\\"error\\":0,\\"name\\":\\"gammu.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1490438},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":11,\\"code\\":10000,\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"dsad\\",\\"bc_date\\":\\"2018-02-26T00:00:00+00:00\\",\\"date\\":\\"2018-02-26T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":\\"gammu.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(82, '2018-02-26 09:21:47', 6, 'delete', '11', 'delete', 11, 'receives', NULL, 'null', 'null', '"[]"'),
(83, '2018-02-26 09:23:36', 6, 'add', NULL, 'create', 13, 'receives', NULL, '"{\\"id\\":13,\\"code\\":9999,\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"sdpsodap\\",\\"bc_date\\":\\"2018-02-14T00:00:00+00:00\\",\\"date\\":\\"2018-02-26T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php9199.tmp\\",\\"error\\":0,\\"name\\":\\"gammu.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1490438},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":13,\\"code\\":9999,\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"sdpsodap\\",\\"bc_date\\":\\"2018-02-14T00:00:00+00:00\\",\\"date\\":\\"2018-02-26T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":\\"gammu.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(84, '2018-02-26 09:31:22', 6, 'add', NULL, 'create', 5, 'receives', NULL, '"{\\"id\\":5,\\"code\\":10000,\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"sdpsodap\\",\\"bc_date\\":\\"2018-02-14T00:00:00+00:00\\",\\"date\\":\\"2018-02-26T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpAA06.tmp\\",\\"error\\":0,\\"name\\":\\"gammu.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1490438},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":5,\\"code\\":10000,\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"sdpsodap\\",\\"bc_date\\":\\"2018-02-14T00:00:00+00:00\\",\\"date\\":\\"2018-02-26T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":\\"gammu.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(85, '2018-02-26 09:32:29', 6, 'delete', '5', 'delete', 5, 'receives', NULL, 'null', 'null', '"[]"'),
(86, '2018-02-26 09:47:55', 6, 'add', NULL, 'create', 14, 'receives', NULL, '"{\\"id\\":14,\\"code\\":9999,\\"bc_type\\":\\"BC 4.0\\",\\"bc_code\\":\\"sdpsodap\\",\\"bc_date\\":\\"2018-02-19T00:00:00+00:00\\",\\"date\\":\\"2018-02-26T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpD4A1.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d_2.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":14,\\"code\\":9999,\\"bc_type\\":\\"BC 4.0\\",\\"bc_code\\":\\"sdpsodap\\",\\"bc_date\\":\\"2018-02-19T00:00:00+00:00\\",\\"date\\":\\"2018-02-26T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":\\"imagetopdf-012355380d_2.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(87, '2018-02-26 09:48:42', 6, 'add', NULL, 'create', 15, 'receives', NULL, '"{\\"id\\":15,\\"code\\":9999,\\"bc_type\\":\\"BC 4.0\\",\\"bc_code\\":\\"sdpsodap\\",\\"bc_date\\":\\"2018-02-19T00:00:00+00:00\\",\\"date\\":\\"2018-02-26T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php8B40.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d_2.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":15,\\"code\\":9999,\\"bc_type\\":\\"BC 4.0\\",\\"bc_code\\":\\"sdpsodap\\",\\"bc_date\\":\\"2018-02-19T00:00:00+00:00\\",\\"date\\":\\"2018-02-26T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":\\"imagetopdf-012355380d_2.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(88, '2018-02-26 09:49:25', 6, 'add', NULL, 'create', 16, 'receives', NULL, '"{\\"id\\":16,\\"code\\":10000,\\"bc_type\\":\\"BC 4.0\\",\\"bc_code\\":\\"sdpsodap\\",\\"bc_date\\":\\"2018-02-19T00:00:00+00:00\\",\\"date\\":\\"2018-02-26T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php34CF.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d_2.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":16,\\"code\\":10000,\\"bc_type\\":\\"BC 4.0\\",\\"bc_code\\":\\"sdpsodap\\",\\"bc_date\\":\\"2018-02-19T00:00:00+00:00\\",\\"date\\":\\"2018-02-26T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":\\"imagetopdf-012355380d_2.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(89, '2018-02-26 09:51:00', 6, 'add', NULL, 'create', 1, 'receives', NULL, '"{\\"id\\":1,\\"code\\":10000,\\"bc_type\\":\\"BC 4.0\\",\\"bc_code\\":\\"sdpsodap\\",\\"bc_date\\":\\"2018-02-19T00:00:00+00:00\\",\\"date\\":\\"2018-02-26T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpA5C5.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d_2.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":1,\\"code\\":10000,\\"bc_type\\":\\"BC 4.0\\",\\"bc_code\\":\\"sdpsodap\\",\\"bc_date\\":\\"2018-02-19T00:00:00+00:00\\",\\"date\\":\\"2018-02-26T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":\\"imagetopdf-012355380d_2.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(90, '2018-02-26 09:55:57', 6, 'delete', '1', 'delete', 1, 'receives', NULL, 'null', 'null', '"[]"'),
(91, '2018-02-26 09:58:01', 6, 'add', NULL, 'create', 2, 'receives', NULL, '"{\\"id\\":2,\\"code\\":9999,\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"sdpsodap\\",\\"bc_date\\":\\"2018-02-26T00:00:00+00:00\\",\\"date\\":\\"2018-02-26T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php12FA.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d_2.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":2,\\"code\\":9999,\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"sdpsodap\\",\\"bc_date\\":\\"2018-02-26T00:00:00+00:00\\",\\"date\\":\\"2018-02-26T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":\\"imagetopdf-012355380d_2.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(92, '2018-02-26 10:00:15', 6, 'add', NULL, 'create', 3, 'receives', NULL, '"{\\"id\\":3,\\"code\\":9999,\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"dsad\\",\\"bc_date\\":\\"2018-02-26T00:00:00+00:00\\",\\"date\\":\\"2018-02-15T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php1BDB.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d_2.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":3,\\"code\\":9999,\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"dsad\\",\\"bc_date\\":\\"2018-02-26T00:00:00+00:00\\",\\"date\\":\\"2018-02-15T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":\\"imagetopdf-012355380d_2.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(93, '2018-02-26 10:00:25', 6, 'add', NULL, 'create', 4, 'receives', NULL, '"{\\"id\\":4,\\"code\\":9999,\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"dsad\\",\\"bc_date\\":\\"2018-02-26T00:00:00+00:00\\",\\"date\\":\\"2018-02-15T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php46C4.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d_2.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":4,\\"code\\":9999,\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"dsad\\",\\"bc_date\\":\\"2018-02-26T00:00:00+00:00\\",\\"date\\":\\"2018-02-15T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":\\"imagetopdf-012355380d_2.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(94, '2018-02-26 10:01:10', 6, 'add', NULL, 'create', 1, 'receives', NULL, '"{\\"id\\":1,\\"code\\":10000,\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"dsad\\",\\"bc_date\\":\\"2018-02-26T00:00:00+00:00\\",\\"date\\":\\"2018-02-15T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpF4A9.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d_2.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":1,\\"code\\":10000,\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"dsad\\",\\"bc_date\\":\\"2018-02-26T00:00:00+00:00\\",\\"date\\":\\"2018-02-15T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":\\"imagetopdf-012355380d_2.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(95, '2018-02-26 10:17:35', 6, 'add', NULL, 'create', 2, 'receives', NULL, '"{\\"id\\":2,\\"code\\":27544,\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"dsad\\",\\"bc_date\\":\\"2018-02-28T00:00:00+00:00\\",\\"date\\":\\"2018-02-26T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpF9D1.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":2,\\"code\\":27544,\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"dsad\\",\\"bc_date\\":\\"2018-02-28T00:00:00+00:00\\",\\"date\\":\\"2018-02-26T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":\\"imagetopdf-012355380d.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(96, '2018-02-26 10:41:05', 6, 'delete', '1', 'delete', 1, 'receives', NULL, 'null', 'null', '"[]"'),
(97, '2018-02-26 10:47:46', 6, 'edit', '2', 'update', 2, 'receives', NULL, '"{\\"modified_by\\":0}"', '"{\\"modified_by\\":6}"', '"[]"'),
(98, '2018-02-26 11:39:52', 1, 'edit', '1', 'update', 1, 'pdplbs', NULL, '"{\\"logo\\":\\"logo.png\\",\\"logo_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\pdplb\\\\\\\\201802\\\\\\\\logo\\\\\\\\\\",\\"modified_by\\":1}"', '"{\\"logo\\":\\"logo (1).png\\",\\"logo_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\pdplb\\\\\\\\201802\\\\\\\\logo\\\\\\\\\\",\\"modified_by\\":1}"', '"[]"'),
(99, '2018-02-26 11:43:29', 1, 'edit', '1', 'update', 1, 'storages', NULL, '"{\\"address\\":\\"JONGGOL\\",\\"modified_by\\":null}"', '"{\\"address\\":\\"Lorem Ipsum is simply dummy text of the printing and typesetting industry scrambled\\",\\"modified_by\\":1}"', '"[]"'),
(100, '2018-02-26 11:46:19', 1, 'edit', '1', 'update', 1, 'storages', NULL, '"{\\"phone_number\\":\\"3213213\\",\\"fax_number\\":\\"32132132\\",\\"modified_by\\":1}"', '"{\\"phone_number\\":\\"5552432432\\",\\"fax_number\\":\\"432432432432\\",\\"modified_by\\":1}"', '"[]"'),
(101, '2018-02-27 04:17:02', 6, 'add', NULL, 'create', 3, 'receives', NULL, '"{\\"id\\":3,\\"code\\":4058,\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"dsad\\",\\"bc_date\\":\\"2018-02-27T00:00:00+00:00\\",\\"date\\":\\"2018-02-27T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php2EBF.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-83257a011e.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":3,\\"code\\":4058,\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"dsad\\",\\"bc_date\\":\\"2018-02-27T00:00:00+00:00\\",\\"date\\":\\"2018-02-27T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":\\"imagetopdf-83257a011e.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201802\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(102, '2018-03-07 04:25:12', 6, 'add', NULL, 'create', 3, 'customers', NULL, '"{\\"id\\":3,\\"name\\":\\"PT BUDITEX\\",\\"address\\":\\"test\\",\\"country_id\\":1,\\"phone\\":\\"0832818\\",\\"fax\\":\\"039218392\\",\\"email\\":\\"wakaw@gmail.com\\",\\"pic\\":\\"odso\\",\\"pic_phone\\":\\"idos\\",\\"status\\":true,\\"pdplb_id\\":1,\\"storage_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":3,\\"name\\":\\"PT BUDITEX\\",\\"address\\":\\"test\\",\\"country_id\\":1,\\"phone\\":\\"0832818\\",\\"fax\\":\\"039218392\\",\\"email\\":\\"wakaw@gmail.com\\",\\"pic\\":\\"odso\\",\\"pic_phone\\":\\"idos\\",\\"status\\":true,\\"pdplb_id\\":1,\\"storage_id\\":1,\\"created_by\\":6}"', '"[]"'),
(103, '2018-03-07 04:26:30', 6, 'add', NULL, 'create', 3, 'locations', NULL, '"{\\"id\\":3,\\"storage_id\\":1,\\"name\\":\\"RAK-02\\",\\"status\\":true,\\"created_by\\":6}"', '"{\\"id\\":3,\\"storage_id\\":1,\\"name\\":\\"RAK-02\\",\\"status\\":true,\\"created_by\\":6}"', '"[]"'),
(104, '2018-03-07 04:26:46', 6, 'edit', '1', 'update', 1, 'products', NULL, '"{\\"name\\":\\"Binih Kapas\\",\\"modified_by\\":null}"', '"{\\"name\\":\\"Biji Kapas\\",\\"modified_by\\":6}"', '"[]"'),
(105, '2018-03-07 04:33:12', 6, 'add', NULL, 'create', 2, 'products', NULL, '"{\\"id\\":2,\\"code\\":\\"111.01.01\\",\\"name\\":\\"Velg\\",\\"pdplb_id\\":1,\\"status\\":true,\\"created_by\\":6}"', '"{\\"id\\":2,\\"code\\":\\"111.01.01\\",\\"name\\":\\"Velg\\",\\"pdplb_id\\":1,\\"status\\":true,\\"created_by\\":6}"', '"[]"'),
(106, '2018-03-07 04:35:37', 6, 'add', NULL, 'create', 4, 'receives', NULL, '"{\\"id\\":4,\\"code\\":88835,\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"02931032019\\",\\"bc_date\\":\\"2018-03-07T00:00:00+00:00\\",\\"date\\":\\"2018-03-07T00:00:00+00:00\\",\\"customer_id\\":3,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php5249.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d_2.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201803\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":4,\\"code\\":88835,\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"02931032019\\",\\"bc_date\\":\\"2018-03-07T00:00:00+00:00\\",\\"date\\":\\"2018-03-07T00:00:00+00:00\\",\\"customer_id\\":3,\\"file\\":\\"imagetopdf-012355380d_2.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201803\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(107, '2018-03-26 05:39:45', 6, 'delete', '2', 'delete', 2, 'receives', NULL, 'null', 'null', '"[]"'),
(108, '2018-03-26 05:39:50', 6, 'delete', '3', 'delete', 3, 'receives', NULL, 'null', 'null', '"[]"'),
(109, '2018-03-26 05:39:56', 6, 'delete', '4', 'delete', 4, 'receives', NULL, 'null', 'null', '"[]"'),
(110, '2018-03-26 05:45:26', 6, 'add', NULL, 'create', 5, 'receives', NULL, '"{\\"id\\":5,\\"code\\":53495,\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"BC2.7-102\\\\/30192\\",\\"bc_date\\":\\"2018-03-27T00:00:00+00:00\\",\\"date\\":\\"2018-03-26T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php640E.tmp\\",\\"error\\":0,\\"name\\":\\"gammu.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1490438},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201803\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":5,\\"code\\":53495,\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"BC2.7-102\\\\/30192\\",\\"bc_date\\":\\"2018-03-27T00:00:00+00:00\\",\\"date\\":\\"2018-03-26T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":\\"gammu.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201803\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(111, '2018-03-26 05:48:38', 6, 'edit', '5', 'update', 5, 'receives', NULL, '"{\\"modified_by\\":0}"', '"{\\"modified_by\\":6}"', '"[]"'),
(112, '2018-03-26 10:46:11', 6, 'add', NULL, 'create', 1, 'deliveries', NULL, '"{\\"id\\":1,\\"code\\":90705,\\"bc_type\\":\\"BC 2.8\\",\\"bc_code\\":\\"BC2.7-102\\\\/30192\\",\\"bc_date\\":\\"2018-02-28\\",\\"date\\":\\"2018-03-26T00:00:00+00:00\\",\\"picking_list_id\\":4,\\"customer_id\\":3,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpB316.tmp\\",\\"error\\":0,\\"name\\":\\"userQuotations_3.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":40915},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\deliveries\\\\\\\\201803\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":1,\\"code\\":90705,\\"bc_type\\":\\"BC 2.8\\",\\"bc_code\\":\\"BC2.7-102\\\\/30192\\",\\"bc_date\\":\\"2018-02-28\\",\\"date\\":\\"2018-03-26T00:00:00+00:00\\",\\"picking_list_id\\":4,\\"customer_id\\":3,\\"file\\":\\"userQuotations_3.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\deliveries\\\\\\\\201803\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(113, '2018-03-26 10:48:05', 6, 'add', NULL, 'create', 2, 'deliveries', NULL, '"{\\"id\\":2,\\"code\\":27678,\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"BC2.7-102\\\\/30192\\",\\"bc_date\\":\\"2018-03-26\\",\\"date\\":\\"2018-03-26T00:00:00+00:00\\",\\"picking_list_id\\":4,\\"customer_id\\":3,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php6F01.tmp\\",\\"error\\":0,\\"name\\":\\"userQuotations_3.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":40915},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\deliveries\\\\\\\\201803\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":2,\\"code\\":27678,\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"BC2.7-102\\\\/30192\\",\\"bc_date\\":\\"2018-03-26\\",\\"date\\":\\"2018-03-26T00:00:00+00:00\\",\\"picking_list_id\\":4,\\"customer_id\\":3,\\"file\\":\\"userQuotations_3.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\deliveries\\\\\\\\201803\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(114, '2018-03-26 10:57:55', 6, 'delete', '2', 'delete', 2, 'deliveries', NULL, 'null', 'null', '"[]"'),
(115, '2018-03-26 10:58:15', 6, 'add', NULL, 'create', 3, 'deliveries', NULL, '"{\\"id\\":3,\\"code\\":75734,\\"bc_type\\":\\"BC 2.8\\",\\"bc_code\\":\\"BC2.7-102\\\\/30192\\",\\"bc_date\\":\\"2018-03-26T00:00:00+00:00\\",\\"date\\":\\"2018-03-26T00:00:00+00:00\\",\\"picking_list_id\\":4,\\"customer_id\\":3,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpBEFB.tmp\\",\\"error\\":0,\\"name\\":\\"userQuotations_3.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":40915},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\deliveries\\\\\\\\201803\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":3,\\"code\\":75734,\\"bc_type\\":\\"BC 2.8\\",\\"bc_code\\":\\"BC2.7-102\\\\/30192\\",\\"bc_date\\":\\"2018-03-26T00:00:00+00:00\\",\\"date\\":\\"2018-03-26T00:00:00+00:00\\",\\"picking_list_id\\":4,\\"customer_id\\":3,\\"file\\":\\"userQuotations_3.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\deliveries\\\\\\\\201803\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(116, '2018-03-26 11:01:29', 6, 'delete', '3', 'delete', 3, 'deliveries', NULL, 'null', 'null', '"[]"'),
(117, '2018-03-26 11:01:53', 6, 'add', NULL, 'create', 4, 'deliveries', NULL, '"{\\"id\\":4,\\"code\\":85989,\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"BC2.7-102\\\\/30192\\",\\"bc_date\\":\\"2018-02-26T00:00:00+00:00\\",\\"date\\":\\"2018-03-26T00:00:00+00:00\\",\\"picking_list_id\\":4,\\"customer_id\\":3,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php13F3.tmp\\",\\"error\\":0,\\"name\\":\\"userQuotations_3.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":40915},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\deliveries\\\\\\\\201803\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":4,\\"code\\":85989,\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"BC2.7-102\\\\/30192\\",\\"bc_date\\":\\"2018-02-26T00:00:00+00:00\\",\\"date\\":\\"2018-03-26T00:00:00+00:00\\",\\"picking_list_id\\":4,\\"customer_id\\":3,\\"file\\":\\"userQuotations_3.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\deliveries\\\\\\\\201803\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(118, '2018-03-26 11:36:03', 6, 'edit', '3', 'update', 3, 'locations', NULL, '"{\\"name\\":\\"RAK-02\\",\\"modified_by\\":null}"', '"{\\"name\\":\\"BLOK A - 001\\",\\"modified_by\\":6}"', '"[]"'),
(119, '2018-03-26 11:36:17', 6, 'edit', '3', 'update', 3, 'locations', NULL, '"{\\"name\\":\\"BLOK A - 001\\",\\"modified_by\\":6}"', '"{\\"name\\":\\"BLOK A - 001 - 001\\",\\"modified_by\\":6}"', '"[]"'),
(120, '2018-03-26 11:36:35', 6, 'edit', '2', 'update', 2, 'locations', NULL, '"{\\"name\\":\\"FLOOR-01\\",\\"modified_by\\":6}"', '"{\\"name\\":\\"BLOK A - 001 - 002\\",\\"modified_by\\":6}"', '"[]"'),
(121, '2018-03-26 11:37:30', 6, 'edit', '1', 'update', 1, 'locations', NULL, '"{\\"name\\":\\"RAK-01\\",\\"modified_by\\":null}"', '"{\\"name\\":\\"BLOK A - 001 - 003\\",\\"modified_by\\":6}"', '"[]"'),
(122, '2018-03-26 11:39:51', 6, 'edit', '3', 'update', 3, 'locations', NULL, '"{\\"name\\":\\"BLOK A - 001 - 001\\",\\"modified_by\\":6}"', '"{\\"name\\":\\"BLOK A - K001 - T001\\",\\"modified_by\\":6}"', '"[]"'),
(123, '2018-03-26 11:40:21', 6, 'edit', '2', 'update', 2, 'locations', NULL, '"{\\"name\\":\\"BLOK A - 001 - 002\\",\\"modified_by\\":6}"', '"{\\"name\\":\\"BLOK A - K001 - T002\\",\\"modified_by\\":6}"', '"[]"'),
(124, '2018-03-26 11:40:33', 6, 'edit', '1', 'update', 1, 'locations', NULL, '"{\\"name\\":\\"BLOK A - 001 - 003\\",\\"modified_by\\":6}"', '"{\\"name\\":\\"BLOK A - K001 - T003\\",\\"modified_by\\":6}"', '"[]"'),
(125, '2018-03-26 11:47:17', 6, 'add', NULL, 'create', 6, 'receives', NULL, '"{\\"id\\":6,\\"code\\":64454,\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"BC1.6\\\\/001\\\\/002\\",\\"bc_date\\":\\"2018-03-26T00:00:00+00:00\\",\\"date\\":\\"2018-03-26T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpA0BF.tmp\\",\\"error\\":0,\\"name\\":\\"userQuotations_3.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":40915},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201803\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":6,\\"code\\":64454,\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"BC1.6\\\\/001\\\\/002\\",\\"bc_date\\":\\"2018-03-26T00:00:00+00:00\\",\\"date\\":\\"2018-03-26T00:00:00+00:00\\",\\"customer_id\\":1,\\"file\\":\\"userQuotations_3.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201803\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(126, '2018-04-09 06:26:34', 6, 'add', NULL, 'create', 12, 'countries', NULL, '"{\\"id\\":12,\\"name\\":\\"Prancis\\",\\"status\\":true,\\"created_by\\":6}"', '"{\\"id\\":12,\\"name\\":\\"Prancis\\",\\"status\\":true,\\"created_by\\":6}"', '"[]"'),
(127, '2018-04-09 08:32:32', 6, 'edit', '4', 'update', 4, 'currencies', NULL, '"{\\"kurs_idr\\":null,\\"modified_by\\":null}"', '"{\\"kurs_idr\\":13000,\\"modified_by\\":6}"', '"[]"'),
(128, '2018-04-10 03:36:34', 6, 'add', NULL, 'create', 1, 'products', NULL, '"{\\"id\\":1,\\"code\\":\\"64029962\\",\\"name\\":\\"SOFT TEA QUAT-PALM\\\\/ETHANOL   STEPANTEX SP-88-2\\",\\"pdplb_id\\":1,\\"status\\":true,\\"created_by\\":6}"', '"{\\"id\\":1,\\"code\\":\\"64029962\\",\\"name\\":\\"SOFT TEA QUAT-PALM\\\\/ETHANOL   STEPANTEX SP-88-2\\",\\"pdplb_id\\":1,\\"status\\":true,\\"created_by\\":6}"', '"[]"'),
(129, '2018-04-10 03:38:08', 6, 'add', NULL, 'create', 2, 'products', NULL, '"{\\"id\\":2,\\"code\\":\\"21037888\\",\\"name\\":\\"NON GMO REFINED BLEACHED DEODORIZED CANOLA OIL \\",\\"pdplb_id\\":1,\\"status\\":true,\\"created_by\\":6}"', '"{\\"id\\":2,\\"code\\":\\"21037888\\",\\"name\\":\\"NON GMO REFINED BLEACHED DEODORIZED CANOLA OIL \\",\\"pdplb_id\\":1,\\"status\\":true,\\"created_by\\":6}"', '"[]"'),
(130, '2018-04-10 03:44:12', 6, 'add', NULL, 'create', 3, 'products', NULL, '"{\\"id\\":3,\\"code\\":\\"62122204\\",\\"name\\":\\"CHOCOLATE DIPPING  YYD-A90541-UID-S75\\",\\"pdplb_id\\":1,\\"status\\":true,\\"created_by\\":6}"', '"{\\"id\\":3,\\"code\\":\\"62122204\\",\\"name\\":\\"CHOCOLATE DIPPING  YYD-A90541-UID-S75\\",\\"pdplb_id\\":1,\\"status\\":true,\\"created_by\\":6}"', '"[]"'),
(131, '2018-04-10 10:47:31', 6, 'add', NULL, 'create', 4, 'products', NULL, '"{\\"id\\":4,\\"code\\":\\"test\\",\\"name\\":\\"wakawa\\",\\"pdplb_id\\":1,\\"status\\":true,\\"created_by\\":6}"', '"{\\"id\\":4,\\"code\\":\\"test\\",\\"name\\":\\"wakawa\\",\\"pdplb_id\\":1,\\"status\\":true,\\"created_by\\":6}"', '"[]"'),
(132, '2018-04-10 10:47:45', 6, 'delete', '4', 'delete', 4, 'products', NULL, 'null', 'null', '"[]"'),
(133, '2018-04-10 03:48:30', 6, 'edit', '1', 'update', 1, 'products', NULL, '"{\\"status\\":true,\\"modified_by\\":null}"', '"{\\"status\\":false,\\"modified_by\\":6}"', '"[]"'),
(134, '2018-04-10 03:49:04', 6, 'edit', '1', 'update', 1, 'products', NULL, '"{\\"status\\":false,\\"modified_by\\":6}"', '"{\\"status\\":true,\\"modified_by\\":6}"', '"[]"'),
(135, '2018-04-10 10:51:37', 6, 'edit', '2', 'update', 2, 'products', NULL, '"{\\"status\\":true,\\"modified_by\\":null}"', '"{\\"status\\":false,\\"modified_by\\":6}"', '"[]"'),
(136, '2018-04-11 12:20:05', 6, 'add', NULL, 'create', 6, 'receives', NULL, '"{\\"id\\":6,\\"code\\":\\"STOCK-IN\\\\/4\\\\/11\\\\/18\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpF500.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\STOCK-IN\\\\/4\\\\/11\\\\/18\\\\/000001\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":6,\\"code\\":\\"STOCK-IN\\\\/4\\\\/11\\\\/18\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"imagetopdf-012355380d.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\STOCK-IN\\\\/4\\\\/11\\\\/18\\\\/000001\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(137, '2018-04-11 12:24:37', 6, 'add', NULL, 'create', 7, 'receives', NULL, '"{\\"id\\":7,\\"code\\":\\"STOCK-IN\\\\/4\\\\/11\\\\/18\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php1DB5.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":7,\\"code\\":\\"STOCK-IN\\\\/4\\\\/11\\\\/18\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"imagetopdf-012355380d.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"');
INSERT INTO `audit_logs` (`id`, `timestamp`, `user_id`, `controller`, `_action`, `type`, `primary_key`, `source`, `parent_source`, `original`, `changed`, `meta`) VALUES
(138, '2018-04-11 12:25:44', 6, 'add', NULL, 'create', 8, 'receives', NULL, '"{\\"id\\":8,\\"code\\":\\"STOCK-IN\\\\/4\\\\/11\\\\/18\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php2080.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":8,\\"code\\":\\"STOCK-IN\\\\/4\\\\/11\\\\/18\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"imagetopdf-012355380d.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(139, '2018-04-11 12:26:11', 6, 'add', NULL, 'create', 9, 'receives', NULL, '"{\\"id\\":9,\\"code\\":\\"STOCK-IN\\\\/4\\\\/11\\\\/18\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php8C0C.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":9,\\"code\\":\\"STOCK-IN\\\\/4\\\\/11\\\\/18\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"imagetopdf-012355380d.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(140, '2018-04-11 12:26:26', 6, 'add', NULL, 'create', 10, 'receives', NULL, '"{\\"id\\":10,\\"code\\":\\"STOCK-IN\\\\/4\\\\/11\\\\/18\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpC7CF.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":10,\\"code\\":\\"STOCK-IN\\\\/4\\\\/11\\\\/18\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"imagetopdf-012355380d.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(141, '2018-04-11 12:26:56', 6, 'add', NULL, 'create', 11, 'receives', NULL, '"{\\"id\\":11,\\"code\\":\\"STOCK-IN\\\\/4\\\\/11\\\\/18\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php3A12.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":11,\\"code\\":\\"STOCK-IN\\\\/4\\\\/11\\\\/18\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"imagetopdf-012355380d.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(142, '2018-04-11 12:27:27', 6, 'add', NULL, 'create', 12, 'receives', NULL, '"{\\"id\\":12,\\"code\\":\\"STOCK-IN\\\\/4\\\\/11\\\\/18\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpB57D.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":12,\\"code\\":\\"STOCK-IN\\\\/4\\\\/11\\\\/18\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"imagetopdf-012355380d.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(143, '2018-04-11 12:28:12', 6, 'add', NULL, 'create', 13, 'receives', NULL, '"{\\"id\\":13,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php643D.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":13,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"imagetopdf-012355380d.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(144, '2018-04-11 12:28:33', 6, 'add', NULL, 'create', 14, 'receives', NULL, '"{\\"id\\":14,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpB4DF.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":14,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"imagetopdf-012355380d.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(145, '2018-04-11 12:29:42', 6, 'add', NULL, 'create', 15, 'receives', NULL, '"{\\"id\\":15,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000002\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpC4F8.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":15,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000002\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"imagetopdf-012355380d.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(146, '2018-04-11 12:30:31', 6, 'add', NULL, 'create', 16, 'receives', NULL, '"{\\"id\\":16,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/0000011\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php829D.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":16,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/0000011\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"imagetopdf-012355380d.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(147, '2018-04-11 12:30:58', 6, 'add', NULL, 'create', 17, 'receives', NULL, '"{\\"id\\":17,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/0000011\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpEC06.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":17,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/0000011\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"imagetopdf-012355380d.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(148, '2018-04-11 12:31:36', 6, 'add', NULL, 'create', 18, 'receives', NULL, '"{\\"id\\":18,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/0000011\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php8191.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":18,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/0000011\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"imagetopdf-012355380d.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(149, '2018-04-11 12:34:56', 6, 'add', NULL, 'create', 19, 'receives', NULL, '"{\\"id\\":19,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/0000012\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php8D3C.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":19,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/0000012\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"imagetopdf-012355380d.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(150, '2018-04-11 12:36:25', 6, 'add', NULL, 'create', 1, 'receives', NULL, '"{\\"id\\":1,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpE972.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":1,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"imagetopdf-012355380d.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(151, '2018-04-11 12:37:07', 6, 'add', NULL, 'create', 1, 'receives', NULL, '"{\\"id\\":1,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php8FF4.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":1,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"imagetopdf-012355380d.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(152, '2018-04-11 12:39:11', 6, 'add', NULL, 'create', 2, 'receives', NULL, '"{\\"id\\":2,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000002\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php7186.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":2,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000002\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"imagetopdf-012355380d.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(153, '2018-04-11 12:39:42', 6, 'add', NULL, 'create', 3, 'receives', NULL, '"{\\"id\\":3,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000003\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpED4F.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":3,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000003\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"imagetopdf-012355380d.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(154, '2018-04-11 12:40:15', 6, 'add', NULL, 'create', 4, 'receives', NULL, '"{\\"id\\":4,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000004\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php6DBB.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":4,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000004\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2\\",\\"bc_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"imagetopdf-012355380d.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(155, '2018-04-11 14:00:23', 6, 'add', NULL, 'create', 9, 'receives', NULL, '"{\\"id\\":9,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"sdpsodap\\",\\"bc_date\\":\\"2018-02-19T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":3,\\"supplier_id\\":1,\\"sender_id\\":1,\\"po_code\\":\\"32132\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"3213\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpC927.tmp\\",\\"error\\":0,\\"name\\":\\"tahupme2018.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":999601},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":9,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"sdpsodap\\",\\"bc_date\\":\\"2018-02-19T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":3,\\"supplier_id\\":1,\\"sender_id\\":1,\\"po_code\\":\\"32132\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"3213\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"tahupme2018.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(156, '2018-04-11 14:12:22', 6, 'add', NULL, 'create', 1, 'receives', NULL, '"{\\"id\\":1,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/03\\\\/000001\\",\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"231321\\",\\"bc_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"date\\":\\"2018-03-29T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":1,\\"sender_id\\":1,\\"po_code\\":\\"2\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"2\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpC1C2.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d_2.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":1,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/03\\\\/000001\\",\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"231321\\",\\"bc_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"date\\":\\"2018-03-29T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":1,\\"sender_id\\":1,\\"po_code\\":\\"2\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"2\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"imagetopdf-012355380d_2.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(157, '2018-04-11 14:12:58', 6, 'add', NULL, 'create', 2, 'receives', NULL, '"{\\"id\\":2,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/03\\\\/000002\\",\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"231321\\",\\"bc_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"date\\":\\"2018-03-29T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":1,\\"sender_id\\":1,\\"po_code\\":\\"2\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"2\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php4F2E.tmp\\",\\"error\\":0,\\"name\\":\\"imagetopdf-012355380d_2.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1081040},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":2,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/03\\\\/000002\\",\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"231321\\",\\"bc_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"date\\":\\"2018-03-29T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":1,\\"sender_id\\":1,\\"po_code\\":\\"2\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"2\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"imagetopdf-012355380d_2.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(158, '2018-04-11 17:27:33', 6, 'add', NULL, 'create', 3, 'receives', NULL, '"{\\"id\\":3,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"321321\\",\\"bc_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":1,\\"sender_id\\":2,\\"po_code\\":\\"321321321\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php6FD5.tmp\\",\\"error\\":0,\\"name\\":\\"userQuotations_2.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":5706},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":3,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"321321\\",\\"bc_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":1,\\"sender_id\\":2,\\"po_code\\":\\"321321321\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"file\\":\\"userQuotations_2.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(159, '2018-04-11 17:44:00', 6, 'edit', '2', 'update', 2, 'products', NULL, '"{\\"status\\":false,\\"modified_by\\":6}"', '"{\\"status\\":true,\\"modified_by\\":6}"', '"[]"'),
(160, '2018-04-11 17:50:59', 6, 'add', NULL, 'create', 1, 'receives', NULL, '"{\\"id\\":1,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"160216-000607-20180103-000286\\",\\"bc_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":3,\\"supplier_id\\":1,\\"sender_id\\":2,\\"po_code\\":\\"P26061\\",\\"po_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"bl_code\\":\\"NAM2895549\\",\\"bl_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"currency_id\\":2,\\"value_type\\":\\"CIF\\",\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpE82D.tmp\\",\\"error\\":0,\\"name\\":\\"userQuotations.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":5020},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":1,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"160216-000607-20180103-000286\\",\\"bc_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":3,\\"supplier_id\\":1,\\"sender_id\\":2,\\"po_code\\":\\"P26061\\",\\"po_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"bl_code\\":\\"NAM2895549\\",\\"bl_date\\":\\"2018-04-10T00:00:00+00:00\\",\\"currency_id\\":2,\\"value_type\\":\\"CIF\\",\\"file\\":\\"userQuotations.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(161, '2018-04-11 20:13:29', 6, 'add', NULL, 'create', 1, 'receives', NULL, '"{\\"id\\":1,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"dsad\\",\\"bc_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":1,\\"sender_id\\":1,\\"po_code\\":\\"PO.02001\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"BL.00212\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"_value\\":3200000,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php5D84.tmp\\",\\"error\\":0,\\"name\\":\\"userQuotations_4.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":5706},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":1,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"dsad\\",\\"bc_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"date\\":\\"2018-04-11T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":1,\\"sender_id\\":1,\\"po_code\\":\\"PO.02001\\",\\"po_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"bl_code\\":\\"BL.00212\\",\\"bl_date\\":\\"2018-04-11T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"_value\\":3200000,\\"file\\":\\"userQuotations_4.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(162, '2018-04-12 09:52:20', 6, 'add', NULL, 'create', 2, 'receives', NULL, '"{\\"id\\":2,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000002\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2313213\\",\\"bc_date\\":\\"2018-04-13T00:00:00+00:00\\",\\"date\\":\\"2018-04-12T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":1,\\"sender_id\\":1,\\"po_code\\":\\"321321\\",\\"po_date\\":\\"2018-04-12T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-12T00:00:00+00:00\\",\\"currency_id\\":2,\\"value_type\\":\\"CIF\\",\\"_value\\":300,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpBDC.tmp\\",\\"error\\":0,\\"name\\":\\"userQuotations_4.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":5706},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":2,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000002\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"2313213\\",\\"bc_date\\":\\"2018-04-13T00:00:00+00:00\\",\\"date\\":\\"2018-04-12T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":1,\\"sender_id\\":1,\\"po_code\\":\\"321321\\",\\"po_date\\":\\"2018-04-12T00:00:00+00:00\\",\\"bl_code\\":\\"321321\\",\\"bl_date\\":\\"2018-04-12T00:00:00+00:00\\",\\"currency_id\\":2,\\"value_type\\":\\"CIF\\",\\"_value\\":300,\\"file\\":\\"userQuotations_4.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(163, '2018-04-12 09:54:03', 6, 'add', NULL, 'create', 3, 'receives', NULL, '"{\\"id\\":3,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000003\\",\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"23132\\",\\"bc_date\\":\\"2018-04-12T00:00:00+00:00\\",\\"date\\":\\"2018-04-12T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-12T00:00:00+00:00\\",\\"bl_code\\":\\"3213\\",\\"bl_date\\":\\"2018-04-12T00:00:00+00:00\\",\\"currency_id\\":2,\\"value_type\\":\\"CIF\\",\\"_value\\":2000,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php9ED1.tmp\\",\\"error\\":0,\\"name\\":\\"userQuotations_4.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":5706},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":3,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000003\\",\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"23132\\",\\"bc_date\\":\\"2018-04-12T00:00:00+00:00\\",\\"date\\":\\"2018-04-12T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":2,\\"sender_id\\":1,\\"po_code\\":\\"32131\\",\\"po_date\\":\\"2018-04-12T00:00:00+00:00\\",\\"bl_code\\":\\"3213\\",\\"bl_date\\":\\"2018-04-12T00:00:00+00:00\\",\\"currency_id\\":2,\\"value_type\\":\\"CIF\\",\\"_value\\":2000,\\"file\\":\\"userQuotations_4.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(164, '2018-04-12 15:52:04', 6, 'add', NULL, 'create', 5, 'deliveries', NULL, '"{\\"id\\":5,\\"code\\":\\"STOCK-OUT\\\\/2018\\\\/04\\\\/000001\\",\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"321321321\\",\\"bc_date\\":\\"2018-04-12T00:00:00+00:00\\",\\"date\\":\\"2018-04-12T00:00:00+00:00\\",\\"customer_id\\":1,\\"receiver\\":\\"TEGAR\\",\\"receiver_address\\":\\"bogor\\",\\"currency_id\\":3,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php655C.tmp\\",\\"error\\":0,\\"name\\":\\"userQuotations_3.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":5696},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\deliveries\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":5,\\"code\\":\\"STOCK-OUT\\\\/2018\\\\/04\\\\/000001\\",\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"321321321\\",\\"bc_date\\":\\"2018-04-12T00:00:00+00:00\\",\\"date\\":\\"2018-04-12T00:00:00+00:00\\",\\"customer_id\\":1,\\"receiver\\":\\"TEGAR\\",\\"receiver_address\\":\\"bogor\\",\\"currency_id\\":3,\\"file\\":\\"userQuotations_3.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\deliveries\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(165, '2018-04-12 16:08:25', 6, 'add', NULL, 'create', 6, 'deliveries', NULL, '"{\\"id\\":6,\\"code\\":\\"STOCK-OUT\\\\/2018\\\\/04\\\\/000002\\",\\"bc_type\\":\\"BC 2.8\\",\\"bc_code\\":\\"dsad\\",\\"bc_date\\":\\"2018-04-12T00:00:00+00:00\\",\\"date\\":\\"2018-04-12T00:00:00+00:00\\",\\"customer_id\\":1,\\"receiver\\":\\"dsadsad\\",\\"receiver_address\\":\\"sadsada\\",\\"currency_id\\":2,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php5CD9.tmp\\",\\"error\\":0,\\"name\\":\\"userQuotations_4.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":5706},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\deliveries\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":6,\\"code\\":\\"STOCK-OUT\\\\/2018\\\\/04\\\\/000002\\",\\"bc_type\\":\\"BC 2.8\\",\\"bc_code\\":\\"dsad\\",\\"bc_date\\":\\"2018-04-12T00:00:00+00:00\\",\\"date\\":\\"2018-04-12T00:00:00+00:00\\",\\"customer_id\\":1,\\"receiver\\":\\"dsadsad\\",\\"receiver_address\\":\\"sadsada\\",\\"currency_id\\":2,\\"file\\":\\"userQuotations_4.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\deliveries\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(166, '2018-04-12 17:12:33', 6, 'add', NULL, 'create', 4, 'receives', NULL, '"{\\"id\\":4,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000004\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"3213213213\\",\\"bc_date\\":\\"2018-04-12T00:00:00+00:00\\",\\"date\\":\\"2018-04-12T00:00:00+00:00\\",\\"customer_id\\":3,\\"supplier_id\\":1,\\"sender_id\\":1,\\"register_code\\":\\"321321\\",\\"register_date\\":\\"2018-04-12T00:00:00+00:00\\",\\"po_code\\":\\"321321\\",\\"po_date\\":\\"2018-04-12T00:00:00+00:00\\",\\"bl_code\\":\\"3213\\",\\"bl_date\\":\\"2018-04-12T00:00:00+00:00\\",\\"currency_id\\":2,\\"value_type\\":\\"CIF\\",\\"_value\\":20000,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php111E.tmp\\",\\"error\\":0,\\"name\\":\\"userQuotations_4.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":5706},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":4,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/04\\\\/000004\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"3213213213\\",\\"bc_date\\":\\"2018-04-12T00:00:00+00:00\\",\\"date\\":\\"2018-04-12T00:00:00+00:00\\",\\"customer_id\\":3,\\"supplier_id\\":1,\\"sender_id\\":1,\\"register_code\\":\\"321321\\",\\"register_date\\":\\"2018-04-12T00:00:00+00:00\\",\\"po_code\\":\\"321321\\",\\"po_date\\":\\"2018-04-12T00:00:00+00:00\\",\\"bl_code\\":\\"3213\\",\\"bl_date\\":\\"2018-04-12T00:00:00+00:00\\",\\"currency_id\\":2,\\"value_type\\":\\"CIF\\",\\"_value\\":20000,\\"file\\":\\"userQuotations_4.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(167, '2018-04-13 01:17:38', 6, 'add', NULL, 'create', 1, 'deliveries', NULL, '"{\\"id\\":1,\\"code\\":\\"STOCK-OUT\\\\/2018\\\\/04\\\\/000001\\",\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"33233213213213\\",\\"bc_date\\":\\"2018-04-13T00:00:00+00:00\\",\\"date\\":\\"2018-04-13T00:00:00+00:00\\",\\"customer_id\\":1,\\"receiver\\":\\"Uniliver\\",\\"receiver_address\\":\\"dsadsa\\",\\"currency_id\\":2,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php2DF6.tmp\\",\\"error\\":0,\\"name\\":\\"userQuotations_4.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":5706},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\deliveries\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":1,\\"code\\":\\"STOCK-OUT\\\\/2018\\\\/04\\\\/000001\\",\\"bc_type\\":\\"BC 2.7\\",\\"bc_code\\":\\"33233213213213\\",\\"bc_date\\":\\"2018-04-13T00:00:00+00:00\\",\\"date\\":\\"2018-04-13T00:00:00+00:00\\",\\"customer_id\\":1,\\"receiver\\":\\"Uniliver\\",\\"receiver_address\\":\\"dsadsa\\",\\"currency_id\\":2,\\"file\\":\\"userQuotations_4.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\deliveries\\\\\\\\201804\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(168, '2018-05-03 20:36:39', 6, 'delete', '1', 'delete', 1, 'deliveries', NULL, 'null', 'null', '"[]"'),
(169, '2018-05-03 20:37:29', 6, 'delete', '4', 'delete', 4, 'receives', NULL, 'null', 'null', '"[]"'),
(170, '2018-05-03 21:09:52', 6, 'edit', '1', 'update', 1, 'products', NULL, '"{\\"name\\":\\"SOFT TEA QUAT-PALM\\\\/ETHANOL   STEPANTEX SP-88-2\\",\\"modified_by\\":6}"', '"{\\"name\\":\\"KAPAS BRAZIL TIDAK DIGARUK\\\\/DISISIR\\",\\"modified_by\\":6}"', '"[]"'),
(171, '2018-05-03 21:11:06', 6, 'edit', '2', 'update', 2, 'products', NULL, '"{\\"name\\":\\"NON GMO REFINED BLEACHED DEODORIZED CANOLA OIL \\",\\"modified_by\\":6}"', '"{\\"name\\":\\"KAPAS MEXICAN TIDAK DIGARUK\\\\/DISISIR\\",\\"modified_by\\":6}"', '"[]"'),
(172, '2018-05-03 21:32:34', 6, 'add', NULL, 'create', 5, 'receives', NULL, '"{\\"id\\":5,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/05\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"0000-000542-20180321-009701\\",\\"bc_date\\":\\"2018-04-04T00:00:00+00:00\\",\\"date\\":\\"2018-05-03T00:00:00+00:00\\",\\"customer_id\\":3,\\"supplier_id\\":1,\\"sender_id\\":1,\\"register_code\\":\\"876789876\\",\\"register_date\\":\\"2018-05-03T00:00:00+00:00\\",\\"po_code\\":\\"90998\\",\\"po_date\\":\\"2018-05-03T00:00:00+00:00\\",\\"bl_code\\":\\"98898\\",\\"bl_date\\":\\"2018-05-03T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"_value\\":158290.69,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpB818.tmp\\",\\"error\\":0,\\"name\\":\\"reportreceives (1).pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1141},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201805\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":5,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/05\\\\/000001\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"0000-000542-20180321-009701\\",\\"bc_date\\":\\"2018-04-04T00:00:00+00:00\\",\\"date\\":\\"2018-05-03T00:00:00+00:00\\",\\"customer_id\\":3,\\"supplier_id\\":1,\\"sender_id\\":1,\\"register_code\\":\\"876789876\\",\\"register_date\\":\\"2018-05-03T00:00:00+00:00\\",\\"po_code\\":\\"90998\\",\\"po_date\\":\\"2018-05-03T00:00:00+00:00\\",\\"bl_code\\":\\"98898\\",\\"bl_date\\":\\"2018-05-03T00:00:00+00:00\\",\\"currency_id\\":1,\\"value_type\\":\\"CIF\\",\\"_value\\":158290.69,\\"file\\":\\"reportreceives (1).pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201805\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"');
INSERT INTO `audit_logs` (`id`, `timestamp`, `user_id`, `controller`, `_action`, `type`, `primary_key`, `source`, `parent_source`, `original`, `changed`, `meta`) VALUES
(173, '2018-05-03 21:47:52', 6, 'add', NULL, 'create', 6, 'receives', NULL, '"{\\"id\\":6,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/05\\\\/000002\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"00000-000542-20180324-009717\\",\\"bc_date\\":\\"2018-04-02T00:00:00+00:00\\",\\"date\\":\\"2018-05-03T00:00:00+00:00\\",\\"customer_id\\":3,\\"supplier_id\\":1,\\"sender_id\\":1,\\"register_code\\":\\"165573\\",\\"register_date\\":\\"2018-04-02T00:00:00+00:00\\",\\"po_code\\":\\"988988\\",\\"po_date\\":\\"2018-04-01T00:00:00+00:00\\",\\"bl_code\\":\\"888776678\\",\\"bl_date\\":\\"2018-05-03T00:00:00+00:00\\",\\"currency_id\\":4,\\"kurs_idr\\":13000,\\"value_type\\":\\"NTR\\",\\"_value\\":119988.56,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpBBE0.tmp\\",\\"error\\":0,\\"name\\":\\"SPPB BC 1.6 AJU 000286.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":13062},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201805\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":6,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/05\\\\/000002\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"00000-000542-20180324-009717\\",\\"bc_date\\":\\"2018-04-02T00:00:00+00:00\\",\\"date\\":\\"2018-05-03T00:00:00+00:00\\",\\"customer_id\\":3,\\"supplier_id\\":1,\\"sender_id\\":1,\\"register_code\\":\\"165573\\",\\"register_date\\":\\"2018-04-02T00:00:00+00:00\\",\\"po_code\\":\\"988988\\",\\"po_date\\":\\"2018-04-01T00:00:00+00:00\\",\\"bl_code\\":\\"888776678\\",\\"bl_date\\":\\"2018-05-03T00:00:00+00:00\\",\\"currency_id\\":4,\\"kurs_idr\\":13000,\\"value_type\\":\\"NTR\\",\\"_value\\":119988.56,\\"file\\":\\"SPPB BC 1.6 AJU 000286.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201805\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(174, '2018-05-03 21:54:14', 6, 'add', NULL, 'create', 7, 'receives', NULL, '"{\\"id\\":7,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/05\\\\/000003\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"00000-000542-20180321-009699\\",\\"bc_date\\":\\"2018-03-28T00:00:00+00:00\\",\\"date\\":\\"2018-05-03T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":1,\\"sender_id\\":1,\\"register_code\\":\\"232819321\\",\\"register_date\\":\\"2018-05-03T00:00:00+00:00\\",\\"po_code\\":\\"3213291\\",\\"po_date\\":\\"2018-05-04T00:00:00+00:00\\",\\"bl_code\\":\\"321\\",\\"bl_date\\":\\"2018-05-03T00:00:00+00:00\\",\\"currency_id\\":4,\\"kurs_idr\\":13000,\\"value_type\\":\\"NTR\\",\\"_value\\":127439.48,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\php937A.tmp\\",\\"error\\":0,\\"name\\":\\"NOPEN BC 1.6 AJU 000286.pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":27132},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201805\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":7,\\"code\\":\\"STOCK-IN\\\\/2018\\\\/05\\\\/000003\\",\\"bc_type\\":\\"BC 1.6\\",\\"bc_code\\":\\"00000-000542-20180321-009699\\",\\"bc_date\\":\\"2018-03-28T00:00:00+00:00\\",\\"date\\":\\"2018-05-03T00:00:00+00:00\\",\\"customer_id\\":1,\\"supplier_id\\":1,\\"sender_id\\":1,\\"register_code\\":\\"232819321\\",\\"register_date\\":\\"2018-05-03T00:00:00+00:00\\",\\"po_code\\":\\"3213291\\",\\"po_date\\":\\"2018-05-04T00:00:00+00:00\\",\\"bl_code\\":\\"321\\",\\"bl_date\\":\\"2018-05-03T00:00:00+00:00\\",\\"currency_id\\":4,\\"kurs_idr\\":13000,\\"value_type\\":\\"NTR\\",\\"_value\\":127439.48,\\"file\\":\\"NOPEN BC 1.6 AJU 000286.pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\receives\\\\\\\\201805\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(175, '2018-05-03 21:56:35', 6, 'add', NULL, 'create', 2, 'deliveries', NULL, '"{\\"id\\":2,\\"code\\":\\"STOCK-OUT\\\\/2018\\\\/05\\\\/000001\\",\\"bc_type\\":\\"BC 2.8\\",\\"bc_code\\":\\"0000-00321-992321-921\\",\\"bc_date\\":\\"2018-05-03T00:00:00+00:00\\",\\"date\\":\\"2018-05-03T00:00:00+00:00\\",\\"customer_id\\":1,\\"receiver\\":\\"Uniliver\\",\\"receiver_address\\":\\"EWQEWQ\\",\\"currency_id\\":1,\\"file\\":{\\"tmp_name\\":\\"C:\\\\\\\\Users\\\\\\\\Thinkpad420s\\\\\\\\AppData\\\\\\\\Local\\\\\\\\Temp\\\\\\\\phpB812.tmp\\",\\"error\\":0,\\"name\\":\\"reportreceives (1).pdf\\",\\"type\\":\\"application\\\\/pdf\\",\\"size\\":1141},\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\deliveries\\\\\\\\201805\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"{\\"id\\":2,\\"code\\":\\"STOCK-OUT\\\\/2018\\\\/05\\\\/000001\\",\\"bc_type\\":\\"BC 2.8\\",\\"bc_code\\":\\"0000-00321-992321-921\\",\\"bc_date\\":\\"2018-05-03T00:00:00+00:00\\",\\"date\\":\\"2018-05-03T00:00:00+00:00\\",\\"customer_id\\":1,\\"receiver\\":\\"Uniliver\\",\\"receiver_address\\":\\"EWQEWQ\\",\\"currency_id\\":1,\\"file\\":\\"reportreceives (1).pdf\\",\\"file_dir\\":\\"webroot\\\\\\\\assets\\\\\\\\deliveries\\\\\\\\201805\\\\\\\\file\\\\\\\\\\",\\"storage_id\\":1,\\"pdplb_id\\":1,\\"created_by\\":6}"', '"[]"'),
(176, '2018-05-03 23:26:26', 6, 'add', NULL, 'create', 13, 'countries', NULL, '"{\\"id\\":13,\\"name\\":\\"Mexico\\",\\"status\\":true,\\"created_by\\":6}"', '"{\\"id\\":13,\\"name\\":\\"Mexico\\",\\"status\\":true,\\"created_by\\":6}"', '"[]"'),
(177, '2018-05-03 23:26:51', 6, 'add', NULL, 'create', 14, 'countries', NULL, '"{\\"id\\":14,\\"name\\":\\"BRAZIL\\",\\"status\\":true,\\"created_by\\":6}"', '"{\\"id\\":14,\\"name\\":\\"BRAZIL\\",\\"status\\":true,\\"created_by\\":6}"', '"[]"');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `status`, `created_by`, `created`, `modified_by`, `modified`) VALUES
(1, 'Indonesia', 1, 1, '2018-02-12 15:21:06', NULL, '2018-02-12 15:21:41'),
(2, 'German', 1, 1, '2018-02-12 15:21:10', NULL, '2018-02-12 15:21:43'),
(3, 'Singapura', 1, 1, '2018-02-12 15:21:14', NULL, '2018-02-12 15:21:47'),
(6, 'Malaysia', 1, 1, '2018-02-12 15:21:16', NULL, '2018-02-12 15:21:49'),
(7, 'Zimbabwe', 1, 1, '2018-02-12 15:21:20', NULL, '2018-02-12 15:21:51'),
(8, 'Thailand', 1, 1, '2018-02-12 15:21:23', NULL, '2018-02-12 15:21:53'),
(9, 'Belanda', 1, 1, '2018-02-12 15:21:28', NULL, '2018-02-12 15:21:59'),
(10, 'China', 1, 1, '2018-02-12 15:21:33', NULL, '2018-02-12 15:22:01'),
(11, 'Beijing', 1, 1, '2018-02-12 15:21:35', 1, '2018-02-20 05:22:05'),
(12, 'Prancis', 1, 6, '2018-04-09 06:26:33', NULL, '2018-04-09 06:26:33'),
(13, 'Mexico', 1, 6, '2018-05-03 23:26:26', NULL, '2018-05-03 23:26:26'),
(14, 'BRAZIL', 1, 6, '2018-05-03 23:26:50', NULL, '2018-05-03 23:26:50');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(11) NOT NULL,
  `code` char(10) NOT NULL,
  `name` varchar(225) NOT NULL,
  `kurs_idr` decimal(22,2) DEFAULT NULL,
  `status` tinyint(41) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `code`, `name`, `kurs_idr`, `status`, `created_by`, `created`, `modified_by`, `modified`) VALUES
(1, 'IDR', 'Indonesia Rupiah', '0.00', 1, 1, '2018-02-12 00:00:00', NULL, NULL),
(2, 'SGD', 'Singapore Dollar', NULL, 1, 1, '2018-02-12 00:00:00', NULL, NULL),
(3, 'ZDR', 'Dollar Zimbabwe', NULL, 1, 1, '2018-02-12 00:00:00', NULL, NULL),
(4, 'USD', 'United States Dollar', '13000.00', 1, 1, '2018-02-12 00:00:00', 6, '2018-04-09 08:32:31');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `name` char(250) DEFAULT NULL,
  `address` text,
  `country_id` int(11) DEFAULT NULL,
  `phone` char(20) DEFAULT NULL,
  `fax` char(20) DEFAULT NULL,
  `email` char(100) DEFAULT NULL,
  `pic` char(50) DEFAULT NULL,
  `pic_phone` char(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `pdplb_id` int(11) NOT NULL,
  `storage_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `address`, `country_id`, `phone`, `fax`, `email`, `pic`, `pic_phone`, `status`, `pdplb_id`, `storage_id`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'Zamasco', 'Jakarta', 1, '08329123', '0232191', 'aiqbalsyah@gmail.com', 'iqbal', '0842391', 1, 1, 1, '2018-02-26 04:33:21', 6, '2018-02-26 04:37:29', 6),
(2, 'Sundanese Tech', 'Bogor', 2, '083219831', '038291', 'sundanese@gmail.com', 'Ardi', '39021390123021', 0, 1, 1, '2018-02-26 04:38:01', 6, '2018-02-26 04:38:01', NULL),
(3, 'PT BUDITEX', 'test', 1, '0832818', '039218392', 'wakaw@gmail.com', 'odso', 'idos', 1, 1, 1, '2018-03-07 04:25:12', 6, '2018-03-07 04:25:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `deliveries`
--

CREATE TABLE `deliveries` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `bc_type` varchar(225) NOT NULL,
  `bc_code` varchar(225) NOT NULL,
  `bc_date` date NOT NULL,
  `date` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `receiver` varchar(225) NOT NULL,
  `receiver_address` text,
  `currency_id` int(11) NOT NULL,
  `kurs_idr` decimal(65,5) DEFAULT '0.00000',
  `file` varchar(225) NOT NULL,
  `file_dir` varchar(225) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `storage_id` int(11) NOT NULL,
  `pdplb_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deliveries`
--

INSERT INTO `deliveries` (`id`, `code`, `bc_type`, `bc_code`, `bc_date`, `date`, `customer_id`, `receiver`, `receiver_address`, `currency_id`, `kurs_idr`, `file`, `file_dir`, `status`, `storage_id`, `pdplb_id`, `created_by`, `created`, `modified_by`, `modified`) VALUES
(2, 'STOCK-OUT/2018/05/000001', 'BC 2.8', '0000-00321-992321-921', '2018-05-03', '2018-05-03', 1, 'Uniliver', 'EWQEWQ', 1, '0.00000', 'reportreceives (1).pdf', 'webroot\\assets\\deliveries\\201805\\file\\', 0, 1, 1, 6, '2018-05-03 21:56:35', NULL, '2018-05-03 21:56:35');

-- --------------------------------------------------------

--
-- Table structure for table `deliveries_details`
--

CREATE TABLE `deliveries_details` (
  `id` int(11) NOT NULL,
  `delivery_id` int(11) NOT NULL,
  `picking_lists_detail_id` int(11) NOT NULL,
  `receives_detail_id` int(11) NOT NULL,
  `put_aways_detail_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(65,2) NOT NULL,
  `value` decimal(65,2) NOT NULL,
  `pdplb_id` int(11) NOT NULL,
  `storage_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created` date NOT NULL,
  `modified_by` int(11) DEFAULT '0',
  `modified` date DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deliveries_details`
--

INSERT INTO `deliveries_details` (`id`, `delivery_id`, `picking_lists_detail_id`, `receives_detail_id`, `put_aways_detail_id`, `product_id`, `qty`, `value`, `pdplb_id`, `storage_id`, `created_by`, `created`, `modified_by`, `modified`, `status`) VALUES
(3, 2, 6, 9, 9, 1, '108.00', '108.00', 1, 1, 6, '2018-05-03', 0, '2018-05-03', NULL),
(4, 2, 8, 8, 8, 1, '69.00', '69.00', 1, 1, 6, '2018-05-03', 0, '2018-05-03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `deliveries_picking_lists`
--

CREATE TABLE `deliveries_picking_lists` (
  `id` int(11) NOT NULL,
  `delivery_id` int(11) NOT NULL,
  `picking_list_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deliveries_picking_lists`
--

INSERT INTO `deliveries_picking_lists` (`id`, `delivery_id`, `picking_list_id`) VALUES
(2, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `storage_id` int(11) DEFAULT '0',
  `pdplb_id` int(11) DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `storage_id`, `pdplb_id`, `name`, `status`, `created_by`, `created`, `modified_by`, `modified`) VALUES
(1, 0, 0, 'administrator', 1, NULL, '2018-02-05 08:03:19', NULL, '2018-02-05 08:03:19'),
(32, 1, 1, 'administrator', 1, 1, '2018-02-20 07:18:35', NULL, '2018-02-20 07:18:35'),
(33, 1, 1, 'GUDANG', 1, 6, '2018-02-21 03:39:30', NULL, '2018-02-21 03:39:30');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `storage_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `storage_id`, `name`, `status`, `created_by`, `created`, `modified_by`, `modified`) VALUES
(1, 1, 'BLOK A - K001 - T003', 1, 6, '2018-02-26 05:16:33', 6, '2018-03-26 11:40:33'),
(2, 1, 'BLOK A - K001 - T002', 1, 6, '2018-02-26 05:16:42', 6, '2018-03-26 11:40:21'),
(3, 1, 'BLOK A - K001 - T001', 1, 6, '2018-03-07 04:26:30', 6, '2018-03-26 11:39:51');

-- --------------------------------------------------------

--
-- Table structure for table `pdplbs`
--

CREATE TABLE `pdplbs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` text,
  `phone_number` varchar(225) NOT NULL,
  `fax_number` varchar(225) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `logo_dir` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pdplbs`
--

INSERT INTO `pdplbs` (`id`, `name`, `address`, `phone_number`, `fax_number`, `status`, `logo`, `logo_dir`, `created_by`, `created`, `modified_by`, `modified`) VALUES
(1, 'PT BUDITEX', 'Jl AM Sangaji 31-B, Petojo Utara, nGambir Jakarta-10130, Indonesia', '21 3519962', '62.21.6320131', 1, 'logo (1).png', 'webroot\\assets\\pdplb\\201802\\logo\\', 1, '2018-02-12 15:17:23', 1, '2018-02-26 11:40:47'),
(2, 'PT ZAMASCO', NULL, '', '', 1, 'logo Zapps-02-1-01 (2).png', 'webroot\\assets\\pdplb\\201802\\logo\\', 1, '2018-02-12 15:17:43', 1, '2018-02-20 04:19:59');

-- --------------------------------------------------------

--
-- Table structure for table `picking_lists`
--

CREATE TABLE `picking_lists` (
  `id` int(11) NOT NULL,
  `code` varchar(225) DEFAULT NULL,
  `date` date NOT NULL,
  `storage_id` int(11) NOT NULL,
  `pdplb_id` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `picking_lists`
--

INSERT INTO `picking_lists` (`id`, `code`, `date`, `storage_id`, `pdplb_id`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(3, 'STOCK-PL/2018/05/000001', '2018-05-03', 1, 1, 1, '2018-05-03 21:55:13', 6, '2018-05-03 21:56:35', 6);

-- --------------------------------------------------------

--
-- Table structure for table `picking_lists_details`
--

CREATE TABLE `picking_lists_details` (
  `id` int(11) NOT NULL,
  `picking_list_id` int(11) NOT NULL,
  `receives_detail_id` int(11) NOT NULL,
  `put_aways_detail_id` int(11) DEFAULT '0',
  `pdplb_id` int(11) NOT NULL,
  `storage_id` int(11) NOT NULL,
  `qty` decimal(65,5) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `picking_lists_details`
--

INSERT INTO `picking_lists_details` (`id`, `picking_list_id`, `receives_detail_id`, `put_aways_detail_id`, `pdplb_id`, `storage_id`, `qty`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(6, 3, 9, 9, 1, 1, '108.00000', '2018-05-03 21:55:13', 6, '2018-05-03 21:55:13', NULL),
(7, 3, 16, 16, 1, 1, '372.00000', '2018-05-03 21:55:13', 6, '2018-05-03 21:55:13', NULL),
(8, 3, 8, 8, 1, 1, '69.00000', '2018-05-03 21:55:13', 6, '2018-05-03 21:55:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `pdplb_id` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `code`, `name`, `pdplb_id`, `status`, `created_by`, `created`, `modified_by`, `modified`) VALUES
(1, '64029962', 'KAPAS BRAZIL TIDAK DIGARUK/DISISIR', 1, 1, 6, '2018-04-10 03:36:34', 6, '2018-05-03 21:09:52'),
(2, '21037888', 'KAPAS MEXICAN TIDAK DIGARUK/DISISIR', 1, 1, 6, '2018-04-10 03:38:08', 6, '2018-05-03 21:11:06'),
(3, '62122204', 'CHOCOLATE DIPPING  YYD-A90541-UID-S75', 1, 1, 6, '2018-04-10 03:44:12', NULL, '2018-04-10 03:44:12');

-- --------------------------------------------------------

--
-- Table structure for table `put_aways`
--

CREATE TABLE `put_aways` (
  `id` int(11) NOT NULL,
  `code` varchar(225) DEFAULT NULL,
  `date` date NOT NULL,
  `location_id` int(11) NOT NULL,
  `storage_id` int(11) NOT NULL,
  `pdplb_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `put_aways`
--

INSERT INTO `put_aways` (`id`, `code`, `date`, `location_id`, `storage_id`, `pdplb_id`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(6, 'STOCK-PA/2018/05/000001', '2018-05-03', 3, 1, 1, '2018-05-03 21:54:42', 6, '2018-05-03 21:54:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `put_aways_details`
--

CREATE TABLE `put_aways_details` (
  `id` int(11) NOT NULL,
  `put_away_id` int(11) NOT NULL,
  `receives_detail_id` int(11) NOT NULL,
  `qty` decimal(65,2) NOT NULL,
  `pdplb_id` int(11) NOT NULL,
  `storage_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `put_aways_details`
--

INSERT INTO `put_aways_details` (`id`, `put_away_id`, `receives_detail_id`, `qty`, `pdplb_id`, `storage_id`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(8, 6, 7, '69.00', 1, 1, '2018-05-03 21:54:42', 6, '2018-05-03 21:54:42', NULL),
(9, 6, 8, '108.00', 1, 1, '2018-05-03 21:54:42', 6, '2018-05-03 21:54:42', NULL),
(10, 6, 9, '108.00', 1, 1, '2018-05-03 21:54:42', 6, '2018-05-03 21:54:42', NULL),
(11, 6, 10, '107.00', 1, 1, '2018-05-03 21:54:42', 6, '2018-05-03 21:54:42', NULL),
(12, 6, 11, '80.00', 1, 1, '2018-05-03 21:54:42', 6, '2018-05-03 21:54:42', NULL),
(13, 6, 12, '80.00', 1, 1, '2018-05-03 21:54:42', 6, '2018-05-03 21:54:42', NULL),
(14, 6, 13, '80.00', 1, 1, '2018-05-03 21:54:42', 6, '2018-05-03 21:54:42', NULL),
(15, 6, 14, '80.00', 1, 1, '2018-05-03 21:54:42', 6, '2018-05-03 21:54:42', NULL),
(16, 6, 15, '372.00', 1, 1, '2018-05-03 21:54:42', 6, '2018-05-03 21:54:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `receives`
--

CREATE TABLE `receives` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `bc_type` varchar(225) NOT NULL,
  `bc_code` varchar(225) NOT NULL,
  `bc_date` date NOT NULL,
  `date` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `register_code` varchar(225) DEFAULT NULL,
  `register_date` date DEFAULT NULL,
  `po_code` varchar(225) DEFAULT NULL,
  `po_date` date DEFAULT NULL,
  `bl_code` varchar(225) DEFAULT NULL,
  `bl_date` date DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `kurs_idr` decimal(22,2) DEFAULT NULL,
  `value_type` varchar(225) DEFAULT NULL,
  `_value` decimal(65,5) DEFAULT NULL,
  `file` varchar(225) NOT NULL,
  `file_dir` varchar(225) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `storage_id` int(11) NOT NULL,
  `pdplb_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by` int(11) DEFAULT '0',
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receives`
--

INSERT INTO `receives` (`id`, `code`, `bc_type`, `bc_code`, `bc_date`, `date`, `customer_id`, `supplier_id`, `sender_id`, `register_code`, `register_date`, `po_code`, `po_date`, `bl_code`, `bl_date`, `currency_id`, `kurs_idr`, `value_type`, `_value`, `file`, `file_dir`, `status`, `storage_id`, `pdplb_id`, `created_by`, `created`, `modified_by`, `modified`) VALUES
(5, 'STOCK-IN/2018/05/000001', 'BC 1.6', '0000-000542-20180321-009701', '2018-04-04', '2018-05-03', 3, 1, 1, '876789876', '2018-05-03', '90998', '2018-05-03', '98898', '2018-05-03', 1, NULL, 'CIF', '158290.69000', 'reportreceives (1).pdf', 'webroot\\assets\\receives\\201805\\file\\', 0, 1, 1, 6, '2018-05-03 21:32:33', 0, '2018-05-03 21:32:33'),
(6, 'STOCK-IN/2018/05/000002', 'BC 1.6', '00000-000542-20180324-009717', '2018-04-02', '2018-05-03', 3, 1, 1, '165573', '2018-04-02', '988988', '2018-04-01', '888776678', '2018-05-03', 4, '13000.00', 'NTR', '119988.56000', 'SPPB BC 1.6 AJU 000286.pdf', 'webroot\\assets\\receives\\201805\\file\\', 0, 1, 1, 6, '2018-05-03 21:47:52', 0, '2018-05-03 21:47:52'),
(7, 'STOCK-IN/2018/05/000003', 'BC 1.6', '00000-000542-20180321-009699', '2018-03-28', '2018-05-03', 1, 1, 1, '232819321', '2018-05-03', '3213291', '2018-05-04', '321', '2018-05-03', 4, '13000.00', 'NTR', '127439.48000', 'NOPEN BC 1.6 AJU 000286.pdf', 'webroot\\assets\\receives\\201805\\file\\', 0, 1, 1, 6, '2018-05-03 21:54:14', 0, '2018-05-03 21:54:14');

-- --------------------------------------------------------

--
-- Table structure for table `receives_containers`
--

CREATE TABLE `receives_containers` (
  `id` int(11) NOT NULL,
  `receive_id` int(11) NOT NULL,
  `code` varchar(225) DEFAULT NULL,
  `size` varchar(225) DEFAULT NULL,
  `type` varchar(225) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receives_containers`
--

INSERT INTO `receives_containers` (`id`, `receive_id`, `code`, `size`, `type`, `created`, `modified`) VALUES
(6, 5, 'TGHU-6043856', '40', 'FCL', '2018-05-03 21:32:34', '2018-05-03 21:32:34'),
(7, 5, 'TCNU-4871320', '40 FEET', 'FCL', '2018-05-03 21:32:34', '2018-05-03 21:32:34'),
(8, 5, 'TCNU4896143', '40 FEET', 'FCL', '2018-05-03 21:32:34', '2018-05-03 21:32:34'),
(9, 5, 'TGHU6043856', '40 FEET', 'FCL', '2018-05-03 21:32:34', '2018-05-03 21:32:34'),
(10, 6, 'UACU5195197', '40 FEET', 'FCL', '2018-05-03 21:47:52', '2018-05-03 21:47:52'),
(11, 6, 'UACU5589987', '40 FEET', 'FCL', '2018-05-03 21:47:52', '2018-05-03 21:47:52'),
(12, 6, 'UACU-5005105', '40 FEET', 'FCL', '2018-05-03 21:47:52', '2018-05-03 21:47:52'),
(13, 6, 'HLXU6281247', '40 FEET', 'FCL', '2018-05-03 21:47:52', '2018-05-03 21:47:52'),
(14, 7, 'FSCU8610117', '40 FEET', 'FCL', '2018-05-03 21:54:14', '2018-05-03 21:54:14');

-- --------------------------------------------------------

--
-- Table structure for table `receives_details`
--

CREATE TABLE `receives_details` (
  `id` int(11) NOT NULL,
  `receive_id` int(11) DEFAULT NULL,
  `receives_container_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(65,2) NOT NULL,
  `unit` varchar(225) NOT NULL,
  `qty_packaging` decimal(65,2) DEFAULT NULL,
  `packaging` varchar(225) DEFAULT NULL,
  `hs` varchar(225) DEFAULT NULL,
  `category` varchar(225) NOT NULL,
  `fasilitas` varchar(225) DEFAULT NULL,
  `no_urut` varchar(225) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `value` decimal(65,5) NOT NULL,
  `value_per_unit` decimal(65,5) DEFAULT NULL,
  `pdplb_id` int(11) NOT NULL,
  `storage_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receives_details`
--

INSERT INTO `receives_details` (`id`, `receive_id`, `receives_container_id`, `product_id`, `qty`, `unit`, `qty_packaging`, `packaging`, `hs`, `category`, `fasilitas`, `no_urut`, `country_id`, `value`, `value_per_unit`, `pdplb_id`, `storage_id`, `created_by`, `modified_by`, `status`, `created`, `modified`) VALUES
(7, 5, 6, 1, '69.00', 'BALE', '0.00', '-', '5201.00.00', 'Ditimbun', '-', '-', 1, '27862.39000', '403.80275', 1, 1, 6, 0, 0, '2018-05-03 21:32:34', '2018-05-03 21:32:34'),
(8, 5, 7, 1, '108.00', 'BALE', '0.00', '-', '5201.00.00', 'Ditimbun', '-', '-', 1, '43610.70000', '403.80278', 1, 1, 6, 0, 0, '2018-05-03 21:32:34', '2018-05-03 21:32:34'),
(9, 5, 8, 1, '108.00', 'BALE', '0.00', '-', '5201.00.00', 'Ditimbun', '-', '-', 1, '43610.70000', '403.80278', 1, 1, 6, 0, 0, '2018-05-03 21:32:34', '2018-05-03 21:32:34'),
(10, 5, 9, 1, '107.00', 'BALE', '0.00', '-', '5201.00.00', 'Ditimbun', '-', '-', 1, '43206.90000', '403.80280', 1, 1, 6, 0, 0, '2018-05-03 21:32:34', '2018-05-03 21:32:34'),
(11, 6, 10, 2, '80.00', 'BALE', '0.00', '-', '5201.00.00', 'Ditimbun', '-', '-', 2, '29997.14000', '374.96425', 1, 1, 6, 0, 0, '2018-05-03 21:47:52', '2018-05-03 21:47:52'),
(12, 6, 11, 2, '80.00', 'BALE', '0.00', '-', '5201.00.00', 'Ditimbun', '-', '-', 2, '29997.14000', '374.96425', 1, 1, 6, 0, 0, '2018-05-03 21:47:52', '2018-05-03 21:47:52'),
(13, 6, 12, 2, '80.00', 'BALE', '0.00', '-', '5201.00.00', 'Ditimbun', '-', '-', 2, '29997.14000', '374.96425', 1, 1, 6, 0, 0, '2018-05-03 21:47:52', '2018-05-03 21:47:52'),
(14, 6, 13, 2, '80.00', 'BALE', '0.00', '-', '5201.00.00', 'Ditimbun', '-', '-', 2, '29997.14000', '374.96425', 1, 1, 6, 0, 0, '2018-05-03 21:47:52', '2018-05-03 21:47:52'),
(15, 7, 14, 1, '372.00', 'BALE', '0.00', '-', '5201.00.00', 'Ditimbun', '--', '-', 1, '127439.48000', '342.57925', 1, 1, 6, 0, 0, '2018-05-03 21:54:14', '2018-05-03 21:54:14');

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` int(11) NOT NULL,
  `code` varchar(225) DEFAULT '0',
  `product_id` int(11) DEFAULT NULL,
  `qty` decimal(22,2) NOT NULL,
  `unit` varchar(225) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `value` decimal(22,2) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `type` tinyint(2) DEFAULT NULL COMMENT '0=IN,1=OUT,2=PRODUCTION,3=ADJUSTMENT',
  `reference_id` int(11) DEFAULT NULL,
  `reference_table` varchar(255) DEFAULT NULL,
  `expired` date DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `storage_id` int(11) NOT NULL,
  `pdplb_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_opnames`
--

CREATE TABLE `stock_opnames` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `date` date NOT NULL,
  `storage_id` int(11) DEFAULT NULL,
  `pdplb_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_opnames_details`
--

CREATE TABLE `stock_opnames_details` (
  `id` int(11) NOT NULL,
  `stock_opname_id` int(11) NOT NULL,
  `put_aways_detail_id` int(11) NOT NULL,
  `receives_detail_id` int(11) NOT NULL,
  `qty_system` decimal(65,2) DEFAULT '0.00',
  `status` tinyint(1) DEFAULT '0',
  `storage_id` int(11) DEFAULT NULL,
  `pdplb_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `storages`
--

CREATE TABLE `storages` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `address` text NOT NULL,
  `phone_number` varchar(225) DEFAULT NULL,
  `fax_number` varchar(225) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `storages`
--

INSERT INTO `storages` (`id`, `name`, `address`, `phone_number`, `fax_number`, `created_by`, `created`, `modified_by`, `modified`) VALUES
(1, 'GUDANG JONGGOL', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry scrambled', '5552432432', '432432432432', 1, '2018-02-20 04:26:49', 1, '2018-02-26 11:46:19');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL,
  `name` char(250) DEFAULT NULL,
  `address` text,
  `country_id` int(11) DEFAULT NULL,
  `phone` char(20) DEFAULT NULL,
  `fax` char(20) DEFAULT NULL,
  `email` char(100) DEFAULT NULL,
  `pic` char(50) DEFAULT NULL,
  `pic_phone` char(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `pdplb_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `address`, `country_id`, `phone`, `fax`, `email`, `pic`, `pic_phone`, `status`, `pdplb_id`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'PT Jaya Inter Supra', 'JAKARTA', 1, '0390129', '392819310', 'inter@gmail.com', 'bagus', '093821912', 1, 1, '2018-04-10 03:27:03', 6, '2018-04-10 03:27:03', 1),
(2, 'Pelayanan BBLK', 'JAKARTA', 1, '', '', '', '', '', 1, 1, '2018-04-10 03:33:17', 6, '2018-04-10 03:33:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` char(60) NOT NULL,
  `name` varchar(225) DEFAULT NULL,
  `email` varchar(225) DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `pdplb_id` int(11) DEFAULT '0',
  `storage_id` int(11) DEFAULT '0',
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `email`, `group_id`, `status`, `created_by`, `created`, `pdplb_id`, `storage_id`, `modified_by`, `modified`) VALUES
(1, 'administrator', '$2y$10$FFIjh73z6y6Yq09cwqWAMuzwZ3eKp8XJ3jwU1ZKlR4Rg0BtV5eogy', 'administrator', 'administrator@app.com', 1, 1, 1, '2018-02-05 08:44:27', 0, 0, 1, '2018-02-06 11:06:20'),
(6, 'administrator', '$2y$10$4b5eLPNy9ePadSR4ChhB..q6VmrlYuTnkIMwDTSdkgML6VodRmrOa', 'administrator', 'superadministrator@gmail.com', 32, 1, 1, '2018-02-20 08:10:45', 1, 1, 1, '2018-02-21 02:18:42'),
(7, 'gudangmanager', '$2y$10$E.NDXXIxpC8LsltEwEGd/u8krq.Kj2zBCbfz.WOHmM1t5yLuofxd.', 'gudangmanager', 'gudang@gmail.com', 32, 1, 6, '2018-02-21 03:18:08', 1, 1, NULL, '2018-02-21 03:18:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acl_phinxlog`
--
ALTER TABLE `acl_phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `acos`
--
ALTER TABLE `acos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lft` (`lft`,`rght`),
  ADD KEY `alias` (`alias`);

--
-- Indexes for table `adjustments`
--
ALTER TABLE `adjustments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `adjustments_details`
--
ALTER TABLE `adjustments_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_settings`
--
ALTER TABLE `app_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aros`
--
ALTER TABLE `aros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lft` (`lft`,`rght`),
  ADD KEY `alias` (`alias`);

--
-- Indexes for table `aros_acos`
--
ALTER TABLE `aros_acos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `aro_id` (`aro_id`,`aco_id`),
  ADD KEY `aco_id` (`aco_id`);

--
-- Indexes for table `audit_logs`
--
ALTER TABLE `audit_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `deliveries`
--
ALTER TABLE `deliveries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deliveries_details`
--
ALTER TABLE `deliveries_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deliveries_picking_lists`
--
ALTER TABLE `deliveries_picking_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pdplbs`
--
ALTER TABLE `pdplbs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `picking_lists`
--
ALTER TABLE `picking_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `picking_lists_details`
--
ALTER TABLE `picking_lists_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `put_aways`
--
ALTER TABLE `put_aways`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `put_aways_details`
--
ALTER TABLE `put_aways_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receives`
--
ALTER TABLE `receives`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receives_containers`
--
ALTER TABLE `receives_containers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receives_details`
--
ALTER TABLE `receives_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_opnames`
--
ALTER TABLE `stock_opnames`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_opnames_details`
--
ALTER TABLE `stock_opnames_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `storages`
--
ALTER TABLE `storages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acos`
--
ALTER TABLE `acos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;
--
-- AUTO_INCREMENT for table `adjustments`
--
ALTER TABLE `adjustments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `adjustments_details`
--
ALTER TABLE `adjustments_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `app_settings`
--
ALTER TABLE `app_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `aros`
--
ALTER TABLE `aros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `aros_acos`
--
ALTER TABLE `aros_acos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=285;
--
-- AUTO_INCREMENT for table `audit_logs`
--
ALTER TABLE `audit_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `deliveries`
--
ALTER TABLE `deliveries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `deliveries_details`
--
ALTER TABLE `deliveries_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `deliveries_picking_lists`
--
ALTER TABLE `deliveries_picking_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pdplbs`
--
ALTER TABLE `pdplbs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `picking_lists`
--
ALTER TABLE `picking_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `picking_lists_details`
--
ALTER TABLE `picking_lists_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `put_aways`
--
ALTER TABLE `put_aways`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `put_aways_details`
--
ALTER TABLE `put_aways_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `receives`
--
ALTER TABLE `receives`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `receives_containers`
--
ALTER TABLE `receives_containers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `receives_details`
--
ALTER TABLE `receives_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stock_opnames`
--
ALTER TABLE `stock_opnames`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stock_opnames_details`
--
ALTER TABLE `stock_opnames_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `storages`
--
ALTER TABLE `storages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
