/*
Navicat MySQL Data Transfer

Source Server         : Local
Source Server Version : 100125
Source Host           : localhost:3306
Source Database       : db_era

Target Server Type    : MYSQL
Target Server Version : 100125
File Encoding         : 65001

Date: 2017-09-07 10:58:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for outsourcing_invoice
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_invoice`;
CREATE TABLE `outsourcing_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `outsourcing_ms_perusahaan_id` int(11) DEFAULT NULL,
  `bulan` tinyint(4) DEFAULT NULL,
  `tahun` smallint(6) DEFAULT NULL,
  `gaji_pokok` decimal(20,2) DEFAULT NULL,
  `bpjs` decimal(20,2) DEFAULT NULL,
  `lembur_nasional` decimal(20,2) DEFAULT NULL,
  `jumlah` decimal(20,2) DEFAULT NULL,
  `lembur_bebas` decimal(20,2) DEFAULT NULL,
  `uang_makan` decimal(20,2) DEFAULT NULL,
  `fee` decimal(20,2) DEFAULT NULL,
  `jumlah2` decimal(20,2) DEFAULT NULL,
  `ppn` decimal(20,2) DEFAULT NULL,
  `total1` decimal(20,2) DEFAULT NULL,
  `pph23` decimal(20,2) DEFAULT NULL,
  `total2` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_invoice
-- ----------------------------
INSERT INTO `outsourcing_invoice` VALUES ('4', '2', '9', '2017', '9900000.00', '1232838.00', '25434.00', '11158272.00', '2312.00', '150000.00', '1338992.00', '12649575.00', '133899.00', '12783474.00', '26779.00', '12917374.00', '2017-09-05 17:14:53', '2', '2017-09-05 17:14:53', null);

-- ----------------------------
-- Table structure for outsourcing_ktk_backup_pegawai
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_ktk_backup_pegawai`;
CREATE TABLE `outsourcing_ktk_backup_pegawai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `outsourcing_ms_perusahaan_id` int(11) NOT NULL,
  `outsourcing_ms_pegawai_nik` int(11) NOT NULL,
  `nama_backup` varchar(255) DEFAULT NULL,
  `tgl_awal` date DEFAULT NULL,
  `tgl_akhir` date DEFAULT NULL,
  `fungsional` varchar(255) DEFAULT NULL,
  `jml_hari` tinyint(4) DEFAULT NULL,
  `alasan` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `outsourcing_ms_pegawai_nik` (`outsourcing_ms_pegawai_nik`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_ktk_backup_pegawai
-- ----------------------------
INSERT INTO `outsourcing_ktk_backup_pegawai` VALUES ('2', '2', '412414', null, '2017-07-25', null, '412', null, null, '2017-07-10 15:29:27', '2', '2017-07-10 15:29:27', null);
INSERT INTO `outsourcing_ktk_backup_pegawai` VALUES ('3', '1', '41241', 'hahaha', '2017-07-17', '2017-07-19', 'wadaw', null, 'wadaw', '2017-07-20 13:25:50', '2', '2017-07-20 13:28:23', '0000-00-00 00:00:00');
INSERT INTO `outsourcing_ktk_backup_pegawai` VALUES ('4', '2', '41241', 'ab', '2017-08-01', '2017-08-31', 'y', null, 'gas', '2017-08-14 16:15:58', '2', '2017-08-14 16:15:58', null);

-- ----------------------------
-- Table structure for outsourcing_ktk_form_kontrak
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_ktk_form_kontrak`;
CREATE TABLE `outsourcing_ktk_form_kontrak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `outsourcing_ms_perusahaan_id` int(11) NOT NULL,
  `no_kps` varchar(50) DEFAULT NULL,
  `jumlah_personil` varchar(10) DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_akhir` date DEFAULT NULL,
  `nilai_kontrak` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_ktk_form_kontrak
-- ----------------------------
INSERT INTO `outsourcing_ktk_form_kontrak` VALUES ('2', '2', '4124', '515', '2017-06-14', '2017-06-12', '241', '2017-06-20 10:51:32', '2', '2017-06-20 10:51:32', null);
INSERT INTO `outsourcing_ktk_form_kontrak` VALUES ('3', '2', '421124', '2', '2017-07-10', '2017-07-13', '4000', '2017-07-13 12:04:35', '2', '2017-07-13 12:04:35', null);
INSERT INTO `outsourcing_ktk_form_kontrak` VALUES ('4', '2', 'pks/asal', '2', '2017-08-31', '2017-09-07', '8000000', '2017-09-06 13:30:32', '2', '2017-09-06 13:30:32', null);

-- ----------------------------
-- Table structure for outsourcing_ktk_pkwt
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_ktk_pkwt`;
CREATE TABLE `outsourcing_ktk_pkwt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `outsourcing_ms_perusahaan_id` int(11) DEFAULT NULL,
  `outsourcing_ms_pegawai_nik` int(11) DEFAULT NULL,
  `jumlah_personil` varchar(50) DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_akhir` date DEFAULT NULL,
  `fungsional` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_ktk_pkwt
-- ----------------------------

-- ----------------------------
-- Table structure for outsourcing_ms_anggaran_bk
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_ms_anggaran_bk`;
CREATE TABLE `outsourcing_ms_anggaran_bk` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `anggaran_bk` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_ms_anggaran_bk
-- ----------------------------

-- ----------------------------
-- Table structure for outsourcing_ms_bpjs
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_ms_bpjs`;
CREATE TABLE `outsourcing_ms_bpjs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `outsourcing_ms_perusahaan_id` int(11) NOT NULL,
  `bpjs_tk_p` decimal(20,2) DEFAULT NULL,
  `bpjs_kes_p` decimal(20,2) DEFAULT NULL,
  `bpjs_tk_k` decimal(20,2) DEFAULT NULL,
  `bpjs_kes_k` decimal(20,2) DEFAULT NULL,
  `jaminan_pensiun_p` decimal(20,2) DEFAULT NULL,
  `jaminan_pensiun_k` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_ms_bpjs
-- ----------------------------
INSERT INTO `outsourcing_ms_bpjs` VALUES ('13', '1', '142283.80', '134230.00', '67115.00', '33557.50', '67115.00', '33557.50', '2017-07-12 23:04:34', null, '2017-09-04 13:24:35', null);
INSERT INTO `outsourcing_ms_bpjs` VALUES ('14', '2', '155018.64', '146244.00', '73122.00', '36561.00', '0.00', '0.00', '2017-07-12 23:04:34', null, '2017-09-04 13:24:35', null);
INSERT INTO `outsourcing_ms_bpjs` VALUES ('16', '4', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2017-07-12 23:04:34', null, '2017-09-04 13:24:35', null);
INSERT INTO `outsourcing_ms_bpjs` VALUES ('17', '6', '0.00', '0.00', '0.00', '0.00', '160000.00', '80000.00', '2017-08-15 09:54:11', null, '2017-09-04 13:24:35', null);

-- ----------------------------
-- Table structure for outsourcing_ms_bpjs_old
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_ms_bpjs_old`;
CREATE TABLE `outsourcing_ms_bpjs_old` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `bpjs` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_ms_bpjs_old
-- ----------------------------

-- ----------------------------
-- Table structure for outsourcing_ms_fee
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_ms_fee`;
CREATE TABLE `outsourcing_ms_fee` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `outsourcing_ms_perusahaan_id` int(11) DEFAULT NULL,
  `list_fee` varchar(100) DEFAULT NULL,
  `presentase_fee` tinyint(4) DEFAULT NULL,
  `pprp` decimal(20,2) DEFAULT '0.00',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_ms_fee
-- ----------------------------
INSERT INTO `outsourcing_ms_fee` VALUES ('9', '1', 'presentase_rupiah_per_pegawai', '0', '20000.00', '2017-08-14 13:47:37', '2', '2017-09-05 16:00:22', null);
INSERT INTO `outsourcing_ms_fee` VALUES ('11', '2', 'gapok,bpjs_kes,seragam,chemical', '12', '0.00', '2017-08-21 10:20:24', '2', '2017-09-05 16:13:32', null);

-- ----------------------------
-- Table structure for outsourcing_ms_fungsional
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_ms_fungsional`;
CREATE TABLE `outsourcing_ms_fungsional` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `outsourcing_ms_perusahaan_id` int(11) NOT NULL,
  `outsourcing_ms_list_fungsional_id` int(11) NOT NULL,
  `gapok` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_ms_fungsional
-- ----------------------------
INSERT INTO `outsourcing_ms_fungsional` VALUES ('2', '2', '1', '3000000.00', '2017-07-12 11:31:25', '2', '2017-07-12 11:31:25', null);
INSERT INTO `outsourcing_ms_fungsional` VALUES ('3', '2', '2', '3300000.00', '2017-07-13 10:40:19', '2', '2017-09-05 11:27:04', '2');
INSERT INTO `outsourcing_ms_fungsional` VALUES ('4', '2', '4', '3000000.00', '2017-09-06 10:31:26', '2', '2017-09-06 10:31:26', null);
INSERT INTO `outsourcing_ms_fungsional` VALUES ('5', '2', '11', '2000000.00', '2017-09-06 13:23:28', '2', '2017-09-06 13:23:28', null);

-- ----------------------------
-- Table structure for outsourcing_ms_gapok
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_ms_gapok`;
CREATE TABLE `outsourcing_ms_gapok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gapok` decimal(20,2) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_ms_gapok
-- ----------------------------
INSERT INTO `outsourcing_ms_gapok` VALUES ('1', '3000000.00', null, null, null, null);
INSERT INTO `outsourcing_ms_gapok` VALUES ('2', '4000000.00', null, null, null, null);
INSERT INTO `outsourcing_ms_gapok` VALUES ('3', '5000000.00', null, null, null, null);
INSERT INTO `outsourcing_ms_gapok` VALUES ('4', '6000000.00', null, null, null, null);

-- ----------------------------
-- Table structure for outsourcing_ms_jenis_cuti
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_ms_jenis_cuti`;
CREATE TABLE `outsourcing_ms_jenis_cuti` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jenis_cuti` varchar(20) NOT NULL,
  `jumlah_hari` int(2) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_ms_jenis_cuti
-- ----------------------------
INSERT INTO `outsourcing_ms_jenis_cuti` VALUES ('1', 'Cuti Tahunan', '0', null, '0', null, null);
INSERT INTO `outsourcing_ms_jenis_cuti` VALUES ('2', 'Cuti Sakit', '0', null, '0', null, null);
INSERT INTO `outsourcing_ms_jenis_cuti` VALUES ('3', 'Cuti Melahirkan', '0', null, '0', null, null);
INSERT INTO `outsourcing_ms_jenis_cuti` VALUES ('4', 'Cuti Alasan Penting', '0', null, '0', null, null);
INSERT INTO `outsourcing_ms_jenis_cuti` VALUES ('5', 'Cuti Diluar Tanggung', '0', null, '0', null, null);
INSERT INTO `outsourcing_ms_jenis_cuti` VALUES ('6', 'Cuti Besar', '0', null, '0', null, null);

-- ----------------------------
-- Table structure for outsourcing_ms_list_fungsional
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_ms_list_fungsional`;
CREATE TABLE `outsourcing_ms_list_fungsional` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fungsional` varchar(150) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_ms_list_fungsional
-- ----------------------------
INSERT INTO `outsourcing_ms_list_fungsional` VALUES ('1', 'Satpam', null, null, null, null);
INSERT INTO `outsourcing_ms_list_fungsional` VALUES ('2', 'Driver', null, null, null, null);
INSERT INTO `outsourcing_ms_list_fungsional` VALUES ('3', 'Cleaning Service', null, null, null, null);
INSERT INTO `outsourcing_ms_list_fungsional` VALUES ('4', 'OB/OG', null, null, null, null);
INSERT INTO `outsourcing_ms_list_fungsional` VALUES ('5', 'Helper', null, null, null, null);
INSERT INTO `outsourcing_ms_list_fungsional` VALUES ('6', 'Teknisi', null, null, null, null);
INSERT INTO `outsourcing_ms_list_fungsional` VALUES ('7', 'Paramedis', null, null, null, null);
INSERT INTO `outsourcing_ms_list_fungsional` VALUES ('8', 'Administrasi', null, null, null, null);
INSERT INTO `outsourcing_ms_list_fungsional` VALUES ('9', 'Gardener', null, null, null, null);
INSERT INTO `outsourcing_ms_list_fungsional` VALUES ('10', 'Marketing', null, null, null, null);
INSERT INTO `outsourcing_ms_list_fungsional` VALUES ('11', 'IT Support', null, null, null, null);
INSERT INTO `outsourcing_ms_list_fungsional` VALUES ('12', 'Operator Telepon', null, null, null, null);
INSERT INTO `outsourcing_ms_list_fungsional` VALUES ('13', 'Lain Lain', null, null, null, null);

-- ----------------------------
-- Table structure for outsourcing_ms_pegawai
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_ms_pegawai`;
CREATE TABLE `outsourcing_ms_pegawai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `outsourcing_ms_perusahaan_id` int(11) NOT NULL DEFAULT '1',
  `outsourcing_ms_gapok_id` int(11) NOT NULL DEFAULT '1',
  `nik` varchar(70) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `alamat` text,
  `no_telp` varchar(20) DEFAULT NULL,
  `pendidikan` varchar(50) DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `fungsional` varchar(255) DEFAULT NULL,
  `nama_user` varchar(255) DEFAULT NULL,
  `unit_kerja` varchar(255) DEFAULT NULL,
  `alamat_unit_kerja` text,
  `no_bpjs_kes` varchar(50) DEFAULT NULL,
  `no_bpjs_tk` varchar(50) DEFAULT NULL,
  `no_npwp` varchar(50) DEFAULT NULL,
  `no_ktp` varchar(50) DEFAULT NULL,
  `no_rekening` varchar(30) DEFAULT NULL,
  `ibu_kandung` varchar(100) DEFAULT NULL,
  `tmt_masuk` date DEFAULT NULL,
  `tmt_keluar` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_ms_pegawai
-- ----------------------------
INSERT INTO `outsourcing_ms_pegawai` VALUES ('2', '2', '2', 'EK1741241', 'Test Pegawai 1', '412', '0000-00-00', '124124', '124124', '1241241', 'Islam', '3', '112', '4124', null, null, null, null, null, null, 'wadaw', null, null, '2017-06-21 10:57:25', '2', '2017-09-05 11:38:55', null);
INSERT INTO `outsourcing_ms_pegawai` VALUES ('3', '2', '1', 'EK17412414', 'Test Pegawai 2', '14214', '2017-06-20', '6346', '124124', '124124', 'Islam', '3', '124124', '124', null, null, null, null, null, null, 'Ibunya', null, null, '2017-06-21 10:58:57', '2', '2017-09-05 11:38:52', null);
INSERT INTO `outsourcing_ms_pegawai` VALUES ('4', '2', '1', 'EK1705815712857', 'Test Pegawai 4', 'batam', '2017-09-12', 'Jl Batan', '0851592', 'SD', 'Islam', '3', null, 'Cabangs', null, null, null, null, null, null, 'Imah', null, null, '2017-09-05 11:38:21', '2', '2017-09-05 15:05:37', null);
INSERT INTO `outsourcing_ms_pegawai` VALUES ('5', '2', '1', '08412421', 'adaw', 'bogor', '2017-09-14', 'alamat', '412849', '78', 'Islam', '3', null, '42', null, '4124', '142', null, '214', '124', '4124', null, null, '2017-09-07 10:32:27', '2', '2017-09-07 10:32:27', null);

-- ----------------------------
-- Table structure for outsourcing_ms_perusahaan
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_ms_perusahaan`;
CREATE TABLE `outsourcing_ms_perusahaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_perusahaan` varchar(100) DEFAULT NULL,
  `alamat` text,
  `email` varchar(30) DEFAULT NULL,
  `npwp` varchar(30) DEFAULT NULL,
  `no_telp` varchar(20) DEFAULT NULL,
  `unit_kerja` varchar(50) DEFAULT '0',
  `hari_kerja` tinyint(4) DEFAULT '30',
  `bpjs` enum('0','1') NOT NULL DEFAULT '0',
  `jaminan_pensiun` enum('0','1') NOT NULL DEFAULT '0',
  `potongan` decimal(20,2) DEFAULT '0.00',
  `nwnp` decimal(20,2) DEFAULT '0.00',
  `uang_makan` decimal(20,2) DEFAULT '0.00',
  `transport` decimal(20,2) DEFAULT NULL,
  `intentif` decimal(20,2) DEFAULT '0.00',
  `gapok` decimal(20,2) DEFAULT '0.00',
  `upah_lembur` enum('thp','gp','tunjangan') NOT NULL DEFAULT 'thp',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_ms_perusahaan
-- ----------------------------
INSERT INTO `outsourcing_ms_perusahaan` VALUES ('1', 'Pt. Kapal Api Global', 'contoh alamat', 'Udang@kerang.dank', '9158', '896785329', 'Darat', '25', '1', '1', '0.00', '70000.00', '50000.00', '80000.00', '500000.00', '500000.00', 'thp', '2017-07-04 15:52:07', '2', '2017-08-15 09:55:52', '2');
INSERT INTO `outsourcing_ms_perusahaan` VALUES ('2', 'Pt. Inti Ganda Perdana', 'aaalala', 'aeahr82', '9732857', '8787', '878', '25', '1', '0', '10000.00', '40000.00', '50000.00', '10000.00', '40000.00', '40000.00', 'tunjangan', '2017-05-04 03:30:54', '2', '2017-09-06 10:35:48', '2');
INSERT INTO `outsourcing_ms_perusahaan` VALUES ('4', 'Pt .Udang', 'mana', 'udang@udang.dang', '02158', '00819251298', 'Darat', null, '0', '0', '201020.00', '0.00', '9520.00', '20000.00', '60000.00', '60000.00', 'thp', '2017-07-04 15:39:22', '2', '2017-08-16 14:10:58', '2');
INSERT INTO `outsourcing_ms_perusahaan` VALUES ('6', 'Pt. lalala', '48129', '4294', '9482984', '982429849', '849', '89', '0', '1', '849.00', '242.00', '24.00', '0.00', '5325.00', '5325.00', 'gp', '2017-08-15 09:45:36', '2', '2017-08-16 14:10:52', '2');
INSERT INTO `outsourcing_ms_perusahaan` VALUES ('7', 'Pt. Pegadaian Purwokerto', 'jl purwekorte', 'a', '64288726487', '41212', 'A', '21', '1', '1', '0.00', '0.00', '20000.00', '0.00', '200000.00', '2000000.00', '', '2017-09-06 11:45:49', '2', '2017-09-06 11:45:49', null);

-- ----------------------------
-- Table structure for outsourcing_ms_pph21
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_ms_pph21`;
CREATE TABLE `outsourcing_ms_pph21` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pph21` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_ms_pph21
-- ----------------------------

-- ----------------------------
-- Table structure for outsourcing_ms_tarif_lembur
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_ms_tarif_lembur`;
CREATE TABLE `outsourcing_ms_tarif_lembur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_tarif` enum('libur','pendek','perminggu') DEFAULT NULL,
  `from` tinyint(4) DEFAULT NULL,
  `to` tinyint(4) DEFAULT NULL,
  `rumus` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_ms_tarif_lembur
-- ----------------------------
INSERT INTO `outsourcing_ms_tarif_lembur` VALUES ('1', 'libur', '1', '7', '2', null, null, null, null);
INSERT INTO `outsourcing_ms_tarif_lembur` VALUES ('2', 'libur', '8', '0', '3', '2017-08-15 14:00:29', null, '2017-08-15 14:00:29', null);
INSERT INTO `outsourcing_ms_tarif_lembur` VALUES ('3', 'libur', '9', '10', '4', '2017-08-15 14:00:30', null, '2017-08-15 14:00:30', null);
INSERT INTO `outsourcing_ms_tarif_lembur` VALUES ('4', 'pendek', '1', '5', '2', '2017-08-16 12:09:13', null, '2017-08-16 12:09:13', null);
INSERT INTO `outsourcing_ms_tarif_lembur` VALUES ('5', 'pendek', '6', '0', '3', '2017-08-15 14:01:03', null, '2017-08-15 14:01:03', null);
INSERT INTO `outsourcing_ms_tarif_lembur` VALUES ('6', 'pendek', '7', '8', '4', '2017-08-15 14:01:24', null, '2017-08-15 14:01:24', null);
INSERT INTO `outsourcing_ms_tarif_lembur` VALUES ('7', 'perminggu', '1', '8', '2', '2017-08-16 12:09:16', null, '2017-08-16 12:09:16', null);
INSERT INTO `outsourcing_ms_tarif_lembur` VALUES ('8', 'perminggu', '9', '0', '3', null, null, null, null);
INSERT INTO `outsourcing_ms_tarif_lembur` VALUES ('9', 'perminggu', '10', '11', '4', null, null, null, null);

-- ----------------------------
-- Table structure for outsourcing_ms_umk
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_ms_umk`;
CREATE TABLE `outsourcing_ms_umk` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `outsourcing_ms_perusahaan_id` int(11) NOT NULL,
  `umk` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_ms_umk
-- ----------------------------
INSERT INTO `outsourcing_ms_umk` VALUES ('5', '4', '3000000', '2017-07-11 13:10:00', '2', '2017-07-11 13:10:00', null);
INSERT INTO `outsourcing_ms_umk` VALUES ('7', '1', '3355750', '2017-07-13 10:51:54', '2', '2017-07-13 10:51:54', null);
INSERT INTO `outsourcing_ms_umk` VALUES ('8', '2', '3656100', '2017-07-13 11:05:00', '2', '2017-07-13 11:05:00', null);
INSERT INTO `outsourcing_ms_umk` VALUES ('10', '6', '8000000', '2017-08-15 09:55:09', '2', '2017-08-15 09:55:09', null);
INSERT INTO `outsourcing_ms_umk` VALUES ('11', '7', '2000000', '2017-09-06 11:46:27', '2', '2017-09-06 11:46:27', null);

-- ----------------------------
-- Table structure for outsourcing_ovt_lembur
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_ovt_lembur`;
CREATE TABLE `outsourcing_ovt_lembur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `outsourcing_ms_perusahaan_id` int(11) DEFAULT NULL,
  `outsourcing_ms_pegawai_nik` varchar(11) DEFAULT NULL,
  `outsourcing_ms_tarif_lembur_id` int(11) DEFAULT NULL,
  `nama_backup` varchar(100) DEFAULT NULL,
  `jenis_lembur` enum('rutin','biasa','nasional','backup') DEFAULT NULL,
  `jml_jam` tinyint(4) DEFAULT NULL,
  `nilai_lembur` decimal(20,2) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_ovt_lembur
-- ----------------------------
INSERT INTO `outsourcing_ovt_lembur` VALUES ('8', '2', 'EK1741241', null, null, 'biasa', '2', '2312.00', 'print report', '2017-09-13 15:24:15', '2', '2017-09-05 15:33:09', null);
INSERT INTO `outsourcing_ovt_lembur` VALUES ('9', '2', 'EK17412414', null, null, 'nasional', '11', '25434.00', 'adaw', '2017-09-06 15:23:45', '2', '2017-09-05 15:23:57', null);
INSERT INTO `outsourcing_ovt_lembur` VALUES ('11', '2', null, null, 'pegawai yang membackup', 'backup', '4', '600000.00', 'backup lemburnya', '2017-08-21 14:05:15', '2', '2017-08-21 14:05:15', null);
INSERT INTO `outsourcing_ovt_lembur` VALUES ('12', '2', 'EK17412414', null, null, 'biasa', '8', '18497.00', 'lembur', '2017-09-06 12:07:44', '2', '2017-09-06 12:07:44', null);

-- ----------------------------
-- Table structure for outsourcing_prl_potongan
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_prl_potongan`;
CREATE TABLE `outsourcing_prl_potongan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `outsourcing_ms_perusahaan_id` int(11) NOT NULL,
  `outsourcing_ms_pegawai_nik` int(11) NOT NULL,
  `bulan` varchar(20) DEFAULT NULL,
  `tahun` smallint(4) DEFAULT NULL,
  `potongan_koperasi` decimal(20,2) DEFAULT NULL,
  `potongan_kmg` decimal(20,2) DEFAULT NULL,
  `potongan_lain` decimal(20,2) DEFAULT NULL,
  `jumlah_alpa` tinyint(4) DEFAULT NULL,
  `potongan_alpa` decimal(20,2) DEFAULT NULL,
  `jumlah_izin` tinyint(4) DEFAULT NULL,
  `potongan_izin` decimal(20,2) DEFAULT NULL,
  `jumlah_sakit` tinyint(4) DEFAULT NULL,
  `potongan_sakit` decimal(20,2) DEFAULT NULL,
  `jumlah_terlambat` tinyint(4) DEFAULT NULL,
  `potongan_terlambat` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_prl_potongan
-- ----------------------------
INSERT INTO `outsourcing_prl_potongan` VALUES ('1', '3', '412414', 'July', '1', '520000.00', '2200000.00', null, null, null, null, null, null, null, null, null, '2017-07-04 11:35:36', '2017-07-06 10:02:53', null, null);
INSERT INTO `outsourcing_prl_potongan` VALUES ('2', '2', '41241', 'July', '2017', '4124214.00', null, '52000000.00', '1', '200000.00', '2', '29054.00', '3', '643581.00', '1', '1453.00', '2017-07-04 11:38:09', '2017-07-06 10:03:12', null, null);
INSERT INTO `outsourcing_prl_potongan` VALUES ('3', '0', '412414', 'July', '2017', null, null, null, null, null, null, null, null, null, null, null, '2017-07-13 08:55:18', '2017-07-13 08:55:18', null, null);
INSERT INTO `outsourcing_prl_potongan` VALUES ('4', '1', '41241', 'Agustus', '17', '4124214.00', null, '1222222.00', null, null, null, null, null, null, null, null, '2017-08-14 14:06:30', '2017-08-14 14:11:31', null, null);
INSERT INTO `outsourcing_prl_potongan` VALUES ('5', '2', '412414', 'Agustus', '17', '4124214.00', '2200000.00', null, null, null, null, null, null, null, null, null, '2017-08-14 14:06:57', '2017-08-14 14:10:43', null, null);
INSERT INTO `outsourcing_prl_potongan` VALUES ('6', '2', '0', 'September', '17', '300000.00', null, null, null, null, null, null, null, null, null, null, '2017-09-06 11:56:35', '2017-09-06 11:56:35', null, null);

-- ----------------------------
-- Table structure for outsourcing_prl_rapel_gaji
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_prl_rapel_gaji`;
CREATE TABLE `outsourcing_prl_rapel_gaji` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `outsourcing_ms_pegawai_nik` varchar(50) NOT NULL,
  `nilai_gaji` decimal(20,2) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_prl_rapel_gaji
-- ----------------------------
INSERT INTO `outsourcing_prl_rapel_gaji` VALUES ('2', '41241', '200000.00', 'wadaw www', '2017-07-06 14:27:23', '2', '2017-07-06 14:27:23', null);
INSERT INTO `outsourcing_prl_rapel_gaji` VALUES ('3', '412414', '8000000.00', 'wadaw t', '2017-07-06 14:28:55', '2', '2017-07-06 14:28:55', null);
INSERT INTO `outsourcing_prl_rapel_gaji` VALUES ('4', 'EK17412414', '9000000.00', 'gaji bulan lalu', '2017-09-06 12:03:24', '2', '2017-09-06 12:03:24', null);

-- ----------------------------
-- Table structure for outsourcing_prl_rapel_lain
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_prl_rapel_lain`;
CREATE TABLE `outsourcing_prl_rapel_lain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `outsourcing_ms_pegawai_nik` varchar(50) NOT NULL,
  `nilai_gaji` decimal(20,2) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_prl_rapel_lain
-- ----------------------------
INSERT INTO `outsourcing_prl_rapel_lain` VALUES ('1', '412414', '800000.00', 'y', '2017-07-07 14:36:31', '2', '2017-07-13 08:56:35', '2');
INSERT INTO `outsourcing_prl_rapel_lain` VALUES ('2', '412414', '132000.00', 'ta', '2017-08-14 14:21:41', '2', '2017-08-14 14:21:41', null);

-- ----------------------------
-- Table structure for outsourcing_prl_rapel_lembur
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_prl_rapel_lembur`;
CREATE TABLE `outsourcing_prl_rapel_lembur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `outsourcing_ms_pegawai_nik` varchar(50) NOT NULL,
  `nilai_gaji` decimal(20,2) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_prl_rapel_lembur
-- ----------------------------

-- ----------------------------
-- Table structure for outsourcing_prl_uang_makan
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_prl_uang_makan`;
CREATE TABLE `outsourcing_prl_uang_makan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `outsourcing_ms_pegawai_nik` varchar(50) NOT NULL,
  `potongan` decimal(20,2) DEFAULT NULL,
  `insentif` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_prl_uang_makan
-- ----------------------------

-- ----------------------------
-- Table structure for outsourcing_trans_cuti
-- ----------------------------
DROP TABLE IF EXISTS `outsourcing_trans_cuti`;
CREATE TABLE `outsourcing_trans_cuti` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `outsourcing_ms_perusahaan_id` int(11) DEFAULT NULL,
  `outsourcing_ms_pegawai_nik` varchar(30) NOT NULL,
  `outsourcing_ms_jenis_cuti_id` int(10) unsigned NOT NULL,
  `tgl_awal` date NOT NULL,
  `tgl_akhir` date NOT NULL,
  `jumlah_hari` int(2) unsigned NOT NULL,
  `keterangan` text NOT NULL,
  `no_telp` varchar(20) DEFAULT NULL,
  `alamat_cuti` text,
  `pimpinan` varchar(50) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sdm_trans_cuti_sdm_ms_jenis_cuti_foreign` (`outsourcing_ms_jenis_cuti_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outsourcing_trans_cuti
-- ----------------------------
SET FOREIGN_KEY_CHECKS=1;
