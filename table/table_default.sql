/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 100113
Source Host           : localhost:3306
Source Database       : era_v2

Target Server Type    : MYSQL
Target Server Version : 100113
File Encoding         : 65001

Date: 2017-09-07 10:21:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------

-- ----------------------------
-- Table structure for module_permissions
-- ----------------------------
DROP TABLE IF EXISTS `module_permissions`;
CREATE TABLE `module_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `allow` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of module_permissions
-- ----------------------------
INSERT INTO `module_permissions` VALUES ('1', '1', '1', '1', '2017-02-24 07:06:35', '2017-02-24 07:06:35');
INSERT INTO `module_permissions` VALUES ('2', '1', '2', '1', '2017-02-24 07:06:35', '2017-02-24 07:06:35');
INSERT INTO `module_permissions` VALUES ('3', '1', '3', '1', '2017-02-24 07:06:35', '2017-02-24 07:06:35');
INSERT INTO `module_permissions` VALUES ('4', '1', '4', '1', '2017-02-24 07:06:35', '2017-02-24 07:22:38');
INSERT INTO `module_permissions` VALUES ('5', '1', '5', '1', '2017-02-24 07:06:35', '2017-02-24 07:06:35');
INSERT INTO `module_permissions` VALUES ('6', '1', '6', '1', '2017-02-24 07:06:35', '2017-02-24 07:06:35');
INSERT INTO `module_permissions` VALUES ('7', '1', '7', '1', '2017-02-24 07:06:35', '2017-02-24 07:06:35');
INSERT INTO `module_permissions` VALUES ('8', '1', '8', '1', '2017-02-24 07:06:35', '2017-02-24 07:06:35');
INSERT INTO `module_permissions` VALUES ('9', '1', '9', '1', null, null);
INSERT INTO `module_permissions` VALUES ('10', '2', '1', '1', '2017-06-14 02:40:36', '2017-06-14 02:40:36');
INSERT INTO `module_permissions` VALUES ('11', '2', '2', '0', '2017-06-14 02:40:36', '2017-06-14 02:40:36');
INSERT INTO `module_permissions` VALUES ('12', '2', '3', '0', '2017-06-14 02:40:36', '2017-06-14 02:40:36');
INSERT INTO `module_permissions` VALUES ('13', '2', '4', '0', '2017-06-14 02:40:36', '2017-06-14 02:40:36');
INSERT INTO `module_permissions` VALUES ('14', '2', '5', '0', '2017-06-14 02:40:36', '2017-06-14 02:40:36');
INSERT INTO `module_permissions` VALUES ('15', '2', '6', '0', '2017-06-14 02:40:36', '2017-06-14 02:40:36');
INSERT INTO `module_permissions` VALUES ('16', '2', '7', '0', '2017-06-14 02:40:36', '2017-06-14 02:40:36');
INSERT INTO `module_permissions` VALUES ('17', '2', '8', '0', '2017-06-14 02:40:36', '2017-06-14 02:40:36');
INSERT INTO `module_permissions` VALUES ('18', '2', '9', '0', '2017-06-14 02:40:36', '2017-06-14 02:40:36');

-- ----------------------------
-- Table structure for modules
-- ----------------------------
DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) NOT NULL,
  `display_name` varchar(225) NOT NULL,
  `description` text NOT NULL,
  `img` varchar(225) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of modules
-- ----------------------------
INSERT INTO `modules` VALUES ('1', 'Inventories', 'Umum', 'Aplikasi Umum', 'inventory.png', '2017-02-23 11:47:52', '0000-00-00 00:00:00');
INSERT INTO `modules` VALUES ('2', 'HumanResources', 'SDM', 'Aplikasi SDM', 'hrd.png', '2017-02-23 11:47:52', '0000-00-00 00:00:00');
INSERT INTO `modules` VALUES ('3', 'OtherBusinesses', 'Usaha Lain', 'Aplikasi Usaha Lain', 'suitcase.png', '2017-02-23 11:51:38', '0000-00-00 00:00:00');
INSERT INTO `modules` VALUES ('4', 'Expeditions', 'Ekspedisi', 'Aplikasi Ekspedisi', 'delivery-truck.png', '2017-02-23 11:51:38', '0000-00-00 00:00:00');
INSERT INTO `modules` VALUES ('5', 'Rents', 'Rental', 'Aplikasi Rental', 'rent-a-car.png', '2017-02-23 15:26:44', '0000-00-00 00:00:00');
INSERT INTO `modules` VALUES ('6', 'OutSourcings', 'OutSourcing', 'Aplikasi Outsourcing', 'reunion.png', '2017-02-23 15:26:44', '0000-00-00 00:00:00');
INSERT INTO `modules` VALUES ('7', 'Credits', 'Credit', 'Aplikasi Credit', 'credit-card.png', '2017-02-23 15:28:12', '0000-00-00 00:00:00');
INSERT INTO `modules` VALUES ('8', 'Finance', 'Keuangan', 'Aplikasi Keuangan', 'money-bag.png', '2017-02-23 15:28:12', '0000-00-00 00:00:00');
INSERT INTO `modules` VALUES ('9', 'Settings', 'Pengaturan', 'Setting Aplikasi', 'gears.png', '2017-03-07 11:35:19', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for ms_akun_budget
-- ----------------------------
DROP TABLE IF EXISTS `ms_akun_budget`;
CREATE TABLE `ms_akun_budget` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sdm_ms_bagian_id` int(10) unsigned NOT NULL,
  `kode_akun` varchar(9) NOT NULL,
  `deskripsi` varchar(60) NOT NULL,
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `position` int(1) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ms_akun_budget_sdm_ms_bagian_id_foreign` (`sdm_ms_bagian_id`) USING BTREE,
  CONSTRAINT `ms_akun_budget_ibfk_1` FOREIGN KEY (`sdm_ms_bagian_id`) REFERENCES `sdm_ms_bagian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=254 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ms_akun_budget
-- ----------------------------
INSERT INTO `ms_akun_budget` VALUES ('1', '1', '511.00.00', 'BIAYA PEGAWAI', '0', '0', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('2', '1', '512.00.00', 'GAJI DIREKSI DAN KOMISARIS', '0', '0', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('3', '1', '513.00.00', 'BIAYA ADMINISTRASI DAN PEMASARAN', '0', '0', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('4', '1', '514.00.00', 'BIAYA UMUM', '0', '0', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('5', '1', '515.00.00', 'BIAYA BUNGA', '0', '0', '2017-05-12 10:01:50', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('6', '1', '511.01.00', 'GAJI POKOK', '1', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('7', '1', '511.02.00', 'TUNJANGAN GRADE', '1', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('8', '1', '511.03.00', 'TUNJANGAN PENGELOLAAN', '1', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('9', '1', '511.04.00', 'INSENTIP KEHADIRAN', '1', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('10', '1', '511.05.00', 'TUNJANGAN PAKAIAN', '1', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('11', '1', '511.06.00', 'TUNJANGAN THR', '1', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('12', '1', '511.07.00', 'TUNJANGAN BONUS', '1', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('13', '1', '511.08.00', 'TUNJANGAN PPH 21', '1', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('14', '1', '511.09.00', 'TUNJANGAN LAINNYA', '1', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('15', '1', '511.10.00', 'BIAYA KESEHATAN', '1', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('16', '1', '511.11.00', 'BIAYA PENSIUN', '1', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('17', '1', '511.12.00', 'BIAYA PEGAWAI LAINNYA', '1', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('18', '1', '512.01.00', 'GAJI DIREKSI', '2', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('19', '1', '512.02.00', 'GAJI KOMISARIS', '2', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('20', '1', '513.01.00', 'BIAYA ATK', '3', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('21', '1', '513.03.00', 'BIAYA PERCETAKAN', '3', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('22', '1', '513.05.00', 'BIAYA PERLENGKAPAN', '3', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('23', '1', '513.06.00', 'BIAYA KONSULTAN', '3', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('24', '1', '513.07.00', 'BIAYA PEMASARAN', '3', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('25', '1', '513.08.00', 'BIAYA IZIN USAHA', '3', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('26', '1', '514.01.00', 'BIAYA USAHA FOTO COPY', '4', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('27', '1', '514.02.00', 'BIAYA EKSPEDISI', '4', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('28', '1', '514.03.00', 'BIAYA OUTSOURCING', '4', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('29', '1', '514.04.00', 'BIAYA PEMELIHARAAN GEDUNG', '4', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('30', '1', '514.05.00', 'BIAYA PEMELIHARAAN INVENTARIS', '4', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('31', '1', '514.06.00', 'BIAYA PEMELIHARAAN KENDARAAN DINAS', '4', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('32', '1', '514.07.01', 'BIAYA BBM & PELUMAS EKS', '4', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('33', '1', '514.08.00', 'BIAYA KENDARAAN RENTAL', '4', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('34', '1', '514.09.00', 'BIAYA AIR', '4', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('35', '1', '514.10.00', 'BIAYA LISTRIK', '4', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('36', '1', '514.11.00', 'BIAYA TELEPON', '4', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('37', '1', '514.12.00', 'BIAYA PERJALANAN DINAS', '4', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('38', '1', '514.13.00', 'BIAYA SEWA', '4', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('39', '1', '514.14.00', 'BIAYA ASURANSI', '4', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('40', '1', '514.15.00', 'BIAYA RAPAT', '4', '1', '2017-05-12 10:01:24', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('41', '1', '514.16.00', 'BIAYA USAHA LAIN', '4', '1', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('42', '1', '514.19.00', 'BIAYA UMUM LAINNYA', '4', '1', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('43', '1', '515.01.00', 'BIAYA BUNGA BANK', '5', '1', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('44', '1', '515.02.00', 'BIAYA BUNGA LEASING', '5', '1', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('45', '1', '515.03.00', 'BIAYA JASA PENGEMBANGAN JPK', '5', '1', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('46', '5', '511.01.01', 'GAJI POKOK PEGAWAI OUTSOURCING INTERNAL', '6', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('47', '3', '511.01.02', 'GAJI POKOK PEGAWAI OUTSOURCING SATPAM', '6', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('48', '3', '511.01.03', 'GAJI POKOK PEGAWAI OUTSOURCING PENGEMUDI', '6', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('49', '3', '511.01.04', 'GAJI POKOK PEGAWAI OUTSOURCING CLEANING SERVICE', '6', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('50', '3', '511.01.05', 'GAJI POKOK PEGAWAI OUTSOURCING LAINNYA', '6', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('51', '5', '511.01.06', 'GAJI POKOK PEGAWAI EXPEDISI INTERNAL', '6', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('52', '5', '511.01.07', 'GAJI POKOK PEGAWAI RENTAL INTERNAL', '6', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('53', '5', '511.01.08', 'GAJI POKOK PEGAWAI KREDIT KARYAWAN INTERNAL', '6', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('54', '5', '511.01.09', 'GAJI POKOK PEGAWAI USAHA LAIN INTERNAL', '6', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('55', '5', '511.01.10', 'GAJI POKOK PEGAWAI SUPPORTING (UMUM, SDM, KEU)', '6', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('56', '5', '511.01.11', 'GAJI POKOK PERWAKILAN', '6', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('57', '5', '511.02.01', 'TUNJ GRADE PEGAWAI OUTSOURCING INTERNAL', '7', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('58', '3', '511.02.02', 'TUNJ GRADE PEGAWAI OUTSOURCING SATPAM', '7', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('59', '3', '511.02.03', 'TUNJ GRADE PEGAWAI OUTSOURCING PENGEMUDI', '7', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('60', '3', '511.02.04', 'TUNJ GRADE PEGAWAI OUTSOURCING CLEANING SERVICE', '7', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('61', '3', '511.02.05', 'TUNJ GRADE PEGAWAI OUTSOURCING LAINNYA', '7', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('62', '5', '511.02.06', 'TUNJ GRADE PEGAWAI EXPEDISI INTERNAL', '7', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('63', '5', '511.02.07', 'TUNJ GRADE PEGAWAI RENTAL INTERNAL', '7', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('64', '5', '511.02.08', 'TUNJ GRADE PEGAWAI KREDIT KARYAWAN INTERNAL', '7', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('65', '5', '511.02.09', 'TUNJ GRADE PEGAWAI USAHA LAIN INTERNAL', '7', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('66', '5', '511.02.10', 'TUNJ GRADE PEGAWAI SUPPORTING (UMUM, SDM, KEU)', '7', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('67', '5', '511.02.11', 'TUNJ GRADE PERWAKILAN', '7', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('68', '5', '511.03.01', 'TUNJ PENGELOLAAN PEGAWAI OUTSOURCING INTERNAL', '8', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('69', '3', '511.03.02', 'TUNJ PENGELOLAAN PEGAWAI OUTSOURCING SATPAM', '8', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('70', '3', '511.03.03', 'TUNJ PENGELOLAAN PEGAWAI OUTSOURCING PENGEMUDI', '8', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('71', '3', '511.03.04', 'TUNJ PENGELOLAAN PEGAWAI OUTSOURCING CLEANING SERVICE', '8', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('72', '3', '511.03.05', 'TUNJ PENGELOLAAN PEGAWAI OUTSOURCING LAINNYA', '8', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('73', '5', '511.03.06', 'TUNJANGAN PENGELOLAAN PEGAWAI EXPEDISI INTERNAL', '8', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('74', '5', '511.03.07', 'TUNJ PENGELOLAAN PEGAWAI RENTAL INTERNAL', '8', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('75', '5', '511.03.08', 'TUNJ PENGELOLAAN PEGAWAI KREDIT KARYAWAN INTERNAL', '8', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('76', '5', '511.03.09', 'TUNJ PENGELOLAAN PEGAWAI USAHA LAIN INTERNAL', '8', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('77', '5', '511.03.10', 'TUNJ PENGELOLAAN PEGAWAI SUPPORTING (UMUM, SDM, KEU)', '8', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('78', '5', '511.03.11', 'TUNJ PENGELOLAAN PERWAKILAN', '8', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('79', '5', '511.04.01', 'INSENTIP KEHADIRAN PEGAWAI OUTSOURCING INTERNAL', '9', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('80', '3', '511.04.02', 'INSENTIP KEHADIRAN PEGAWAI OUTSOURCING SATPAM', '9', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('81', '3', '511.04.03', 'INSENTIP KEHADIRAN PEGAWAI OUTSOURCING PENGEMUDI', '9', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('82', '3', '511.04.04', 'INSENTIP KEHADIRAN PEGAWAI OUTSOURCING CLEANING SERVICE', '9', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('83', '3', '511.04.05', 'INSENTIP KEHADIRAN PEGAWAI OUTSOURCING LAINNYA', '9', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('84', '5', '511.04.06', 'INSENTIP KEHADIRAN PEGAWAI EXPEDISI INTERNAL', '9', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('85', '5', '511.04.07', 'INSENTIP KEHADIRAN PEGAWAI RENTAL INTERNAL', '9', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('86', '5', '511.04.08', 'INSENTIP KEHADIRAN PEGAWAI KREDIT KARYAWAN INTERNAL', '9', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('87', '5', '511.04.09', 'INSENTIP KEHADIRAN PEGAWAI USAHA LAIN INTERNAL', '9', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('88', '5', '511.04.10', 'INSENTIP KEHADIRAN PEGAWAI SUPPORTING (UM, SDM, KEU)', '9', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('89', '5', '511.04.11', 'INSENTIP KEHADIRAN PEGAWAI PERWAKILAN', '9', '2', '2017-05-12 10:01:25', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('90', '5', '511.05.01', 'TUNJ PAKAIAN PEGAWAI OUTSOURCING INTERNAL', '10', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('91', '3', '511.05.02', 'TUNJ PAKAIAN PEGAWAI OUTSOURCING SATPAM', '10', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('92', '3', '511.05.03', 'TUNJ PAKAIAN PEGAWAI OUTSOURCING PENGEMUDI', '10', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('93', '3', '511.05.04', 'TUNJ PAKAIAN PEGAWAI OUTSOURCING CLEANING SERVICE', '10', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('94', '3', '511.05.05', 'TUNJ PAKAIAN PEGAWAI OUTSOURCING LAINNYA', '10', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('95', '5', '511.05.06', 'TUNJ PAKAIAN PEGAWAI EXPEDISI INTERNAL', '10', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('96', '5', '511.05.07', 'TUNJ PAKAIAN PEGAWAI RENTAL INTERNAL', '10', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('97', '5', '511.05.08', 'TUNJ PAKAIAN PEGAWAI KREDIT KARYAWAN INTERNAL', '10', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('98', '5', '511.05.09', 'TUNJ PAKAIAN PEGAWAI USAHA LAIN INTERNAL', '10', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('99', '5', '511.05.10', 'TUNJ PAKAIAN PEGAWAI SUPPORTING (UMUM, SDM, KEU)', '10', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('100', '5', '511.05.11', 'TUNJ PAKAIAN PERWAKILAN', '10', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('101', '5', '511.06.01', 'TUNJ HARI RAYA PEGAWAI OUTSOURCING INTERNAL', '11', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('102', '3', '511.06.02', 'TUNJ HARI RAYA PEGAWAI OUTSOURCING SATPAM', '11', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('103', '3', '511.06.03', 'TUNJ HARI RAYA PEGAWAI OUTSOURCING PENGEMUDI', '11', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('104', '3', '511.06.04', 'TUNJ HARI RAYA PEGAWAI OUTSOURCING CLEANING SERVICE', '11', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('105', '3', '511.06.05', 'TUNJ HARI RAYA PEGAWAI OUTSOURCING LAINNYA', '11', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('106', '5', '511.06.06', 'TUNJ HARI RAYA PEGAWAI EXPEDISI INTERNAL', '11', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('107', '5', '511.06.07', 'TUNJ HARI RAYA PEGAWAI RENTAL INTERNAL', '11', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('108', '5', '511.06.08', 'TUNJ HARI RAYA PEGAWAI KREDIT KARYAWAN INTERNAL', '11', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('109', '5', '511.06.09', 'TUNJ HARI RAYA PEGAWAI USAHA LAIN INTERNAL', '11', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('110', '5', '511.06.10', 'TUNJ HARI RAYA PEGAWAI SUPPORTING (UMUM, SDM, KEU)', '11', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('111', '5', '511.06.11', 'TUNJ HARI RAYA PERWAKILAN', '11', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('112', '5', '511.07.01', 'TUNJ BONUS PEGAWAI OUTSOURCING INTERNAL', '12', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('113', '3', '511.07.02', 'TUNJ BONUS PEGAWAI OUTSOURCING SATPAM', '12', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('114', '3', '511.07.03', 'TUNJ BONUS PEGAWAI OUTSOURCING PENGEMUDI', '12', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('115', '3', '511.07.04', 'TUNJ BONUS PEGAWAI OUTSOURCING CLEANING SERVICE', '12', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('116', '3', '511.07.05', 'TUNJ BONUS PEGAWAI OUTSOURCING LAINNYA', '12', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('117', '5', '511.07.06', 'TUNJ BONUS PEGAWAI EXPEDISI INTERNAL', '12', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('118', '5', '511.07.07', 'TUNJ BONUS PEGAWAI RENTAL INTERNAL', '12', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('119', '5', '511.07.08', 'TUNJ BONUS PEGAWAI KREDIT KARYAWAN INTERNAL', '12', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('120', '5', '511.07.09', 'TUNJ BONUS PEGAWAI USAHA LAIN INTERNAL', '12', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('121', '5', '511.07.10', 'TUNJ BONUS PEGAWAI SUPPORTING (UMUM, SDM, KEU)', '12', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('122', '5', '511.07.11', 'TUNJ BONUS PERWAKILAN', '12', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('123', '5', '511.08.01', 'TUNJ PPH 21 PEGAWAI OUTSOURCING INTERNAL', '13', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('124', '3', '511.08.02', 'TUNJ PPH 21 PEGAWAI OUTSOURCING SATPAM', '13', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('125', '3', '511.08.03', 'TUNJ PPH 21 PEGAWAI OUTSOURCING PENGEMUDI', '13', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('126', '3', '511.08.04', 'TUNJ PPH 21 PEGAWAI OUTSOURCING CLEANING SERVICE', '13', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('127', '3', '511.08.05', 'TUNJ PPH 21 PEGAWAI OUTSOURCING LAINNYA', '13', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('128', '5', '511.08.06', 'TUNJ PPH 21 PEGAWAI EXPEDISI INTERNAL', '13', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('129', '5', '511.08.07', 'TUNJ PPH 21 PEGAWAI RENTAL INTERNAL', '13', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('130', '5', '511.08.08', 'TUNJ PPH 21 PEGAWAI KREDIT KARYAWAN INTERNAL', '13', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('131', '5', '511.08.09', 'TUNJ PPH 21 PEGAWAI USAHA LAIN INTERNAL', '13', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('132', '5', '511.08.10', 'TUNJ PPH 21 PEGAWAI SUPPORTING (UMUM, SDM, KEU)', '13', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('133', '5', '511.08.11', 'TUNJ PPH 21 PERWAKILAN', '13', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('134', '5', '511.09.01', 'TUNJ LAINNYA PEGAWAI OUTSOURCING INTERNAL', '14', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('135', '3', '511.09.02', 'TUNJ LAINNYA PEGAWAI OUTSOURCING SATPAM', '14', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('136', '3', '511.09.03', 'TUNJ LAINNYA PEGAWAI OUTSOURCING PENGEMUDI', '14', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('137', '3', '511.09.04', 'TUNJ LAINNYA PEGAWAI OUTSOURCING CLEANING SERVICE', '14', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('138', '3', '511.09.05', 'TUNJ LAINNYA PEGAWAI OUTSOURCING LAINNYA', '14', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('139', '5', '511.09.06', 'TUNJ LAINNYA PEGAWAI EXPEDISI INTERNAL', '14', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('140', '5', '511.09.07', 'TUNJ LAINNYA PEGAWAI RENTAL INTERNAL', '14', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('141', '5', '511.09.08', 'TUNJ LAINNYA PEGAWAI KREDIT KARYAWAN INTERNAL', '14', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('142', '5', '511.09.09', 'TUNJ LAINNYA PEGAWAI USAHA LAIN INTERNAL', '14', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('143', '5', '511.09.10', 'TUNJ LAINNYA PEGAWAI SUPPORTING (UMUM, SDM, KEU)', '14', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('144', '5', '511.09.11', 'TUNJ LAINNYA PERWAKILAN', '14', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('145', '5', '511.10.01', 'BIAYA BANTUAN PENGOBATAN', '15', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('146', '3', '511.10.02', 'BIAYA PREMI ASURANSI KESEHATAN', '15', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('147', '3', '511.10.03', 'BIAYA PREMI JAMSOSTEK', '15', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('148', '5', '511.11.01', 'BIAYA IURAN DANA PENSIUN (DPLK)', '16', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('149', '5', '511.11.02', 'BIAYA PESANGON', '16', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('150', '5', '511.11.03', 'BIAYA IMBALAN PASCA KERJA', '16', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('151', '5', '511.12.01', 'BIAYA LEMBUR', '17', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('152', '5', '511.12.02', 'BIAYA HONORARIUM', '17', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('153', '5', '511.12.03', 'BIAYA BANTUAN SEWA RUMAH', '17', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('154', '5', '511.12.04', 'BIAYA REKRUT PEGAWAI', '17', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('155', '3', '511.12.05', 'BIAYA PENDIDIKAN DAN PELATIHAN', '17', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('156', '5', '511.12.06', 'BIAYA SEMINAR', '17', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('157', '5', '511.12.07', 'BIAYA JASMANI DAN ROHANI', '17', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('158', '5', '511.12.08', 'BIAYA PEGAWAI LAINNYA', '17', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('159', '5', '512.01.01', 'TUNJ PENGELOLAAN', '18', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('160', '5', '512.01.02', 'TUNJ BAHAN BAKAR MINYAK (BBM)', '18', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('161', '5', '512.01.03', 'TUNJ TELEPON, AIR, LISTRIK (TAL)', '18', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('162', '5', '512.01.04', 'TUNJ SEWA RUMAH', '18', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('163', '5', '512.01.05', 'TUNJ PAKAIAN', '18', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('164', '5', '512.01.06', 'TUNJ HARI RAYA (THR)', '18', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('165', '5', '512.01.07', 'TANTIEM/JASA PRODUKSI', '18', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('166', '5', '512.01.08', 'TUNJ CUTI', '18', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('167', '5', '512.01.09', 'TUNJ PURNA JABATAN', '18', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('168', '5', '512.01.10', 'TUNJ PPH PASAL 21', '18', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('169', '5', '512.01.11', 'TUNJ LAINNYA', '18', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('170', '5', '512.02.01', 'HONORARIUM', '19', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('171', '5', '512.02.02', 'TUNJ PAKAIAN', '19', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('172', '5', '512.02.03', 'TUNJ HARI RAYA (THR)', '19', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('173', '5', '512.02.04', 'TANTIEM/JASA PRODUKSI', '19', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('174', '5', '512.02.05', 'TUNJ CUTI', '19', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('175', '5', '512.02.06', 'TUNJ PURNA JABATAN', '19', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('176', '5', '512.02.07', 'TUNJ PPH PASAL 21', '19', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('177', '5', '512.02.08', 'TUNJ LAINNYA', '19', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('178', '6', '513.01.01', 'BIAYA ATK PUSAT', '20', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('179', '6', '513.02.01', 'BIAYA FOTO COPY PUSAT', '20', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('180', '6', '513.03.01', 'BIAYA PENCETAKAN FORMULIR', '21', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('181', '6', '513.03.02', 'BIAYA PENCETAKAN BUKU', '21', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('182', '6', '513.04.01', 'BIAYA PEMBELIAN BUKU DAN PERATURAN', '21', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('183', '6', '513.04.02', 'BIAYA DOKUMENTASI', '21', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('184', '6', '513.05.01', 'BIAYA PERLENGKAPAN KANTOR', '22', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('185', '6', '513.07.01', 'BIAYA PROMOSI', '24', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('186', '6', '513.07.02', 'BIAYA IKLAN', '24', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('187', '6', '513.07.03', 'BIAYA SPONSOR', '24', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('188', '6', '513.07.04', 'BIAYA REPRESENTASI', '24', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('189', '6', '513.07.05', 'BIAYA SUMBANGAN', '24', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('190', '3', '513.07.06', 'BIAYA PEMASARAN LAINNYA', '24', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('191', '5', '513.08.01', 'BIAYA IZIN USAHA', '25', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('192', '7', '513.07.06', 'BIAYA PEMASARAN LAINNYA', '24', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('193', '4', '513.07.06', 'BIAYA PEMASARAN LAINNYA', '24', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('194', '2', '513.07.06', 'BIAYA PEMASARAN LAINNYA', '24', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('195', '1', '513.07.06', 'BIAYA PEMASARAN LAINNYA', '24', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('196', '7', '514.01.01', 'BIAYA KERTAS', '26', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('197', '7', '514.01.02', 'BIAYA BINDER JILID', '26', '2', '2017-06-02 10:45:56', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('198', '7', '514.01.03', 'BIAYA SAMPUL JILID', '26', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('199', '7', '514.01.04', 'BIAYA TINTA', '26', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('200', '7', '514.01.05', 'BIAYA SERVICE MESIN', '26', '2', '2017-05-24 15:33:20', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('201', '7', '514.01.06', 'BIAYA TEKNISI', '26', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('202', '7', '514.01.07', 'BIAYA OPERASIONAL FOTO COPY LAINNYA', '26', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('203', '1', '514.02.01', 'BIAYA PENGIRIMAN', '27', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('204', '1', '514.02.02', 'BIAYA HANDLING', '27', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('205', '1', '514.02.03', 'BIAYA PACKING', '27', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('206', '1', '514.02.04', 'BIAYA IURAN ASPRINDO', '27', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('207', '1', '514.02.05', 'BIAYA OPERASIONAL EXPEDISI LAINNYA', '27', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('208', '3', '514.03.01', 'BIAYA CHEMICAL', '28', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('209', '1', '514.03.02', 'BIAYA ALAT HABIS SEKALI PAKAI', '28', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('210', '1', '514.03.03', 'BIAYA SARANA KERJA', '28', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('211', '3', '514.03.04', 'BIAYA OPERASIONAL CLEANING SERVICE LAINNYA', '28', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('212', '6', '514.04.01', 'BIAYA PEMELIHARAAN GEDUNG KANTOR', '29', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('213', '6', '514.04.04', 'BIAYA PAJAK BUMI BANGUNAN (PBB)', '29', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('214', '6', '514.05.01', 'BIAYA PEMELIHARAAN INVENTARIS KANTOR', '30', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('215', '6', '514.06.01', 'BIAYA BBM DAN PELUMAS MOBIL/MOTOR DINAS', '31', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('216', '6', '514.06.02', 'BIAYA PERBAIKAN DAN SUKU CADANG EKS', '31', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('217', '6', '514.06.03', 'BIAYA BPKB, STNK DAN UJI ULANG KENDARAAN (KIR) EKS', '31', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('218', '1', '514.07.00', 'BIAYA PEMELIHARAAN KENDARAAN EKSPEDISI', '31', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('219', '4', '514.07.02', 'BIAYA PERBAIKAN DAN SUKU CADANG', '32', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('220', '4', '514.07.03', 'BIAYA BPKB, STNK DAN UJI ULANG KENDARAAN (KIR)', '32', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('221', '4', '514.08.01', 'BIAYA BBM DAN PELUMAS MOBIL RENTAL', '33', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('222', '4', '514.08.02', 'BIAYA PERBAIKAN DAN SUKU CADANG  MOBIL RENTAL', '33', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('223', '4', '514.08.03', 'BIAYA BPKB, STNK DAN KIR MOBIL RENTAL', '33', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('224', '4', '514.08.04', 'BIAYA SEWA MOBIL PENGGANTI RENTAL', '33', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('225', '4', '514.08.05', 'BIAYA PEMELIHARAAN DAN SURAT-SURAT SEPEDA MOTOR RENTAL', '33', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('226', '6', '514.09.01', 'BIAYA AIR KANTOR', '34', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('227', '6', '514.10.01', 'BIAYA LISTRIK KANTOR', '35', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('228', '6', '514.11.01', 'BIAYA TELEPON KANTOR', '36', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('229', '5', '514.12.01', 'BIAYA PERJALANAN DAN PENGINAPAN', '37', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('230', '5', '514.12.02', 'BIAYA PERJALANAN (PP)', '37', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('231', '6', '514.13.01', 'BIAYA SEWA GEDUNG', '38', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('232', '4', '514.14.01', 'BIAYA ASURANSI KENDARAAN', '39', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('233', '6', '514.15.01', 'BIAYA RAPAT UMUM PEMEGANG SAHAM (RUPS)', '40', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('234', '6', '514.15.02', 'BIAYA RAPAT LAINNYA', '40', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('235', '7', '514.16.01', 'BIAYA OPERASIONAL USAHA LAIN', '41', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('236', '6', '514.19.02', 'BIAYA LANGGANAN SURAT KABAR/MAJALAH', '42', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('237', '6', '514.19.03', 'BIAYA PERANGKO/MATEREI', '42', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('238', '6', '514.19.04', 'BIAYA ADMINISTRASI PAJAK', '42', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('239', '6', '514.19.05', 'BIAYA KEAMANAN LINGKUNGAN', '42', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('240', '6', '514.19.06', 'BIAYA RETRIBUSI', '42', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('241', '6', '514.19.07', 'BIAYA OPERASIONAL LAINNYA', '42', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('242', '2', '514.07.02', 'BIAYA PERBAIKAN DAN SUKU CADANG', '32', '2', '2017-05-12 10:01:26', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('243', '2', '514.07.03', 'BIAYA BPKB, STNK DAN UJI ULANG KENDARAAN (KIR)', '32', '2', '2017-05-12 10:01:27', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('244', '2', '514.08.01', 'BIAYA BBM DAN PELUMAS MOBIL RENTAL', '33', '2', '2017-05-12 10:01:27', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('245', '2', '514.08.02', 'BIAYA PERBAIKAN DAN SUKU CADANG  MOBIL RENTAL', '33', '2', '2017-05-12 10:01:27', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('246', '2', '514.08.03', 'BIAYA BPKB, STNK DAN KIR MOBIL RENTAL', '33', '2', '2017-05-12 10:01:27', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('247', '2', '514.08.04', 'BIAYA SEWA MOBIL PENGGANTI RENTAL', '33', '2', '2017-05-12 10:01:27', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('248', '2', '514.08.05', 'BIAYA PEMELIHARAAN DAN SURAT-SURAT SEPEDA MOTOR RENTAL', '33', '2', '2017-05-12 10:01:27', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('249', '2', '514.14.01', 'BIAYA ASURANSI KENDARAAN', '39', '2', '2017-05-12 10:01:27', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('250', '4', '515.02.01', 'BIAYA BUNGA PEMBIAYAAN (LEASING)', '44', '2', '2017-05-12 10:01:27', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('251', '2', '515.03.01', 'BIAYA JASA PENGEMBANGAN JPK', '45', '2', '2017-05-12 10:01:27', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('252', '2', '515.02.01', 'BIAYA BUNGA PEMBIAYAAN (LEASING)', '44', '2', '2017-05-12 10:01:27', '1', null, null);
INSERT INTO `ms_akun_budget` VALUES ('253', '6', '12312312', 'atk', '42', '2', '2017-07-11 11:15:13', '3', null, null);

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `description` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'admin', 'User Administrator', 'Administrator', '2017-02-23 00:00:00', '2017-02-23 00:00:00');
INSERT INTO `roles` VALUES ('2', 'Umum', 'umum', 'umum', '2017-06-14 02:40:03', '2017-06-14 02:40:03');

-- ----------------------------
-- Table structure for sub_module_permissions
-- ----------------------------
DROP TABLE IF EXISTS `sub_module_permissions`;
CREATE TABLE `sub_module_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `sub_module_id` int(11) NOT NULL,
  `allow` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=643 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sub_module_permissions
-- ----------------------------
INSERT INTO `sub_module_permissions` VALUES ('1', '1', '9', '1', '1', '2017-02-28 07:03:07', '2017-02-28 07:03:07');
INSERT INTO `sub_module_permissions` VALUES ('2', '1', '9', '2', '1', '2017-02-28 07:03:07', '2017-02-28 07:03:07');
INSERT INTO `sub_module_permissions` VALUES ('3', '1', '9', '3', '1', '2017-02-28 07:03:07', '2017-02-28 07:03:07');
INSERT INTO `sub_module_permissions` VALUES ('4', '1', '9', '4', '1', '2017-02-28 07:03:07', '2017-02-28 07:03:07');
INSERT INTO `sub_module_permissions` VALUES ('6', '1', '2', '6', '1', '2017-03-02 08:18:59', '2017-03-02 08:18:59');
INSERT INTO `sub_module_permissions` VALUES ('7', '1', '4', '8', '1', '2017-03-02 08:36:39', '2017-03-02 08:36:39');
INSERT INTO `sub_module_permissions` VALUES ('9', '1', '6', '10', '1', '2017-03-02 08:36:46', '2017-03-02 08:36:46');
INSERT INTO `sub_module_permissions` VALUES ('10', '1', '7', '11', '1', '2017-03-02 08:36:49', '2017-03-02 08:36:49');
INSERT INTO `sub_module_permissions` VALUES ('11', '1', '2', '12', '1', '2017-02-28 00:03:07', '2017-02-28 00:03:07');
INSERT INTO `sub_module_permissions` VALUES ('12', '1', '2', '13', '1', '2017-02-28 00:03:07', '2017-02-28 00:03:07');
INSERT INTO `sub_module_permissions` VALUES ('13', '1', '2', '14', '1', '2017-02-28 00:03:07', '2017-02-28 00:03:07');
INSERT INTO `sub_module_permissions` VALUES ('14', '1', '2', '15', '1', '2017-02-28 00:03:07', '2017-02-28 00:03:07');
INSERT INTO `sub_module_permissions` VALUES ('15', '1', '2', '16', '1', '2017-02-28 02:51:20', '2017-02-28 02:51:20');
INSERT INTO `sub_module_permissions` VALUES ('16', '1', '2', '17', '1', '2017-03-02 01:18:59', '2017-03-02 01:18:59');
INSERT INTO `sub_module_permissions` VALUES ('17', '1', '2', '18', '1', '2017-03-02 01:36:39', '2017-03-02 01:36:39');
INSERT INTO `sub_module_permissions` VALUES ('18', '1', '2', '19', '1', '2017-03-09 06:27:20', '2017-03-09 06:27:20');
INSERT INTO `sub_module_permissions` VALUES ('19', '1', '2', '20', '1', '2017-03-09 06:27:20', '2017-03-09 06:27:20');
INSERT INTO `sub_module_permissions` VALUES ('20', '1', '2', '21', '1', '2017-03-09 06:27:20', '2017-03-09 06:27:20');
INSERT INTO `sub_module_permissions` VALUES ('21', '1', '2', '22', '1', '2017-03-09 06:27:20', '2017-03-09 06:27:20');
INSERT INTO `sub_module_permissions` VALUES ('22', '1', '2', '23', '1', '2017-03-09 06:27:20', '2017-03-09 06:27:20');
INSERT INTO `sub_module_permissions` VALUES ('23', '1', '2', '24', '1', '2017-03-09 06:27:20', '2017-03-09 06:27:20');
INSERT INTO `sub_module_permissions` VALUES ('24', '1', '2', '25', '1', '2017-03-09 06:27:21', '2017-03-09 06:27:21');
INSERT INTO `sub_module_permissions` VALUES ('25', '1', '2', '26', '1', '2017-03-09 06:27:21', '2017-03-09 06:27:21');
INSERT INTO `sub_module_permissions` VALUES ('26', '1', '2', '27', '1', '2017-03-09 06:27:21', '2017-03-09 06:27:21');
INSERT INTO `sub_module_permissions` VALUES ('27', '1', '2', '28', '1', '2017-03-09 06:27:21', '2017-03-09 06:27:21');
INSERT INTO `sub_module_permissions` VALUES ('28', '1', '2', '29', '1', '2017-03-09 06:27:21', '2017-03-09 06:27:21');
INSERT INTO `sub_module_permissions` VALUES ('29', '1', '2', '30', '1', '2017-03-09 06:40:45', '2017-03-09 06:40:45');
INSERT INTO `sub_module_permissions` VALUES ('30', '1', '2', '31', '1', '2017-03-09 06:40:45', '2017-03-09 06:40:45');
INSERT INTO `sub_module_permissions` VALUES ('31', '1', '2', '32', '1', '2017-03-09 06:40:45', '2017-03-09 06:40:45');
INSERT INTO `sub_module_permissions` VALUES ('32', '1', '2', '33', '1', '2017-03-09 06:40:45', '2017-03-09 06:40:45');
INSERT INTO `sub_module_permissions` VALUES ('33', '1', '2', '34', '1', '2017-03-09 06:40:45', '2017-03-09 06:40:45');
INSERT INTO `sub_module_permissions` VALUES ('34', '1', '2', '35', '1', '2017-03-09 06:40:45', '2017-03-09 06:40:45');
INSERT INTO `sub_module_permissions` VALUES ('35', '1', '2', '36', '1', '2017-03-09 06:40:45', '2017-03-09 06:40:45');
INSERT INTO `sub_module_permissions` VALUES ('36', '1', '2', '37', '1', '2017-03-09 06:40:45', '2017-03-09 06:40:45');
INSERT INTO `sub_module_permissions` VALUES ('37', '1', '2', '38', '1', '2017-03-09 06:40:45', '2017-03-09 06:40:45');
INSERT INTO `sub_module_permissions` VALUES ('38', '1', '2', '39', '1', '2017-03-09 06:40:45', '2017-03-09 06:40:45');
INSERT INTO `sub_module_permissions` VALUES ('39', '1', '2', '40', '1', '2017-03-09 06:40:45', '2017-03-09 06:40:45');
INSERT INTO `sub_module_permissions` VALUES ('40', '1', '2', '41', '1', '2017-03-09 06:40:45', '2017-03-09 06:40:45');
INSERT INTO `sub_module_permissions` VALUES ('41', '1', '2', '42', '1', '2017-03-09 06:40:45', '2017-03-09 06:40:45');
INSERT INTO `sub_module_permissions` VALUES ('42', '1', '2', '43', '1', '2017-03-09 06:40:45', '2017-03-09 06:40:45');
INSERT INTO `sub_module_permissions` VALUES ('43', '1', '2', '44', '1', '2017-03-09 06:40:46', '2017-03-09 06:40:46');
INSERT INTO `sub_module_permissions` VALUES ('44', '1', '2', '45', '1', '2017-03-09 06:40:46', '2017-03-09 06:40:46');
INSERT INTO `sub_module_permissions` VALUES ('45', '1', '2', '46', '1', '2017-03-09 06:40:46', '2017-03-09 06:40:46');
INSERT INTO `sub_module_permissions` VALUES ('46', '1', '2', '47', '1', '2017-03-09 06:59:00', '2017-03-09 06:59:00');
INSERT INTO `sub_module_permissions` VALUES ('47', '1', '2', '48', '1', '2017-03-09 06:59:00', '2017-03-09 06:59:00');
INSERT INTO `sub_module_permissions` VALUES ('48', '1', '2', '49', '1', '2017-03-09 06:59:00', '2017-03-09 06:59:00');
INSERT INTO `sub_module_permissions` VALUES ('49', '1', '2', '50', '1', '2017-03-09 06:59:00', '2017-03-09 06:59:00');
INSERT INTO `sub_module_permissions` VALUES ('50', '1', '2', '51', '1', '2017-03-09 06:59:00', '2017-03-09 06:59:00');
INSERT INTO `sub_module_permissions` VALUES ('51', '1', '2', '52', '1', '2017-03-09 06:59:00', '2017-03-09 06:59:00');
INSERT INTO `sub_module_permissions` VALUES ('52', '1', '2', '53', '1', '2017-03-09 06:59:00', '2017-03-09 06:59:00');
INSERT INTO `sub_module_permissions` VALUES ('53', '1', '2', '54', '1', '2017-03-09 06:59:00', '2017-03-09 06:59:00');
INSERT INTO `sub_module_permissions` VALUES ('54', '1', '2', '55', '1', '2017-03-09 06:59:01', '2017-03-09 06:59:01');
INSERT INTO `sub_module_permissions` VALUES ('55', '1', '2', '56', '1', '2017-03-09 06:59:01', '2017-03-09 06:59:01');
INSERT INTO `sub_module_permissions` VALUES ('56', '1', '2', '57', '1', '2017-03-09 06:59:01', '2017-03-09 06:59:01');
INSERT INTO `sub_module_permissions` VALUES ('57', '1', '2', '58', '1', '2017-03-09 06:59:01', '2017-03-09 06:59:01');
INSERT INTO `sub_module_permissions` VALUES ('58', '1', '2', '59', '1', '2017-03-09 06:59:01', '2017-03-09 06:59:01');
INSERT INTO `sub_module_permissions` VALUES ('59', '1', '2', '60', '1', '2017-03-09 06:59:01', '2017-03-09 06:59:01');
INSERT INTO `sub_module_permissions` VALUES ('60', '1', '2', '61', '1', '2017-03-09 06:59:01', '2017-03-09 06:59:01');
INSERT INTO `sub_module_permissions` VALUES ('61', '1', '2', '62', '1', '2017-03-09 06:59:01', '2017-03-09 06:59:01');
INSERT INTO `sub_module_permissions` VALUES ('62', '1', '2', '63', '1', '2017-03-09 06:59:01', '2017-03-09 06:59:01');
INSERT INTO `sub_module_permissions` VALUES ('63', '1', '2', '64', '1', '2017-03-09 06:59:01', '2017-03-09 06:59:01');
INSERT INTO `sub_module_permissions` VALUES ('64', '1', '2', '65', '1', '2017-03-09 06:59:01', '2017-03-09 06:59:01');
INSERT INTO `sub_module_permissions` VALUES ('65', '1', '2', '66', '1', '2017-03-09 06:59:01', '2017-03-09 06:59:01');
INSERT INTO `sub_module_permissions` VALUES ('66', '1', '2', '67', '1', '2017-03-09 06:59:01', '2017-03-09 06:59:01');
INSERT INTO `sub_module_permissions` VALUES ('67', '1', '2', '68', '1', '2017-03-09 06:59:01', '2017-03-09 06:59:01');
INSERT INTO `sub_module_permissions` VALUES ('68', '1', '2', '69', '1', '2017-03-09 06:59:01', '2017-03-09 06:59:01');
INSERT INTO `sub_module_permissions` VALUES ('69', '1', '2', '70', '1', '2017-03-09 06:59:01', '2017-03-09 06:59:03');
INSERT INTO `sub_module_permissions` VALUES ('70', '1', '2', '71', '1', '2017-03-09 07:26:52', '2017-03-09 07:26:52');
INSERT INTO `sub_module_permissions` VALUES ('71', '1', '2', '72', '1', '2017-03-09 07:26:52', '2017-03-09 07:26:52');
INSERT INTO `sub_module_permissions` VALUES ('72', '1', '2', '73', '1', '2017-03-09 07:26:52', '2017-03-09 07:26:52');
INSERT INTO `sub_module_permissions` VALUES ('73', '1', '2', '74', '1', '2017-03-09 07:26:52', '2017-03-09 07:26:52');
INSERT INTO `sub_module_permissions` VALUES ('74', '1', '2', '75', '1', '2017-03-09 07:26:52', '2017-03-09 07:26:52');
INSERT INTO `sub_module_permissions` VALUES ('75', '1', '2', '76', '1', '2017-03-09 07:26:52', '2017-03-09 07:26:52');
INSERT INTO `sub_module_permissions` VALUES ('76', '1', '2', '77', '1', '2017-03-09 07:26:52', '2017-03-09 07:26:52');
INSERT INTO `sub_module_permissions` VALUES ('77', '1', '2', '78', '1', '2017-03-09 07:26:52', '2017-03-09 07:26:52');
INSERT INTO `sub_module_permissions` VALUES ('78', '1', '2', '79', '1', '2017-03-09 07:26:52', '2017-03-09 07:26:52');
INSERT INTO `sub_module_permissions` VALUES ('79', '1', '2', '80', '1', '2017-03-09 07:26:52', '2017-03-09 07:26:52');
INSERT INTO `sub_module_permissions` VALUES ('80', '1', '2', '81', '1', '2017-03-09 07:26:52', '2017-03-09 07:26:52');
INSERT INTO `sub_module_permissions` VALUES ('81', '1', '2', '82', '1', '2017-03-09 07:26:52', '2017-03-09 07:26:52');
INSERT INTO `sub_module_permissions` VALUES ('82', '1', '2', '83', '1', '2017-03-09 07:26:52', '2017-03-09 07:26:52');
INSERT INTO `sub_module_permissions` VALUES ('83', '1', '2', '84', '1', '2017-03-09 07:26:52', '2017-03-09 07:26:52');
INSERT INTO `sub_module_permissions` VALUES ('84', '1', '2', '85', '1', '2017-03-09 07:26:52', '2017-03-09 07:26:52');
INSERT INTO `sub_module_permissions` VALUES ('85', '1', '2', '86', '1', '2017-03-09 07:26:52', '2017-03-09 07:26:52');
INSERT INTO `sub_module_permissions` VALUES ('86', '1', '2', '87', '1', '2017-03-09 07:26:52', '2017-03-09 07:26:52');
INSERT INTO `sub_module_permissions` VALUES ('87', '1', '2', '88', '1', '2017-03-09 07:26:53', '2017-03-09 07:26:53');
INSERT INTO `sub_module_permissions` VALUES ('88', '1', '2', '89', '1', '2017-03-09 07:26:53', '2017-03-09 07:26:53');
INSERT INTO `sub_module_permissions` VALUES ('89', '1', '2', '90', '1', '2017-03-09 07:26:53', '2017-03-09 07:26:53');
INSERT INTO `sub_module_permissions` VALUES ('90', '1', '2', '91', '1', '2017-03-09 07:26:53', '2017-03-09 07:26:53');
INSERT INTO `sub_module_permissions` VALUES ('91', '1', '2', '92', '1', '2017-03-09 07:26:53', '2017-03-09 07:26:53');
INSERT INTO `sub_module_permissions` VALUES ('92', '1', '2', '93', '1', '2017-03-09 07:26:53', '2017-03-09 07:26:53');
INSERT INTO `sub_module_permissions` VALUES ('93', '1', '2', '94', '1', '2017-03-09 07:26:53', '2017-03-09 07:26:53');
INSERT INTO `sub_module_permissions` VALUES ('94', '1', '2', '95', '1', '2017-03-09 07:26:53', '2017-03-09 07:26:53');
INSERT INTO `sub_module_permissions` VALUES ('95', '1', '2', '96', '1', '2017-03-09 07:26:53', '2017-03-09 07:26:53');
INSERT INTO `sub_module_permissions` VALUES ('96', '1', '1', '97', '1', '2017-03-13 04:06:13', '2017-03-13 04:06:13');
INSERT INTO `sub_module_permissions` VALUES ('97', '1', '1', '98', '1', '2017-03-13 04:06:13', '2017-03-13 04:06:13');
INSERT INTO `sub_module_permissions` VALUES ('98', '1', '1', '99', '1', '2017-03-13 04:06:13', '2017-03-13 04:06:13');
INSERT INTO `sub_module_permissions` VALUES ('99', '1', '1', '100', '1', '2017-03-13 04:06:13', '2017-03-13 04:06:13');
INSERT INTO `sub_module_permissions` VALUES ('100', '1', '1', '101', '1', '2017-03-13 04:06:13', '2017-03-13 04:06:13');
INSERT INTO `sub_module_permissions` VALUES ('101', '1', '1', '102', '1', '2017-03-13 04:06:13', '2017-03-13 04:06:13');
INSERT INTO `sub_module_permissions` VALUES ('102', '1', '1', '103', '1', '2017-03-13 04:06:16', '2017-03-13 04:06:16');
INSERT INTO `sub_module_permissions` VALUES ('103', '1', '1', '104', '1', '2017-03-13 04:06:16', '2017-03-13 04:06:16');
INSERT INTO `sub_module_permissions` VALUES ('104', '1', '1', '105', '1', '2017-03-13 04:06:16', '2017-03-13 04:06:16');
INSERT INTO `sub_module_permissions` VALUES ('105', '1', '1', '106', '1', '2017-03-13 04:06:16', '2017-03-13 04:06:16');
INSERT INTO `sub_module_permissions` VALUES ('106', '1', '1', '107', '1', '2017-03-13 04:06:16', '2017-03-13 04:06:16');
INSERT INTO `sub_module_permissions` VALUES ('107', '1', '1', '108', '1', '2017-03-13 04:06:16', '2017-03-13 04:06:16');
INSERT INTO `sub_module_permissions` VALUES ('108', '1', '1', '109', '1', '2017-03-13 04:06:16', '2017-03-13 04:06:16');
INSERT INTO `sub_module_permissions` VALUES ('109', '1', '1', '110', '1', '2017-03-13 04:06:16', '2017-03-13 04:06:16');
INSERT INTO `sub_module_permissions` VALUES ('110', '1', '1', '111', '1', '2017-03-13 04:06:17', '2017-03-13 04:06:17');
INSERT INTO `sub_module_permissions` VALUES ('111', '1', '1', '112', '1', '2017-03-13 04:06:17', '2017-03-13 04:06:17');
INSERT INTO `sub_module_permissions` VALUES ('112', '1', '1', '113', '1', '2017-03-13 04:06:17', '2017-03-13 04:06:17');
INSERT INTO `sub_module_permissions` VALUES ('113', '1', '1', '114', '1', '2017-03-13 04:06:17', '2017-03-13 04:06:17');
INSERT INTO `sub_module_permissions` VALUES ('114', '1', '1', '115', '1', '2017-03-13 04:06:17', '2017-03-13 04:06:17');
INSERT INTO `sub_module_permissions` VALUES ('115', '1', '1', '116', '1', '2017-03-13 04:06:17', '2017-03-13 04:06:17');
INSERT INTO `sub_module_permissions` VALUES ('116', '1', '1', '117', '1', '2017-03-13 04:06:17', '2017-03-13 04:06:17');
INSERT INTO `sub_module_permissions` VALUES ('117', '1', '1', '118', '1', '2017-03-13 04:06:17', '2017-03-13 04:06:17');
INSERT INTO `sub_module_permissions` VALUES ('118', '1', '1', '119', '1', '2017-03-13 04:06:17', '2017-03-13 04:06:17');
INSERT INTO `sub_module_permissions` VALUES ('119', '1', '1', '120', '1', '2017-03-13 04:06:17', '2017-03-13 04:06:17');
INSERT INTO `sub_module_permissions` VALUES ('120', '1', '1', '121', '1', '2017-03-13 04:06:17', '2017-03-13 04:06:17');
INSERT INTO `sub_module_permissions` VALUES ('121', '1', '1', '122', '1', '2017-03-13 04:06:18', '2017-03-13 04:06:18');
INSERT INTO `sub_module_permissions` VALUES ('122', '1', '1', '123', '1', '2017-03-13 04:06:18', '2017-03-13 04:06:18');
INSERT INTO `sub_module_permissions` VALUES ('123', '1', '1', '124', '1', '2017-03-13 04:06:18', '2017-03-13 04:06:18');
INSERT INTO `sub_module_permissions` VALUES ('124', '1', '1', '125', '1', '2017-03-13 04:06:18', '2017-03-13 04:06:18');
INSERT INTO `sub_module_permissions` VALUES ('125', '1', '1', '126', '1', '2017-03-13 04:06:18', '2017-03-13 04:06:18');
INSERT INTO `sub_module_permissions` VALUES ('126', '1', '1', '127', '1', '2017-03-13 04:06:18', '2017-03-13 04:06:18');
INSERT INTO `sub_module_permissions` VALUES ('127', '1', '1', '128', '1', '2017-03-13 04:06:18', '2017-03-13 04:06:18');
INSERT INTO `sub_module_permissions` VALUES ('128', '1', '1', '129', '1', '2017-03-13 04:06:18', '2017-03-13 04:06:18');
INSERT INTO `sub_module_permissions` VALUES ('129', '1', '1', '130', '1', '2017-03-13 04:06:18', '2017-03-13 04:06:18');
INSERT INTO `sub_module_permissions` VALUES ('130', '1', '1', '131', '1', '2017-03-13 04:06:18', '2017-03-13 04:06:18');
INSERT INTO `sub_module_permissions` VALUES ('131', '1', '1', '132', '1', '2017-03-13 04:06:18', '2017-03-13 04:06:18');
INSERT INTO `sub_module_permissions` VALUES ('132', '1', '1', '133', '1', '2017-03-13 04:06:18', '2017-03-13 04:06:18');
INSERT INTO `sub_module_permissions` VALUES ('133', '1', '1', '134', '1', '2017-03-13 04:06:18', '2017-03-13 04:06:18');
INSERT INTO `sub_module_permissions` VALUES ('134', '1', '1', '135', '1', '2017-03-13 04:06:18', '2017-03-13 04:06:18');
INSERT INTO `sub_module_permissions` VALUES ('135', '1', '1', '136', '1', '2017-03-13 04:06:18', '2017-03-13 04:06:18');
INSERT INTO `sub_module_permissions` VALUES ('136', '1', '1', '138', '1', '2017-03-13 07:40:46', '2017-03-13 07:40:46');
INSERT INTO `sub_module_permissions` VALUES ('137', '1', '1', '137', '1', '2017-03-13 07:40:49', '2017-03-13 07:40:49');
INSERT INTO `sub_module_permissions` VALUES ('200', '1', '3', '7', '1', '2017-04-11 02:26:35', '2017-04-11 02:26:35');
INSERT INTO `sub_module_permissions` VALUES ('201', '1', '3', '201', '1', '2017-04-11 02:26:35', '2017-04-11 02:26:35');
INSERT INTO `sub_module_permissions` VALUES ('202', '1', '3', '202', '1', '2017-04-11 02:26:35', '2017-04-11 02:26:35');
INSERT INTO `sub_module_permissions` VALUES ('203', '1', '3', '203', '1', '2017-04-11 02:26:35', '2017-04-11 02:26:35');
INSERT INTO `sub_module_permissions` VALUES ('204', '1', '3', '204', '1', '2017-04-11 02:26:35', '2017-04-11 02:26:35');
INSERT INTO `sub_module_permissions` VALUES ('205', '1', '3', '205', '1', '2017-04-11 02:26:35', '2017-04-11 02:26:35');
INSERT INTO `sub_module_permissions` VALUES ('206', '1', '3', '206', '1', '2017-04-11 02:26:35', '2017-04-11 02:26:35');
INSERT INTO `sub_module_permissions` VALUES ('207', '1', '3', '207', '1', '2017-04-11 02:26:35', '2017-04-11 02:26:35');
INSERT INTO `sub_module_permissions` VALUES ('208', '1', '3', '208', '1', '2017-04-11 02:26:35', '2017-04-11 02:26:35');
INSERT INTO `sub_module_permissions` VALUES ('209', '1', '3', '209', '1', '2017-04-11 02:26:35', '2017-04-11 02:26:35');
INSERT INTO `sub_module_permissions` VALUES ('210', '1', '3', '210', '1', '2017-04-11 02:26:35', '2017-04-11 02:26:35');
INSERT INTO `sub_module_permissions` VALUES ('211', '1', '3', '211', '1', '2017-04-11 02:26:35', '2017-04-11 02:26:35');
INSERT INTO `sub_module_permissions` VALUES ('212', '1', '3', '212', '1', '2017-04-11 02:26:35', '2017-04-11 02:26:35');
INSERT INTO `sub_module_permissions` VALUES ('213', '1', '3', '213', '1', '2017-04-11 02:26:35', '2017-04-11 02:26:35');
INSERT INTO `sub_module_permissions` VALUES ('214', '1', '3', '214', '1', '2017-04-11 02:26:36', '2017-04-11 02:26:36');
INSERT INTO `sub_module_permissions` VALUES ('215', '1', '3', '215', '1', '2017-04-11 02:26:36', '2017-04-11 02:26:36');
INSERT INTO `sub_module_permissions` VALUES ('216', '1', '3', '216', '1', '2017-04-11 02:26:36', '2017-04-11 02:26:36');
INSERT INTO `sub_module_permissions` VALUES ('217', '1', '3', '217', '1', '2017-04-11 02:26:36', '2017-04-11 02:26:36');
INSERT INTO `sub_module_permissions` VALUES ('218', '1', '3', '218', '1', '2017-04-11 02:26:36', '2017-04-11 02:26:36');
INSERT INTO `sub_module_permissions` VALUES ('219', '1', '3', '219', '1', '2017-04-11 02:26:36', '2017-04-11 02:26:36');
INSERT INTO `sub_module_permissions` VALUES ('220', '1', '3', '220', '1', '2017-04-11 02:26:36', '2017-04-11 02:26:36');
INSERT INTO `sub_module_permissions` VALUES ('221', '1', '3', '221', '1', '2017-04-11 02:26:36', '2017-04-11 02:26:36');
INSERT INTO `sub_module_permissions` VALUES ('222', '1', '3', '222', '1', '2017-04-11 02:26:36', '2017-04-11 02:26:36');
INSERT INTO `sub_module_permissions` VALUES ('223', '1', '3', '223', '1', '2017-04-11 02:26:36', '2017-04-11 02:26:36');
INSERT INTO `sub_module_permissions` VALUES ('224', '1', '3', '224', '1', '2017-04-11 02:26:36', '2017-04-11 02:26:36');
INSERT INTO `sub_module_permissions` VALUES ('225', '1', '3', '225', '1', '2017-04-11 02:26:36', '2017-04-11 02:26:36');
INSERT INTO `sub_module_permissions` VALUES ('226', '1', '3', '226', '1', '2017-04-11 02:26:36', '2017-04-11 02:26:36');
INSERT INTO `sub_module_permissions` VALUES ('227', '1', '3', '227', '1', '2017-04-11 02:26:36', '2017-04-11 02:26:36');
INSERT INTO `sub_module_permissions` VALUES ('228', '1', '3', '228', '1', '2017-04-11 02:26:36', '2017-04-11 02:26:36');
INSERT INTO `sub_module_permissions` VALUES ('229', '1', '3', '229', '1', '2017-04-11 02:26:36', '2017-04-11 02:26:36');
INSERT INTO `sub_module_permissions` VALUES ('230', '1', '3', '230', '1', '2017-04-11 02:26:36', '2017-04-11 02:26:36');
INSERT INTO `sub_module_permissions` VALUES ('231', '1', '3', '231', '1', '2017-04-11 02:26:36', '2017-04-11 02:26:36');
INSERT INTO `sub_module_permissions` VALUES ('232', '1', '3', '232', '1', '2017-04-11 02:26:36', '2017-04-11 02:26:36');
INSERT INTO `sub_module_permissions` VALUES ('233', '1', '3', '233', '1', '2017-04-11 02:26:36', '2017-04-11 02:26:36');
INSERT INTO `sub_module_permissions` VALUES ('234', '1', '3', '234', '1', '2017-04-11 02:26:36', '2017-04-11 02:26:36');
INSERT INTO `sub_module_permissions` VALUES ('235', '1', '3', '235', '1', '2017-04-11 02:26:36', '2017-04-11 02:26:36');
INSERT INTO `sub_module_permissions` VALUES ('236', '1', '3', '236', '1', '2017-04-11 02:26:37', '2017-04-11 02:26:37');
INSERT INTO `sub_module_permissions` VALUES ('237', '1', '3', '237', '1', '2017-04-11 02:26:37', '2017-04-11 02:26:37');
INSERT INTO `sub_module_permissions` VALUES ('238', '1', '3', '238', '1', '2017-04-11 02:26:37', '2017-04-11 02:26:37');
INSERT INTO `sub_module_permissions` VALUES ('239', '1', '3', '239', '1', '2017-04-11 02:26:37', '2017-04-11 02:26:37');
INSERT INTO `sub_module_permissions` VALUES ('240', '1', '3', '240', '1', '2017-04-11 02:26:37', '2017-04-11 02:26:37');
INSERT INTO `sub_module_permissions` VALUES ('241', '1', '3', '241', '1', '2017-04-11 02:26:37', '2017-04-11 02:26:37');
INSERT INTO `sub_module_permissions` VALUES ('242', '1', '4', '243', '1', '2017-04-11 03:42:35', '2017-04-11 03:42:35');
INSERT INTO `sub_module_permissions` VALUES ('243', '1', '4', '244', '1', '2017-04-11 03:42:35', '2017-04-11 03:42:35');
INSERT INTO `sub_module_permissions` VALUES ('244', '1', '4', '245', '1', '2017-04-11 03:42:35', '2017-04-11 03:42:35');
INSERT INTO `sub_module_permissions` VALUES ('245', '1', '4', '246', '1', '2017-04-11 03:42:36', '2017-04-11 03:42:36');
INSERT INTO `sub_module_permissions` VALUES ('246', '1', '4', '247', '1', '2017-04-11 03:42:36', '2017-04-11 03:42:36');
INSERT INTO `sub_module_permissions` VALUES ('247', '1', '4', '248', '1', '2017-04-11 03:42:36', '2017-04-11 03:42:36');
INSERT INTO `sub_module_permissions` VALUES ('248', '1', '4', '249', '1', '2017-04-11 03:42:36', '2017-04-11 03:42:36');
INSERT INTO `sub_module_permissions` VALUES ('249', '1', '4', '250', '1', '2017-04-11 03:42:36', '2017-04-11 03:42:36');
INSERT INTO `sub_module_permissions` VALUES ('250', '1', '4', '251', '1', '2017-04-11 03:42:36', '2017-04-11 03:42:36');
INSERT INTO `sub_module_permissions` VALUES ('251', '1', '4', '252', '1', '2017-04-11 03:42:36', '2017-04-11 03:42:36');
INSERT INTO `sub_module_permissions` VALUES ('252', '1', '4', '253', '1', '2017-04-11 03:42:36', '2017-04-11 03:42:36');
INSERT INTO `sub_module_permissions` VALUES ('253', '1', '4', '254', '1', '2017-04-11 03:42:36', '2017-04-11 03:42:36');
INSERT INTO `sub_module_permissions` VALUES ('254', '1', '4', '255', '1', '2017-04-11 03:42:36', '2017-04-11 03:42:36');
INSERT INTO `sub_module_permissions` VALUES ('255', '1', '4', '256', '1', '2017-04-11 03:42:36', '2017-04-11 03:42:36');
INSERT INTO `sub_module_permissions` VALUES ('256', '1', '4', '257', '1', '2017-04-11 03:42:36', '2017-04-11 03:42:36');
INSERT INTO `sub_module_permissions` VALUES ('257', '1', '4', '258', '1', '2017-04-11 03:42:36', '2017-04-11 03:42:36');
INSERT INTO `sub_module_permissions` VALUES ('258', '1', '4', '259', '1', '2017-04-11 03:42:36', '2017-04-11 03:42:36');
INSERT INTO `sub_module_permissions` VALUES ('259', '1', '4', '260', '1', '2017-04-11 03:42:36', '2017-04-11 03:42:36');
INSERT INTO `sub_module_permissions` VALUES ('260', '1', '4', '261', '1', '2017-04-11 03:42:36', '2017-04-11 03:42:36');
INSERT INTO `sub_module_permissions` VALUES ('261', '1', '4', '262', '1', '2017-04-11 03:42:36', '2017-04-11 03:42:36');
INSERT INTO `sub_module_permissions` VALUES ('262', '1', '4', '263', '1', '2017-04-11 03:42:36', '2017-04-11 03:42:36');
INSERT INTO `sub_module_permissions` VALUES ('263', '1', '4', '264', '1', '2017-04-11 03:42:36', '2017-04-11 03:42:36');
INSERT INTO `sub_module_permissions` VALUES ('264', '1', '4', '265', '1', '2017-04-11 03:42:36', '2017-04-11 03:42:36');
INSERT INTO `sub_module_permissions` VALUES ('265', '1', '4', '266', '1', '2017-04-11 03:42:36', '2017-04-11 03:42:36');
INSERT INTO `sub_module_permissions` VALUES ('266', '1', '4', '267', '1', '2017-04-11 03:42:36', '2017-04-11 03:42:36');
INSERT INTO `sub_module_permissions` VALUES ('267', '1', '4', '268', '1', '2017-04-11 03:42:37', '2017-04-11 03:42:37');
INSERT INTO `sub_module_permissions` VALUES ('268', '1', '4', '269', '1', '2017-04-11 03:42:37', '2017-04-11 03:42:37');
INSERT INTO `sub_module_permissions` VALUES ('269', '1', '4', '270', '1', '2017-04-11 03:42:37', '2017-04-11 03:42:37');
INSERT INTO `sub_module_permissions` VALUES ('270', '1', '4', '271', '1', '2017-04-11 03:42:37', '2017-04-11 03:42:37');
INSERT INTO `sub_module_permissions` VALUES ('271', '1', '4', '272', '1', '2017-04-11 03:42:37', '2017-04-11 03:42:37');
INSERT INTO `sub_module_permissions` VALUES ('272', '1', '4', '273', '1', '2017-04-11 03:42:37', '2017-04-11 03:42:37');
INSERT INTO `sub_module_permissions` VALUES ('273', '1', '4', '274', '1', '2017-04-11 03:42:37', '2017-04-11 03:42:37');
INSERT INTO `sub_module_permissions` VALUES ('274', '1', '4', '275', '1', '2017-04-11 03:42:37', '2017-04-11 03:42:37');
INSERT INTO `sub_module_permissions` VALUES ('275', '1', '4', '276', '1', '2017-04-11 03:42:37', '2017-04-11 03:42:37');
INSERT INTO `sub_module_permissions` VALUES ('276', '1', '4', '277', '1', '2017-04-11 03:42:37', '2017-04-11 03:42:37');
INSERT INTO `sub_module_permissions` VALUES ('277', '1', '4', '278', '1', '2017-04-11 03:42:37', '2017-04-11 03:42:37');
INSERT INTO `sub_module_permissions` VALUES ('278', '1', '4', '279', '1', '2017-04-11 03:42:37', '2017-04-11 03:42:37');
INSERT INTO `sub_module_permissions` VALUES ('279', '1', '4', '280', '1', '2017-04-11 03:42:37', '2017-04-11 03:42:37');
INSERT INTO `sub_module_permissions` VALUES ('280', '1', '4', '281', '1', '2017-04-11 03:42:37', '2017-04-11 03:42:37');
INSERT INTO `sub_module_permissions` VALUES ('281', '1', '4', '282', '1', '2017-04-11 03:42:37', '2017-04-11 03:42:37');
INSERT INTO `sub_module_permissions` VALUES ('282', '1', '6', '290', '1', '2017-04-12 10:03:15', '2017-04-12 10:03:15');
INSERT INTO `sub_module_permissions` VALUES ('283', '1', '6', '291', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('284', '1', '6', '292', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('285', '1', '6', '293', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('286', '1', '6', '294', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('287', '1', '6', '295', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('288', '1', '6', '296', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('289', '1', '6', '297', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('290', '1', '6', '298', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('291', '1', '6', '299', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('292', '1', '6', '300', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('293', '1', '6', '301', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('294', '1', '6', '302', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('295', '1', '6', '303', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('296', '1', '6', '304', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('297', '1', '6', '305', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('298', '1', '6', '306', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('299', '1', '6', '307', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('300', '1', '6', '308', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('301', '1', '6', '309', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('302', '1', '6', '310', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('303', '1', '6', '311', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('304', '1', '6', '312', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('305', '1', '6', '313', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('306', '1', '6', '314', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('307', '1', '6', '315', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('308', '1', '6', '316', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('309', '1', '6', '317', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('310', '1', '6', '318', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('311', '1', '6', '319', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('312', '1', '6', '320', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('313', '1', '6', '321', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('314', '1', '6', '322', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('315', '1', '6', '323', '1', '2017-04-12 10:03:16', '2017-04-12 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('316', '1', '6', '324', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('317', '1', '6', '325', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('318', '1', '6', '326', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('319', '1', '6', '327', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('320', '1', '6', '328', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('321', '1', '6', '329', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('322', '1', '6', '330', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('323', '1', '6', '331', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('324', '1', '6', '332', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('325', '1', '6', '333', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('326', '1', '6', '334', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('327', '1', '6', '335', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('328', '1', '6', '336', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('329', '1', '6', '337', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('330', '1', '6', '338', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('331', '1', '6', '339', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('332', '1', '6', '340', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('333', '1', '6', '341', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('334', '1', '6', '342', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('335', '1', '6', '343', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('336', '1', '6', '344', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('337', '1', '6', '345', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('338', '1', '6', '346', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('339', '1', '6', '347', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('340', '1', '6', '348', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('341', '1', '6', '349', '1', '2017-04-12 10:03:17', '2017-04-12 10:03:17');
INSERT INTO `sub_module_permissions` VALUES ('342', '1', '6', '350', '1', '2017-04-12 10:03:18', '2017-04-12 10:03:18');
INSERT INTO `sub_module_permissions` VALUES ('343', '1', '6', '351', '1', '2017-04-12 10:03:18', '2017-04-12 10:03:18');
INSERT INTO `sub_module_permissions` VALUES ('344', '1', '6', '352', '1', '2017-04-12 10:03:18', '2017-04-12 10:03:18');
INSERT INTO `sub_module_permissions` VALUES ('345', '1', '6', '353', '1', '2017-04-12 10:03:18', '2017-04-12 10:03:18');
INSERT INTO `sub_module_permissions` VALUES ('346', '1', '6', '354', '1', '2017-04-12 10:03:18', '2017-04-12 10:03:18');
INSERT INTO `sub_module_permissions` VALUES ('347', '1', '6', '355', '1', '2017-04-12 10:03:18', '2017-04-12 10:03:18');
INSERT INTO `sub_module_permissions` VALUES ('348', '1', '6', '356', '1', '2017-04-12 10:03:18', '2017-04-12 10:03:18');
INSERT INTO `sub_module_permissions` VALUES ('349', '1', '7', '357', '1', '2017-04-12 10:03:34', '2017-04-12 10:03:34');
INSERT INTO `sub_module_permissions` VALUES ('350', '1', '7', '358', '1', '2017-04-12 10:03:34', '2017-04-12 10:03:34');
INSERT INTO `sub_module_permissions` VALUES ('351', '1', '7', '359', '1', '2017-04-12 10:03:34', '2017-04-12 10:03:34');
INSERT INTO `sub_module_permissions` VALUES ('352', '1', '7', '360', '1', '2017-04-12 10:03:34', '2017-04-12 10:03:34');
INSERT INTO `sub_module_permissions` VALUES ('353', '1', '7', '361', '1', '2017-04-12 10:03:34', '2017-04-12 10:03:34');
INSERT INTO `sub_module_permissions` VALUES ('354', '1', '7', '362', '1', '2017-04-12 10:03:34', '2017-04-12 10:03:34');
INSERT INTO `sub_module_permissions` VALUES ('355', '1', '7', '363', '1', '2017-04-12 10:03:34', '2017-04-12 10:03:34');
INSERT INTO `sub_module_permissions` VALUES ('356', '1', '7', '364', '1', '2017-04-12 10:03:34', '2017-04-12 10:03:34');
INSERT INTO `sub_module_permissions` VALUES ('357', '1', '7', '365', '1', '2017-04-12 10:03:34', '2017-04-12 10:03:34');
INSERT INTO `sub_module_permissions` VALUES ('358', '1', '7', '366', '1', '2017-04-12 10:03:34', '2017-04-12 10:03:34');
INSERT INTO `sub_module_permissions` VALUES ('359', '1', '7', '367', '1', '2017-04-12 10:03:34', '2017-04-12 10:03:34');
INSERT INTO `sub_module_permissions` VALUES ('360', '1', '7', '368', '1', '2017-04-12 10:03:34', '2017-04-12 10:03:34');
INSERT INTO `sub_module_permissions` VALUES ('361', '1', '7', '369', '1', '2017-04-12 10:03:34', '2017-04-12 10:03:34');
INSERT INTO `sub_module_permissions` VALUES ('362', '1', '7', '370', '1', '2017-04-12 10:03:34', '2017-04-12 10:03:34');
INSERT INTO `sub_module_permissions` VALUES ('363', '1', '7', '371', '1', '2017-04-12 10:03:34', '2017-04-12 10:03:34');
INSERT INTO `sub_module_permissions` VALUES ('364', '1', '7', '372', '1', '2017-04-12 10:03:34', '2017-04-12 10:03:34');
INSERT INTO `sub_module_permissions` VALUES ('365', '1', '7', '373', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('366', '1', '7', '374', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('367', '1', '7', '375', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('368', '1', '7', '376', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('369', '1', '7', '377', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('370', '1', '7', '378', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('371', '1', '7', '379', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('372', '1', '7', '380', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('373', '1', '7', '381', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('374', '1', '7', '382', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('375', '1', '7', '383', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('376', '1', '7', '384', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('377', '1', '7', '385', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('378', '1', '7', '386', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('379', '1', '7', '387', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('380', '1', '7', '388', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('381', '1', '7', '389', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('382', '1', '7', '390', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('383', '1', '7', '391', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('384', '1', '7', '392', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('385', '1', '7', '393', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('386', '1', '7', '394', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('387', '1', '7', '395', '1', '2017-04-12 10:03:35', '2017-04-12 10:03:35');
INSERT INTO `sub_module_permissions` VALUES ('388', '1', '7', '396', '1', '2017-04-12 10:03:36', '2017-04-12 10:03:36');
INSERT INTO `sub_module_permissions` VALUES ('389', '1', '7', '397', '1', '2017-04-12 10:03:36', '2017-04-12 10:03:36');
INSERT INTO `sub_module_permissions` VALUES ('390', '1', '7', '398', '1', '2017-04-12 10:03:36', '2017-04-12 10:03:36');
INSERT INTO `sub_module_permissions` VALUES ('391', '1', '7', '399', '1', '2017-04-12 10:03:36', '2017-04-12 10:03:36');
INSERT INTO `sub_module_permissions` VALUES ('392', '1', '7', '400', '1', '2017-04-12 10:03:36', '2017-04-12 10:03:36');
INSERT INTO `sub_module_permissions` VALUES ('393', '1', '7', '401', '1', '2017-04-12 10:03:36', '2017-04-12 10:03:36');
INSERT INTO `sub_module_permissions` VALUES ('394', '1', '7', '402', '1', '2017-04-12 10:03:36', '2017-04-12 10:03:36');
INSERT INTO `sub_module_permissions` VALUES ('395', '1', '7', '403', '1', '2017-04-12 10:03:36', '2017-04-12 10:03:36');
INSERT INTO `sub_module_permissions` VALUES ('396', '1', '7', '404', '1', '2017-04-12 10:03:36', '2017-04-12 10:03:36');
INSERT INTO `sub_module_permissions` VALUES ('397', '1', '2', '405', '1', '2017-05-10 03:47:46', '2017-05-10 03:47:46');
INSERT INTO `sub_module_permissions` VALUES ('398', '1', '2', '406', '1', '2017-05-10 03:47:46', '2017-05-10 03:47:46');
INSERT INTO `sub_module_permissions` VALUES ('399', '1', '2', '409', '1', '2017-05-15 03:14:06', '2017-05-15 03:14:06');
INSERT INTO `sub_module_permissions` VALUES ('445', '2', '1', '97', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('446', '2', '1', '98', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('447', '2', '1', '99', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('448', '2', '1', '100', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('449', '2', '1', '101', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('450', '2', '1', '102', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('451', '2', '1', '103', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('452', '2', '1', '104', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('453', '2', '1', '105', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('454', '2', '1', '138', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('455', '2', '1', '106', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('456', '2', '1', '107', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('457', '2', '1', '108', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('458', '2', '1', '109', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('459', '2', '1', '110', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('460', '2', '1', '111', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('461', '2', '1', '112', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('462', '2', '1', '113', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('463', '2', '1', '114', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('464', '2', '1', '115', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('465', '2', '1', '116', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('466', '2', '1', '117', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('467', '2', '1', '118', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('468', '2', '1', '119', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('469', '2', '1', '120', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('470', '2', '1', '121', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('471', '2', '1', '122', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('472', '2', '1', '123', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('473', '2', '1', '124', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('474', '2', '1', '125', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('475', '2', '1', '126', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('476', '2', '1', '127', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('477', '2', '1', '128', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('478', '2', '1', '129', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('479', '2', '1', '130', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('480', '2', '1', '131', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('481', '2', '1', '132', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('482', '2', '1', '133', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('483', '2', '1', '134', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('484', '2', '1', '135', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('485', '2', '1', '136', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('486', '2', '1', '137', '1', '2017-06-14 02:46:56', '2017-06-14 02:46:56');
INSERT INTO `sub_module_permissions` VALUES ('487', '1', '5', '411', '1', '2017-07-10 15:56:52', '2017-07-10 15:56:52');
INSERT INTO `sub_module_permissions` VALUES ('488', '1', '5', '412', '1', '2017-07-10 15:56:52', '2017-07-10 15:56:52');
INSERT INTO `sub_module_permissions` VALUES ('489', '1', '5', '413', '1', '2017-07-10 16:00:00', '2017-07-10 16:00:00');
INSERT INTO `sub_module_permissions` VALUES ('490', '1', '5', '414', '1', '2017-07-10 16:36:03', '2017-07-10 16:36:03');
INSERT INTO `sub_module_permissions` VALUES ('491', '1', '5', '415', '1', '2017-07-10 17:25:56', '2017-07-10 17:25:56');
INSERT INTO `sub_module_permissions` VALUES ('492', '1', '5', '416', '1', '2017-07-10 17:28:11', '2017-07-10 17:28:11');
INSERT INTO `sub_module_permissions` VALUES ('493', '1', '5', '417', '1', '2017-07-10 17:28:11', '2017-07-10 17:28:11');
INSERT INTO `sub_module_permissions` VALUES ('494', '1', '5', '418', '1', '2017-07-10 17:36:16', '2017-07-10 17:36:16');
INSERT INTO `sub_module_permissions` VALUES ('495', '1', '5', '419', '1', '2017-07-10 17:36:16', '2017-07-10 17:36:16');
INSERT INTO `sub_module_permissions` VALUES ('496', '1', '5', '420', '1', '2017-07-10 17:38:19', '2017-07-10 17:38:19');
INSERT INTO `sub_module_permissions` VALUES ('497', '1', '5', '421', '1', '2017-07-10 20:14:14', '2017-07-10 20:14:14');
INSERT INTO `sub_module_permissions` VALUES ('498', '1', '5', '422', '1', '2017-07-10 22:06:01', '2017-07-10 22:06:01');
INSERT INTO `sub_module_permissions` VALUES ('499', '1', '5', '423', '1', '2017-07-11 08:49:05', '2017-07-11 08:49:05');
INSERT INTO `sub_module_permissions` VALUES ('500', '1', '5', '424', '1', '2017-07-11 08:49:05', '2017-07-11 08:49:05');
INSERT INTO `sub_module_permissions` VALUES ('501', '1', '5', '425', '1', '2017-07-11 08:49:05', '2017-07-11 08:49:05');
INSERT INTO `sub_module_permissions` VALUES ('502', '1', '5', '426', '1', '2017-07-11 08:49:05', '2017-07-11 08:49:05');
INSERT INTO `sub_module_permissions` VALUES ('503', '1', '5', '427', '1', '2017-07-11 08:49:05', '2017-07-11 08:49:05');
INSERT INTO `sub_module_permissions` VALUES ('504', '1', '5', '428', '1', '2017-07-11 08:52:24', '2017-07-11 08:52:24');
INSERT INTO `sub_module_permissions` VALUES ('505', '1', '5', '429', '1', '2017-07-11 08:52:24', '2017-07-11 08:52:24');
INSERT INTO `sub_module_permissions` VALUES ('506', '1', '5', '430', '1', '2017-07-11 08:52:24', '2017-07-11 08:52:24');
INSERT INTO `sub_module_permissions` VALUES ('507', '1', '5', '431', '1', '2017-07-11 08:52:24', '2017-07-11 08:52:24');
INSERT INTO `sub_module_permissions` VALUES ('508', '1', '5', '432', '1', '2017-07-11 08:57:01', '2017-07-11 08:57:01');
INSERT INTO `sub_module_permissions` VALUES ('509', '1', '5', '433', '1', '2017-07-11 08:57:01', '2017-07-11 08:57:01');
INSERT INTO `sub_module_permissions` VALUES ('510', '1', '5', '434', '1', '2017-07-11 08:57:01', '2017-07-11 08:57:01');
INSERT INTO `sub_module_permissions` VALUES ('511', '1', '5', '435', '1', '2017-07-11 08:57:01', '2017-07-11 08:57:01');
INSERT INTO `sub_module_permissions` VALUES ('512', '1', '5', '436', '1', '2017-07-11 09:01:24', '2017-07-11 09:01:24');
INSERT INTO `sub_module_permissions` VALUES ('513', '1', '5', '437', '1', '2017-07-11 09:01:24', '2017-07-11 09:01:24');
INSERT INTO `sub_module_permissions` VALUES ('514', '1', '5', '438', '1', '2017-07-11 09:01:24', '2017-07-11 09:01:24');
INSERT INTO `sub_module_permissions` VALUES ('515', '1', '5', '439', '1', '2017-07-11 09:01:24', '2017-07-11 09:01:24');
INSERT INTO `sub_module_permissions` VALUES ('516', '1', '5', '440', '1', '2017-07-11 09:01:24', '2017-07-11 09:01:24');
INSERT INTO `sub_module_permissions` VALUES ('517', '1', '5', '441', '1', '2017-07-11 09:06:17', '2017-07-11 09:06:17');
INSERT INTO `sub_module_permissions` VALUES ('518', '1', '5', '442', '1', '2017-07-11 09:06:17', '2017-07-11 09:06:17');
INSERT INTO `sub_module_permissions` VALUES ('519', '1', '5', '443', '1', '2017-07-11 09:06:17', '2017-07-11 09:06:17');
INSERT INTO `sub_module_permissions` VALUES ('520', '1', '5', '444', '1', '2017-07-11 09:06:17', '2017-07-11 09:06:17');
INSERT INTO `sub_module_permissions` VALUES ('521', '1', '5', '445', '1', '2017-07-11 09:06:17', '2017-07-11 09:06:17');
INSERT INTO `sub_module_permissions` VALUES ('522', '1', '5', '446', '1', '2017-07-11 09:11:02', '2017-07-11 09:11:02');
INSERT INTO `sub_module_permissions` VALUES ('523', '1', '5', '447', '1', '2017-07-11 09:11:02', '2017-07-11 09:11:02');
INSERT INTO `sub_module_permissions` VALUES ('524', '1', '5', '448', '1', '2017-07-11 09:11:02', '2017-07-11 09:11:02');
INSERT INTO `sub_module_permissions` VALUES ('525', '1', '5', '449', '1', '2017-07-11 09:11:02', '2017-07-11 09:11:02');
INSERT INTO `sub_module_permissions` VALUES ('526', '1', '5', '451', '1', '2017-07-13 16:34:04', '2017-07-13 16:34:04');
INSERT INTO `sub_module_permissions` VALUES ('527', '1', '5', '450', '0', '2017-07-13 16:34:04', '2017-07-13 16:34:04');
INSERT INTO `sub_module_permissions` VALUES ('528', '1', '5', '452', '1', '2017-07-19 13:19:44', '2017-07-19 13:19:44');
INSERT INTO `sub_module_permissions` VALUES ('529', '1', '5', '453', '1', '2017-08-03 08:58:41', '2017-08-03 08:58:41');
INSERT INTO `sub_module_permissions` VALUES ('530', '1', '5', '454', '1', '2017-08-03 08:58:41', '2017-08-03 08:58:41');
INSERT INTO `sub_module_permissions` VALUES ('531', '1', '5', '455', '1', '2017-08-03 08:58:41', '2017-08-03 08:58:41');
INSERT INTO `sub_module_permissions` VALUES ('532', '1', '5', '456', '1', '2017-08-03 08:58:41', '2017-08-03 08:58:41');
INSERT INTO `sub_module_permissions` VALUES ('533', '1', '5', '457', '1', '2017-08-03 08:58:41', '2017-08-03 08:58:41');
INSERT INTO `sub_module_permissions` VALUES ('534', '1', '5', '458', '1', '2017-08-03 08:58:41', '2017-08-03 08:58:41');
INSERT INTO `sub_module_permissions` VALUES ('535', '1', '5', '459', '1', '2017-08-03 08:58:41', '2017-08-03 08:58:41');
INSERT INTO `sub_module_permissions` VALUES ('536', '1', '5', '460', '1', '2017-08-03 08:58:41', '2017-08-03 08:58:41');
INSERT INTO `sub_module_permissions` VALUES ('537', '1', '5', '461', '1', '2017-08-03 08:58:41', '2017-08-03 08:58:41');
INSERT INTO `sub_module_permissions` VALUES ('538', '1', '5', '462', '1', '2017-08-03 08:58:41', '2017-08-03 08:58:41');
INSERT INTO `sub_module_permissions` VALUES ('539', '1', '8', '464', '1', '2017-08-03 09:21:52', '2017-08-03 09:21:52');
INSERT INTO `sub_module_permissions` VALUES ('540', '1', '8', '465', '1', '2017-08-03 09:21:52', '2017-08-03 09:21:52');
INSERT INTO `sub_module_permissions` VALUES ('541', '1', '8', '466', '1', '2017-08-03 09:21:52', '2017-08-03 09:21:52');
INSERT INTO `sub_module_permissions` VALUES ('542', '1', '8', '467', '1', '2017-08-03 09:21:52', '2017-08-03 09:21:52');
INSERT INTO `sub_module_permissions` VALUES ('543', '1', '8', '468', '1', '2017-08-03 09:21:52', '2017-08-03 09:21:52');
INSERT INTO `sub_module_permissions` VALUES ('544', '1', '8', '469', '1', '2017-08-03 09:21:52', '2017-08-03 09:21:52');
INSERT INTO `sub_module_permissions` VALUES ('545', '1', '8', '470', '1', '2017-08-03 09:21:52', '2017-08-03 09:21:52');
INSERT INTO `sub_module_permissions` VALUES ('546', '1', '5', '471', '1', '2017-08-16 09:33:46', '2017-08-16 09:33:46');
INSERT INTO `sub_module_permissions` VALUES ('547', '1', '5', '472', '1', '2017-08-16 09:33:46', '2017-08-16 09:33:46');
INSERT INTO `sub_module_permissions` VALUES ('548', '1', '5', '473', '1', '2017-08-16 09:33:46', '2017-08-16 09:33:46');
INSERT INTO `sub_module_permissions` VALUES ('549', '1', '5', '474', '1', '2017-08-16 09:33:46', '2017-08-16 09:33:46');
INSERT INTO `sub_module_permissions` VALUES ('550', '1', '5', '475', '1', '2017-08-16 09:33:46', '2017-08-16 09:33:46');
INSERT INTO `sub_module_permissions` VALUES ('551', '1', '5', '476', '1', '2017-08-16 09:33:46', '2017-08-16 09:33:46');
INSERT INTO `sub_module_permissions` VALUES ('552', '1', '5', '477', '1', '2017-08-16 09:33:46', '2017-08-16 09:33:46');
INSERT INTO `sub_module_permissions` VALUES ('553', '1', '5', '478', '1', '2017-08-16 09:33:46', '2017-08-16 09:33:46');
INSERT INTO `sub_module_permissions` VALUES ('554', '1', '5', '479', '1', '2017-08-16 09:33:46', '2017-08-16 09:33:46');
INSERT INTO `sub_module_permissions` VALUES ('555', '1', '5', '480', '1', '2017-08-16 09:33:46', '2017-08-16 09:33:46');
INSERT INTO `sub_module_permissions` VALUES ('556', '1', '5', '481', '1', '2017-08-16 09:33:46', '2017-08-16 09:33:46');
INSERT INTO `sub_module_permissions` VALUES ('557', '1', '5', '482', '1', '2017-08-16 09:33:46', '2017-08-16 09:33:46');
INSERT INTO `sub_module_permissions` VALUES ('558', '1', '5', '483', '1', '2017-08-16 09:33:46', '2017-08-16 09:33:46');
INSERT INTO `sub_module_permissions` VALUES ('559', '1', '5', '484', '1', '2017-08-16 09:33:46', '2017-08-16 09:33:46');
INSERT INTO `sub_module_permissions` VALUES ('560', '1', '5', '485', '1', '2017-08-16 09:33:46', '2017-08-16 09:33:46');
INSERT INTO `sub_module_permissions` VALUES ('561', '1', '5', '486', '1', '2017-08-16 09:33:46', '2017-08-16 09:33:46');
INSERT INTO `sub_module_permissions` VALUES ('562', '1', '5', '487', '1', '2017-08-16 09:33:46', '2017-08-16 09:33:46');
INSERT INTO `sub_module_permissions` VALUES ('563', '1', '5', '488', '1', '2017-08-16 09:33:46', '2017-08-16 09:33:46');
INSERT INTO `sub_module_permissions` VALUES ('564', '1', '5', '489', '1', '2017-08-16 09:44:17', '2017-08-16 09:44:17');
INSERT INTO `sub_module_permissions` VALUES ('565', '1', '5', '490', '1', '2017-08-16 09:44:17', '2017-08-16 09:44:17');
INSERT INTO `sub_module_permissions` VALUES ('566', '1', '5', '491', '1', '2017-08-16 09:44:17', '2017-08-16 09:44:17');
INSERT INTO `sub_module_permissions` VALUES ('567', '1', '5', '492', '1', '2017-08-16 09:44:17', '2017-08-16 09:44:17');
INSERT INTO `sub_module_permissions` VALUES ('568', '1', '5', '493', '1', '2017-08-16 09:44:17', '2017-08-16 09:44:17');
INSERT INTO `sub_module_permissions` VALUES ('569', '1', '5', '494', '1', '2017-08-16 09:44:17', '2017-08-16 09:44:17');
INSERT INTO `sub_module_permissions` VALUES ('570', '1', '5', '495', '1', '2017-08-16 09:44:17', '2017-08-16 09:44:17');
INSERT INTO `sub_module_permissions` VALUES ('571', '1', '5', '496', '1', '2017-08-16 09:44:17', '2017-08-16 09:44:17');
INSERT INTO `sub_module_permissions` VALUES ('572', '1', '5', '497', '1', '2017-08-16 09:44:17', '2017-08-16 09:44:17');
INSERT INTO `sub_module_permissions` VALUES ('573', '1', '5', '498', '1', '2017-08-16 09:44:17', '2017-08-16 09:44:17');
INSERT INTO `sub_module_permissions` VALUES ('574', '1', '5', '499', '1', '2017-08-16 09:44:17', '2017-08-16 09:44:17');
INSERT INTO `sub_module_permissions` VALUES ('575', '1', '5', '500', '1', '2017-08-16 09:44:17', '2017-08-16 09:44:17');
INSERT INTO `sub_module_permissions` VALUES ('576', '1', '5', '501', '1', '2017-08-16 09:44:17', '2017-08-16 09:44:17');
INSERT INTO `sub_module_permissions` VALUES ('577', '1', '5', '502', '1', '2017-08-16 10:03:16', '2017-08-16 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('578', '1', '5', '503', '1', '2017-08-16 10:03:16', '2017-08-16 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('579', '1', '5', '504', '1', '2017-08-16 10:03:16', '2017-08-16 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('580', '1', '5', '505', '1', '2017-08-16 10:03:16', '2017-08-16 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('581', '1', '5', '506', '1', '2017-08-16 10:03:16', '2017-08-16 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('582', '1', '5', '507', '1', '2017-08-16 10:03:16', '2017-08-16 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('583', '1', '5', '508', '1', '2017-08-16 10:03:16', '2017-08-16 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('584', '1', '5', '509', '1', '2017-08-16 10:03:16', '2017-08-16 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('585', '1', '5', '510', '1', '2017-08-16 10:03:16', '2017-08-16 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('586', '1', '5', '511', '1', '2017-08-16 10:03:16', '2017-08-16 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('587', '1', '5', '512', '1', '2017-08-16 10:03:16', '2017-08-16 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('588', '1', '5', '513', '1', '2017-08-16 10:03:16', '2017-08-16 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('589', '1', '5', '514', '1', '2017-08-16 10:03:16', '2017-08-16 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('590', '1', '5', '515', '1', '2017-08-16 10:03:16', '2017-08-16 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('591', '1', '5', '516', '1', '2017-08-16 10:03:16', '2017-08-16 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('592', '1', '5', '517', '1', '2017-08-16 10:03:16', '2017-08-16 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('593', '1', '5', '518', '1', '2017-08-16 10:03:16', '2017-08-16 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('594', '1', '5', '519', '1', '2017-08-16 10:03:16', '2017-08-16 10:03:16');
INSERT INTO `sub_module_permissions` VALUES ('595', '1', '5', '520', '1', '2017-08-30 14:27:23', '2017-08-30 14:27:23');
INSERT INTO `sub_module_permissions` VALUES ('596', '1', '5', '521', '1', '2017-08-30 14:27:23', '2017-08-30 14:27:23');
INSERT INTO `sub_module_permissions` VALUES ('597', '1', '5', '522', '1', '2017-08-30 14:27:23', '2017-08-30 14:27:23');
INSERT INTO `sub_module_permissions` VALUES ('598', '1', '5', '523', '1', '2017-08-30 14:27:23', '2017-08-30 14:27:23');
INSERT INTO `sub_module_permissions` VALUES ('599', '1', '5', '524', '1', '2017-08-30 14:27:23', '2017-08-30 14:27:23');
INSERT INTO `sub_module_permissions` VALUES ('600', '1', '5', '525', '1', '2017-08-30 14:27:23', '2017-08-30 14:27:23');
INSERT INTO `sub_module_permissions` VALUES ('601', '1', '5', '526', '1', '2017-08-30 14:27:23', '2017-08-30 14:27:23');
INSERT INTO `sub_module_permissions` VALUES ('602', '1', '5', '527', '1', '2017-08-30 14:27:23', '2017-08-30 14:27:23');
INSERT INTO `sub_module_permissions` VALUES ('603', '1', '5', '528', '1', '2017-08-30 14:27:23', '2017-08-30 14:27:23');
INSERT INTO `sub_module_permissions` VALUES ('604', '1', '5', '529', '1', '2017-08-30 14:27:23', '2017-08-30 14:27:23');
INSERT INTO `sub_module_permissions` VALUES ('605', '1', '5', '530', '1', '2017-08-30 14:27:23', '2017-08-30 14:27:23');
INSERT INTO `sub_module_permissions` VALUES ('606', '1', '5', '531', '1', '2017-08-30 14:27:23', '2017-08-30 14:27:23');
INSERT INTO `sub_module_permissions` VALUES ('607', '1', '5', '532', '1', '2017-08-30 14:27:23', '2017-08-30 14:27:23');
INSERT INTO `sub_module_permissions` VALUES ('608', '1', '8', '533', '1', '2017-08-30 14:35:06', '2017-08-30 14:35:06');
INSERT INTO `sub_module_permissions` VALUES ('609', '1', '8', '534', '1', '2017-08-30 14:35:06', '2017-08-30 14:35:06');
INSERT INTO `sub_module_permissions` VALUES ('610', '1', '8', '535', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('611', '1', '8', '538', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('612', '1', '8', '539', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('613', '1', '8', '540', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('614', '1', '8', '541', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('615', '1', '8', '542', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('616', '1', '8', '543', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('617', '1', '8', '544', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('618', '1', '8', '545', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('619', '1', '8', '546', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('620', '1', '8', '547', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('621', '1', '8', '548', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('622', '1', '8', '549', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('623', '1', '8', '550', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('624', '1', '8', '551', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('625', '1', '8', '552', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('626', '1', '8', '553', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('627', '1', '8', '554', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('628', '1', '8', '555', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('629', '1', '8', '556', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('630', '1', '8', '557', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('631', '1', '8', '558', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('632', '1', '8', '559', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('633', '1', '8', '560', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('634', '1', '8', '561', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('635', '1', '8', '562', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('636', '1', '8', '563', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('637', '1', '8', '564', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('638', '1', '8', '565', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('639', '1', '8', '566', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('640', '1', '8', '567', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('641', '1', '8', '568', '1', '2017-09-05 10:10:33', '2017-09-05 10:10:33');
INSERT INTO `sub_module_permissions` VALUES ('642', '1', '8', '570', '1', '2017-09-05 10:31:07', '2017-09-05 10:31:07');

-- ----------------------------
-- Table structure for sub_modules
-- ----------------------------
DROP TABLE IF EXISTS `sub_modules`;
CREATE TABLE `sub_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `sub_module_id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `display_name` varchar(225) NOT NULL,
  `description` text NOT NULL,
  `icon` varchar(225) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=571 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sub_modules
-- ----------------------------
INSERT INTO `sub_modules` VALUES ('2', '9', '0', 'Master Data', 'Master Data', '', 'fa fa-book', '2017-02-24 09:31:41', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('3', '9', '2', 'roles', 'Group', '', 'fa fa-users', '2017-02-24 09:33:58', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('4', '9', '2', 'users', 'Users', '', 'fa fa-group', '2017-02-24 09:33:58', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('12', '2', '0', 'master', 'Master Data', '', 'fa fa-book', '2017-02-24 02:31:41', '2017-03-06 14:30:21');
INSERT INTO `sub_modules` VALUES ('13', '2', '12', 'pegawai', 'Pegawai', '', 'fa fa-database', '2017-02-24 02:31:41', '2017-03-06 14:30:21');
INSERT INTO `sub_modules` VALUES ('14', '2', '12', 'jabatan', 'Jabatan', '', 'fa fa-group', '2017-02-24 02:33:58', '2017-03-06 14:30:21');
INSERT INTO `sub_modules` VALUES ('15', '2', '12', 'bagian', 'Bagian', '', 'fa fa-dashboard', '2017-02-28 09:48:09', '2017-03-06 14:30:21');
INSERT INTO `sub_modules` VALUES ('16', '2', '12', 'agama', 'Agama', '', 'fa fa-dashboard', '2017-03-02 08:18:04', '2017-03-06 14:30:21');
INSERT INTO `sub_modules` VALUES ('17', '2', '12', 'pendidikan', 'Pendidikan', '', 'fa fa-dashboard', '2017-03-02 08:35:53', '2017-03-06 14:30:21');
INSERT INTO `sub_modules` VALUES ('18', '2', '12', 'perhitungan', 'Perhitungan', '', 'fa fa-dashboard', '2017-03-02 08:35:57', '2017-03-06 14:30:21');
INSERT INTO `sub_modules` VALUES ('19', '2', '0', 'Payrol Gaji', 'Payrol Gaji', '', 'fa fa-money', '2017-03-09 13:20:39', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('20', '2', '19', 'potongan jamsostek', 'Potongan Jamsostek', '', '', '2017-03-09 13:22:33', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('21', '2', '19', 'potongan jaminan kesehatan', 'Potongan Jaminan Kesehatan', '', '', '2017-03-09 13:23:02', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('22', '2', '19', 'potongan jaminan pensiun', 'Potongan Jaminan Pensiun', '', '', '2017-03-09 13:23:27', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('23', '2', '19', 'potongan pph', 'Potongan PPH', '', '', '2017-03-09 13:23:52', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('24', '2', '19', 'potongan kredit karyawan', 'Potongan Kredit Karyawan', '', '', '2017-03-09 13:24:33', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('25', '2', '19', 'potongan kredit kendaraan', 'Potongan Kredit Kendaraan', '', '', '2017-03-09 13:24:57', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('26', '2', '19', 'rekap angsuran karyawan', 'Rekap Angsuran Karyawan', '', '', '2017-03-09 13:25:12', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('27', '2', '19', 'perhitungan potongan absen', 'Perhitungan Potongan Absen', '', '', '2017-03-09 13:25:53', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('28', '2', '0', 'Cuti', 'Cuti', '', 'fa fa-calendar-check-o', '2017-03-09 13:26:07', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('29', '2', '28', 'cuti tahunan', 'Cuti Tahunan', '', '', '2017-03-09 13:26:43', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('30', '2', '28', 'cuti sakit', 'Cuti Sakit', '', '', '2017-03-09 13:28:21', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('31', '2', '28', 'cuti melahirkan', 'Cuti Melahirkan', '', '', '2017-03-09 13:28:25', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('32', '2', '28', 'cuti alasan penting', 'Cuti Alasan Penting', '', '', '2017-03-09 13:34:53', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('33', '2', '28', 'cuti diluar tanggungan', 'Cuti Diluar Tanggungan', '', '', '2017-03-09 13:35:07', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('34', '2', '28', 'cuti besar', 'Cuti Besar', '', '', '2017-03-09 13:35:16', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('35', '2', '28', 'revisi cuti', 'Revisi Cuti', '', '', '2017-03-09 13:35:26', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('36', '2', '28', 'tanjungan cuti', 'Tanjungan Cuti', '', '', '2017-03-09 13:35:42', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('37', '2', '28', 'riwayat cuti', 'Riwayat Cuti', '', '', '2017-03-09 13:35:51', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('38', '2', '28', 'rekap cuti', 'Rekap Cuti', '', '', '2017-03-09 13:36:05', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('39', '2', '0', 'Lembur Pegawai', 'Lembur Pegawai', '', 'fa fa-clock-o', '2017-03-09 13:36:22', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('40', '2', '39', 'form1 a', 'Form 1A Pengajuan Lembur', '', '', '2017-03-09 13:36:47', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('41', '2', '39', 'form1 b', 'Form 1B Persetujuan Lembur', '', '', '2017-03-09 13:36:49', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('42', '2', '39', 'form1 c', 'Form 1C Lap. PJ lembur', '', '', '2017-03-09 13:37:34', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('43', '2', '39', 'perhitungan lembur', 'Perhitungan Lembur Pegawai', '', '', '2017-03-09 13:37:54', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('44', '2', '39', 'tarif hari kerja', 'Perhitungan Tarif Lembur Hari Kerja', '', '', '2017-03-09 13:39:23', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('45', '2', '39', 'tarif hari libur', 'Perhitungan Tarif Lembur Hari Libur', '', '', '2017-03-09 13:39:34', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('46', '2', '39', 'rekap tarif hari kerja', 'Rekap Lembur Hari Kerja', '', '', '2017-03-09 13:39:36', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('47', '2', '39', 'rekap tarif lembur hari libur', 'Rekap Lembur Hari Libur', '', '', '2017-03-09 13:44:57', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('48', '2', '39', '50besar', '50 Besar Biaya Lembur per Pegawai', '', '', '2017-03-09 13:45:32', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('49', '2', '0', 'Pegawai', 'Pegawai', '', 'fa fa-user-o', '2017-03-09 13:47:00', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('50', '2', '49', 'rekap data pegawai', 'Rekap Data Pegawai', '', '', '2017-03-09 13:47:20', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('51', '2', '49', 'grafik rekap pegawai', 'Grafik Rekap Pegawai', '', '', '2017-03-09 13:47:36', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('52', '2', '49', 'data tabulasi rekap pegawai', 'Data Tabulasi Rekap Pegawai', '', '', '2017-03-09 13:48:00', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('53', '2', '0', 'Table Tarif', 'Table Tarif', '', 'fa fa-table', '2017-03-09 13:48:12', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('54', '2', '53', 'dinas dalam kota', 'Dinas Dalam Kota', '', '', '2017-03-09 13:49:47', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('55', '2', '53', 'dinas luar kota', 'Dinas Luar Kota', '', '', '2017-03-09 13:50:01', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('56', '2', '53', 'master perjalanan dinas', 'Master Perjalanan Dinas', '', '', '2017-03-09 13:50:30', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('57', '2', '53', 'rekap perjalanan dinas', 'Rekap Perjalanan Dinas', '', '', '2017-03-09 13:50:55', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('58', '2', '53', 'perjanjian kerja', 'Perjanjian Kerja / Kontrak Kerja', '', '', '2017-03-09 13:51:29', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('59', '2', '53', 'form perjanjian kerja', 'Form Perjanjian Kerja', '', '', '2017-03-09 13:51:42', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('60', '2', '53', 'digital dokumen', 'Digital Dokumen', '', '', '2017-03-09 13:51:56', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('61', '2', '53', 'notifikasi perjanjian', 'Notifikasi Perjanjian Kontrak Kerja', '', '', '2017-03-09 13:52:21', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('62', '2', '53', 'view sk', 'View SK', '', '', '2017-03-09 13:52:30', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('63', '2', '0', 'Perjanjian Kerja Sama', 'Perjanjian Kerja Sama Dan Perijinan', '', 'fa fa-handshake-o', '2017-03-09 13:53:12', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('64', '2', '63', 'form perijinan', 'Form Perijinan', '', '', '2017-03-09 13:53:41', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('65', '2', '63', 'form kerjasama', 'Form Kerjasama', '', '', '2017-03-09 13:53:50', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('66', '2', '63', 'notifikasi perijinan', 'Notifikasi Perijinan', '', '', '2017-03-09 13:54:05', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('67', '2', '63', 'notifikasi kerjasama', 'Notifikasi Kerjasama', '', '', '2017-03-09 13:54:39', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('68', '2', '63', 'rekap perijinan', 'Rekap Perijinan', '', '', '2017-03-09 13:54:49', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('69', '2', '63', 'rekap kerjasama', 'Rekap Kerjasama', '', '', '2017-03-09 13:55:01', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('70', '2', '0', 'Perhitungan Bonus', 'Perhitungan Bonus', '', 'fa fa-gift', '2017-03-09 13:55:15', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('71', '2', '70', 'perhitungan bonus', 'Perhitungan Bonus', '', '', '2017-03-09 14:00:29', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('72', '2', '70', 'perhitungan thr', 'Perhitungan THR', '', '', '2017-03-09 14:01:01', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('73', '2', '70', 'rekap perhitungan', 'Rekap Perhitungan', '', '', '2017-03-09 14:01:20', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('74', '2', '70', 'penilaian kinerja bagian', 'Penilaian Kinerja Bagian', '', '', '2017-03-09 14:01:51', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('75', '2', '70', 'penilaian kinerja karyawan', 'Penilaian Kinerja Karyawan', '', '', '2017-03-09 14:02:17', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('76', '2', '70', 'form perhitungan tunjangan', 'Form Perhitungan Tunjangan Pakaian Kerja', '', '', '2017-03-09 14:04:06', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('77', '2', '70', 'rekap perhitungan tunjangan', 'Rekap Perhitungan Tunjangan Pakaian Kerja', '', '', '2017-03-09 14:05:07', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('78', '2', '0', 'Pembayaran', 'Pembayaran', '', 'fa fa-credit-card', '2017-03-09 14:11:36', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('79', '2', '78', 'pengajuan gaji', 'Pengajuan Gaji', '', '', '2017-03-09 14:15:29', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('80', '2', '78', 'pengajuan lembur', 'Pengajuan Lembur', '', '', '2017-03-09 14:16:10', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('81', '2', '78', 'pengajuan sppd', 'Pengajuan SPPD', '', '', '2017-03-09 14:16:46', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('82', '2', '78', 'pengajuan thr', 'Pengajuan THR', '', '', '2017-03-09 14:17:04', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('83', '2', '78', 'pengajuan bonus', 'Pengajuan Bonus', '', '', '2017-03-09 14:17:15', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('84', '2', '78', 'pengajuan tunjangan pakaian', 'Pengajuan Tunjangan Pakaian', '', '', '2017-03-09 14:17:31', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('85', '2', '78', 'pengajuan tunjangan cuti', 'Pengajuan Tunjangan Cuti', '', '', '2017-03-09 14:17:44', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('86', '2', '0', 'Rapel Penghasilan', 'Rapel Penghasilan', '', 'fa fa-retweet', '2017-03-09 14:17:56', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('87', '2', '0', 'Form UMP', 'Form UMP', '', 'fa fa-question-circle-o', '2017-03-09 14:18:22', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('88', '2', '87', 'pengajuan ump', 'Pengajuan UMP', '', '', '2017-03-09 14:18:42', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('89', '2', '87', 'notifikasi ump', 'Notifikasi UMP', '', '', '2017-03-09 14:18:45', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('90', '2', '87', 'perpanjangan ump', 'Perpanjangan UMp', '', '', '2017-03-09 14:19:14', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('91', '2', '87', 'penyelesaian ump', 'Penyelesaian UMP', '', '', '2017-03-09 14:19:24', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('92', '2', '0', 'Budgeting', 'Budgeting', '', 'fa fa-asterisk', '2017-03-09 14:19:39', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('93', '2', '92', 'pengajuan anggaran', 'Pengajuan Anggaran', '', '', '2017-03-09 14:19:51', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('94', '2', '92', 'pergeseran anggaran', 'Pergeseran Anggaran', '', '', '2017-03-09 14:19:52', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('95', '2', '92', 'revisi anggaran', 'Revisi Anggaran', '', '', '2017-03-09 14:20:18', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('96', '2', '92', 'realisasi', 'Realisasi', '', '', '2017-03-09 14:20:28', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('97', '1', '0', 'Master Data', 'Master Data', '', 'fa fa-book', '2017-03-09 15:40:46', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('98', '1', '97', 'vendor', 'Vendor', '', '', '2017-03-09 15:41:37', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('99', '1', '97', 'barang', 'Barang', '', '', '2017-03-09 15:43:26', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('100', '1', '97', 'kategori barang', 'Kategori Barang', '', '', '2017-03-09 15:43:45', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('101', '1', '97', 'jasa', 'Jasa', '', '', '2017-03-10 12:42:37', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('102', '1', '0', 'budgeting', 'Budgeting', '', 'fa fa-asterisk', '2017-03-10 13:18:16', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('103', '1', '102', 'pengajuan anggaran', 'Pengajuan Anggaran', '', '', '2017-03-10 13:20:07', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('104', '1', '102', 'pergesaran anggaran', 'Pergeseran Anggaran', '', '', '2017-03-10 13:20:52', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('105', '1', '102', 'realisasi', 'Realisasi', '', '', '2017-03-10 14:13:55', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('106', '1', '0', 'persediaan', 'Persedian', '', 'fa fa-database', '2017-03-10 14:16:32', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('107', '1', '106', 'buffer stock', 'Buffer Stock', '', '', '2017-03-10 14:37:48', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('108', '1', '106', 'permintaan user', 'Permintaan User	\r\n', '', '', '2017-03-10 14:38:11', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('109', '1', '106', 'distribusi', 'Distribusi', '', '', '2017-03-10 14:38:31', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('110', '1', '106', 'internal receive', 'Internal Receive', '', '', '2017-03-10 14:38:47', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('111', '1', '106', 'spoa', 'SPOA', '', '', '2017-03-10 14:39:05', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('112', '1', '106', 'sopa', 'SOPA', '', '', '2017-03-10 14:39:21', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('113', '1', '106', 'pembatalan purchase order', 'Pembatalan Purchase Order', '', '', '2017-03-10 15:22:27', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('114', '1', '106', 'penerimaan barang', 'Penerimaan Barang', '', '', '2017-03-13 10:49:42', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('115', '1', '106', 'monitoring stok', 'Monitoring Stok', '', '', '2017-03-13 10:50:18', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('116', '1', '106', 'kartu stok', 'Kartu Stok', '', '', '2017-03-13 10:50:33', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('117', '1', '106', 'stock opname', 'Stock Opname', '', '', '2017-03-13 10:50:56', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('118', '1', '106', 'adjustment stok', 'Adjustment Stok', '', '', '2017-03-13 10:51:23', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('119', '1', '0', 'Pemeliharaan', 'Pemeliharaan', '', 'fa fa-refresh', '2017-03-13 10:52:11', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('120', '1', '119', 'barang akhir', 'Barang Akhir', '', '', '2017-03-13 10:52:41', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('121', '1', '0', 'kendaraan operasional', 'Kendaraan Operasional', '', 'fa fa-car', '2017-03-13 10:53:12', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('122', '1', '121', 'permintaan user', 'Permintaan User', '', '', '2017-03-13 10:53:49', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('123', '1', '121', 'approval', 'Approval', '', '', '2017-03-13 10:54:04', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('124', '1', '121', 'jadwal', 'Jadwal', '', '', '2017-03-13 10:54:16', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('125', '1', '121', 'penerimaan mutasi', ' Penerimaan Mutasi Kendaraan', '', '', '2017-03-13 10:54:32', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('126', '1', '0', 'form ump', 'Form UMP', '', 'fa fa-question-circle-o', '2017-03-13 10:55:08', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('127', '1', '126', 'pengajuan ump', 'Pengajuan UMP', '', '', '2017-03-13 10:55:51', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('128', '1', '126', 'notifikasi ump', 'Notifikasi UMP', '', '', '2017-03-13 10:56:26', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('129', '1', '126', 'perpanjangan ump', 'Perpanjangan UMP', '', '', '2017-03-13 10:59:47', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('130', '1', '126', 'penyelesaian ump', 'Penyelesaian UMP', '', '', '2017-03-13 11:01:58', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('131', '1', '0', 'Laporan', 'Laporan', '', 'fa fa-window-restore', '2017-03-13 11:02:10', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('132', '1', '131', 'laporan aktiva inventaris', 'Laporan Inventaris Aktiva', '', '', '2017-03-13 11:03:04', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('133', '1', '131', 'laporan register inventaris', 'Laporan Register Inventaris', '', '', '2017-03-13 11:03:55', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('134', '1', '131', 'laporan barang akhir', 'Laporan Barang Akhir', '', '', '2017-03-13 11:04:18', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('135', '1', '131', 'laporan stok barang inventaris', 'Laporan Stok Barang Inventaris', '', '', '2017-03-13 11:04:51', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('136', '1', '131', 'laporan kendaraan', 'Laporan Kendaraan', '', '', '2017-03-13 11:05:10', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('137', '1', '131', 'laporan stock opname', 'Laporan Stock Opname', '', '', '2017-03-13 14:38:56', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('138', '1', '102', 'revisi anggaran', 'Revisi', '', '', '2017-03-13 14:39:20', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('201', '3', '0', 'master', 'Master Data', '', 'fa fa-book', '2017-04-11 09:03:56', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('202', '3', '201', 'customer', 'Customer', '', '', '2017-04-11 09:07:03', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('203', '3', '201', 'supplier', 'Supplier', '', '', '2017-04-11 09:07:15', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('204', '3', '201', 'barang', 'Barang', '', '', '2017-04-11 09:07:21', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('205', '3', '201', 'tarif', 'Tarif/Harga', '', '', '2017-04-11 09:07:48', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('206', '3', '201', 'anggaran', 'Anggaran', '', '', '2017-04-11 09:07:59', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('207', '3', '201', 'maintenance', 'Maintenance', '', '', '2017-04-11 09:08:28', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('208', '3', '0', 'Budgeting', 'Budgeting', '', 'fa fa-asterisk', '2017-04-11 09:10:19', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('209', '3', '208', 'pengajuan anggaran', 'Pengajuan Anggaran', '', '', '2017-04-11 09:10:20', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('210', '3', '208', 'pergeseran anggaran', 'Pergeseran Anggaran', '', '', '2017-04-11 09:10:22', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('211', '3', '208', 'revisi anggaran', 'Revisi Anggaran', '', '', '2017-04-11 09:10:23', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('212', '3', '208', 'realisasi', 'Realisasi', '', '', '2017-04-11 09:10:24', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('213', '3', '0', 'form ump', 'Form UMP', '', 'fa fa-question-circle-o', '2017-04-11 09:12:02', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('214', '3', '213', 'pengajuan ump', 'Pengajuan UMP', '', '', '2017-04-11 09:12:03', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('215', '3', '213', 'notifikasi ump', 'Notifikasi UMP', '', '', '2017-04-11 09:12:04', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('216', '3', '213', 'perpanjangan ump', 'Perpanjangan UMP', '', '', '2017-04-11 09:12:04', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('217', '3', '213', 'penyelesaian', 'Penyelesaian UMP', '', '', '2017-04-11 09:12:05', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('218', '3', '0', 'persediaan', 'Persediaan', '', 'fa fa-database', '2017-04-11 09:12:06', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('219', '3', '218', 'buffer', 'Buffer Stok', '', '', '2017-04-11 09:13:41', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('220', '3', '218', 'penerimaan', 'Penerimaan', '', '', '2017-04-11 09:13:57', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('221', '3', '218', 'pemakaian', 'Pemakaian', '', '', '2017-04-11 09:14:13', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('222', '3', '0', 'sales', 'Sales Order', '', 'fa fa-credit-card', '2017-04-11 09:14:35', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('223', '3', '222', 'spk', 'SPK', '', '', '2017-04-11 09:14:45', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('224', '3', '222', 'bast', 'BAST', '', '', '2017-04-11 09:15:02', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('225', '3', '222', 'po', 'PO/SOPA/SPOA', '', '', '2017-04-11 09:15:24', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('226', '3', '0', 'pemeliharaan', 'Pemeliharaan', '', 'fa fa-cubes', '2017-04-11 09:16:00', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('227', '3', '226', 'po', 'PO/SOPA/SPOA', '', '', '2017-04-11 09:16:13', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('228', '3', '226', 'penerimaan', 'Penerimaan', '', '', '2017-04-11 09:16:15', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('229', '3', '226', 'maintenance', 'Maintenance', '', '', '2017-04-11 09:16:18', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('230', '3', '0', 'penyimpanan', 'Penyimpanan Dokumen', '', 'fa fa-briefcase', '2017-04-11 09:16:19', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('231', '3', '230', 'simpan', 'Form Simpan Dokumen', '', '', '2017-04-11 09:20:55', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('232', '3', '230', 'upload', 'Upload Dokumen', '', '', '2017-04-11 09:20:55', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('233', '3', '230', 'cari', 'Cari Dokumen', '', '', '2017-04-11 09:20:55', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('234', '3', '230', 'download', 'Download Dokumen', '', '', '2017-04-11 09:20:56', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('235', '3', '0', 'laporan', 'Laporan', '', 'fa fa-window-restore', '2017-04-11 09:20:56', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('236', '3', '235', 'omset', 'Omset', '', '', '2017-04-11 09:20:57', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('237', '3', '235', 'omset customer', 'Omset Customer', '', '', '2017-04-11 09:20:57', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('238', '3', '235', 'jenis usaha', 'Jenis Usaha', '', '', '2017-04-11 09:20:57', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('239', '3', '235', 'realisasi', 'Realisasi Anggaran', '', '', '2017-04-11 09:20:59', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('240', '3', '235', 'persediaan', 'Persediaan', '', '', '2017-04-11 09:20:59', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('241', '3', '235', 'maintenance', 'Maintenance', '', '', '2017-04-11 09:20:59', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('243', '4', '0', 'master', 'Master Data', '', 'fa fa-book', '2017-04-11 09:47:05', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('244', '4', '243', 'perusahaan', 'Perusahaan', '', '', '2017-04-11 10:08:25', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('245', '4', '243', 'suplier', 'Suplier', '', '', '2017-04-11 10:08:28', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('246', '4', '243', 'tarif', 'Tarif Umum', '', '', '2017-04-11 10:08:28', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('247', '4', '243', 'tarif khusus', 'Tarif Khusus', '', '', '2017-04-11 10:08:28', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('248', '4', '243', 'tarif insidential', 'Tarif Insidential', '', '', '2017-04-11 10:08:28', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('249', '4', '243', 'kendaraan khusus ekspedisi', 'Kendaraan Khusus EKspedisi', '', '', '2017-04-11 10:08:29', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('250', '4', '243', 'alat kerja', 'Alat Kerja', '', '', '2017-04-11 10:08:29', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('251', '4', '243', 'barang', 'Barang', '', '', '2017-04-11 10:08:29', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('252', '4', '243', 'maintenance', 'Maintenance', '', '', '2017-04-11 10:08:30', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('253', '4', '243', 'provinsi', 'Provinsi', '', '', '2017-04-11 10:08:30', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('254', '4', '243', 'kabupaten', 'Kabupaten/Kota', '', '', '2017-04-11 10:08:30', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('255', '4', '243', 'kecamatan', 'Kecamatan', '', '', '2017-04-11 10:08:30', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('256', '4', '243', 'kelurahan', 'Kelurahan', '', '', '2017-04-11 10:08:31', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('257', '4', '0', 'Budgeting', 'Budgeting', '', 'fa fa-asterisk', '2017-04-11 10:08:31', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('258', '4', '257', 'pengajuan anggaran', 'Pengajuan Anggaran', '', '', '2017-04-11 10:08:32', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('259', '4', '257', 'pergeseran anggaran', 'Pergeseran Anggaran', '', '', '2017-04-11 10:08:32', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('260', '4', '257', 'revisi anggaran', 'Revisi Anggaran', '', '', '2017-04-11 10:08:32', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('261', '4', '257', 'realisasi', 'Realisasi', '', '', '2017-04-11 10:08:33', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('262', '4', '0', 'form ump', 'Form UMP', '', 'fa fa-question-circle-o', '2017-04-11 10:08:33', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('263', '4', '262', 'pengajuan ump', 'Pengajuan UMP', '', '', '2017-04-11 10:08:33', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('264', '4', '262', 'notifikasi ump', 'Notifikasi UMP', '', '', '2017-04-11 10:08:34', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('265', '4', '262', 'perpanjangan ump', 'Perpanjangan UMP', '', '', '2017-04-11 10:08:34', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('266', '4', '262', 'penyelesaian', 'Penyelesaian UMP', '', '', '2017-04-11 10:22:42', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('267', '4', '0', 'sales', 'Sales Order', '', 'fa fa-credit-card', '2017-04-11 10:34:17', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('268', '4', '267', 'tunai', 'Billing Tunai', '', '', '2017-04-11 10:34:18', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('269', '4', '267', 'piutang', 'Billing Piutang', '', '', '2017-04-11 10:34:18', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('270', '4', '267', 'resi', 'Resi', '', '', '2017-04-11 10:34:18', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('271', '4', '267', 'bap', 'Berita Acara Pengiriman', '', '', '2017-04-11 10:34:18', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('272', '4', '0', 'update pengiriman', 'Update Pengiriman', '', 'fa fa-truck', '2017-04-11 10:34:19', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('273', '4', '272', 'update', 'Update Pengiriman', '', '', '2017-04-11 10:34:19', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('274', '4', '272', 'monitoring', 'Monitoring', '', '', '2017-04-11 10:34:19', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('275', '4', '0', 'pemeliharaan', 'Pemeliharaan', '', 'fa fa-cubes', '2017-04-11 10:34:19', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('276', '4', '275', 'po', 'PO/SOPA/SPOA', '', '', '2017-04-11 10:34:20', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('277', '4', '275', 'penerimaan', 'Penerimaan Barang', '', '', '2017-04-11 10:34:20', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('278', '4', '275', 'maintenance', 'Maintenance Order', '', '', '2017-04-11 10:34:20', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('279', '4', '0', 'laporan', 'Laporan', '', 'fa fa-window-restore', '2017-04-11 10:34:20', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('280', '4', '279', 'omset', 'Omset', '', '', '2017-04-11 10:34:21', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('281', '4', '279', 'omset customer', 'Omset Customer', '', '', '2017-04-11 10:34:21', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('282', '4', '279', 'jenis usaha', 'Jenis Usaha', '', '', '2017-04-11 10:34:22', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('283', '4', '279', 'frekuensi', 'Frekuensi', '', '', '2017-04-11 10:38:45', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('284', '4', '279', 'tonnase', 'Tonnase', '', '', '2017-04-11 10:38:52', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('285', '4', '279', 'tujuan', 'Tujuan Pengiriman', '', '', '2017-04-11 10:39:03', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('286', '4', '279', 'realisasi', 'Realisasi Anggaran', '', '', '2017-04-11 10:39:18', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('287', '4', '279', 'maintennance', 'Maintenance', '', '', '2017-04-11 10:39:34', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('288', '4', '279', 'pemakaian', 'Pemakaian Kendaraan', '', '', '2017-04-11 10:39:51', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('289', '4', '279', 'piutang divisi', 'Piutang Divisi', '', '', '2017-04-11 10:40:09', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('290', '6', '0', 'master', 'Master Data', '', 'fa fa-book', '2017-04-12 10:26:07', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('291', '6', '290', 'fungsional', 'Fungsional', '', '', '2017-04-12 10:26:08', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('292', '6', '290', 'perusahaan', 'Perusahaan', '', '', '2017-04-12 10:26:08', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('293', '6', '290', 'fee', 'Fee', '', '', '2017-04-12 10:26:08', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('294', '6', '290', 'ump', 'UMP (Upah Minimum Propinsi)', '', '', '2017-04-12 10:26:08', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('295', '6', '290', 'pegawai', 'Pegawai', '', '', '2017-04-12 10:26:09', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('296', '6', '290', 'bpjs', 'BPJS', '', '', '2017-04-12 10:26:09', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('297', '6', '290', 'pph21', 'PPH21', '', '', '2017-04-12 10:26:09', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('298', '6', '290', 'anggaran', 'Anggaran Bagian Keuangan', '', '', '2017-04-12 10:26:09', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('299', '6', '0', 'Budgeting', 'Budgeting', '', 'fa fa-asterisk', '2017-04-12 10:26:10', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('300', '6', '299', 'pengajuan anggaran', 'Pengajuan Anggaran', '', '', '2017-04-12 10:26:10', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('301', '6', '299', 'pergeseran anggaran', 'Pergeseran Anggaran', '', '', '2017-04-12 10:26:10', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('302', '6', '299', 'revisi anggaran', 'Revisi Anggaran', '', '', '2017-04-12 10:26:11', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('303', '6', '299', 'realisasi', 'Realisasi', '', '', '2017-04-12 10:26:11', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('304', '6', '0', 'form ump', 'Form UMP', '', 'fa fa-question-circle-o', '2017-04-12 10:26:11', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('305', '6', '304', 'pengajuan ump', 'Pengajuan UMP', '', '', '2017-04-12 10:26:11', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('306', '6', '304', 'notifikasi ump', 'Notifikasi UMP', '', '', '2017-04-12 10:26:11', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('307', '6', '304', 'perpanjangan ump', 'Perpanjangan UMP', '', '', '2017-04-12 10:26:12', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('308', '6', '304', 'penyelesaian', 'Penyelesaian UMP', '', '', '2017-04-12 10:26:12', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('309', '6', '0', 'payroll', 'Payroll', '', 'fa fa-money', '2017-04-12 10:26:12', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('310', '6', '309', 'potongan bpjs', 'Potongan BPJS Ketenagakerjaan', '', '', '2017-04-12 10:26:12', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('311', '6', '309', 'bpjs', 'BPJS Kesehatan', '', '', '2017-04-12 10:26:13', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('312', '6', '309', 'jaminan pensiun', 'Jaminan Pensiun', '', '', '2017-04-12 10:26:13', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('313', '6', '309', 'potongan pph21', 'Potongan PPH21', '', '', '2017-04-12 10:26:13', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('314', '6', '309', 'kredit', 'Kredit Koperasi', '', '', '2017-04-12 10:26:14', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('315', '6', '309', 'potongan kmg', 'Potongan KMG', '', '', '2017-04-12 10:26:14', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('316', '6', '309', 'potongan lain', 'Potongan Lain', '', '', '2017-04-12 10:26:14', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('317', '6', '309', 'potongan absen', 'Potongan Absen', '', '', '2017-04-12 10:59:57', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('318', '6', '309', 'uang makan', 'Uang Makan', '', '', '2017-04-12 11:00:10', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('319', '6', '309', 'rapel gaji', 'Rapel Gaji', '', '', '2017-04-12 11:00:19', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('320', '6', '309', 'rapel lembur', 'Rapel Lembur', '', '', '2017-04-12 11:00:37', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('321', '6', '309', 'rapel lain', 'Rapel Lain', '', '', '2017-04-12 11:00:45', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('322', '6', '0', 'lembur', 'Lembur Pegawai', '', 'fa fa-columns', '2017-04-12 11:01:03', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('323', '6', '322', 'tarif lembur', 'Perhitungan Tarif Lembur', '', '', '2017-04-12 11:01:48', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('324', '6', '322', 'lembur rutin', 'Lembur Rutin', '', '', '2017-04-12 11:01:59', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('325', '6', '322', 'lembur biasa', 'Lembur Biasa', '', '', '2017-04-12 11:02:08', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('326', '6', '322', 'lembur nasional', 'Lembur Nasional', '', '', '2017-04-12 11:03:08', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('327', '6', '322', 'backup lembur', 'Backup Lembur', '', '', '2017-04-12 11:03:31', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('328', '6', '322', 'skpd', 'SKPD (Surat Keterangan Perjalanan Dinas)', '', '', '2017-04-12 11:03:52', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('329', '6', '322', 'presensi', 'Presensi, Keterlambatan dan Tidak Masuk Kerja', '', '', '2017-04-12 11:04:14', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('330', '6', '322', 'rekap absen', 'Rekap Absen', '', '', '2017-04-12 11:04:30', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('331', '6', '322', 'cuti', 'Cuti', '', '', '2017-04-12 11:04:35', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('332', '6', '0', 'pengelompokan fungsional', 'Pengelompokan Fungsional', '', 'fa fa-chain', '2017-04-12 11:05:36', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('333', '6', '0', 'invoicing', 'Invoicing', '', 'fa fa-file', '2017-04-12 11:05:47', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('334', '6', '333', 'invoice', 'Pembuatan Invoice', '', '', '2017-04-12 11:10:28', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('335', '6', '333', 'kwitansi', 'Pembuatan Kwitansi', '', '', '2017-04-12 11:28:26', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('336', '6', '333', 'pengantar tagihan', 'Pembuatan Pengantar Tagihan', '', '', '2017-04-12 11:28:42', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('337', '6', '333', 'revisi tagihan', 'Revisi Tagihan', '', '', '2017-04-12 11:28:57', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('338', '6', '333', 'pembatalan tagihan', 'Pembatalan Tagihan', '', '', '2017-04-12 11:29:11', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('339', '6', '333', 'pph23', 'PPH23', '', '', '2017-04-12 11:29:25', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('340', '6', '333', 'ppn', 'PPN', '', '', '2017-04-12 11:29:31', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('341', '6', '333', 'pengajuan pembayaran gaji', 'Pengajuan Pembayaran Gaji', '', '', '2017-04-12 11:37:34', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('342', '6', '333', 'pengajuan pembayaran lembur', 'Pengajuan Pembayaran Lembur, Uang Makan dan SKPD', '', '', '2017-04-12 11:38:49', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('343', '6', '333', 'pengajuan pembayaran thr', 'Pengajuan Pembayaran THR', '', '', '2017-04-12 11:39:15', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('344', '6', '333', 'pengajuan pembayaran bonus', 'Pengajuan Pembayaran Bonus', '', '', '2017-04-12 11:39:54', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('345', '6', '0', 'kontrak', 'Kontrak', '', 'fa fa-list', '2017-04-12 11:40:35', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('346', '6', '345', 'form kontrak', 'Form Kontrak', '', '', '2017-04-12 11:40:54', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('347', '6', '345', 'pkwt', 'Master PKWT (Perjanjian Kerja Waktu Tertentu)', '', '', '2017-04-12 11:41:20', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('348', '6', '345', 'backup pegawai', 'Backup Pegawai', '', '', '2017-04-12 11:41:37', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('349', '6', '345', 'pengalaman kerja', 'Pengalaman Kerja', '', '', '2017-04-12 11:41:58', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('350', '6', '0', 'laporan', 'Laporan', '', 'fa fa-window-restore', '2017-04-12 11:42:04', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('351', '6', '350', 'pegawai', 'Pegawai', '', '', '2017-04-12 11:42:45', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('352', '6', '350', 'potongan', 'Potongan', '', '', '2017-04-12 11:42:52', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('353', '6', '350', 'pendapatan', 'Pendapatan', '', '', '2017-04-12 11:42:59', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('354', '6', '350', 'tagihan', 'Tagihan', '', '', '2017-04-12 11:43:06', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('355', '6', '350', 'ump', 'UMP', '', '', '2017-04-12 11:43:14', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('356', '6', '350', 'kontrak', 'Kontrak', '', '', '2017-04-12 11:43:21', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('357', '7', '0', 'master', 'Master Data', '', 'fa fa-book', '2017-04-12 16:37:53', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('358', '7', '357', 'kredit macet', 'Kredit Macet', '', '', '2017-04-12 16:38:09', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('359', '7', '357', 'tenor kendaraan', 'Tenor Kendaraan', '', '', '2017-04-12 16:38:22', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('360', '7', '357', 'tenor pembiayaan', 'Tenor Pembiayaan', '', '', '2017-04-12 16:38:34', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('361', '7', '357', 'kelompok nasabah', 'Kelompok Nasabah', '', '', '2017-04-12 16:38:51', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('362', '7', '357', 'sub kelompok nasabah', 'Sub Kelompok Nasabah', '', '', '2017-04-12 16:39:21', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('363', '7', '357', 'nasabah Pembiayaan', 'Nasabah Pembiayaan', '', '', '2017-04-12 16:39:41', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('364', '7', '357', 'nasabah kendaraan', 'Nasabah Kendaraan', '', '', '2017-04-12 16:39:58', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('365', '7', '0', 'Budgeting', 'Budgeting', '', 'fa fa-asterisk', '2017-04-12 16:40:36', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('366', '7', '365', 'pengajuan anggaran', 'Pengajuan Anggaran', '', '', '2017-04-12 16:40:36', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('367', '7', '365', 'pergeseran anggaran', 'Pergeseran Anggaran', '', '', '2017-04-12 16:40:37', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('368', '7', '365', 'revisi anggaran', 'Revisi Anggaran', '', '', '2017-04-12 16:40:38', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('369', '7', '365', 'realisasi', 'Realisasi', '', '', '2017-04-12 16:40:39', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('370', '7', '0', 'form ump', 'Form UMP', '', 'fa fa-question-circle-o', '2017-04-12 16:40:39', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('371', '7', '370', 'pengajuan ump', 'Pengajuan UMP', '', '', '2017-04-12 16:40:40', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('372', '7', '370', 'notifikasi ump', 'Notifikasi UMP', '', '', '2017-04-12 16:40:40', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('373', '7', '370', 'perpanjangan ump', 'Perpanjangan UMP', '', '', '2017-04-12 16:40:41', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('374', '7', '370', 'penyelesaian', 'Penyelesaian UMP', '', '', '2017-04-12 16:40:43', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('375', '7', '0', 'aplikasi', 'Aplikasi', '', 'fa fa-th', '2017-04-12 16:40:53', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('376', '7', '375', 'pengajuan kredit pembiayaan', 'Pengajuan Kredit Pembiayaan', '', '', '2017-04-12 16:43:35', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('377', '7', '375', 'pengajuan kredit pembiayaan penggadaian', 'Pengajuan Kredit Pembiayaan Penggadaian', '', '', '2017-04-12 16:44:03', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('378', '7', '375', 'registrasi kredit pembiayaan non penggadaian', 'Registrasi Data Pengajuan Kredit Pembiayaan Non Penggadaian', '', '', '2017-04-12 16:48:23', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('379', '7', '375', 'pencairan kredit pembiayaan dengan dan tanpa adm', 'Pencairan Kredit Pembiayaan Dengan dan Tanpa ADM', '', '', '2017-04-12 16:49:06', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('380', '7', '375', 'registrasi pengajuan kredit kendaraan', 'Registrasi Pengajuan Kredit Kendaraan', '', '', '2017-04-12 16:49:36', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('381', '7', '375', 'pencairan kredit kendaraan dengan dp', 'Pencairan Kredit Kendaraan dengan DP', '', '', '2017-04-12 16:50:09', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('382', '7', '375', 'pencairan kredit kendaraan tanpa dp', 'Pencairan Kredit Kendaraan tanpa DP', '', '', '2017-04-12 16:50:38', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('383', '7', '0', 'nasabah', 'Data Nasabah', '', 'fa fa-table', '2017-04-12 16:51:25', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('384', '7', '383', 'nasabah orang', 'Data Nasabah Orang', '', '', '2017-04-12 16:51:44', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('385', '7', '383', 'nasabah kanwil', 'Data Nasabah Kanwil', '', '', '2017-04-12 16:52:05', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('386', '7', '383', 'mutasi nasabah', 'Mutasi Nasabah', '', '', '2017-04-12 16:53:48', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('387', '7', '0', 'piutang', 'Piutang', '', 'fa fa-list-ol', '2017-04-12 16:53:55', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('388', '7', '387', 'piutang nasabah pembiayaan', 'Piutang Nasabah Pembiayaan', '', '', '2017-04-12 16:54:18', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('389', '7', '387', 'piutang nasabah kendaraan', 'Piutang Nasabah Kendaraan', '', '', '2017-04-12 16:54:37', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('390', '7', '387', 'piutang nasabah penggadaian', 'Piutang Nasabah Penggadaian', '', '', '2017-04-12 16:55:11', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('391', '7', '387', 'piutang nasabah internal', 'Piutang Nasabah Internal', '', '', '2017-04-12 16:55:39', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('392', '7', '387', 'piutang nasabah cash', ' Piutang Nasabah Cash', '', '', '2017-04-12 16:55:51', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('393', '7', '0', 'tagihan', 'Tagihan', '', 'fa fa-tty', '2017-04-12 16:56:32', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('394', '7', '393', 'tagihan penggadaian', 'Tagihan Penggadaian', '', '', '2017-04-12 16:56:58', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('395', '7', '393', 'tagihan non penggadaian', 'Tagihan Non Penggadaian', '', '', '2017-04-12 16:57:15', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('396', '7', '393', 'tagihan kendaraan', 'Tagihan Kendaraan', '', '', '2017-04-12 16:57:30', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('397', '7', '393', 'penyelesaian tagihan', 'Penyelesaian tagihan', '', '', '2017-04-12 16:57:45', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('398', '7', '393', 'top up', 'Top Up', '', '', '2017-04-12 16:58:01', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('399', '7', '0', 'laporan', 'Laporan', '', 'fa fa-window-restore', '2017-04-12 16:58:16', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('400', '7', '399', 'rekap nasabah', 'Rekapitulasi Nasabah', '', '', '2017-04-12 16:58:29', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('401', '7', '399', 'nasabah pencairan', 'Nasabah berdasarkan Pencairan', '', '', '2017-04-12 16:58:50', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('402', '7', '399', 'ump', 'UMP', '', '', '2017-04-12 16:58:58', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('403', '7', '399', 'kredit macet', 'Kredit Macet', '', '', '2017-04-12 16:59:07', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('404', '7', '399', 'tagihan', 'Tagihan', '', '', '2017-04-12 16:59:15', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('405', '2', '49', 'data disiplin', 'Data Disiplin', '', '', '2017-04-27 10:18:25', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('406', '2', '49', 'data jabatan', 'Data Jabatan', '', '', '2017-04-27 10:18:56', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('407', '9', '2', 'akun', 'Akun Budget', '', '', '2017-05-10 11:52:39', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('408', '9', '2', 'budget', 'Budget', '', '', '2017-05-10 11:52:47', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('409', '2', '12', 'biaya perjalanan dinas', 'Biaya Perjalanan Dinas', '', '', '2017-05-15 10:12:17', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('464', '8', '0', 'master', 'Master', '', 'fa fa-book', '2017-08-03 09:03:44', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('465', '8', '464', 'coa', 'COA', '', '', '2017-08-03 09:04:04', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('466', '8', '464', 'umur piutang', 'Umur Piutang', '', '', '2017-08-03 09:04:25', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('467', '8', '464', 'parameter jurnal', 'Parameter Jurnal', '', '', '2017-08-03 09:05:44', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('468', '8', '0', 'budgeting', 'Budgeting', '', 'fa fa-asterisk', '2017-08-03 09:06:24', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('469', '8', '468', 'pengajuan anggaran', 'Pengajuan Anggaran', '', '', '2017-08-03 09:07:01', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('470', '8', '468', 'pergeseran anggaran', 'Pergeseran Anggaran', '', '', '2017-08-03 09:10:51', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('471', '5', '0', 'master', 'Master', '', 'fa fa-book', '2017-08-16 09:24:35', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('472', '5', '471', 'kategori', 'Kategori Kendaraan', '', '', '2017-08-16 09:25:20', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('473', '5', '471', 'merk kendaraan', 'Merk Kendaraan', '', '', '2017-08-16 09:25:44', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('474', '5', '471', 'kendaraan', 'Type Kendaraan', '', '', '2017-08-16 09:25:59', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('475', '5', '471', 'customer', 'Customer', '', '', '2017-08-16 09:26:16', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('476', '5', '471', 'rekanan', 'Rekanan', '', '', '2017-08-16 09:26:24', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('477', '5', '0', 'Kontrak Perjanjian', 'Kerja Sama Rental', '', 'fa fa-handshake-o', '2017-08-16 09:28:18', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('478', '5', '477', 'spk', 'SPK / BA', '', '', '2017-08-16 09:28:39', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('479', '5', '477', 'purchase order', 'Purchase Order', '', '', '2017-08-16 09:28:54', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('480', '5', '477', 'receive', 'Penerimaan Kendaraan', '', '', '2017-08-16 09:29:11', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('481', '5', '477', 'delivery', 'Pengiriman Kendaraan', '', '', '2017-08-16 09:29:35', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('482', '5', '477', 'pks', 'Perjanjian Kerja Sama', '', '', '2017-08-16 09:29:58', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('483', '5', '477', 'adendum kontrak', 'Adendum Kontrak PKS', '', '', '2017-08-16 09:30:23', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('484', '5', '477', 'kontrak selesai', 'Penyelesaian PKS', '', '', '2017-08-16 09:31:39', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('485', '5', '0', 'invoicing rental', 'Invoicing Rental', '', 'fa fa-file-archive-o', '2017-08-16 09:32:17', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('486', '5', '485', 'invoicing', 'Pembuatan Invoice', '', '', '2017-08-16 09:32:26', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('487', '5', '485', 'revisi invoice', 'Revisi Invoice', '', '', '2017-08-16 09:33:09', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('488', '5', '485', 'monitoring invoice', 'Monitoring Invoice', '', '', '2017-08-16 09:33:27', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('489', '5', '0', 'register invoice', 'Register Invoice', '', 'fa fa-download', '2017-08-16 09:37:24', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('490', '5', '489', 'update invoice masuk', 'Invoice Masuk', '', '', '2017-08-16 09:37:51', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('491', '5', '489', 'cari dokumen', 'Cari Dokumen', '', '', '2017-08-16 09:38:10', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('492', '5', '0', 'dokumen kendaraan', 'Dokumen Kendaraan', '', 'fa fa-file', '2017-08-16 09:38:40', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('493', '5', '492', 'registrasi stnk', 'Register STNK', '', '', '2017-08-16 09:39:03', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('494', '5', '492', 'lock stnk', 'Lock STNK', '', '', '2017-08-16 09:39:14', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('495', '5', '492', 'registrasi bpkb', 'Registrasi BPKB', '', '', '2017-08-16 09:42:02', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('496', '5', '492', 'cari bpkb', 'Cari BPKB', '', '', '2017-08-16 09:42:15', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('497', '5', '492', 'mutasi bpkb', 'Peminjaman / Pengembalian BPKB', '', '', '2017-08-16 09:42:40', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('498', '5', '492', 'registrasi asuransi', 'Register Asuransi', '', '', '2017-08-16 09:43:02', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('499', '5', '492', 'lock asuransi', 'Lock Asuransi', '', '', '2017-08-16 09:43:14', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('500', '5', '492', 'registrasi leasing', 'Register Angsuran Leasing', '', '', '2017-08-16 09:43:31', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('501', '5', '492', 'lock leasing', 'Lock Angsuran Leasing', '', '', '2017-08-16 09:43:46', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('502', '5', '0', 'pemeliharaan kendaraan', 'Pemeliharaan Kendaraan', '', 'fa fa-life-ring', '2017-08-16 09:48:55', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('503', '5', '502', 'registrasi service', 'Register Service Berkala', '', '', '2017-08-16 09:49:41', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('504', '5', '502', 'lock service', 'Lock Service Berkala', '', '', '2017-08-16 09:50:06', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('505', '5', '502', 'service', 'Service Kendaraan', '', '', '2017-08-16 09:51:23', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('506', '5', '502', 'backup', 'Backup', '', '', '2017-08-16 09:51:32', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('507', '5', '502', 'backup update', 'Backup Update', '', '', '2017-08-16 09:51:46', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('508', '5', '0', 'kendaraan operasional', 'Kendaraan Operasional', '', 'fa fa-car', '2017-08-16 09:52:27', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('509', '5', '508', 'permintaan user', 'Permintaan User', '', '', '2017-08-16 09:52:39', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('510', '5', '508', 'approval', 'Approval', '', '', '2017-08-16 09:52:48', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('511', '5', '508', 'jadwal', 'Jadwal', '', '', '2017-08-16 09:55:41', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('512', '5', '508', 'penjualan pengalihan kendaraan', 'Penjualan dan Pengalihan Kendaraan', '', '', '2017-08-16 09:56:06', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('513', '5', '0', 'notifikasi', 'Notifikasi', '', 'fa fa-bell', '2017-08-16 09:56:16', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('514', '5', '513', 'notifikasi stnk', 'STNK', '', '', '2017-08-16 09:56:29', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('515', '5', '513', 'notifikasi asuransi', 'Asuransi', '', '', '2017-08-16 09:56:46', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('516', '5', '513', 'notifikasi leasing', 'Angsuran Leasing', '', '', '2017-08-16 09:57:09', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('517', '5', '513', 'notifikasi bpkb', 'BPKB Keluar', '', '', '2017-08-16 10:01:17', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('518', '5', '513', 'notifikasi service', 'Service Berkala', '', '', '2017-08-16 10:01:38', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('519', '5', '513', 'notifikasi kontrak', 'Kontrak Perjanjian Kerja Sama', '', '', '2017-08-16 10:02:31', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('520', '5', '0', 'anggaran', 'Budgeting', '', 'fa fa-asterisk', '2017-08-30 11:03:06', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('521', '5', '520', 'realisasi anggaran', 'Realisasi', '', '', '2017-08-30 11:03:20', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('522', '5', '0', 'ump', 'UMP', '', 'fa fa-question-circle-o', '2017-08-30 11:03:40', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('523', '5', '522', 'pengajuan ump', 'Pengajuan UMP', '', '', '2017-08-30 11:04:44', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('524', '5', '522', 'notifikasi ump', 'Notifikasi UMP', '', '', '2017-08-30 11:05:12', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('525', '5', '522', 'perpanjangan ump', 'Perpanjangan UMP', '', '', '2017-08-30 11:05:26', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('526', '5', '522', 'penyelesaian ump', 'Penyelesaian UMP', '', '', '2017-08-30 11:05:39', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('527', '5', '0', 'laporan', 'Laporan', '', 'fa fa-window-restore', '2017-08-30 11:06:02', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('528', '5', '527', 'data kendaraan', 'Data Kendaraan', '', '', '2017-08-30 11:06:27', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('529', '5', '527', 'kendaraan perkontrak', 'Kendaraan Perkontrak', '', '', '2017-08-30 11:07:08', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('530', '5', '527', 'kendaraan perperusahaan', 'Kendaraan Perperusahaan', '', '', '2017-08-30 11:07:25', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('531', '5', '527', 'pendapatan biaya', 'Pendapatan Biaya', '', '', '2017-08-30 11:10:57', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('532', '0', '527', 'register dokumen', 'Register Dokumen', '', '', '2017-08-30 14:26:29', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('533', '8', '0', 'pengakuan hutang ', 'Pengakuan Hutang', '', '', '2017-08-30 14:32:52', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('534', '8', '533', 'pengakuan hutang kendaraan rental', 'Hutang Kendaraan Rental', '', '', '2017-08-30 14:32:58', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('535', '8', '533', 'pengakuan hutang expedisi', 'Hutang Kendaraan Expedisi', '', '', '2017-09-05 09:13:11', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('538', '8', '0', 'sistem kasir', 'Sistem Kasir', '', 'fa fa-dollar', '2017-09-05 09:29:07', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('539', '8', '538', 'pemasukan', 'Pemasukan', '', '', '2017-09-05 09:29:49', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('540', '8', '538', 'pengeluaran', 'Pengeluaran', '', '', '2017-09-05 09:30:02', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('541', '8', '538', 'edit kasir', 'Edit Kasir', '', '', '2017-09-05 09:30:16', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('542', '8', '538', 'pencairan ump', 'Pencairan UMP', '', '', '2017-09-05 09:33:31', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('543', '8', '538', 'jurnal koreksi', 'Jurnal Koreksi', '', '', '2017-09-05 09:38:22', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('544', '8', '0', 'revisi tagihan', 'Revisi Tagihan', '', 'fa fa-pencil-square', '2017-09-05 09:38:26', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('545', '8', '544', 'koreksi tagihan', 'Koreksi Tagihan', '', '', '2017-09-05 09:39:53', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('546', '8', '0', 'kontrol ar ap', 'Kontrol AR / AP ', '', 'fa fa-balance-scale', '2017-09-05 09:40:34', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('547', '8', '546', 'buku piutang', 'Buku Piutang', '', '', '2017-09-05 09:42:56', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('548', '8', '546', 'umur piutang', 'Umur Piutang', '', '', '2017-09-05 09:43:09', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('549', '8', '546', 'cadangan piutang tak tertagih', 'Cadangan Piutangan Tak Tertagih', '', '', '2017-09-05 09:52:40', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('550', '8', '546', 'laporan piutang', 'Laporan Piutang', '', '', '2017-09-05 09:53:23', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('551', '8', '546', 'laporan hutang', 'Laporan Hutang', '', '', '2017-09-05 09:53:35', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('552', '8', '546', 'laporan akun tak terdeteksi', 'Laporan Akun Tak Terdeteksi', '', '', '2017-09-05 09:54:24', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('553', '8', '0', 'kontrol ump', 'Kontrol UMP', '', 'fa fa-eye', '2017-09-05 10:01:25', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('554', '8', '553', 'reminder', 'Reminder', '', '', '2017-09-05 10:01:46', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('555', '8', '553', 'monitoring ump', 'UMP', '', '', '2017-09-05 10:02:02', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('556', '8', '553', 'laporan pertanggung jawaban ump', 'Laporan Pertanggung Jawaban UMP', '', '', '2017-09-05 10:02:29', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('557', '8', '0', 'laporan keuangan', 'Laporan Keuangan', '', 'fa fa-window-restore', '2017-09-05 10:03:27', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('558', '8', '557', 'laporan neraca', 'Laporan Neraca', '', '', '2017-09-05 10:03:47', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('559', '8', '557', 'laporan laba rugi', 'Laporan Laba Rugi', '', '', '2017-09-05 10:04:11', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('560', '8', '557', 'lapoaran cash flow', 'Laporan Cash Flow', '', '', '2017-09-05 10:04:44', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('561', '8', '557', 'laporan perubahan ekuitas', 'Laporan Ekuitas', '', '', '2017-09-05 10:05:12', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('562', '8', '0', 'penyusutan', 'Penyusutan', '', 'fa fa-minus-square', '2017-09-05 10:05:27', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('563', '8', '562', 'penyusutan bangunan', 'Penyusutan Bangunan', '', '', '2017-09-05 10:05:46', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('564', '8', '562', 'penyusutan inventaris', 'Penyusutan Inventaris', '', '', '2017-09-05 10:06:05', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('565', '8', '562', 'penyusutan kendaraan', 'Penyusutan Kendaraan', '', '', '2017-09-05 10:06:28', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('566', '8', '562', 'penghapusan penyusutan kendaraan', 'penghapusan penyusutan kendaraan', '', '', '2017-09-05 10:06:56', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('567', '8', '0', 'rekap', 'Rekap', '', 'fa fa-file', '2017-09-05 10:07:13', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('568', '8', '567', 'rekap data pajak', 'Rekap Data Pajak', '', '', '2017-09-05 10:07:32', '0000-00-00 00:00:00');
INSERT INTO `sub_modules` VALUES ('570', '8', '546', 'view jurnal', 'view jurnal', '', '', '2017-09-05 10:30:40', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for trans_budget
-- ----------------------------
DROP TABLE IF EXISTS `trans_budget`;
CREATE TABLE `trans_budget` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sdm_ms_bagian_id` int(10) unsigned NOT NULL,
  `kode_anggaran` varchar(30) DEFAULT NULL,
  `nama_anggaran` varchar(200) DEFAULT NULL,
  `total_anggaran` int(10) DEFAULT '0',
  `tahun_anggaran` int(4) unsigned DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `trans_budget_sdm_ms_bagian_id_foreign` (`sdm_ms_bagian_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of trans_budget
-- ----------------------------

-- ----------------------------
-- Table structure for trans_budget_detail
-- ----------------------------
DROP TABLE IF EXISTS `trans_budget_detail`;
CREATE TABLE `trans_budget_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trans_budget_id` int(10) unsigned NOT NULL,
  `ms_akun_budget_id` int(10) unsigned NOT NULL,
  `budget` int(10) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `trans_budget_detail_trans_budget_id_foreign` (`trans_budget_id`) USING BTREE,
  KEY `trans_budget_detail_ms_akun_budget_id_foreign` (`ms_akun_budget_id`) USING BTREE,
  CONSTRAINT `trans_budget_detail_ibfk_1` FOREIGN KEY (`trans_budget_id`) REFERENCES `trans_budget` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `trans_budget_detail_ibfk_2` FOREIGN KEY (`ms_akun_budget_id`) REFERENCES `ms_akun_budget` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of trans_budget_detail
-- ----------------------------

-- ----------------------------
-- Table structure for trans_sopa_spoa
-- ----------------------------
DROP TABLE IF EXISTS `trans_sopa_spoa`;
CREATE TABLE `trans_sopa_spoa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sdm_ms_bagian_id` int(10) unsigned DEFAULT NULL,
  `nomor_sopa_spoa` varchar(30) NOT NULL,
  `kode` int(10) unsigned DEFAULT NULL,
  `kepada` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `perihal` text NOT NULL,
  `maksimum_pengeluaran` int(10) unsigned NOT NULL,
  `with_ump` enum('y','n') CHARACTER SET utf8 NOT NULL,
  `penyelesaian` enum('equal','less','greater') NOT NULL,
  `jenis` enum('0','register','aktiva') CHARACTER SET utf8 DEFAULT NULL,
  `triwulan` int(1) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `trans_sopa_spoa_sdm_ms_bagian_id` (`sdm_ms_bagian_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of trans_sopa_spoa
-- ----------------------------

-- ----------------------------
-- Table structure for trans_ump
-- ----------------------------
DROP TABLE IF EXISTS `trans_ump`;
CREATE TABLE `trans_ump` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sdm_ms_bagian_id` int(10) unsigned NOT NULL,
  `trans_budget_detail_id` int(10) unsigned NOT NULL,
  `no_ump` varchar(30) NOT NULL,
  `jumlah_ump` int(10) unsigned NOT NULL,
  `deskripsi` text NOT NULL,
  `tgl_pencairan` date DEFAULT NULL,
  `tgl_jatuh_tempo` date DEFAULT NULL,
  `tgl_perpanjangan` date DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0 = pengajuan; 1 = pencairan; 2 = penyelesaian',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `trans_ump_sdm_ms_bagian_id_foreign` (`sdm_ms_bagian_id`) USING BTREE,
  KEY `trans_ump_trans_budget_detail_id_foreign` (`trans_budget_detail_id`) USING BTREE,
  CONSTRAINT `trans_ump_ibfk_1` FOREIGN KEY (`trans_budget_detail_id`) REFERENCES `trans_budget_detail` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trans_ump
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Iqbal Ardiansyah', 'aiqbalsyah@gmail.com', '$2y$10$MyxRxfJ66qhLI/G08YedHeeJ1lc03/JkcEVwPy646RoqtfWlBRxTy', '76Atgo0nYDcIVqUSoeNy01GAazgiEysXpXOEZnGkAlmMsFpYbvFbyBxFSNSX', '1', '2017-02-23 02:07:26', '2017-02-23 02:07:26');
INSERT INTO `users` VALUES ('2', 'dev', 'tegarfiqri.b@gmail.com', '$2y$10$sKMHdTRr7ym.ihLbOqAxneQfqXlD5XNl5F.WjEUpUERrAeuHQpwDi', 'qfdcsYv81bWVmLvdMondOXSELNvGb8vt4Cgl1aPgfswRTjxik4o4OnktH2xB', '1', '2017-03-06 05:34:22', '2017-03-06 05:34:22');
INSERT INTO `users` VALUES ('3', 'fahmi nur rahman', 'fachminur@gmail.com', '$2y$10$3.EAklPn73KMmLlvos6abeCpeZ8bPUHNSoGyJrcQJq5hDL42n.T9e', null, '1', '2017-03-13 05:36:17', '2017-03-13 05:36:17');
INSERT INTO `users` VALUES ('4', 'umum', 'umum@gmail.com', '$2y$10$QPvuUl7.CKIqR.6WGjh2nOMsL.8l90ZI.xOHNh/0MHcBzqmF6NY9C', 'r2yAdr5RM52O2K2KosINZ4ig8U1dhxJo5c1YGKs6CjPoARAFouAYBB3NN7Vy', '2', '2017-06-14 02:40:20', '2017-09-06 10:08:52');
INSERT INTO `users` VALUES ('5', 'rental', 'rental@gmail.com', '$2y$10$0rwIoic7KWjMcUy8pki1v.ZfKqj.qFel8wZI0elqPDT44r47A4KYO', null, '1', '2017-09-06 10:18:54', '2017-09-06 10:18:54');
