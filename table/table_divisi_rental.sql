/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 100113
Source Host           : localhost:3306
Source Database       : era_v2

Target Server Type    : MYSQL
Target Server Version : 100113
File Encoding         : 65001

Date: 2017-09-07 10:16:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for rental_adendum_kontrak
-- ----------------------------
DROP TABLE IF EXISTS `rental_adendum_kontrak`;
CREATE TABLE `rental_adendum_kontrak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rental_pks_id` int(11) NOT NULL,
  `urut` int(11) DEFAULT '0',
  `tgl_adendum_kontrak` date DEFAULT NULL,
  `nomor_adendum_kontrak` varchar(255) DEFAULT NULL,
  `masa_kontrak` int(11) NOT NULL,
  `total` decimal(22,2) NOT NULL,
  `discount` decimal(22,2) NOT NULL,
  `ppn` decimal(22,2) NOT NULL,
  `grand_total` decimal(22,2) NOT NULL,
  `status` int(1) DEFAULT '0',
  `status_invoice` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_adendum_kontrak
-- ----------------------------
INSERT INTO `rental_adendum_kontrak` VALUES ('1', '1', '1', '2017-09-01', '1/EPS.RENT/ADD/IX/2017', '12', '96000000.00', '0.00', '9600000.00', '105600000.00', '0', '1', '2', '2017-08-29 11:18:24', null, '2017-08-29 11:18:24');
INSERT INTO `rental_adendum_kontrak` VALUES ('2', '2', '1', '2017-09-20', '2/EPS.RENT/ADD/IX/2017', '5', '4500000.00', '0.00', '450000.00', '4950000.00', '0', '0', '5', '2017-09-06 10:48:13', null, '2017-09-06 10:48:13');

-- ----------------------------
-- Table structure for rental_adendum_kontrak_detail
-- ----------------------------
DROP TABLE IF EXISTS `rental_adendum_kontrak_detail`;
CREATE TABLE `rental_adendum_kontrak_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rental_adendum_kontrak_id` int(11) NOT NULL,
  `rental_pks_detail_id` int(11) NOT NULL,
  `rental_kendaraan_id` int(11) NOT NULL,
  `lama_sewa` int(11) NOT NULL,
  `harga` decimal(30,2) NOT NULL,
  `subtotal` decimal(30,2) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_adendum_kontrak_detail
-- ----------------------------
INSERT INTO `rental_adendum_kontrak_detail` VALUES ('1', '1', '6', '1', '12', '2000000.00', '24000000.00', '2017-08-29 11:17:47', '2017-08-29 11:17:47');
INSERT INTO `rental_adendum_kontrak_detail` VALUES ('2', '1', '7', '2', '12', '2000000.00', '24000000.00', '2017-08-29 11:17:47', '2017-08-29 11:17:47');
INSERT INTO `rental_adendum_kontrak_detail` VALUES ('3', '1', '8', '3', '12', '2000000.00', '24000000.00', '2017-08-29 11:17:47', '2017-08-29 11:17:47');
INSERT INTO `rental_adendum_kontrak_detail` VALUES ('4', '1', '9', '4', '12', '2000000.00', '24000000.00', '2017-08-29 11:17:47', '2017-08-29 11:17:47');
INSERT INTO `rental_adendum_kontrak_detail` VALUES ('5', '2', '10', '12', '5', '900000.00', '4500000.00', '2017-09-06 10:48:13', '2017-09-06 10:48:13');

-- ----------------------------
-- Table structure for rental_backup
-- ----------------------------
DROP TABLE IF EXISTS `rental_backup`;
CREATE TABLE `rental_backup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_backup` date DEFAULT NULL,
  `rental_kendaraan_id` int(11) DEFAULT NULL,
  `rental_backup_kendaraan_id` int(11) DEFAULT NULL,
  `nomor_backup` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `approved_by` int(11) NOT NULL,
  `created_at` datetime(6) NOT NULL DEFAULT '0000-00-00 00:00:00.000000',
  `status` int(11) DEFAULT '0',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `updated_at` datetime(6) NOT NULL DEFAULT '0000-00-00 00:00:00.000000' ON UPDATE CURRENT_TIMESTAMP(6),
  `updated_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_backup
-- ----------------------------
INSERT INTO `rental_backup` VALUES ('1', '2017-08-03', '1', '5', '1/EPS.RENT/BACKUP/VIII/2017', null, '2', '2017-08-29 11:24:32.000000', '0', '2', '2017-08-29 11:24:32.000000', '0');

-- ----------------------------
-- Table structure for rental_delivery
-- ----------------------------
DROP TABLE IF EXISTS `rental_delivery`;
CREATE TABLE `rental_delivery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor_delivery` varchar(225) NOT NULL,
  `rental_spk_id` int(11) NOT NULL,
  `rental_receive_id` int(11) DEFAULT '0',
  `tgl_delivery` date NOT NULL,
  `status` int(1) DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_delivery
-- ----------------------------
INSERT INTO `rental_delivery` VALUES ('1', '1/EPS.RENT/DEV/VIII/2017', '1', '0', '2017-08-02', '0', '2', '2017-08-29 10:12:11', '0', '2017-08-29 10:12:11');
INSERT INTO `rental_delivery` VALUES ('2', '1/EPS.RENT/DEV/IX/2017', '2', '3', '2017-09-16', '0', '5', '2017-09-06 10:43:10', '0', '2017-09-06 10:43:10');
INSERT INTO `rental_delivery` VALUES ('3', '2/EPS.RENT/DEV/IX/2017', '2', '0', '2017-09-13', '0', '5', '2017-09-06 10:44:09', '0', '2017-09-06 10:44:09');

-- ----------------------------
-- Table structure for rental_delivery_detail
-- ----------------------------
DROP TABLE IF EXISTS `rental_delivery_detail`;
CREATE TABLE `rental_delivery_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rental_delivery_id` int(11) NOT NULL,
  `rental_kendaraan_id` int(10) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rental_ms_kendaraan_rental_ms_kategori_kendaran_id_foreign` (`rental_kendaraan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_delivery_detail
-- ----------------------------
INSERT INTO `rental_delivery_detail` VALUES ('1', '1', '1', '2017-08-29 10:12:11', '0', '2017-08-29 10:12:11', null);
INSERT INTO `rental_delivery_detail` VALUES ('2', '1', '2', '2017-08-29 10:12:11', '0', '2017-08-29 10:12:11', null);
INSERT INTO `rental_delivery_detail` VALUES ('3', '1', '3', '2017-08-29 10:12:11', '0', '2017-08-29 10:12:11', null);
INSERT INTO `rental_delivery_detail` VALUES ('4', '1', '4', '2017-08-29 10:12:11', '0', '2017-08-29 10:12:11', null);
INSERT INTO `rental_delivery_detail` VALUES ('5', '1', '5', '2017-08-29 10:12:11', '0', '2017-08-29 10:12:11', null);
INSERT INTO `rental_delivery_detail` VALUES ('6', '1', '6', '2017-08-29 10:12:11', '0', '2017-08-29 10:12:11', null);
INSERT INTO `rental_delivery_detail` VALUES ('7', '1', '7', '2017-08-29 10:12:11', '0', '2017-08-29 10:12:11', null);
INSERT INTO `rental_delivery_detail` VALUES ('8', '1', '8', '2017-08-29 10:12:11', '0', '2017-08-29 10:12:11', null);
INSERT INTO `rental_delivery_detail` VALUES ('9', '1', '9', '2017-08-29 10:12:11', '0', '2017-08-29 10:12:11', null);
INSERT INTO `rental_delivery_detail` VALUES ('10', '2', '12', '2017-09-06 10:43:10', '0', '2017-09-06 10:43:10', null);
INSERT INTO `rental_delivery_detail` VALUES ('11', '3', '10', '2017-09-06 10:44:09', '0', '2017-09-06 10:44:09', null);
INSERT INTO `rental_delivery_detail` VALUES ('12', '3', '11', '2017-09-06 10:44:09', '0', '2017-09-06 10:44:09', null);

-- ----------------------------
-- Table structure for rental_invoice_in
-- ----------------------------
DROP TABLE IF EXISTS `rental_invoice_in`;
CREATE TABLE `rental_invoice_in` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('INVOICE','CLAIM','STNK','BPKB','LEASING','ASURANSI','DOKUMEN LAINNYA') NOT NULL,
  `nomor_surat_pengirim` varchar(225) NOT NULL,
  `nomor_invoice` varchar(225) NOT NULL,
  `rental_ms_rekanan_id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `rental_kendaraan_id` int(11) NOT NULL,
  `nominal` int(11) NOT NULL,
  `keterangan` varchar(225) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_invoice_in
-- ----------------------------
INSERT INTO `rental_invoice_in` VALUES ('1', 'INVOICE', '321321', '321321', '1', '2017-08-24', '6', '1000000', '321321321', '2017-08-29 11:22:31', '2', '2017-08-29 11:22:31', null);
INSERT INTO `rental_invoice_in` VALUES ('2', 'INVOICE', 'SERVICE/3201/3213', 'Service/21321/321321', '1', '2017-09-29', '1', '3000000', 'INVOICE SERVICE NO SPK : SPK-BENGKEL-001', '2017-09-06 11:06:53', '5', '2017-09-06 11:06:53', null);
INSERT INTO `rental_invoice_in` VALUES ('3', 'STNK', '321321', '321321', '5', '2017-09-26', '1', '500000', 'PERPANJANG', '2017-09-06 11:40:52', '5', '2017-09-06 11:40:52', null);

-- ----------------------------
-- Table structure for rental_invoice_out
-- ----------------------------
DROP TABLE IF EXISTS `rental_invoice_out`;
CREATE TABLE `rental_invoice_out` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rental_pks_id` int(11) NOT NULL,
  `rental_adendum_kontrak_id` int(11) NOT NULL,
  `tanggal_invoice` date NOT NULL,
  `nomor_invoice` varchar(225) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `total` decimal(22,2) NOT NULL,
  `discount` decimal(22,2) NOT NULL,
  `ppn` decimal(22,2) NOT NULL,
  `grand_total` decimal(22,2) NOT NULL,
  `revisi` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_invoice_out
-- ----------------------------
INSERT INTO `rental_invoice_out` VALUES ('4', '1', '0', '2017-09-01', '1/EPS/KEU/RENT/IX/2017', '2017-09-01', '2017-09-20', '0', '11000000.00', '0.00', '1100000.00', '12100000.00', '1', '2017-09-05 13:40:20', '2', '2017-09-05 13:45:07', '2');
INSERT INTO `rental_invoice_out` VALUES ('5', '1', '0', '2017-09-21', '2/EPS/KEU/RENT/IX/2017', '2017-09-16', '2017-10-28', '0', '7700000.00', '0.00', '770000.00', '8470000.00', '0', '2017-09-06 00:42:56', '2', '2017-09-06 00:42:56', '0');
INSERT INTO `rental_invoice_out` VALUES ('6', '2', '0', '2017-09-06', '3/EPS/KEU/RENT/IX/2017', '2017-08-31', '2017-08-31', '0', '3000000.00', '0.00', '300000.00', '3300000.00', '0', '2017-09-06 10:52:19', '5', '2017-09-06 10:52:19', '0');

-- ----------------------------
-- Table structure for rental_invoice_out_detail
-- ----------------------------
DROP TABLE IF EXISTS `rental_invoice_out_detail`;
CREATE TABLE `rental_invoice_out_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rental_invoice_out_id` int(11) NOT NULL,
  `rental_kendaraan_id` int(11) NOT NULL,
  `rental_pks_detail_id` int(11) NOT NULL,
  `rental_adendum_kontrak_detail_id` int(11) DEFAULT '0',
  `subtotal` decimal(22,2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_invoice_out_detail
-- ----------------------------
INSERT INTO `rental_invoice_out_detail` VALUES ('49', '4', '8', '4', '0', '1000000.00', '2017-09-05 13:45:07', '2017-09-05 13:45:07');
INSERT INTO `rental_invoice_out_detail` VALUES ('50', '4', '7', '3', '0', '1000000.00', '2017-09-05 13:45:07', '2017-09-05 13:45:07');
INSERT INTO `rental_invoice_out_detail` VALUES ('51', '4', '6', '2', '0', '1000000.00', '2017-09-05 13:45:07', '2017-09-05 13:45:07');
INSERT INTO `rental_invoice_out_detail` VALUES ('52', '4', '5', '1', '0', '1000000.00', '2017-09-05 13:45:07', '2017-09-05 13:45:07');
INSERT INTO `rental_invoice_out_detail` VALUES ('53', '4', '9', '5', '0', '1000000.00', '2017-09-05 13:45:07', '2017-09-05 13:45:07');
INSERT INTO `rental_invoice_out_detail` VALUES ('54', '4', '3', '8', '0', '2000000.00', '2017-09-05 13:45:07', '2017-09-05 13:45:07');
INSERT INTO `rental_invoice_out_detail` VALUES ('55', '4', '2', '7', '0', '2000000.00', '2017-09-05 13:45:07', '2017-09-05 13:45:07');
INSERT INTO `rental_invoice_out_detail` VALUES ('56', '4', '1', '6', '0', '2000000.00', '2017-09-05 13:45:07', '2017-09-05 13:45:07');
INSERT INTO `rental_invoice_out_detail` VALUES ('57', '5', '5', '1', '0', '7700000.00', '2017-09-06 00:42:56', '2017-09-06 00:42:56');
INSERT INTO `rental_invoice_out_detail` VALUES ('58', '6', '11', '12', '0', '1000000.00', '2017-09-06 10:52:19', '2017-09-06 10:52:19');
INSERT INTO `rental_invoice_out_detail` VALUES ('59', '6', '12', '10', '0', '1000000.00', '2017-09-06 10:52:19', '2017-09-06 10:52:19');
INSERT INTO `rental_invoice_out_detail` VALUES ('60', '6', '10', '11', '0', '1000000.00', '2017-09-06 10:52:19', '2017-09-06 10:52:19');

-- ----------------------------
-- Table structure for rental_kendaraan
-- ----------------------------
DROP TABLE IF EXISTS `rental_kendaraan`;
CREATE TABLE `rental_kendaraan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rental_ms_kendaraan_id` int(10) unsigned NOT NULL,
  `rental_spk_detail_id` int(11) NOT NULL,
  `rental_po_detail_id` int(11) NOT NULL,
  `rental_receive_detail_id` int(11) NOT NULL,
  `tahun` int(11) DEFAULT NULL,
  `warna` varchar(10) NOT NULL,
  `nomor_polisi` varchar(12) DEFAULT '0',
  `nomor_rangka` varchar(20) NOT NULL,
  `nomor_mesin` varchar(20) NOT NULL,
  `bahan_bakar` varchar(10) NOT NULL,
  `nomor_bpkb` varchar(225) NOT NULL,
  `stnk_daerah` varchar(20) NOT NULL,
  `leasing` varchar(40) NOT NULL,
  `asuransi` varchar(40) NOT NULL,
  `status_bpkb` enum('IN','OUT') NOT NULL DEFAULT 'IN',
  `mutasi_date_bpkb` date DEFAULT NULL,
  `masa_berlaku_stnk` date DEFAULT NULL,
  `status` int(11) NOT NULL,
  `status_kendaraan` enum('NONE','DIALIHKAN','DIJUAL') NOT NULL DEFAULT 'NONE',
  `status_cicilan` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nomor_polisi` (`nomor_polisi`) USING BTREE,
  KEY `rental_ms_kendaraan_rental_ms_kategori_kendaran_id_foreign` (`rental_ms_kendaraan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_kendaraan
-- ----------------------------
INSERT INTO `rental_kendaraan` VALUES ('1', '1', '2', '1', '1', '2017', 'BIRU', 'B2121AF', '321983217398', '382193921', 'PREMIUM', '213213213213', '', '', '', 'OUT', '2017-09-14', '2018-09-30', '1', 'NONE', '0', '2017-08-29 10:11:37', '0', '2017-09-06 11:12:47', '5');
INSERT INTO `rental_kendaraan` VALUES ('2', '1', '2', '1', '2', '2017', 'BIRU', null, '3213892138921', '321398213219', '', '', '', '', '', 'IN', null, null, '1', 'NONE', '0', '2017-08-29 10:11:37', '0', '2017-08-29 10:12:11', null);
INSERT INTO `rental_kendaraan` VALUES ('3', '1', '2', '1', '3', '2017', 'BIRU', null, '21382193', '32321321', '', '', '', '', '', 'IN', null, null, '1', 'NONE', '0', '2017-08-29 10:11:37', '0', '2017-08-29 10:12:11', null);
INSERT INTO `rental_kendaraan` VALUES ('4', '1', '2', '1', '4', '2017', 'BIRU', null, '3219382199', '3282193819', '', '', '', '', '', 'IN', null, null, '1', 'NONE', '0', '2017-08-29 10:11:37', '0', '2017-08-29 10:12:11', null);
INSERT INTO `rental_kendaraan` VALUES ('5', '2', '1', '2', '5', '2017', 'MERAH', null, '3213213213', '321321321', '', '', '', '', '', 'IN', null, null, '1', 'NONE', '0', '2017-08-29 10:11:37', '0', '2017-08-29 10:12:11', null);
INSERT INTO `rental_kendaraan` VALUES ('6', '2', '1', '2', '6', '2017', 'MERAH', null, '321321321', '21321321', '', '', '', '', '', 'IN', null, null, '1', 'DIALIHKAN', '0', '2017-08-29 10:11:37', '0', '2017-08-30 10:27:23', null);
INSERT INTO `rental_kendaraan` VALUES ('7', '2', '1', '2', '7', '2017', 'MERAH', null, '3213213', '321321321', '', '', '', '', '', 'IN', null, null, '1', 'NONE', '0', '2017-08-29 10:11:37', '0', '2017-08-29 10:12:11', null);
INSERT INTO `rental_kendaraan` VALUES ('8', '2', '1', '2', '8', '2017', 'MERAH', null, '21321321321', '213213213', '', '', '', '', '', 'IN', null, null, '1', 'NONE', '0', '2017-08-29 10:11:37', '0', '2017-08-29 10:12:11', null);
INSERT INTO `rental_kendaraan` VALUES ('9', '2', '1', '2', '9', '2017', 'MERAH', null, '321321321', '321321321', '', '', '', '', '', 'IN', null, null, '1', 'NONE', '0', '2017-08-29 10:11:37', '0', '2017-08-29 10:12:11', null);
INSERT INTO `rental_kendaraan` VALUES ('10', '4', '4', '3', '10', '2017', 'BIRU', null, '4324324', '43243324324', '', '', '', '', '', 'IN', null, null, '1', 'NONE', '0', '2017-09-06 10:42:25', '0', '2017-09-06 10:44:09', null);
INSERT INTO `rental_kendaraan` VALUES ('11', '4', '4', '3', '11', '2017', 'BIRU', null, '4324324234343', '3432434324', '', '', '', '', '', 'IN', null, null, '1', 'NONE', '0', '2017-09-06 10:42:25', '0', '2017-09-06 10:44:09', null);
INSERT INTO `rental_kendaraan` VALUES ('12', '4', '4', '3', '12', '2017', 'BIRU', null, '321321321', '321321321', '', '', '', '', '', 'IN', null, null, '1', 'NONE', '0', '2017-09-06 10:43:10', '0', '2017-09-06 10:43:10', null);

-- ----------------------------
-- Table structure for rental_kontrak_selesai
-- ----------------------------
DROP TABLE IF EXISTS `rental_kontrak_selesai`;
CREATE TABLE `rental_kontrak_selesai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rental_ms_customer_id` int(11) NOT NULL,
  `rental_pks_id` int(11) NOT NULL,
  `nomor_kontrak_selesai` varchar(225) NOT NULL,
  `tgl_kontrak_selesai` date NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_kontrak_selesai
-- ----------------------------

-- ----------------------------
-- Table structure for rental_lock_stnk
-- ----------------------------
DROP TABLE IF EXISTS `rental_lock_stnk`;
CREATE TABLE `rental_lock_stnk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rental_kendaraan_id` int(11) NOT NULL,
  `tgl_lock` date NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_lock_stnk
-- ----------------------------

-- ----------------------------
-- Table structure for rental_ms_customer
-- ----------------------------
DROP TABLE IF EXISTS `rental_ms_customer`;
CREATE TABLE `rental_ms_customer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nomor_kartu_pengenal` varchar(16) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `nomor_telp` varchar(16) NOT NULL,
  `alamat` text NOT NULL,
  `created_at` date DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nomor_kartu` (`nomor_kartu_pengenal`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_ms_customer
-- ----------------------------
INSERT INTO `rental_ms_customer` VALUES ('1', '10293101', 'PT Zamasco', 'aiqbalsyah@gmail.com', '03921839', 'dlsajdlsajd asaldjsaldsad', '2017-05-30', '2', '2017-07-17', null);
INSERT INTO `rental_ms_customer` VALUES ('2', '327606180792002', 'PT Agung', 'arfianagung@gmail.com', '021849459454', 'jl. margonda raya', '2017-07-05', '2', '2017-07-17', null);
INSERT INTO `rental_ms_customer` VALUES ('3', 'Logistic Pegadai', 'PT Pegadaian', 'pegadaian@gmail.com', '085666553', 'Jakarta, jl kramat', '2017-09-06', '5', '2017-09-06', null);

-- ----------------------------
-- Table structure for rental_ms_kategori_kendaraan
-- ----------------------------
DROP TABLE IF EXISTS `rental_ms_kategori_kendaraan`;
CREATE TABLE `rental_ms_kategori_kendaraan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kategori_kendaraan` varchar(40) NOT NULL,
  `jenis_kendaraan` enum('mobil','motor') NOT NULL,
  `created_at` date DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_ms_kategori_kendaraan
-- ----------------------------
INSERT INTO `rental_ms_kategori_kendaraan` VALUES ('2', 'Convertible', 'mobil', '2017-05-18', '2', '2017-05-18', '2');
INSERT INTO `rental_ms_kategori_kendaraan` VALUES ('3', 'Coupe', 'mobil', '2017-07-05', '2', '2017-07-05', null);
INSERT INTO `rental_ms_kategori_kendaraan` VALUES ('4', 'Hatchback', 'mobil', '2017-07-13', '2', null, null);
INSERT INTO `rental_ms_kategori_kendaraan` VALUES ('5', 'Sedan', 'mobil', '2017-07-13', '2', null, null);
INSERT INTO `rental_ms_kategori_kendaraan` VALUES ('6', 'SUV', 'mobil', '2017-07-13', '2', null, null);
INSERT INTO `rental_ms_kategori_kendaraan` VALUES ('7', 'MPV', 'mobil', '2017-07-13', '2', null, null);
INSERT INTO `rental_ms_kategori_kendaraan` VALUES ('8', 'BEBEK', 'motor', '2017-09-06', '5', '2017-09-06', null);

-- ----------------------------
-- Table structure for rental_ms_kategori_rekanan
-- ----------------------------
DROP TABLE IF EXISTS `rental_ms_kategori_rekanan`;
CREATE TABLE `rental_ms_kategori_rekanan` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(40) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_ms_kategori_rekanan
-- ----------------------------
INSERT INTO `rental_ms_kategori_rekanan` VALUES ('1', 'Dealer', null, null, null, null);
INSERT INTO `rental_ms_kategori_rekanan` VALUES ('2', 'Leasing', null, null, null, null);
INSERT INTO `rental_ms_kategori_rekanan` VALUES ('3', 'Asuransi', null, null, null, null);
INSERT INTO `rental_ms_kategori_rekanan` VALUES ('4', 'Bengkel', null, null, null, null);

-- ----------------------------
-- Table structure for rental_ms_kendaraan
-- ----------------------------
DROP TABLE IF EXISTS `rental_ms_kendaraan`;
CREATE TABLE `rental_ms_kendaraan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rental_ms_nama_kendaraan_id` int(11) NOT NULL,
  `type` varchar(225) NOT NULL,
  `transmisi` enum('AT','MT') NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_ms_kendaraan
-- ----------------------------
INSERT INTO `rental_ms_kendaraan` VALUES ('1', '1', 'Avanza Hoream IV-tech', 'MT', '2017-08-09 09:33:59', '2', '2017-08-09 09:33:59', null);
INSERT INTO `rental_ms_kendaraan` VALUES ('2', '2', 'Type G', 'AT', '2017-08-09 09:34:20', '2', '2017-08-09 09:34:20', null);
INSERT INTO `rental_ms_kendaraan` VALUES ('3', '2', 'Type GH', 'MT', '2017-08-09 09:34:38', '2', '2017-08-09 09:34:38', null);
INSERT INTO `rental_ms_kendaraan` VALUES ('4', '1', 'Avanza G', 'MT', '2017-09-06 10:24:58', '5', '2017-09-06 10:24:58', null);
INSERT INTO `rental_ms_kendaraan` VALUES ('5', '1', 'Avanza G', 'MT', '2017-09-06 10:25:00', '5', '2017-09-06 10:25:00', null);

-- ----------------------------
-- Table structure for rental_ms_kontrak
-- ----------------------------
DROP TABLE IF EXISTS `rental_ms_kontrak`;
CREATE TABLE `rental_ms_kontrak` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isi_kontrak` text NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_ms_kontrak
-- ----------------------------

-- ----------------------------
-- Table structure for rental_ms_nama_kendaraan
-- ----------------------------
DROP TABLE IF EXISTS `rental_ms_nama_kendaraan`;
CREATE TABLE `rental_ms_nama_kendaraan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rental_ms_kategori_kendaraan_id` int(10) unsigned NOT NULL,
  `nama_kendaraan` varchar(20) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rental_ms_kendaraan_rental_ms_kategori_kendaran_id_foreign` (`rental_ms_kategori_kendaraan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_ms_nama_kendaraan
-- ----------------------------
INSERT INTO `rental_ms_nama_kendaraan` VALUES ('1', '3', 'Toyota', '2017-07-17 10:49:06', '2', '2017-07-17 10:49:06', null);
INSERT INTO `rental_ms_nama_kendaraan` VALUES ('2', '2', 'Ford Mustang', '2017-07-17 10:49:38', '2', '2017-07-17 10:52:06', '2');
INSERT INTO `rental_ms_nama_kendaraan` VALUES ('3', '7', 'Suzuki', '2017-09-06 10:24:02', '5', '2017-09-06 10:24:02', null);

-- ----------------------------
-- Table structure for rental_ms_rekanan
-- ----------------------------
DROP TABLE IF EXISTS `rental_ms_rekanan`;
CREATE TABLE `rental_ms_rekanan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rental_ms_kategori_rekanan_id` int(10) NOT NULL,
  `nama_perusahaan` varchar(40) NOT NULL,
  `npwp_perusahaan` varchar(25) NOT NULL,
  `email` varchar(30) NOT NULL,
  `nomor_telp_perusahaan` varchar(16) NOT NULL,
  `no_fax` varchar(225) NOT NULL,
  `alamat_perusahaan` text NOT NULL,
  `created_at` date DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_ms_rekanan
-- ----------------------------
INSERT INTO `rental_ms_rekanan` VALUES ('1', '1', 'HONDA CEMPAKA PUTIH', '2121', 'aiqbalsyah@gmail.com', '2122112', '', 'adddaa', '2017-05-23', '2', '2017-05-23', '2');
INSERT INTO `rental_ms_rekanan` VALUES ('2', '2', 'PT ZAMASCO', '131243556545557', 'arfianagung@gmail.com', '0218394737', '', 'jl. margonda raya', '2017-07-05', '2', '2017-07-05', '2');
INSERT INTO `rental_ms_rekanan` VALUES ('3', '3', 'SINAR MAS', '32190219301', 'sinarmas@gmail.com', '1320183218', '', 'jldsadlksadjsal adsdsadsad', '2017-08-16', '2', '2017-08-16', null);
INSERT INTO `rental_ms_rekanan` VALUES ('4', '4', 'AUTO2000 CEMPAKA MAS', '21321321321', '323213213', '213213213', '', '321321321', '2017-08-16', '2', '2017-08-16', null);
INSERT INTO `rental_ms_rekanan` VALUES ('5', '1', 'Honda Salemba', '23132981', 'salemba@gmail.com', '0389283921', '', 'JAkarata', '2017-09-06', '5', '2017-09-06', null);

-- ----------------------------
-- Table structure for rental_ms_tarif
-- ----------------------------
DROP TABLE IF EXISTS `rental_ms_tarif`;
CREATE TABLE `rental_ms_tarif` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rental_ms_kategori_kendaraan_id` int(10) unsigned DEFAULT NULL,
  `tarif` int(11) NOT NULL,
  `lama_rental` tinyint(3) NOT NULL,
  `periode_rental` enum('hari','minggu','bulan') NOT NULL,
  `jenis_tarif` enum('umum','khusus') NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rental_ms_tarif_rental_ms_kategori_kendaraan_id_foreign` (`rental_ms_kategori_kendaraan_id`),
  CONSTRAINT `rental_ms_tarif_ibfk_1` FOREIGN KEY (`rental_ms_kategori_kendaraan_id`) REFERENCES `rental_ms_kategori_kendaraan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_ms_tarif
-- ----------------------------
INSERT INTO `rental_ms_tarif` VALUES ('1', '2', '20000', '10', 'hari', 'umum', '2017-05-23 07:12:01', '2', '2017-05-23 07:12:01', null);
INSERT INTO `rental_ms_tarif` VALUES ('2', '3', '1000000', '7', 'hari', 'umum', '2017-07-05 10:45:27', '2', '2017-07-05 10:45:27', null);
INSERT INTO `rental_ms_tarif` VALUES ('3', '2', '1000000', '10', 'hari', 'umum', '2017-07-11 10:24:10', '2', '2017-07-11 10:24:10', null);

-- ----------------------------
-- Table structure for rental_ms_tarif_khusus
-- ----------------------------
DROP TABLE IF EXISTS `rental_ms_tarif_khusus`;
CREATE TABLE `rental_ms_tarif_khusus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rental_ms_tarif_id` int(10) unsigned NOT NULL,
  `rental_ms_rekanan_id` int(10) unsigned NOT NULL,
  `tarif` decimal(8,2) NOT NULL,
  `jenis_tarif` enum('percen','rupiah') NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rental_ms_tarif_khusus_rental_ms_tarif_id_foreign` (`rental_ms_tarif_id`),
  KEY `rental_ms_tarif_khusus_rental_ms_rekanan_id_foreign` (`rental_ms_rekanan_id`),
  CONSTRAINT `rental_ms_tarif_khusus_ibfk_1` FOREIGN KEY (`rental_ms_rekanan_id`) REFERENCES `rental_ms_rekanan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rental_ms_tarif_khusus_ibfk_2` FOREIGN KEY (`rental_ms_tarif_id`) REFERENCES `rental_ms_tarif` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_ms_tarif_khusus
-- ----------------------------
INSERT INTO `rental_ms_tarif_khusus` VALUES ('1', '2', '2', '999999.99', 'rupiah', '2017-07-05 10:54:20', '2', '2017-07-05 10:54:36', null);

-- ----------------------------
-- Table structure for rental_mutasi_bpkb
-- ----------------------------
DROP TABLE IF EXISTS `rental_mutasi_bpkb`;
CREATE TABLE `rental_mutasi_bpkb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rental_kendaraan_id` int(11) NOT NULL,
  `nomor_mutasi` varchar(225) NOT NULL,
  `tgl_mutasi` date NOT NULL,
  `type` enum('IN','OUT') NOT NULL,
  `peminjam` varchar(225) NOT NULL,
  `approval` varchar(225) NOT NULL,
  `kelengkapan_1` varchar(225) NOT NULL,
  `kelengkapan_2` varchar(225) NOT NULL,
  `kelengkapan_3` varchar(225) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_mutasi_bpkb
-- ----------------------------
INSERT INTO `rental_mutasi_bpkb` VALUES ('1', '1', '1/EPS.RENT.BPKB-OUT/IX/2017', '2017-09-14', 'OUT', 'Ardi', 'Bpk. Iwan', 'STNK', '', '', '2017-09-06 11:09:52', '5', '2017-09-06 11:09:52', null);

-- ----------------------------
-- Table structure for rental_pengalihan_kendaraan
-- ----------------------------
DROP TABLE IF EXISTS `rental_pengalihan_kendaraan`;
CREATE TABLE `rental_pengalihan_kendaraan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rental_kendaraan_id` int(11) NOT NULL,
  `type` enum('DIALIHKAN','DIJUAL') NOT NULL,
  `tgl` date NOT NULL,
  `harga_jual` decimal(22,2) NOT NULL,
  `harga_beli` decimal(22,2) NOT NULL,
  `biaya` decimal(22,2) NOT NULL,
  `pendapatan` decimal(22,2) NOT NULL,
  `laba_rugi` decimal(22,2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_pengalihan_kendaraan
-- ----------------------------
INSERT INTO `rental_pengalihan_kendaraan` VALUES ('1', '6', 'DIALIHKAN', '2017-08-07', '100000000.00', '149000000.00', '1000000.00', '1000000.00', '-49000000.00', '0', '2017-08-30 10:27:23', '2', '2017-08-30 10:27:23', null);

-- ----------------------------
-- Table structure for rental_permintaan_user
-- ----------------------------
DROP TABLE IF EXISTS `rental_permintaan_user`;
CREATE TABLE `rental_permintaan_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rental_kendaraan_id` int(11) NOT NULL,
  `peminjam` varchar(225) NOT NULL,
  `keperluan` varchar(225) NOT NULL,
  `tgl` date NOT NULL,
  `status` int(1) DEFAULT '0',
  `approved_by` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_permintaan_user
-- ----------------------------
INSERT INTO `rental_permintaan_user` VALUES ('1', '1', 'adsads', 'fdsldlkjsajdlksa', '2017-08-16', '1', '0', '2', '2017-08-29 11:51:04', null, '2017-08-29 12:00:03');

-- ----------------------------
-- Table structure for rental_pks
-- ----------------------------
DROP TABLE IF EXISTS `rental_pks`;
CREATE TABLE `rental_pks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rental_spk_id` int(11) NOT NULL,
  `tgl_pks` date DEFAULT NULL,
  `nomor_pks` varchar(255) DEFAULT NULL,
  `masa_kontrak` int(11) NOT NULL,
  `total` decimal(22,2) NOT NULL,
  `discount` decimal(22,2) NOT NULL,
  `ppn` decimal(22,2) NOT NULL,
  `grand_total` decimal(22,2) NOT NULL,
  `status` int(11) DEFAULT '0',
  `status_invoice` int(11) DEFAULT '0',
  `status_adendum` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_pks
-- ----------------------------
INSERT INTO `rental_pks` VALUES ('1', '1', '2017-08-26', '1/EPS.RENT/PKS/VIII/2017', '12', '156000000.00', '0.00', '15600000.00', '171600000.00', '0', '1', '1', '2', '2017-08-30 09:21:56', null, '2017-08-30 09:21:56');
INSERT INTO `rental_pks` VALUES ('2', '2', '2017-09-13', '1/EPS.RENT/PKS/IX/2017', '10', '30000000.00', '0.00', '3000000.00', '33000000.00', '0', '1', '1', '5', '2017-09-06 10:52:19', null, '2017-09-06 10:52:19');

-- ----------------------------
-- Table structure for rental_pks_detail
-- ----------------------------
DROP TABLE IF EXISTS `rental_pks_detail`;
CREATE TABLE `rental_pks_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rental_pks_id` int(11) NOT NULL,
  `rental_delivery_detail_id` int(11) NOT NULL,
  `rental_kendaraan_id` int(11) NOT NULL,
  `lama_sewa` int(11) NOT NULL,
  `harga` decimal(30,2) NOT NULL,
  `subtotal` decimal(30,2) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_pks_detail
-- ----------------------------
INSERT INTO `rental_pks_detail` VALUES ('1', '1', '5', '5', '12', '1000000.00', '12000000.00', '2017-08-29 10:12:32', '2017-08-29 10:12:32');
INSERT INTO `rental_pks_detail` VALUES ('2', '1', '6', '6', '12', '1000000.00', '12000000.00', '2017-08-29 10:12:32', '2017-08-29 10:12:32');
INSERT INTO `rental_pks_detail` VALUES ('3', '1', '7', '7', '12', '1000000.00', '12000000.00', '2017-08-29 10:12:32', '2017-08-29 10:12:32');
INSERT INTO `rental_pks_detail` VALUES ('4', '1', '8', '8', '12', '1000000.00', '12000000.00', '2017-08-29 10:12:32', '2017-08-29 10:12:32');
INSERT INTO `rental_pks_detail` VALUES ('5', '1', '9', '9', '12', '1000000.00', '12000000.00', '2017-08-29 10:12:32', '2017-08-29 10:12:32');
INSERT INTO `rental_pks_detail` VALUES ('6', '1', '1', '1', '12', '2000000.00', '24000000.00', '2017-08-29 10:12:32', '2017-08-29 10:12:32');
INSERT INTO `rental_pks_detail` VALUES ('7', '1', '2', '2', '12', '2000000.00', '24000000.00', '2017-08-29 10:12:32', '2017-08-29 10:12:32');
INSERT INTO `rental_pks_detail` VALUES ('8', '1', '3', '3', '12', '2000000.00', '24000000.00', '2017-08-29 10:12:32', '2017-08-29 10:12:32');
INSERT INTO `rental_pks_detail` VALUES ('9', '1', '4', '4', '12', '2000000.00', '24000000.00', '2017-08-29 10:12:32', '2017-08-29 10:12:32');
INSERT INTO `rental_pks_detail` VALUES ('10', '2', '10', '12', '10', '1000000.00', '10000000.00', '2017-09-06 10:45:43', '2017-09-06 10:45:43');
INSERT INTO `rental_pks_detail` VALUES ('11', '2', '11', '10', '10', '1000000.00', '10000000.00', '2017-09-06 10:45:43', '2017-09-06 10:45:43');
INSERT INTO `rental_pks_detail` VALUES ('12', '2', '12', '11', '10', '1000000.00', '10000000.00', '2017-09-06 10:45:43', '2017-09-06 10:45:43');

-- ----------------------------
-- Table structure for rental_po
-- ----------------------------
DROP TABLE IF EXISTS `rental_po`;
CREATE TABLE `rental_po` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor_po` varchar(225) NOT NULL,
  `rental_spk_id` int(11) NOT NULL,
  `rental_ms_rekanan_id` int(11) NOT NULL,
  `tgl_po` date NOT NULL,
  `total` decimal(22,2) NOT NULL,
  `discount` decimal(22,2) NOT NULL,
  `grand_total` decimal(22,2) NOT NULL,
  `status` int(11) DEFAULT '0',
  `count_receive` int(11) DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_po
-- ----------------------------
INSERT INTO `rental_po` VALUES ('1', '1/EPS.RENT/PO/VIII/2017', '1', '1', '2017-08-22', '1133000000.00', '0.00', '1133000000.00', '2', '0', '2', '2017-08-29 10:11:37', '0', '2017-08-29 10:11:37');
INSERT INTO `rental_po` VALUES ('2', '1/EPS.RENT/PO/IX/2017', '2', '1', '2017-09-12', '294000000.00', '0.00', '294000000.00', '2', '0', '5', '2017-09-06 10:43:10', '0', '2017-09-06 10:43:10');
INSERT INTO `rental_po` VALUES ('3', '2/EPS.RENT/PO/IX/2017', '2', '5', '2017-09-30', '5176000000.00', '0.00', '5176000000.00', '0', '0', '5', '2017-09-06 10:39:45', '0', '2017-09-06 10:39:45');

-- ----------------------------
-- Table structure for rental_po_detail
-- ----------------------------
DROP TABLE IF EXISTS `rental_po_detail`;
CREATE TABLE `rental_po_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rental_po_id` int(11) NOT NULL,
  `rental_spk_id` int(11) NOT NULL,
  `rental_spk_detail_id` int(11) NOT NULL,
  `rental_ms_kendaraan_id` int(11) NOT NULL,
  `warna` varchar(225) NOT NULL,
  `tahun` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga` decimal(22,2) NOT NULL,
  `discount` decimal(22,2) NOT NULL,
  `subtotal` decimal(22,2) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_po_detail
-- ----------------------------
INSERT INTO `rental_po_detail` VALUES ('1', '1', '1', '2', '1', 'BIRU', '2017', '4', '100000000.00', '3000000.00', '388000000.00', '2017-08-29 10:10:51', '2017-08-29 10:10:51');
INSERT INTO `rental_po_detail` VALUES ('2', '1', '1', '1', '2', 'MERAH', '2017', '5', '150000000.00', '1000000.00', '745000000.00', '2017-08-29 10:10:51', '2017-08-29 10:10:51');
INSERT INTO `rental_po_detail` VALUES ('3', '2', '2', '4', '4', 'BIRU', '2017', '3', '100000000.00', '2000000.00', '294000000.00', '2017-09-06 10:37:36', '2017-09-06 10:37:36');
INSERT INTO `rental_po_detail` VALUES ('4', '3', '2', '4', '4', 'HITAM', '2017', '2', '100000000.00', '2000000.00', '196000000.00', '2017-09-06 10:39:45', '2017-09-06 10:39:45');
INSERT INTO `rental_po_detail` VALUES ('5', '3', '2', '3', '2', 'SILVER', '2017', '10', '500000000.00', '2000000.00', '4980000000.00', '2017-09-06 10:39:45', '2017-09-06 10:39:45');

-- ----------------------------
-- Table structure for rental_receive
-- ----------------------------
DROP TABLE IF EXISTS `rental_receive`;
CREATE TABLE `rental_receive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor_receive` varchar(225) NOT NULL,
  `rental_po_id` int(11) NOT NULL,
  `tempat_penerimaan` int(11) NOT NULL,
  `tgl_receive` date NOT NULL,
  `status_pks` int(1) DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_receive
-- ----------------------------
INSERT INTO `rental_receive` VALUES ('1', '1/EPS.RENT/RC/VIII/2017', '1', '1', '2017-08-31', '0', '2', '2017-08-29 10:11:37', '0', '2017-08-29 10:11:37');
INSERT INTO `rental_receive` VALUES ('2', '1/EPS.RENT/RC/IX/2017', '2', '1', '2017-09-13', '0', '5', '2017-09-06 10:42:25', '0', '2017-09-06 10:42:25');
INSERT INTO `rental_receive` VALUES ('3', '2/EPS.RENT/RC/IX/2017', '2', '2', '2017-09-16', '0', '5', '2017-09-06 10:43:10', '0', '2017-09-06 10:43:10');

-- ----------------------------
-- Table structure for rental_receive_detail
-- ----------------------------
DROP TABLE IF EXISTS `rental_receive_detail`;
CREATE TABLE `rental_receive_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rental_receive_id` int(11) NOT NULL,
  `rental_spk_detail_id` int(11) NOT NULL,
  `rental_po_detail_id` int(10) unsigned NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rental_ms_kendaraan_rental_ms_kategori_kendaran_id_foreign` (`rental_po_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_receive_detail
-- ----------------------------
INSERT INTO `rental_receive_detail` VALUES ('1', '1', '2', '1', '0', '2017-08-29 10:11:37', '0', '2017-08-29 10:11:37', null);
INSERT INTO `rental_receive_detail` VALUES ('2', '1', '2', '1', '0', '2017-08-29 10:11:37', '0', '2017-08-29 10:11:37', null);
INSERT INTO `rental_receive_detail` VALUES ('3', '1', '2', '1', '0', '2017-08-29 10:11:37', '0', '2017-08-29 10:11:37', null);
INSERT INTO `rental_receive_detail` VALUES ('4', '1', '2', '1', '0', '2017-08-29 10:11:37', '0', '2017-08-29 10:11:37', null);
INSERT INTO `rental_receive_detail` VALUES ('5', '1', '1', '2', '0', '2017-08-29 10:11:37', '0', '2017-08-29 10:11:37', null);
INSERT INTO `rental_receive_detail` VALUES ('6', '1', '1', '2', '0', '2017-08-29 10:11:37', '0', '2017-08-29 10:11:37', null);
INSERT INTO `rental_receive_detail` VALUES ('7', '1', '1', '2', '0', '2017-08-29 10:11:37', '0', '2017-08-29 10:11:37', null);
INSERT INTO `rental_receive_detail` VALUES ('8', '1', '1', '2', '0', '2017-08-29 10:11:37', '0', '2017-08-29 10:11:37', null);
INSERT INTO `rental_receive_detail` VALUES ('9', '1', '1', '2', '0', '2017-08-29 10:11:37', '0', '2017-08-29 10:11:37', null);
INSERT INTO `rental_receive_detail` VALUES ('10', '2', '4', '3', '0', '2017-09-06 10:42:25', '0', '2017-09-06 10:42:25', null);
INSERT INTO `rental_receive_detail` VALUES ('11', '2', '4', '3', '0', '2017-09-06 10:42:25', '0', '2017-09-06 10:42:25', null);
INSERT INTO `rental_receive_detail` VALUES ('12', '3', '4', '3', '0', '2017-09-06 10:43:10', '0', '2017-09-06 10:43:10', null);

-- ----------------------------
-- Table structure for rental_register_asuransi
-- ----------------------------
DROP TABLE IF EXISTS `rental_register_asuransi`;
CREATE TABLE `rental_register_asuransi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rental_kendaraan_id` int(11) NOT NULL,
  `rental_rekanan_id` int(11) NOT NULL,
  `nominal` decimal(22,2) NOT NULL,
  `tgl_jatuh_tempo` date NOT NULL,
  `date_start` date DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `bln_update` date NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_register_asuransi
-- ----------------------------
INSERT INTO `rental_register_asuransi` VALUES ('1', '1', '3', '300000000.00', '2017-08-30', null, '1', '2018-09-12', '2017-08-30 12:42:24', '2', '2017-08-30 12:58:21', null);

-- ----------------------------
-- Table structure for rental_register_bpkb
-- ----------------------------
DROP TABLE IF EXISTS `rental_register_bpkb`;
CREATE TABLE `rental_register_bpkb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rental_kendaraan_id` int(11) NOT NULL,
  `rental_receive_detail_id` int(11) NOT NULL,
  `nomor_polisi` varchar(225) NOT NULL,
  `nomor_bpkb` varchar(225) NOT NULL,
  `bahan_bakar` varchar(225) NOT NULL,
  `file` varchar(225) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_register_bpkb
-- ----------------------------
INSERT INTO `rental_register_bpkb` VALUES ('1', '1', '1', '', '213213213213', 'PREMIUM', '213213213213.jpg', '5', '2017-09-06 11:08:16', '0', '2017-09-06 11:08:16');

-- ----------------------------
-- Table structure for rental_register_leasing
-- ----------------------------
DROP TABLE IF EXISTS `rental_register_leasing`;
CREATE TABLE `rental_register_leasing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rental_kendaraan_id` int(11) NOT NULL,
  `rental_rekanan_id` int(11) NOT NULL,
  `nominal` decimal(22,2) NOT NULL,
  `dp` decimal(22,2) NOT NULL,
  `tenor` int(11) NOT NULL,
  `cicilan` decimal(22,2) NOT NULL,
  `adm` decimal(22,2) NOT NULL,
  `tgl_jatuh_tempo` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `bln_update` date NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_register_leasing
-- ----------------------------
INSERT INTO `rental_register_leasing` VALUES ('2', '2', '3', '0.00', '10000000.00', '12', '3000000.00', '150000.00', '16', '1', '2017-09-16', '2017-08-30 14:18:30', '2', '2017-08-30 14:18:30', null);

-- ----------------------------
-- Table structure for rental_register_service
-- ----------------------------
DROP TABLE IF EXISTS `rental_register_service`;
CREATE TABLE `rental_register_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rental_kendaraan_id` int(11) NOT NULL,
  `rental_rekanan_id` int(11) NOT NULL,
  `tgl_jatuh_tempo` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `bln_update` date NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_register_service
-- ----------------------------

-- ----------------------------
-- Table structure for rental_service
-- ----------------------------
DROP TABLE IF EXISTS `rental_service`;
CREATE TABLE `rental_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rental_rekanan_id` int(11) NOT NULL,
  `km` varchar(225) NOT NULL,
  `nomor_spk` varchar(225) NOT NULL,
  `tgl_spk` date NOT NULL,
  `tgl_service` date NOT NULL,
  `tgl_booking` date NOT NULL,
  `rental_kendaraan_id` int(11) NOT NULL,
  `pemakai` varchar(225) NOT NULL,
  `atas_nama` varchar(225) NOT NULL,
  `keperluan` text NOT NULL,
  `suku_cadang` text NOT NULL,
  `acecoris_dll` text NOT NULL,
  `keterangan` text NOT NULL,
  `catatan` text NOT NULL,
  `sa` varchar(225) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_service
-- ----------------------------
INSERT INTO `rental_service` VALUES ('1', '3', '01000', '1/EPS/SPK/RENT/VIII/2017', '2017-08-11', '2017-08-15', '2017-08-16', '1', 'dsadsad', 'sadsadsadsa', 'dsadsad', 'sdsadsads', 'adsdsadsadsadsdsa', 'dsadsad', 'dsadsa', 'dsadsadsadsa', '2017-08-29 12:34:58', '2', '2017-08-30 09:36:09', '2');

-- ----------------------------
-- Table structure for rental_service_detail
-- ----------------------------
DROP TABLE IF EXISTS `rental_service_detail`;
CREATE TABLE `rental_service_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_pemeliharaan` varchar(225) NOT NULL,
  `nominal` decimal(22,2) NOT NULL,
  `keterangan` varchar(225) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_service_detail
-- ----------------------------

-- ----------------------------
-- Table structure for rental_spk
-- ----------------------------
DROP TABLE IF EXISTS `rental_spk`;
CREATE TABLE `rental_spk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `nama_pengerjaan` varchar(225) NOT NULL,
  `tgl_spk` date DEFAULT NULL,
  `nomor_spk` varchar(255) DEFAULT NULL,
  `masa_kontrak` int(11) NOT NULL,
  `rental_ms_customer_id` int(11) DEFAULT NULL,
  `total` decimal(22,2) NOT NULL,
  `discount` decimal(22,2) NOT NULL,
  `ppn` decimal(22,2) NOT NULL,
  `grand_total` decimal(22,2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `status_po` int(11) DEFAULT '0',
  `status_po_delete` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_spk
-- ----------------------------
INSERT INTO `rental_spk` VALUES ('1', '0', 'PENGERJAAN', '2017-08-22', 'SPK29183291', '12', '1', '156000000.00', '0.00', '15600000.00', '171600000.00', '5', '1', '1', '2', '2017-08-29 10:12:32', null, '2017-08-29 10:12:32');
INSERT INTO `rental_spk` VALUES ('2', '0', 'Pengadaan Sewa Unit Manager Area', '2017-09-13', 'SPK-CONTOH-01', '10', '3', '200000000.00', '0.00', '20000000.00', '220000000.00', '5', '1', '1', '5', '2017-09-06 10:45:43', null, '2017-09-06 10:45:43');

-- ----------------------------
-- Table structure for rental_spk_detail
-- ----------------------------
DROP TABLE IF EXISTS `rental_spk_detail`;
CREATE TABLE `rental_spk_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rental_spk_id` int(11) NOT NULL,
  `rental_ms_kendaraan_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `lama_sewa` int(11) NOT NULL,
  `harga` decimal(30,2) NOT NULL,
  `subtotal` decimal(30,2) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rental_spk_detail
-- ----------------------------
INSERT INTO `rental_spk_detail` VALUES ('1', '1', '2', '5', '12', '1000000.00', '60000000.00', '2017-08-29 10:09:07', '2017-08-29 10:09:07');
INSERT INTO `rental_spk_detail` VALUES ('2', '1', '1', '4', '12', '2000000.00', '96000000.00', '2017-08-29 10:09:07', '2017-08-29 10:09:07');
INSERT INTO `rental_spk_detail` VALUES ('3', '2', '2', '10', '10', '1500000.00', '150000000.00', '2017-09-06 10:34:05', '2017-09-06 10:34:05');
INSERT INTO `rental_spk_detail` VALUES ('4', '2', '4', '5', '10', '1000000.00', '50000000.00', '2017-09-06 10:34:05', '2017-09-06 10:34:05');
