/*
Navicat MySQL Data Transfer

Source Server         : Local
Source Server Version : 100125
Source Host           : localhost:3306
Source Database       : db_era

Target Server Type    : MYSQL
Target Server Version : 100125
File Encoding         : 65001

Date: 2017-09-07 10:58:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sdm_ct_cuti_bersama
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ct_cuti_bersama`;
CREATE TABLE `sdm_ct_cuti_bersama` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tahun` smallint(4) NOT NULL,
  `jumlah_hari` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ct_cuti_bersama
-- ----------------------------
INSERT INTO `sdm_ct_cuti_bersama` VALUES ('2', '2016', '8', '2017-07-19 14:29:23', null, '2017-08-15 11:22:47', '2');
INSERT INTO `sdm_ct_cuti_bersama` VALUES ('7', '2017', '5', '2017-07-19 14:42:17', '2', '2017-07-19 14:43:58', '2');

-- ----------------------------
-- Table structure for sdm_emp_data_disiplin
-- ----------------------------
DROP TABLE IF EXISTS `sdm_emp_data_disiplin`;
CREATE TABLE `sdm_emp_data_disiplin` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sdm_ms_pegawai_id` int(10) DEFAULT NULL,
  `sdm_ms_pegawai_nik` varchar(30) NOT NULL,
  `peringatan` varchar(255) DEFAULT NULL,
  `keterangan` text,
  `no_sk` varchar(50) DEFAULT NULL,
  `nama_jabatan_penerbit` varchar(100) DEFAULT NULL,
  `nama_pejabat_penerbit` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_emp_data_disiplin
-- ----------------------------
INSERT INTO `sdm_emp_data_disiplin` VALUES ('2', '1', '26464363252', 'Teguran Lisan', 'haha', '0581250', 'aja', 'anam', '2017-05-02 03:30:55', '2', '2017-05-22 16:11:46', null);
INSERT INTO `sdm_emp_data_disiplin` VALUES ('3', null, '94848125128571', 'Pernyataan tidak puas tertulis', 'kahakah 2', '048129', '2141', 'adaw', '2017-05-22 09:05:25', '2', '2017-05-22 09:07:25', null);

-- ----------------------------
-- Table structure for sdm_emp_data_jabatan
-- ----------------------------
DROP TABLE IF EXISTS `sdm_emp_data_jabatan`;
CREATE TABLE `sdm_emp_data_jabatan` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sdm_ms_pegawai_id` int(10) DEFAULT NULL,
  `sdm_ms_pegawai_nik` varchar(30) NOT NULL,
  `sdm_ms_jabatan_id` int(10) NOT NULL,
  `sdm_ms_bagian_id` int(10) NOT NULL,
  `no_sk` varchar(50) DEFAULT NULL,
  `tgl_sk` date DEFAULT NULL,
  `tmt_sk` date DEFAULT NULL,
  `nama_jabatan_penerbit` varchar(100) DEFAULT NULL,
  `nama_pejabat_penerbit` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_emp_data_jabatan
-- ----------------------------
INSERT INTO `sdm_emp_data_jabatan` VALUES ('2', null, '94848125128571', '1', '1', '51251251', '2017-05-29', '2017-05-22', 'sad', 'cx', '2017-05-22 07:51:50', '2', '2017-05-22 07:51:50', null);

-- ----------------------------
-- Table structure for sdm_ms_agama
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_agama`;
CREATE TABLE `sdm_ms_agama` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `agama` varchar(20) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_agama
-- ----------------------------
INSERT INTO `sdm_ms_agama` VALUES ('1', 'Islam', '2017-04-11 05:21:25', '2', '2017-04-11 05:21:25', null);
INSERT INTO `sdm_ms_agama` VALUES ('2', 'Hindu', '2017-09-06 14:11:12', '2', '2017-09-06 14:11:12', null);
INSERT INTO `sdm_ms_agama` VALUES ('3', 'Budha', '2017-09-06 14:11:26', '2', '2017-09-06 14:11:26', null);
INSERT INTO `sdm_ms_agama` VALUES ('4', 'Kristen Protestan', '2017-09-06 14:11:45', '2', '2017-09-06 14:11:45', null);
INSERT INTO `sdm_ms_agama` VALUES ('5', 'Katolik', '2017-09-06 14:12:02', '2', '2017-09-06 14:12:02', null);

-- ----------------------------
-- Table structure for sdm_ms_bagian
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_bagian`;
CREATE TABLE `sdm_ms_bagian` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode_perkiraan` varchar(15) NOT NULL,
  `nama_bagian` varchar(30) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_bagian
-- ----------------------------
INSERT INTO `sdm_ms_bagian` VALUES ('1', 'K04', 'Ekspedisi', '2017-04-11 05:13:04', '2', '2017-04-11 05:13:04', null);
INSERT INTO `sdm_ms_bagian` VALUES ('2', '', 'Kredit', null, '0', null, null);
INSERT INTO `sdm_ms_bagian` VALUES ('3', '', 'Outsourcing', null, '0', null, null);
INSERT INTO `sdm_ms_bagian` VALUES ('4', '', 'Rental', null, '0', null, null);
INSERT INTO `sdm_ms_bagian` VALUES ('5', '', 'Sdm', null, '0', null, null);
INSERT INTO `sdm_ms_bagian` VALUES ('6', '', 'Umum', null, '0', null, null);
INSERT INTO `sdm_ms_bagian` VALUES ('7', '', 'Usaha Lain', null, '0', null, null);
INSERT INTO `sdm_ms_bagian` VALUES ('9', '', 'percetakan', '2017-08-15 10:18:51', '2', '2017-08-15 10:18:51', null);

-- ----------------------------
-- Table structure for sdm_ms_biaya_perjalanan_dinas
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_biaya_perjalanan_dinas`;
CREATE TABLE `sdm_ms_biaya_perjalanan_dinas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_biaya` varchar(20) NOT NULL,
  `jenis_biaya` smallint(2) NOT NULL,
  `nama_jenis_biaya` varchar(255) DEFAULT NULL,
  `keterangan` text,
  `golongan` varchar(2) NOT NULL,
  `rata_rata` decimal(20,2) DEFAULT NULL,
  `papua_maluku` decimal(20,2) DEFAULT NULL,
  `manado_ntt` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_biaya_perjalanan_dinas
-- ----------------------------
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('8', '160517236862182', '4', 'Perjalanan Identil Yang Ditempuh PP (Tidak Menginap)', '> 60 s.d 75 Km', 'A', '165600.00', '187200.00', '180000.00', '2017-05-16 05:15:06', null, '2017-08-14 10:28:30', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('9', '160517236862182', '4', 'Perjalanan Identil Yang Ditempuh PP (Tidak Menginap)', '> 60 s.d 75 Km', 'B', '144000.00', '153000.00', '149400.00', '2017-05-16 05:15:06', null, '2017-08-14 10:28:30', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('10', '160517236862182', '4', 'Perjalanan Identil Yang Ditempuh PP (Tidak Menginap)', '> 60 s.d 75 Km', 'C', '108000.00', '126000.00', '114000.00', '2017-05-16 05:15:06', null, '2017-08-14 10:28:30', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('11', '160517236862182', '4', 'Perjalanan Identil Yang Ditempuh PP (Tidak Menginap)', '> 60 s.d 75 Km', 'D', '81000.00', '99000.00', '93600.00', '2017-05-16 05:15:06', null, '2017-08-14 10:28:30', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('12', '160517997335815', '2', 'Tempat Lain', 'Biaya Penginapan', 'A', '625000.00', '687000.00', '647000.00', '2017-05-16 05:24:17', null, '2017-08-14 10:17:54', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('13', '160517997335815', '2', 'Tempat Lain', 'Biaya Penginapan', 'B', '400000.00', '440000.00', '414000.00', '2017-05-16 05:24:17', null, '2017-08-14 10:17:54', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('14', '160517997335815', '2', 'Tempat Lain', 'Biaya Penginapan', 'C', '330000.00', '363000.00', '341000.00', '2017-05-16 05:24:17', null, '2017-08-14 10:17:54', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('15', '160517997335815', '2', 'Tempat Lain', 'Biaya Penginapan', 'D', '230000.00', '253000.00', '238000.00', '2017-05-16 05:24:17', null, '2017-08-14 10:17:54', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('16', '170517118759155', '3', 'Biaya Perjalanan Pulang', 'Biaya Perjalanan Pulang', 'A', '400000.00', '200000.00', '100000.00', '2017-05-17 03:10:45', null, '2017-05-17 03:10:45', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('17', '170517118759155', '3', 'Biaya Perjalanan Pulang', 'Biaya Perjalanan Pulang', 'B', '90000.00', '340000.00', '50100.00', '2017-05-17 03:10:45', null, '2017-05-17 03:10:45', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('18', '170517118759155', '3', 'Biaya Perjalanan Pulang', 'Biaya Perjalanan Pulang', 'C', '90000.00', '200000.00', '400000.00', '2017-05-17 03:10:45', null, '2017-05-17 03:10:45', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('19', '170517118759155', '3', 'Biaya Perjalanan Pulang', 'Biaya Perjalanan Pulang', 'D', '104032.00', '440000.00', '1000000.00', '2017-05-17 03:10:45', null, '2017-05-17 03:10:45', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('20', '170517674475097', '1', 'Ibu Kota Propinsi', 'Uang Harian', 'A', '250000.00', '275000.00', '259000.00', '2017-05-17 03:35:12', null, '2017-08-14 10:11:28', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('21', '170517674475097', '1', 'Ibu Kota Propinsi', 'Uang Harian', 'B', '175000.00', '192000.00', '181000.00', '2017-05-17 03:35:12', null, '2017-08-14 10:11:29', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('22', '170517674475097', '1', 'Ibu Kota Propinsi', 'Uang Harian', 'C', '125000.00', '137000.00', '129000.00', '2017-05-17 03:35:12', null, '2017-08-14 10:36:49', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('23', '170517674475097', '1', 'Ibu Kota Propinsi', 'Uang Harian', 'D', '100000.00', '110000.00', '103000.00', '2017-05-17 03:35:12', null, '2017-08-14 10:11:29', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('24', '170517529702758', '1', 'Ibu Kota Propinsi', 'Biaya Penginapan', 'A', '750000.00', '825000.00', '776000.00', '2017-05-17 03:36:50', null, '2017-08-14 10:12:42', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('25', '170517529702758', '1', 'Ibu Kota Propinsi', 'Biaya Penginapan', 'B', '500000.00', '550000.00', '515000.00', '2017-05-17 03:36:50', null, '2017-08-14 10:12:42', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('26', '170517529702758', '1', 'Ibu Kota Propinsi', 'Biaya Penginapan', 'C', '400000.00', '440000.00', '414000.00', '2017-05-17 03:36:50', null, '2017-08-14 10:12:42', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('27', '170517529702758', '1', 'Ibu Kota Propinsi', 'Biaya Penginapan', 'D', '300000.00', '330000.00', '310000.00', '2017-05-17 03:36:50', null, '2017-08-14 10:12:42', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('28', '170517964239501', '2', 'Tempat Lain', 'Uang Harian', 'A', '200000.00', '220000.00', '207000.00', '2017-05-17 03:38:13', null, '2017-08-14 10:19:06', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('29', '170517964239501', '2', 'Tempat Lain', 'Uang Harian', 'B', '150000.00', '165000.00', '155000.00', '2017-05-17 03:38:13', null, '2017-08-14 10:19:06', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('30', '170517964239501', '2', 'Tempat Lain', 'Uang Harian', 'C', '100000.00', '110000.00', '103000.00', '2017-05-17 03:38:13', null, '2017-08-14 10:19:06', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('31', '170517964239501', '2', 'Tempat Lain', 'Uang Harian', 'D', '75000.00', '82000.00', '77000.00', '2017-05-17 03:38:13', null, '2017-08-14 10:19:06', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('32', '170517442526245', '4', 'Perjalanan Identil Yang Ditempuh PP (Tidak Menginap)', '> 30 s.d 60 Km', 'A', '117000.00', '126000.00', '126000.00', '2017-05-17 03:39:15', null, '2017-08-14 10:25:52', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('33', '170517442526245', '4', 'Perjalanan Identil Yang Ditempuh PP (Tidak Menginap)', '> 30 s.d 60 Km', 'B', '99000.00', '108000.00', '102600.00', '2017-05-17 03:39:15', null, '2017-08-14 10:25:52', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('34', '170517442526245', '4', 'Perjalanan Identil Yang Ditempuh PP (Tidak Menginap)', '> 30 s.d 60 Km', 'C', '72000.00', '90000.00', '81000.00', '2017-05-17 03:39:15', null, '2017-08-14 10:25:52', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('35', '170517442526245', '4', 'Perjalanan Identil Yang Ditempuh PP (Tidak Menginap)', '> 30 s.d 60 Km', 'D', '63000.00', '63000.00', '68400.00', '2017-05-17 03:39:16', null, '2017-08-14 10:25:53', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('36', '140817344116210', '4', 'Perjalanan Identil Yang Ditempuh PP (Tidak Menginap)', '> 75 s.d 100 Km', 'A', '225000.00', '261000.00', '243000.00', '2017-08-14 10:45:53', null, '2017-08-14 10:45:53', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('37', '140817344116210', '4', 'Perjalanan Identil Yang Ditempuh PP (Tidak Menginap)', '> 75 s.d 100 Km', 'B', '198000.00', '126000.00', '208800.00', '2017-08-14 10:45:53', null, '2017-08-14 10:45:53', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('38', '140817344116210', '4', 'Perjalanan Identil Yang Ditempuh PP (Tidak Menginap)', '> 75 s.d 100 Km', 'C', '153000.00', '180000.00', '171000.00', '2017-08-14 10:45:53', null, '2017-08-14 10:45:53', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('39', '140817344116210', '4', 'Perjalanan Identil Yang Ditempuh PP (Tidak Menginap)', '> 75 s.d 100 Km', 'D', '126000.00', '135000.00', '135000.00', '2017-08-14 10:45:54', null, '2017-08-14 10:45:54', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('40', '140817745309448', '4', 'Perjalanan Identil Yang Ditempuh PP (Tidak Menginap)', '> 100 s.d 150 Km', 'A', '279000.00', '315000.00', '297000.00', '2017-08-14 10:47:30', null, '2017-08-14 10:47:30', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('41', '140817745309448', '4', 'Perjalanan Identil Yang Ditempuh PP (Tidak Menginap)', '> 100 s.d 150 Km', 'B', '234000.00', '270000.00', '252000.00', '2017-08-14 10:47:31', null, '2017-08-14 10:47:31', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('42', '140817745309448', '4', 'Perjalanan Identil Yang Ditempuh PP (Tidak Menginap)', '> 100 s.d 150 Km', 'C', '189000.00', '216000.00', '207000.00', '2017-08-14 10:47:31', null, '2017-08-14 10:47:31', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('43', '140817745309448', '4', 'Perjalanan Identil Yang Ditempuh PP (Tidak Menginap)', '> 100 s.d 150 Km', 'D', '153000.00', '171000.00', '158400.00', '2017-08-14 10:47:31', null, '2017-08-14 10:47:31', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('44', '140817434478759', '4', 'Perjalanan Identil Yang Ditempuh PP (Tidak Menginap)', '> 150 Km', 'A', '333000.00', '378000.00', '360000.00', '2017-08-14 10:49:27', null, '2017-08-14 10:49:27', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('45', '140817434478759', '4', 'Perjalanan Identil Yang Ditempuh PP (Tidak Menginap)', '> 150 Km', 'B', '279000.00', '315000.00', '300600.00', '2017-08-14 10:49:27', null, '2017-08-14 10:49:27', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('46', '140817434478759', '4', 'Perjalanan Identil Yang Ditempuh PP (Tidak Menginap)', '> 150 Km', 'C', '234000.00', '261000.00', '244800.00', '2017-08-14 10:49:27', null, '2017-08-14 10:49:27', null);
INSERT INTO `sdm_ms_biaya_perjalanan_dinas` VALUES ('47', '140817434478759', '4', 'Perjalanan Identil Yang Ditempuh PP (Tidak Menginap)', '> 150 Km', 'D', '176400.00', '199800.00', '187200.00', '2017-08-14 10:49:27', null, '2017-08-14 10:49:27', null);

-- ----------------------------
-- Table structure for sdm_ms_index_prestasi
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_index_prestasi`;
CREATE TABLE `sdm_ms_index_prestasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdm_ms_perhitungan_rating_id` int(11) DEFAULT NULL,
  `sdm_ms_bagian_id` int(11) DEFAULT NULL,
  `nilai_pertumbuhan` decimal(10,2) DEFAULT NULL,
  `nilai_kontribusi` decimal(10,2) DEFAULT NULL,
  `nilai_pertumbuhan_pendapatan` decimal(10,2) DEFAULT NULL,
  `nilai_pencapaian` decimal(10,2) DEFAULT NULL,
  `total_nilai` decimal(10,2) DEFAULT NULL,
  `index_prestasi` decimal(10,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_index_prestasi
-- ----------------------------
INSERT INTO `sdm_ms_index_prestasi` VALUES ('3', '26', '3', '1.08', '0.24', '0.22', '0.90', '2.44', '0.95', '2017-08-28 23:54:22', '2', '2017-08-29 13:54:22', null);
INSERT INTO `sdm_ms_index_prestasi` VALUES ('4', '26', '1', '1.80', '1.20', '0.22', '0.90', '4.12', '1.10', '2017-08-29 02:28:22', '2', '2017-08-29 16:28:22', null);

-- ----------------------------
-- Table structure for sdm_ms_jabatan
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_jabatan`;
CREATE TABLE `sdm_ms_jabatan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_jabatan` varchar(60) NOT NULL,
  `grade` int(4) NOT NULL,
  `posisi` int(4) NOT NULL,
  `upah_pokok` decimal(20,2) DEFAULT NULL,
  `tunjangan_tetap` decimal(20,2) DEFAULT NULL,
  `tunjangan_tidak_tetap` decimal(20,2) DEFAULT NULL,
  `tunjangan_pengelolaan` decimal(20,2) DEFAULT NULL,
  `total` decimal(20,2) DEFAULT NULL,
  `tunjangan_pakaian` decimal(20,2) DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `jenis` enum('tetap','kontrak') DEFAULT 'tetap',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_jabatan
-- ----------------------------
INSERT INTO `sdm_ms_jabatan` VALUES ('1', 'Staff Pelaksana', '1', '5', '2000000.00', '500000.00', '100000.00', '400000.00', '2900000.00', '15000000.00', null, 'tetap', '2017-04-11 05:09:32', '2', '2017-08-29 01:18:16', null);
INSERT INTO `sdm_ms_jabatan` VALUES ('2', 'Staf Administrasi I', '2', '1', '2670000.00', '890000.00', '180000.00', '1000000.00', '3740000.00', '6000000.00', null, 'tetap', '2017-05-05 08:15:22', '2', '2017-08-29 01:18:38', null);
INSERT INTO `sdm_ms_jabatan` VALUES ('3', 'Staf Administrasi II', '2', '4', '2670000.00', '890000.00', '840000.00', '500000.00', '4400000.00', '5500000.00', null, 'tetap', '2017-05-10 06:01:45', '2', '2017-08-29 01:18:44', null);
INSERT INTO `sdm_ms_jabatan` VALUES ('4', 'Asman Supporting Bisnis I', '3', '1', '3885000.00', '1295000.00', '1860000.00', '1100000.00', '7040000.00', '5000000.00', null, 'tetap', '2017-06-08 09:27:39', '2', '2017-08-29 01:18:46', null);
INSERT INTO `sdm_ms_jabatan` VALUES ('5', 'Asman Supporting Bisnis II', '3', '4', '3885000.00', '1295000.00', '2740000.00', '3000000.00', '7920000.00', '4500000.00', null, 'tetap', '2017-08-07 09:04:07', '2', '2017-08-29 01:18:41', null);
INSERT INTO `sdm_ms_jabatan` VALUES ('6', 'Asman Bisnis I', '3', '0', '3885000.00', '1295000.00', '2300000.00', null, '7480000.00', null, null, 'tetap', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('7', 'Asman Bisnis II', '3', '0', '3885000.00', '1295000.00', '3180000.00', null, '8360000.00', null, null, 'tetap', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('8', 'Manajer Supporting Bisnis I', '4', '0', '4854000.00', '1618000.00', '3538000.00', null, '10010000.00', null, null, 'tetap', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('9', 'Manajer Supporting Bisnis II', '4', '0', '4854000.00', '1618000.00', '4418000.00', null, '10890000.00', null, null, 'tetap', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('10', 'Manajer Bisnis I', '4', '0', '4854000.00', '1618000.00', '4088000.00', null, '10560000.00', null, null, 'tetap', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('11', 'Manajer Bisnis II', '4', '0', '4854000.00', '1618000.00', '4968000.00', null, '11440000.00', null, null, 'tetap', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('12', 'JM Supporting Bisnis I', '5', '0', '5835000.00', '1945000.00', '9820000.00', null, '17600000.00', null, null, 'tetap', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('13', 'JM Supporting Bisnis II', '5', '0', '5835000.00', '1945000.00', '10700000.00', null, '18480000.00', null, null, 'tetap', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('14', 'JM Bisnis I', '5', '0', '5835000.00', '1945000.00', '10370000.00', null, '18150000.00', null, null, 'tetap', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('15', 'JM Bisnis II', '5', '0', '5835000.00', '1945000.00', '11250000.00', null, '19030000.00', null, null, 'tetap', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('22', 'Staf Pelaksana', '1', '0', '2325000.00', '775000.00', '100000.00', null, '3200000.00', null, null, 'kontrak', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('23', 'Staf Administrasi I', '2', '0', '2670000.00', '890000.00', '180000.00', null, '3740000.00', null, null, 'kontrak', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('24', 'Staf Administrasi II', '2', '0', '2670000.00', '890000.00', '480000.00', null, '4040000.00', null, null, 'kontrak', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('25', 'Asman Supporting Bisnis I', '3', '0', '3885000.00', '1295000.00', '760000.00', null, '5940000.00', null, null, 'kontrak', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('26', 'Asman Supporting Bisnis II', '3', '0', '3885000.00', '1295000.00', '1260000.00', null, '6440000.00', null, null, 'kontrak', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('27', 'Asman Bisnis I', '3', '0', '3885000.00', '1295000.00', '1420000.00', null, '6600000.00', null, null, 'kontrak', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('28', 'Asman Bisnis II', '3', '0', '3885000.00', '1295000.00', '1920000.00', null, '7100000.00', null, null, 'kontrak', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('29', 'Manajer Supporting Bisnis I', '4', '0', '4854000.00', '1618000.00', '2438000.00', null, '8910000.00', null, null, 'kontrak', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('30', 'Manajer Supporting Bisnis II', '4', '0', '4854000.00', '1618000.00', '2938000.00', null, '9410000.00', null, null, 'kontrak', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('31', 'Manajer Bisnis I', '4', '0', '4854000.00', '1618000.00', '2988000.00', null, '9460000.00', null, null, 'kontrak', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('32', 'Manajer Bisnis II', '4', '0', '4854000.00', '1618000.00', '3488000.00', null, '9960000.00', null, null, 'kontrak', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('33', 'JM Supporting Bisnis I', '5', '0', '5835000.00', '1945000.00', '8720000.00', null, '16500000.00', null, null, 'kontrak', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('34', 'JM Supporting Bisnis II', '5', '0', '5835000.00', '1945000.00', '9220000.00', null, '17000000.00', null, null, 'kontrak', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('35', 'JM Bisnis I', '5', '0', '5835000.00', '1945000.00', '9270000.00', null, '17050000.00', null, null, 'kontrak', null, '0', null, null);
INSERT INTO `sdm_ms_jabatan` VALUES ('36', 'JM Bisnis II', '5', '0', '5835000.00', '1945000.00', '9770000.00', null, '17550000.00', null, null, 'kontrak', null, '0', null, null);

-- ----------------------------
-- Table structure for sdm_ms_jabatan_old
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_jabatan_old`;
CREATE TABLE `sdm_ms_jabatan_old` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_jabatan` varchar(60) NOT NULL,
  `grade` int(4) NOT NULL,
  `posisi` int(4) NOT NULL,
  `upah_pokok` decimal(20,2) DEFAULT NULL,
  `tunjangan_tetap` decimal(20,2) DEFAULT NULL,
  `tunjangan_tidak_tetap` decimal(20,2) DEFAULT NULL,
  `tunjangan_pengelolaan` decimal(20,2) DEFAULT NULL,
  `total` decimal(20,2) DEFAULT NULL,
  `tunjangan_pakaian` decimal(20,2) DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_jabatan_old
-- ----------------------------
INSERT INTO `sdm_ms_jabatan_old` VALUES ('1', 'Staff Pelaksana', '1', '5', '2000000.00', '500000.00', '100000.00', '400000.00', '2900000.00', '15000000.00', null, '2017-04-11 05:09:32', '2', '2017-08-29 01:18:16', null);
INSERT INTO `sdm_ms_jabatan_old` VALUES ('2', 'Staff Administrasi I', '2', '1', '2670000.00', '200000.00', '1000000.00', '1000000.00', '4200000.00', '6000000.00', null, '2017-05-05 08:15:22', '2', '2017-08-29 01:18:38', null);
INSERT INTO `sdm_ms_jabatan_old` VALUES ('3', 'Staff Administrasi II', '2', '4', '2670000.00', '400000.00', '100000.00', '500000.00', '2900000.00', '5500000.00', null, '2017-05-10 06:01:45', '2', '2017-08-29 01:18:44', null);
INSERT INTO `sdm_ms_jabatan_old` VALUES ('4', 'Asmen Supporting Bisnis', '3', '1', '1200000.00', '1100000.00', '1300000.00', '1100000.00', '3400000.00', '5000000.00', null, '2017-06-08 09:27:39', '2', '2017-08-29 01:18:46', null);
INSERT INTO `sdm_ms_jabatan_old` VALUES ('5', 'Asmen Supporting Bisnis I', '3', '4', '2000000.00', '3000000.00', '100000.00', '3000000.00', '8000000.00', '4500000.00', null, '2017-08-07 09:04:07', '2', '2017-08-29 01:18:41', null);
INSERT INTO `sdm_ms_jabatan_old` VALUES ('6', 'Asmen Supporting Bisnis II', '3', '0', '3885000.00', '1295000.00', '2740000.00', null, null, null, null, null, '0', null, null);
INSERT INTO `sdm_ms_jabatan_old` VALUES ('7', 'Asman Bisnis \r\n', '3', '0', null, null, null, null, null, null, null, null, '0', null, null);
INSERT INTO `sdm_ms_jabatan_old` VALUES ('8', 'Asman Bisnis I\r\n', '3', '0', null, null, null, null, null, null, null, null, '0', null, null);
INSERT INTO `sdm_ms_jabatan_old` VALUES ('9', 'Asman Bisnis II\r\n', '3', '0', null, null, null, null, null, null, null, null, '0', null, null);
INSERT INTO `sdm_ms_jabatan_old` VALUES ('10', 'Manajer Supporting Bisnis\r\n', '4', '0', null, null, null, null, null, null, null, null, '0', null, null);
INSERT INTO `sdm_ms_jabatan_old` VALUES ('11', 'Manajer Supporting Bisnis I\r\n', '4', '0', null, null, null, null, null, null, null, null, '0', null, null);
INSERT INTO `sdm_ms_jabatan_old` VALUES ('12', 'Manajer Supporting Bisnis II\r\n', '4', '0', null, null, null, null, null, null, null, null, '0', null, null);
INSERT INTO `sdm_ms_jabatan_old` VALUES ('13', 'Manajer Bisnis\r\n', '4', '0', null, null, null, null, null, null, null, null, '0', null, null);
INSERT INTO `sdm_ms_jabatan_old` VALUES ('14', 'Manajer Bisnis I\r\n', '4', '0', null, null, null, null, null, null, null, null, '0', null, null);
INSERT INTO `sdm_ms_jabatan_old` VALUES ('15', 'Manajer Bisnis II\r\n', '4', '0', null, null, null, null, null, null, null, null, '0', null, null);
INSERT INTO `sdm_ms_jabatan_old` VALUES ('16', 'JM Supporting Bisnis\r\n', '5', '0', null, null, null, null, null, null, null, null, '0', null, null);
INSERT INTO `sdm_ms_jabatan_old` VALUES ('17', 'JM Supporting Bisnis I\r\n', '5', '0', null, null, null, null, null, null, null, null, '0', null, null);
INSERT INTO `sdm_ms_jabatan_old` VALUES ('18', 'JM Supporting Bisnis II\r\n', '5', '0', null, null, null, null, null, null, null, null, '0', null, null);
INSERT INTO `sdm_ms_jabatan_old` VALUES ('19', 'JM Bisnis\r\n', '5', '0', null, null, null, null, null, null, null, null, '0', null, null);
INSERT INTO `sdm_ms_jabatan_old` VALUES ('20', 'JM Bisnis I\r\n', '5', '0', null, null, null, null, null, null, null, null, '0', null, null);
INSERT INTO `sdm_ms_jabatan_old` VALUES ('21', 'JM Bisnis II\r\n', '5', '0', null, null, null, null, null, null, null, null, '0', null, null);

-- ----------------------------
-- Table structure for sdm_ms_jenis_cuti
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_jenis_cuti`;
CREATE TABLE `sdm_ms_jenis_cuti` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jenis_cuti` varchar(20) NOT NULL,
  `jumlah_hari` int(2) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_jenis_cuti
-- ----------------------------
INSERT INTO `sdm_ms_jenis_cuti` VALUES ('1', 'Cuti Tahunan', '0', null, '0', null, null);
INSERT INTO `sdm_ms_jenis_cuti` VALUES ('2', 'Cuti Sakit', '0', null, '0', null, null);
INSERT INTO `sdm_ms_jenis_cuti` VALUES ('3', 'Cuti Melahirkan', '0', null, '0', null, null);
INSERT INTO `sdm_ms_jenis_cuti` VALUES ('4', 'Cuti Alasan Penting', '0', null, '0', null, null);
INSERT INTO `sdm_ms_jenis_cuti` VALUES ('5', 'Cuti Diluar Tanggung', '0', null, '0', null, null);
INSERT INTO `sdm_ms_jenis_cuti` VALUES ('6', 'Cuti Besar', '0', null, '0', null, null);

-- ----------------------------
-- Table structure for sdm_ms_laba
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_laba`;
CREATE TABLE `sdm_ms_laba` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tahun` smallint(6) NOT NULL,
  `sdm_ms_bagian_id` int(11) NOT NULL,
  `anggaran` decimal(20,2) DEFAULT NULL,
  `unit_lalu` smallint(6) DEFAULT NULL,
  `laba_lalu` decimal(20,2) DEFAULT NULL,
  `pendapatan` decimal(20,2) DEFAULT NULL,
  `unit` smallint(6) DEFAULT NULL,
  `laba` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_laba
-- ----------------------------
INSERT INTO `sdm_ms_laba` VALUES ('1', '2016', '3', '70000000.00', '200', '3000000.00', '100000000.00', '300', '12000000.00', '2017-08-17 18:57:08', '2', '2017-08-17 18:57:08', null);
INSERT INTO `sdm_ms_laba` VALUES ('2', '2016', '6', '10000000.00', '100', '1000000.00', '13000000.00', '120', '4000000.00', '2017-08-17 18:58:41', '2', '2017-08-17 18:58:41', null);
INSERT INTO `sdm_ms_laba` VALUES ('3', '2015', '3', '40000000.00', '400', '2000000.00', '60000000.00', '450', '2000000.00', '2017-08-17 22:30:10', '2', '2017-08-17 22:30:10', null);

-- ----------------------------
-- Table structure for sdm_ms_list_kinerja
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_list_kinerja`;
CREATE TABLE `sdm_ms_list_kinerja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keterangan` text,
  `bobot` tinyint(4) DEFAULT NULL,
  `tipe` enum('1','2','3') DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_list_kinerja
-- ----------------------------
INSERT INTO `sdm_ms_list_kinerja` VALUES ('1', 'Penguasaan / Keterampilan Kerja', '15', '1', null, null);
INSERT INTO `sdm_ms_list_kinerja` VALUES ('2', 'Tanggung Jawab', '15', '1', null, null);
INSERT INTO `sdm_ms_list_kinerja` VALUES ('3', 'Kerjasama', '10', '1', null, '2017-08-09 09:48:57');
INSERT INTO `sdm_ms_list_kinerja` VALUES ('4', 'Disiplin', '10', '1', null, '2017-08-09 09:48:59');
INSERT INTO `sdm_ms_list_kinerja` VALUES ('5', 'Komunikasi / Realisasi Interpersonal', '5', '1', null, '2017-08-09 09:49:00');
INSERT INTO `sdm_ms_list_kinerja` VALUES ('6', 'Kualitas Kerja', '15', '1', null, '2017-08-09 09:49:02');
INSERT INTO `sdm_ms_list_kinerja` VALUES ('7', 'Kuantitias Kerja', '15', '1', null, '2017-08-09 09:49:03');
INSERT INTO `sdm_ms_list_kinerja` VALUES ('8', 'Prakarsa (Inisiatif)', '5', '1', null, '2017-08-09 09:49:04');
INSERT INTO `sdm_ms_list_kinerja` VALUES ('9', 'Hasrat Berprestasi', '10', '1', null, '2017-08-09 09:49:05');
INSERT INTO `sdm_ms_list_kinerja` VALUES ('10', 'Kemampuan untuk menghasilkan pendapatan usaha (sesuai bidang tugasnya) dikaitkan dengan hasil yang terealisasi', '13', '2', null, '2017-08-09 09:50:49');
INSERT INTO `sdm_ms_list_kinerja` VALUES ('11', 'Kemampuan untuk melakukan efisiensi biaya usaha khususnya biaya produksi (harga pokok) atas jasa yang dijual dikaitkan dengan realisasi biayanya', '11', '2', null, null);
INSERT INTO `sdm_ms_list_kinerja` VALUES ('12', 'Kemampuan untuk memaksimalkan laba usaha dikaitkan dengan realisasi laba yang diperoleh', '10', '2', null, null);
INSERT INTO `sdm_ms_list_kinerja` VALUES ('13', 'Kemampuan dalam menjalani kerjasama dan koordinasi dengan mitra bisnis, baik sisi internal (pegawai bagian lain) maupun sisi eksternal (pihak luar yang terkait dengan bidang tugasnya)', '7', '2', null, '2017-08-09 09:52:44');
INSERT INTO `sdm_ms_list_kinerja` VALUES ('14', 'Kemampuan dalam melakukan pembinaan bawahan', '9', '2', null, null);
INSERT INTO `sdm_ms_list_kinerja` VALUES ('15', 'Penguasaan / Keterampilan Jabatan', '6', '3', null, null);
INSERT INTO `sdm_ms_list_kinerja` VALUES ('16', 'Tanggung Jawab', '8', '3', null, '2017-08-09 09:53:41');
INSERT INTO `sdm_ms_list_kinerja` VALUES ('17', 'Perencanaan Kerja', '5', '3', null, null);
INSERT INTO `sdm_ms_list_kinerja` VALUES ('18', 'Pengorganisasian', '5', '3', null, null);
INSERT INTO `sdm_ms_list_kinerja` VALUES ('19', 'Pengendalian', '5', '3', null, null);
INSERT INTO `sdm_ms_list_kinerja` VALUES ('20', 'Pengambilan Keputusan', '7', '3', null, null);
INSERT INTO `sdm_ms_list_kinerja` VALUES ('21', 'Kepemimpinan', '6', '3', null, null);
INSERT INTO `sdm_ms_list_kinerja` VALUES ('22', 'Prakarsa', '4', '3', null, null);
INSERT INTO `sdm_ms_list_kinerja` VALUES ('23', 'Kerjasama', '4', '3', null, null);

-- ----------------------------
-- Table structure for sdm_ms_pegawai
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_pegawai`;
CREATE TABLE `sdm_ms_pegawai` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sdm_ms_pendidikan_id` int(10) unsigned NOT NULL,
  `sdm_ms_jabatan_id` int(10) unsigned NOT NULL,
  `sdm_ms_bagian_id` int(10) unsigned NOT NULL,
  `sdm_ms_agama_id` int(10) unsigned NOT NULL,
  `sdm_ms_ptkp_id` int(10) NOT NULL DEFAULT '1',
  `nik` varchar(25) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `tempat_lahir` varchar(25) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `gender` enum('l','p') NOT NULL,
  `no_telp` varchar(16) NOT NULL,
  `email` varchar(30) NOT NULL,
  `npwp` varchar(25) NOT NULL,
  `no_ktp` varchar(20) NOT NULL,
  `no_rek` varchar(20) NOT NULL,
  `no_dplk` varchar(25) NOT NULL,
  `tmt_masuk` date NOT NULL,
  `tmt_sk` date NOT NULL,
  `status_pegawai` enum('aktif','tidak aktif') NOT NULL,
  `status_kepegawaian` enum('tetap','kontrak') NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sdm_ms_pegawai_sdm_ms_jabatan_id_foreign` (`sdm_ms_jabatan_id`),
  KEY `sdm_ms_pegawai_sdm_ms_bagian_id_foreign` (`sdm_ms_bagian_id`),
  KEY `sdm_ms_pegawai_sdm_ms_agama_id_foreign` (`sdm_ms_agama_id`),
  KEY `sdm_ms_pegawai_sdm_ms_pendidikan_id_foreign` (`sdm_ms_pendidikan_id`),
  CONSTRAINT `sdm_ms_pegawai_ibfk_1` FOREIGN KEY (`sdm_ms_pendidikan_id`) REFERENCES `sdm_ms_pendidikan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `sdm_ms_pegawai_ibfk_2` FOREIGN KEY (`sdm_ms_agama_id`) REFERENCES `sdm_ms_agama` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `sdm_ms_pegawai_ibfk_3` FOREIGN KEY (`sdm_ms_bagian_id`) REFERENCES `sdm_ms_bagian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `sdm_ms_pegawai_ibfk_4` FOREIGN KEY (`sdm_ms_jabatan_id`) REFERENCES `sdm_ms_jabatan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_pegawai
-- ----------------------------
INSERT INTO `sdm_ms_pegawai` VALUES ('2', '1', '3', '1', '1', '1', '94848125128571', 'Test Pegawai 2', 'Bogor', '2009-01-28', 'p', '085571857182758', 'test@test.co.id.org.jp', '08521851257', '0581251251259', '97581257185', '985125723859', '2015-08-14', '2017-05-22', 'aktif', 'tetap', '2017-05-10 06:04:04', '2', '2017-06-09 10:05:57', null);
INSERT INTO `sdm_ms_pegawai` VALUES ('3', '1', '2', '1', '1', '1', '412414', 'Test Pegawai 1', '14214', '2017-06-20', 'l', '124124', 'adaw@gmail.com', '0512859128', '958129318', '98537', '8798539', '2015-06-17', '2017-05-08', 'aktif', 'tetap', '2017-05-22 06:46:22', '2', '2017-07-10 21:47:43', null);
INSERT INTO `sdm_ms_pegawai` VALUES ('6', '1', '1', '1', '1', '1', '26464363252', 'Test 3', '0481204', '2017-05-22', 'l', '412', '532', '53', '5235', '53', '53', '2017-05-29', '2017-05-15', 'aktif', 'tetap', '2017-05-22 07:03:39', '2', '2017-07-10 21:46:59', null);
INSERT INTO `sdm_ms_pegawai` VALUES ('7', '1', '4', '1', '1', '3', '085182651276571', 'Test No 5', 'Semarang', '2017-06-14', 'l', '084781247182', 'Test@test.test', '012j9hushs', '0419849128192489', '8492841924808', '492849184128', '2017-06-23', '2017-06-23', 'aktif', 'tetap', '2017-06-09 08:03:54', '2', '2017-06-09 08:03:54', null);
INSERT INTO `sdm_ms_pegawai` VALUES ('8', '1', '4', '4', '1', '3', '05718512941829', 'test other bgian', 'bogor', '2017-09-11', 'p', '08512571265', 'bagian@rental.com', '0571285', '05813', '87', '868635', '2017-09-27', '2017-09-05', 'aktif', 'tetap', '2017-09-04 15:44:01', '2', '2017-09-04 15:44:01', null);

-- ----------------------------
-- Table structure for sdm_ms_pendidikan
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_pendidikan`;
CREATE TABLE `sdm_ms_pendidikan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pendidikan` varchar(25) NOT NULL,
  `jenjang` enum('smp','sma/sederajat','d3','s1','s2','s3') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_pendidikan
-- ----------------------------
INSERT INTO `sdm_ms_pendidikan` VALUES ('1', 'SMK', 'sma/sederajat', '2017-04-11 05:21:53', '2', '2017-09-06 14:13:31', null);

-- ----------------------------
-- Table structure for sdm_ms_perhitungan
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_perhitungan`;
CREATE TABLE `sdm_ms_perhitungan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deskripsi` varchar(100) NOT NULL,
  `nominal` double(10,2) NOT NULL,
  `bilangan` enum('persen','rupiah','bulat') NOT NULL,
  `status` enum('aktif','tidak aktif') NOT NULL,
  `kategori` enum('bonus','thr','ump') NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_perhitungan
-- ----------------------------
INSERT INTO `sdm_ms_perhitungan` VALUES ('1', 'Ngitung', '2000.00', 'rupiah', 'aktif', 'bonus', '2017-04-11 05:22:35', '2', '2017-04-11 05:22:35', null);

-- ----------------------------
-- Table structure for sdm_ms_perhitungan_bonus
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_perhitungan_bonus`;
CREATE TABLE `sdm_ms_perhitungan_bonus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdm_ms_bagian_id` int(11) DEFAULT NULL,
  `input_var` decimal(5,2) DEFAULT NULL,
  `indeks_prestasi` decimal(10,2) DEFAULT NULL,
  `kinerja_perusahaan` decimal(10,2) DEFAULT NULL,
  `total_bonus` decimal(20,2) DEFAULT NULL,
  `bonus_diterima` decimal(20,2) DEFAULT NULL,
  `pph21` decimal(20,2) DEFAULT NULL,
  `total_terima` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_perhitungan_bonus
-- ----------------------------
INSERT INTO `sdm_ms_perhitungan_bonus` VALUES ('1', '1', '1.80', '1.10', '105.05', '24120000.00', '27871866.00', '1393594.00', '26478272.00', '2017-09-02 18:31:19', '2', '2017-09-02 18:31:19', null);

-- ----------------------------
-- Table structure for sdm_ms_perhitungan_bonus_detail
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_perhitungan_bonus_detail`;
CREATE TABLE `sdm_ms_perhitungan_bonus_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdm_ms_perhitungan_bonus_id` int(11) DEFAULT NULL,
  `sdm_ms_pegawai_nik` varchar(25) DEFAULT NULL,
  `total_bonus` decimal(20,2) DEFAULT NULL,
  `bonus_diterima` decimal(20,2) DEFAULT NULL,
  `pph21` decimal(20,2) DEFAULT NULL,
  `total_terima` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_perhitungan_bonus_detail
-- ----------------------------
INSERT INTO `sdm_ms_perhitungan_bonus_detail` VALUES ('1', '1', '94848125128571', '5220000.00', '6031971.00', '301599.00', '5730372.00', '2017-09-02 18:31:19', '2', '2017-09-02 18:31:19', null);
INSERT INTO `sdm_ms_perhitungan_bonus_detail` VALUES ('2', '1', '412414', '7560000.00', '8735958.00', '436798.00', '8299160.00', '2017-09-02 18:31:19', '2', '2017-09-02 18:31:19', null);
INSERT INTO `sdm_ms_perhitungan_bonus_detail` VALUES ('3', '1', '26464363252', '5220000.00', '6031971.00', '301599.00', '5730372.00', '2017-09-02 18:31:19', '2', '2017-09-02 18:31:19', null);
INSERT INTO `sdm_ms_perhitungan_bonus_detail` VALUES ('4', '1', '085182651276571', '6120000.00', '7071966.00', '353598.00', '6718368.00', '2017-09-02 18:31:19', '2', '2017-09-02 18:31:19', null);

-- ----------------------------
-- Table structure for sdm_ms_perhitungan_rating
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_perhitungan_rating`;
CREATE TABLE `sdm_ms_perhitungan_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tahun` smallint(6) DEFAULT NULL,
  `laba_perusahaan` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_perhitungan_rating
-- ----------------------------
INSERT INTO `sdm_ms_perhitungan_rating` VALUES ('26', '2017', '5125215.00', '2017-08-23 19:23:53', '2', '2017-08-23 19:23:53', null);

-- ----------------------------
-- Table structure for sdm_ms_perhitungan_rating_failed
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_perhitungan_rating_failed`;
CREATE TABLE `sdm_ms_perhitungan_rating_failed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tahun` smallint(6) DEFAULT NULL,
  `pencapaian` smallint(6) DEFAULT NULL,
  `rating` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_perhitungan_rating_failed
-- ----------------------------

-- ----------------------------
-- Table structure for sdm_ms_perhitungan_rating_failed2
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_perhitungan_rating_failed2`;
CREATE TABLE `sdm_ms_perhitungan_rating_failed2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdm_ms_bagian_id` int(11) DEFAULT NULL,
  `tahun` smallint(6) DEFAULT NULL,
  `anggaran` decimal(20,2) DEFAULT NULL,
  `pendapatan_tahun_lalu` decimal(20,2) DEFAULT NULL,
  `pendapatan_tahun_sekarang` decimal(20,2) DEFAULT NULL,
  `laba_tahun_lalu` decimal(20,2) DEFAULT NULL,
  `laba_tahun_sekarang` decimal(20,2) DEFAULT NULL,
  `laba_perusahaan` decimal(20,2) DEFAULT NULL,
  `laba_divisi` decimal(20,2) DEFAULT NULL,
  `persentase_target_pendapatan` smallint(6) DEFAULT NULL,
  `persentase_pertumbuhan_pendapatan` smallint(6) DEFAULT NULL,
  `persentase_pertumbuhan_laba` smallint(6) DEFAULT NULL,
  `presentase_kontribusi_laba` smallint(6) DEFAULT NULL,
  `rating_target_pendapatan` tinyint(4) DEFAULT NULL,
  `rating_pertumbuhan_pendapatan` tinyint(4) DEFAULT NULL,
  `rating_pertumbuhan_laba` tinyint(4) DEFAULT NULL,
  `rating_kontribusi_laba` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_perhitungan_rating_failed2
-- ----------------------------

-- ----------------------------
-- Table structure for sdm_ms_ptkp
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_ptkp`;
CREATE TABLE `sdm_ms_ptkp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(20) DEFAULT NULL,
  `nominal` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_ptkp
-- ----------------------------
INSERT INTO `sdm_ms_ptkp` VALUES ('1', 'TK', '54000000.00', null, null, '2017-06-08 11:50:15', null);
INSERT INTO `sdm_ms_ptkp` VALUES ('2', 'K', '58500000.00', null, null, '2017-06-08 11:50:23', null);
INSERT INTO `sdm_ms_ptkp` VALUES ('3', 'K1', '63000000.00', null, null, '2017-06-08 11:50:29', null);
INSERT INTO `sdm_ms_ptkp` VALUES ('4', 'K2', '67500000.00', null, null, '2017-06-08 11:50:34', null);
INSERT INTO `sdm_ms_ptkp` VALUES ('5', 'K3', '72000000.00', null, null, '2017-06-08 11:50:38', null);

-- ----------------------------
-- Table structure for sdm_ms_rating_kontribusi_laba
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_rating_kontribusi_laba`;
CREATE TABLE `sdm_ms_rating_kontribusi_laba` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdm_ms_bagian_id` int(11) DEFAULT NULL,
  `sdm_ms_perhitungan_rating_id` int(11) DEFAULT NULL,
  `laba_divisi` decimal(20,2) DEFAULT NULL,
  `kontribusi` int(4) DEFAULT NULL,
  `rating` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_rating_kontribusi_laba
-- ----------------------------
INSERT INTO `sdm_ms_rating_kontribusi_laba` VALUES ('22', '1', '25', '25255125.00', '493', '5', '2017-08-23 19:23:32', null, '2017-08-23 19:23:32', null);
INSERT INTO `sdm_ms_rating_kontribusi_laba` VALUES ('23', '2', '25', '2135215.00', '42', '5', '2017-08-23 19:23:32', null, '2017-08-23 19:23:32', null);
INSERT INTO `sdm_ms_rating_kontribusi_laba` VALUES ('24', '3', '25', '221525.00', '4', '1', '2017-08-23 19:23:33', null, '2017-08-23 19:23:33', null);
INSERT INTO `sdm_ms_rating_kontribusi_laba` VALUES ('25', '4', '25', '2525.00', '0', '1', '2017-08-23 19:23:33', null, '2017-08-23 19:23:33', null);
INSERT INTO `sdm_ms_rating_kontribusi_laba` VALUES ('26', '5', '25', '2112515125.00', '41218', '5', '2017-08-23 19:23:33', null, '2017-08-23 19:23:33', null);
INSERT INTO `sdm_ms_rating_kontribusi_laba` VALUES ('27', '6', '25', '22.00', '0', '1', '2017-08-23 19:23:34', null, '2017-08-23 19:23:34', null);
INSERT INTO `sdm_ms_rating_kontribusi_laba` VALUES ('28', '7', '25', '21.00', '0', '1', '2017-08-23 19:23:34', null, '2017-08-23 19:23:34', null);
INSERT INTO `sdm_ms_rating_kontribusi_laba` VALUES ('29', '1', '26', '25255125.00', '493', '5', '2017-08-23 19:23:53', null, '2017-08-23 19:23:53', null);
INSERT INTO `sdm_ms_rating_kontribusi_laba` VALUES ('30', '2', '26', '2135215.00', '42', '5', '2017-08-23 19:23:53', null, '2017-08-23 19:23:53', null);
INSERT INTO `sdm_ms_rating_kontribusi_laba` VALUES ('31', '3', '26', '221525.00', '4', '1', '2017-08-23 19:23:53', null, '2017-08-23 19:23:53', null);
INSERT INTO `sdm_ms_rating_kontribusi_laba` VALUES ('32', '4', '26', '2525.00', '0', '1', '2017-08-23 19:23:54', null, '2017-08-23 19:23:54', null);
INSERT INTO `sdm_ms_rating_kontribusi_laba` VALUES ('33', '5', '26', '2112515125.00', '41218', '5', '2017-08-23 19:23:54', null, '2017-08-23 19:23:54', null);
INSERT INTO `sdm_ms_rating_kontribusi_laba` VALUES ('34', '6', '26', '22.00', '0', '1', '2017-08-23 19:23:54', null, '2017-08-23 19:23:54', null);
INSERT INTO `sdm_ms_rating_kontribusi_laba` VALUES ('35', '7', '26', '21.00', '0', '1', '2017-08-23 19:23:54', null, '2017-08-23 19:23:54', null);

-- ----------------------------
-- Table structure for sdm_ms_rating_pencapaian
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_rating_pencapaian`;
CREATE TABLE `sdm_ms_rating_pencapaian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdm_ms_bagian_id` int(11) DEFAULT NULL,
  `sdm_ms_perhitungan_rating_id` int(11) DEFAULT NULL,
  `anggaran_sekarang` decimal(20,2) DEFAULT NULL,
  `pendapatan_sekarang` decimal(20,2) DEFAULT NULL,
  `pertumbuhan_target` int(4) DEFAULT NULL,
  `rating` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_rating_pencapaian
-- ----------------------------
INSERT INTO `sdm_ms_rating_pencapaian` VALUES ('93', '1', '25', '25.00', '252.00', '1008', '5', '2017-08-23 19:23:32', null, '2017-08-23 19:23:32', null);
INSERT INTO `sdm_ms_rating_pencapaian` VALUES ('94', '2', '25', '25125.00', '251251.00', '1000', '5', '2017-08-23 19:23:32', null, '2017-08-23 19:23:32', null);
INSERT INTO `sdm_ms_rating_pencapaian` VALUES ('95', '3', '25', '2521.00', '25125.00', '997', '5', '2017-08-23 19:23:33', null, '2017-08-23 19:23:33', null);
INSERT INTO `sdm_ms_rating_pencapaian` VALUES ('96', '4', '25', '2512.00', '2512.00', '100', '1', '2017-08-23 19:23:33', null, '2017-08-23 19:23:33', null);
INSERT INTO `sdm_ms_rating_pencapaian` VALUES ('97', '5', '25', '22.00', '2.00', '9', '1', '2017-08-23 19:23:33', null, '2017-08-23 19:23:33', null);
INSERT INTO `sdm_ms_rating_pencapaian` VALUES ('98', '6', '25', '22.00', '2.00', '9', '1', '2017-08-23 19:23:33', null, '2017-08-23 19:23:33', null);
INSERT INTO `sdm_ms_rating_pencapaian` VALUES ('99', '7', '25', '22.00', '22.00', '100', '1', '2017-08-23 19:23:34', null, '2017-08-23 19:23:34', null);
INSERT INTO `sdm_ms_rating_pencapaian` VALUES ('100', '1', '26', '25.00', '252.00', '1008', '5', '2017-08-23 19:23:53', null, '2017-08-23 19:23:53', null);
INSERT INTO `sdm_ms_rating_pencapaian` VALUES ('101', '2', '26', '25125.00', '251251.00', '1000', '5', '2017-08-23 19:23:53', null, '2017-08-23 19:23:53', null);
INSERT INTO `sdm_ms_rating_pencapaian` VALUES ('102', '3', '26', '2521.00', '25125.00', '997', '5', '2017-08-23 19:23:53', null, '2017-08-23 19:23:53', null);
INSERT INTO `sdm_ms_rating_pencapaian` VALUES ('103', '4', '26', '2512.00', '2512.00', '100', '1', '2017-08-23 19:23:53', null, '2017-08-23 19:23:53', null);
INSERT INTO `sdm_ms_rating_pencapaian` VALUES ('104', '5', '26', '22.00', '2.00', '9', '1', '2017-08-23 19:23:54', null, '2017-08-23 19:23:54', null);
INSERT INTO `sdm_ms_rating_pencapaian` VALUES ('105', '6', '26', '22.00', '2.00', '9', '1', '2017-08-23 19:23:54', null, '2017-08-23 19:23:54', null);
INSERT INTO `sdm_ms_rating_pencapaian` VALUES ('106', '7', '26', '22.00', '22.00', '100', '1', '2017-08-23 19:23:54', null, '2017-08-23 19:23:54', null);

-- ----------------------------
-- Table structure for sdm_ms_rating_pertumbuhan
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_rating_pertumbuhan`;
CREATE TABLE `sdm_ms_rating_pertumbuhan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdm_ms_bagian_id` int(11) DEFAULT NULL,
  `sdm_ms_perhitungan_rating_id` int(11) DEFAULT NULL,
  `pendapatan_tahun_lalu` decimal(20,2) DEFAULT NULL,
  `pendapatan_tahun_sekarang` decimal(20,2) DEFAULT NULL,
  `pertumbuhan` int(4) DEFAULT NULL,
  `rating` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_rating_pertumbuhan
-- ----------------------------
INSERT INTO `sdm_ms_rating_pertumbuhan` VALUES ('49', '1', '25', '252.00', '2.00', '1', '1', '2017-08-23 19:23:32', null, '2017-08-23 19:23:32', null);
INSERT INTO `sdm_ms_rating_pertumbuhan` VALUES ('50', '2', '25', '22.00', '2512.00', '11418', '5', '2017-08-23 19:23:32', null, '2017-08-23 19:23:32', null);
INSERT INTO `sdm_ms_rating_pertumbuhan` VALUES ('51', '3', '25', '25.00', '2.00', '8', '1', '2017-08-23 19:23:33', null, '2017-08-23 19:23:33', null);
INSERT INTO `sdm_ms_rating_pertumbuhan` VALUES ('52', '4', '25', '2.00', '2.00', '0', null, '2017-08-23 19:23:33', null, '2017-08-23 19:23:33', null);
INSERT INTO `sdm_ms_rating_pertumbuhan` VALUES ('53', '5', '25', '2222222.00', '2.00', '0', '1', '2017-08-23 19:23:33', null, '2017-08-23 19:23:33', null);
INSERT INTO `sdm_ms_rating_pertumbuhan` VALUES ('54', '6', '25', '25222.00', '222222.00', '881', '5', '2017-08-23 19:23:34', null, '2017-08-23 19:23:34', null);
INSERT INTO `sdm_ms_rating_pertumbuhan` VALUES ('55', '7', '25', '25.00', '2.00', '8', '1', '2017-08-23 19:23:34', null, '2017-08-23 19:23:34', null);
INSERT INTO `sdm_ms_rating_pertumbuhan` VALUES ('56', '1', '26', '252.00', '2.00', '1', '1', '2017-08-23 19:23:53', null, '2017-08-23 19:23:53', null);
INSERT INTO `sdm_ms_rating_pertumbuhan` VALUES ('57', '2', '26', '22.00', '2512.00', '11418', '5', '2017-08-23 19:23:53', null, '2017-08-23 19:23:53', null);
INSERT INTO `sdm_ms_rating_pertumbuhan` VALUES ('58', '3', '26', '25.00', '2.00', '8', '1', '2017-08-23 19:23:53', null, '2017-08-23 19:23:53', null);
INSERT INTO `sdm_ms_rating_pertumbuhan` VALUES ('59', '4', '26', '2.00', '2.00', '0', '4', '2017-08-23 19:23:53', null, '2017-08-28 11:27:46', null);
INSERT INTO `sdm_ms_rating_pertumbuhan` VALUES ('60', '5', '26', '2222222.00', '2.00', '0', '1', '2017-08-23 19:23:54', null, '2017-08-23 19:23:54', null);
INSERT INTO `sdm_ms_rating_pertumbuhan` VALUES ('61', '6', '26', '25222.00', '222222.00', '881', '5', '2017-08-23 19:23:54', null, '2017-08-23 19:23:54', null);
INSERT INTO `sdm_ms_rating_pertumbuhan` VALUES ('62', '7', '26', '25.00', '2.00', '8', '1', '2017-08-23 19:23:54', null, '2017-08-23 19:23:54', null);

-- ----------------------------
-- Table structure for sdm_ms_rating_pertumbuhan_laba
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_rating_pertumbuhan_laba`;
CREATE TABLE `sdm_ms_rating_pertumbuhan_laba` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdm_ms_bagian_id` int(11) DEFAULT NULL,
  `sdm_ms_perhitungan_rating_id` int(11) DEFAULT NULL,
  `laba_tahun_lalu` decimal(20,2) DEFAULT NULL,
  `laba_tahun_sekarang` decimal(20,2) DEFAULT NULL,
  `pertumbuhan` tinyint(4) DEFAULT NULL,
  `rating` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_rating_pertumbuhan_laba
-- ----------------------------
INSERT INTO `sdm_ms_rating_pertumbuhan_laba` VALUES ('26', '1', '25', '2521.00', '25252.00', '127', '5', '2017-08-23 19:23:32', null, '2017-08-23 19:23:32', null);
INSERT INTO `sdm_ms_rating_pertumbuhan_laba` VALUES ('27', '2', '25', '2.00', '252.00', '127', '5', '2017-08-23 19:23:32', null, '2017-08-23 19:23:32', null);
INSERT INTO `sdm_ms_rating_pertumbuhan_laba` VALUES ('28', '3', '25', '25252.00', '25252.00', '100', '1', '2017-08-23 19:23:33', null, '2017-08-23 19:23:33', null);
INSERT INTO `sdm_ms_rating_pertumbuhan_laba` VALUES ('29', '4', '25', '251251.00', '2521515.00', '127', '5', '2017-08-23 19:23:33', null, '2017-08-23 19:23:33', null);
INSERT INTO `sdm_ms_rating_pertumbuhan_laba` VALUES ('30', '5', '25', '25521.00', '2.00', '0', '1', '2017-08-23 19:23:33', null, '2017-08-23 19:23:33', null);
INSERT INTO `sdm_ms_rating_pertumbuhan_laba` VALUES ('31', '6', '25', '22.00', '2.00', '9', '1', '2017-08-23 19:23:34', null, '2017-08-23 19:23:34', null);
INSERT INTO `sdm_ms_rating_pertumbuhan_laba` VALUES ('32', '7', '25', '22.00', '2.00', '9', '1', '2017-08-23 19:23:34', null, '2017-08-28 00:53:03', null);
INSERT INTO `sdm_ms_rating_pertumbuhan_laba` VALUES ('33', '1', '26', '2521.00', '25252.00', '127', '5', '2017-08-23 19:23:53', null, '2017-08-23 19:23:53', null);
INSERT INTO `sdm_ms_rating_pertumbuhan_laba` VALUES ('34', '2', '26', '2.00', '252.00', '127', '5', '2017-08-23 19:23:53', null, '2017-08-23 19:23:53', null);
INSERT INTO `sdm_ms_rating_pertumbuhan_laba` VALUES ('35', '3', '26', '25252.00', '25252.00', '100', '3', '2017-08-23 19:23:53', null, '2017-08-28 00:53:01', null);
INSERT INTO `sdm_ms_rating_pertumbuhan_laba` VALUES ('36', '4', '26', '251251.00', '2521515.00', '127', '5', '2017-08-23 19:23:53', null, '2017-08-28 12:25:12', null);
INSERT INTO `sdm_ms_rating_pertumbuhan_laba` VALUES ('37', '5', '26', '25521.00', '2.00', '0', '1', '2017-08-23 19:23:54', null, '2017-08-23 19:23:54', null);
INSERT INTO `sdm_ms_rating_pertumbuhan_laba` VALUES ('38', '6', '26', '22.00', '2.00', '9', '1', '2017-08-23 19:23:54', null, '2017-08-23 19:23:54', null);
INSERT INTO `sdm_ms_rating_pertumbuhan_laba` VALUES ('39', '7', '26', '22.00', '2.00', '9', '1', '2017-08-23 19:23:54', null, '2017-08-23 19:23:54', null);

-- ----------------------------
-- Table structure for sdm_ms_thr
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ms_thr`;
CREATE TABLE `sdm_ms_thr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdm_ms_pegawai_nik` varchar(50) NOT NULL,
  `tahun` smallint(6) DEFAULT NULL,
  `thr` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ms_thr
-- ----------------------------
INSERT INTO `sdm_ms_thr` VALUES ('1', '94848125128571', '2017', '4400000.00', '2017-09-06 14:24:46', null, '2017-09-06 14:24:46', null);
INSERT INTO `sdm_ms_thr` VALUES ('2', '412414', '2017', '3740000.00', '2017-09-06 14:24:46', null, '2017-09-06 14:24:46', null);
INSERT INTO `sdm_ms_thr` VALUES ('3', '26464363252', '2017', '725000.00', '2017-09-06 14:24:46', null, '2017-09-06 14:24:46', null);
INSERT INTO `sdm_ms_thr` VALUES ('4', '085182651276571', '2017', '1173333.33', '2017-09-06 14:24:46', null, '2017-09-06 14:24:46', null);
INSERT INTO `sdm_ms_thr` VALUES ('5', '05718512941829', '2017', '0.00', '2017-09-06 14:24:46', null, '2017-09-06 14:24:46', null);

-- ----------------------------
-- Table structure for sdm_ovt_lembur
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ovt_lembur`;
CREATE TABLE `sdm_ovt_lembur` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sdm_ms_bagian_id` int(10) NOT NULL,
  `no_lembur` varchar(50) NOT NULL,
  `tgl_awal` date NOT NULL,
  `tgl_akhir` date NOT NULL,
  `status` enum('requested','approved','done') NOT NULL DEFAULT 'requested',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ovt_lembur
-- ----------------------------
INSERT INTO `sdm_ovt_lembur` VALUES ('1', '1', '1a/EPS.SDM.02/XII/2017', '2017-06-13', '2017-06-14', 'done', '2017-06-08 16:03:04', '2', '2017-06-08 16:08:51', null);
INSERT INTO `sdm_ovt_lembur` VALUES ('2', '1', '2a/EPS.SDM.02/XII/2017', '2017-07-03', '2017-07-05', 'done', '2017-07-10 10:33:18', '2', '2017-07-10 10:35:36', null);
INSERT INTO `sdm_ovt_lembur` VALUES ('3', '1', '3a/EPS.SDM.02/XII/2017', '2017-07-10', '2017-07-11', 'done', '2017-07-10 22:08:02', '2', '2017-07-10 22:10:02', null);
INSERT INTO `sdm_ovt_lembur` VALUES ('4', '1', '4a/EPS.SDM.02/XII/2017', '2017-07-05', '2017-07-07', 'approved', '2017-07-11 11:10:53', '2', '2017-07-11 11:11:33', null);
INSERT INTO `sdm_ovt_lembur` VALUES ('5', '1', '5a/EPS.SDM.02/XII/2017', '2017-08-21', '2017-08-21', 'requested', '2017-08-08 15:05:13', '2', '2017-08-08 15:05:13', null);
INSERT INTO `sdm_ovt_lembur` VALUES ('6', '5', '6a/EPS.SDM.02/XII/2017', '2017-09-14', '2017-09-14', 'done', '2017-09-06 14:37:59', '2', '2017-09-06 14:40:44', null);

-- ----------------------------
-- Table structure for sdm_ovt_lembur_detail
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ovt_lembur_detail`;
CREATE TABLE `sdm_ovt_lembur_detail` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sdm_ovt_lembur_ket_id` int(10) NOT NULL,
  `dari_jam` time NOT NULL,
  `sampai_jam` time NOT NULL,
  `dari_tgl` date NOT NULL,
  `sampai_tgl` date NOT NULL,
  `jml_jam` varchar(25) NOT NULL,
  `ket` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ovt_lembur_detail
-- ----------------------------
INSERT INTO `sdm_ovt_lembur_detail` VALUES ('1', '1', '13:25:00', '18:00:00', '2017-06-13', '2017-06-13', '4 Jam & 35 Menit', 'hahahahawa', '2017-06-08 16:08:51', '2', '2017-06-08 16:08:51', null);
INSERT INTO `sdm_ovt_lembur_detail` VALUES ('2', '2', '12:20:00', '14:50:00', '2017-06-14', '2017-06-14', '2 Jam & 30 Menit', 'wadaw', '2017-06-08 16:08:51', '2', '2017-06-08 16:08:51', null);
INSERT INTO `sdm_ovt_lembur_detail` VALUES ('3', '3', '08:20:00', '09:50:00', '2017-07-03', '2017-07-03', '1 Jam & 30 Menit', null, '2017-07-10 10:35:36', '2', '2017-07-10 10:35:36', null);
INSERT INTO `sdm_ovt_lembur_detail` VALUES ('4', '4', '04:20:00', '10:30:00', '2017-07-04', '2017-07-04', '6 Jam & 10 Menit', null, '2017-07-10 10:35:36', '2', '2017-07-10 10:35:36', null);
INSERT INTO `sdm_ovt_lembur_detail` VALUES ('5', '5', '12:40:00', '22:45:00', '2017-07-05', '2017-07-05', '10 Jam & 5 Menit', null, '2017-07-10 10:35:36', '2', '2017-07-10 10:35:36', null);
INSERT INTO `sdm_ovt_lembur_detail` VALUES ('6', '6', '17:10:00', '22:45:00', '2017-07-10', '2017-07-10', '5 Jam & 35 Menit', 'membuat dokumen', '2017-07-10 22:10:02', '2', '2017-07-10 22:10:02', null);
INSERT INTO `sdm_ovt_lembur_detail` VALUES ('7', '7', '04:00:00', '17:45:00', '2017-07-11', '2017-07-11', '13 Jam & 45 Menit', 'membuat dokumen', '2017-07-10 22:10:02', '2', '2017-07-10 22:10:02', null);
INSERT INTO `sdm_ovt_lembur_detail` VALUES ('8', '12', '14:20:00', '18:45:00', '2017-09-14', '2017-09-14', '4 Jam & 25 Menit', 'lembur', '2017-09-06 14:40:44', '2', '2017-09-06 14:40:44', null);

-- ----------------------------
-- Table structure for sdm_ovt_lembur_ket
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ovt_lembur_ket`;
CREATE TABLE `sdm_ovt_lembur_ket` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sdm_ovt_lembur_id` int(10) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `jam` varchar(10) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ovt_lembur_ket
-- ----------------------------
INSERT INTO `sdm_ovt_lembur_ket` VALUES ('1', '1', '2017-06-13', '2 Jam', 'hahahaha', '2017-06-08 16:03:04', '2', '2017-06-08 16:03:04', null);
INSERT INTO `sdm_ovt_lembur_ket` VALUES ('2', '1', '2017-06-14', '5 Jam', 'gawe bozzzz', '2017-06-08 16:03:04', '2', '2017-06-08 16:03:04', null);
INSERT INTO `sdm_ovt_lembur_ket` VALUES ('3', '2', '2017-07-03', '4 Jam', 'pembuatan report', '2017-07-10 10:33:19', '2', '2017-07-10 10:33:19', null);
INSERT INTO `sdm_ovt_lembur_ket` VALUES ('4', '2', '2017-07-04', '1 Jam', 'pembuatan report', '2017-07-10 10:33:19', '2', '2017-07-10 10:33:19', null);
INSERT INTO `sdm_ovt_lembur_ket` VALUES ('5', '2', '2017-07-05', '8 Jam', 'pembuatan report', '2017-07-10 10:33:19', '2', '2017-07-10 10:33:19', null);
INSERT INTO `sdm_ovt_lembur_ket` VALUES ('6', '3', '2017-07-10', '4 Jam', 'print report', '2017-07-10 22:08:02', '2', '2017-07-10 22:08:02', null);
INSERT INTO `sdm_ovt_lembur_ket` VALUES ('7', '3', '2017-07-11', '1 Jam', 'pekerjaan dadakan', '2017-07-10 22:08:02', '2', '2017-07-10 22:08:02', null);
INSERT INTO `sdm_ovt_lembur_ket` VALUES ('8', '4', '2017-07-05', '1 Jam', 'print laporan', '2017-07-11 11:10:53', '2', '2017-07-11 11:10:53', null);
INSERT INTO `sdm_ovt_lembur_ket` VALUES ('9', '4', '2017-07-06', '1 Jam', 'print laporan', '2017-07-11 11:10:54', '2', '2017-07-11 11:10:54', null);
INSERT INTO `sdm_ovt_lembur_ket` VALUES ('10', '4', '2017-07-07', '1 Jam', 'print laporan', '2017-07-11 11:10:54', '2', '2017-07-11 11:10:54', null);
INSERT INTO `sdm_ovt_lembur_ket` VALUES ('11', '5', '2017-08-21', '1 Jam', 'print laporan', '2017-08-08 15:05:14', '2', '2017-08-08 15:05:14', null);
INSERT INTO `sdm_ovt_lembur_ket` VALUES ('12', '6', '2017-09-14', '2 Jam', 'keteranganag', '2017-09-06 14:38:00', '2', '2017-09-06 14:38:00', null);

-- ----------------------------
-- Table structure for sdm_ovt_list_pegawai
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ovt_list_pegawai`;
CREATE TABLE `sdm_ovt_list_pegawai` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sdm_ovt_lembur_id` int(10) NOT NULL,
  `sdm_ms_pegawai_id` int(10) DEFAULT NULL,
  `sdm_ms_pegawai_nik` varchar(30) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ovt_list_pegawai
-- ----------------------------
INSERT INTO `sdm_ovt_list_pegawai` VALUES ('1', '1', null, '26464363252', '2017-06-08 16:03:04', '2', '2017-06-08 16:03:04', null);
INSERT INTO `sdm_ovt_list_pegawai` VALUES ('2', '1', null, '94848125128571', '2017-06-08 16:03:04', '2', '2017-06-08 16:03:04', null);
INSERT INTO `sdm_ovt_list_pegawai` VALUES ('3', '2', null, '94848125128571', '2017-07-10 10:33:19', '2', '2017-07-10 10:33:19', null);
INSERT INTO `sdm_ovt_list_pegawai` VALUES ('4', '2', null, '085182651276571', '2017-07-10 10:33:19', '2', '2017-07-10 10:33:19', null);
INSERT INTO `sdm_ovt_list_pegawai` VALUES ('5', '3', null, '085182651276571', '2017-07-10 22:08:02', '2', '2017-07-10 22:08:02', null);
INSERT INTO `sdm_ovt_list_pegawai` VALUES ('6', '4', null, '94848125128571', '2017-07-11 11:10:53', '2', '2017-07-11 11:10:53', null);
INSERT INTO `sdm_ovt_list_pegawai` VALUES ('7', '4', null, '26464363252', '2017-07-11 11:10:53', '2', '2017-07-11 11:10:53', null);
INSERT INTO `sdm_ovt_list_pegawai` VALUES ('8', '5', null, '085182651276571', '2017-08-08 15:05:13', '2', '2017-08-08 15:05:13', null);
INSERT INTO `sdm_ovt_list_pegawai` VALUES ('9', '6', null, '94848125128571', '2017-09-06 14:37:59', '2', '2017-09-06 14:37:59', null);

-- ----------------------------
-- Table structure for sdm_ovt_perhitungan_lembur
-- ----------------------------
DROP TABLE IF EXISTS `sdm_ovt_perhitungan_lembur`;
CREATE TABLE `sdm_ovt_perhitungan_lembur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdm_ovt_list_pegawai_id` int(11) NOT NULL,
  `sdm_ovt_lembur_id` int(11) NOT NULL,
  `sdm_ovt_lembur_ket_id` int(11) DEFAULT NULL,
  `jenis_hari` enum('libur','kerja') DEFAULT 'kerja',
  `jumlah_lembur_jam1` varchar(10) DEFAULT NULL,
  `jumlah_lembur_jam9` varchar(10) DEFAULT NULL,
  `jumlah_lembur_jam10` varchar(10) DEFAULT NULL,
  `uang_makan_lembur` varchar(10) DEFAULT NULL,
  `upah_jam1` decimal(20,2) DEFAULT NULL,
  `upah_jam9` decimal(20,2) DEFAULT NULL,
  `upah_jam10` decimal(20,2) DEFAULT NULL,
  `upah_uang_makan` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_ovt_perhitungan_lembur
-- ----------------------------
INSERT INTO `sdm_ovt_perhitungan_lembur` VALUES ('1', '1', '1', '1', 'kerja', '1', '2', '1', '1', '52023.00', '39017.34', null, '15000.00', '2017-06-08 16:10:07', '2', '2017-06-08 16:10:07', null);
INSERT INTO `sdm_ovt_perhitungan_lembur` VALUES ('2', '1', '1', '2', null, '2', '1', '1', '2', '52023.00', '39017.34', null, '30000.00', '2017-06-08 16:10:07', '2', '2017-06-08 16:10:07', null);
INSERT INTO `sdm_ovt_perhitungan_lembur` VALUES ('3', '2', '1', '1', 'libur', '3', '2', '0', '1', '0.00', '32514.45', null, '15000.00', '2017-06-08 16:10:07', '2', '2017-06-14 08:41:03', null);
INSERT INTO `sdm_ovt_perhitungan_lembur` VALUES ('4', '2', '1', '2', null, '2', '1', '0', '0', '0.00', '32514.45', null, '0.00', '2017-06-08 16:10:07', '2', '2017-06-08 16:10:07', null);
INSERT INTO `sdm_ovt_perhitungan_lembur` VALUES ('7', '8', '5', '11', null, '1', '2', '1', '1', '29480.00', '88440.00', '58960.00', '15000.00', '2017-09-01 04:32:04', '2', '2017-09-01 18:32:04', null);
INSERT INTO `sdm_ovt_perhitungan_lembur` VALUES ('8', '9', '6', '12', 'kerja', '2', '1', '2', '1', '61734.00', '46301.00', '123468.00', '15000.00', '2017-09-06 14:42:03', '2', '2017-09-06 14:42:03', null);

-- ----------------------------
-- Table structure for sdm_pks_kerjasama
-- ----------------------------
DROP TABLE IF EXISTS `sdm_pks_kerjasama`;
CREATE TABLE `sdm_pks_kerjasama` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdm_ms_bagian_id` int(11) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `no_pks` varchar(50) DEFAULT NULL,
  `prihal` text,
  `tgl_mulai` date NOT NULL,
  `tgl_akhir` date NOT NULL,
  `file` text,
  `notif_status` enum('0','1') DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_pks_kerjasama
-- ----------------------------
INSERT INTO `sdm_pks_kerjasama` VALUES ('4', '1', 'adaw', '94812491', '9841928', '2017-06-19', '2017-10-19', 'exemple04.pdf', '1', '2017-06-12 13:26:03', '2', '2017-08-30 10:36:01', null);
INSERT INTO `sdm_pks_kerjasama` VALUES ('5', '2', 'adaw2', '42142', 'r2141', '2017-08-21', '2017-10-17', 'exemple03.pdf', '1', '2017-08-02 10:46:48', '2', '2017-08-30 10:35:54', null);
INSERT INTO `sdm_pks_kerjasama` VALUES ('6', '2', 'perusahaan', '084128', 'kerja', '2017-09-01', '2017-09-30', 'Daftar Hadir SDM.pdf', '1', '2017-09-06 15:04:31', '2', '2017-09-06 15:04:31', null);

-- ----------------------------
-- Table structure for sdm_pks_perijinan
-- ----------------------------
DROP TABLE IF EXISTS `sdm_pks_perijinan`;
CREATE TABLE `sdm_pks_perijinan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdm_ms_bagian_id` int(11) NOT NULL,
  `prihal` text,
  `tgl_mulai` date NOT NULL,
  `tgl_akhir` date NOT NULL,
  `file` text,
  `notif_status` enum('0','1') DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_pks_perijinan
-- ----------------------------
INSERT INTO `sdm_pks_perijinan` VALUES ('4', '1', 'wadaw', '2017-06-26', '2017-06-29', 'exemple02.pdf', '1', '2017-06-15 09:32:31', '2', '2017-08-30 10:26:40', null);
INSERT INTO `sdm_pks_perijinan` VALUES ('6', '1', 'adaw', '2017-08-31', '2017-09-06', 'exemple04.pdf', '1', '2017-08-02 10:39:56', '2', '2017-08-30 10:26:41', null);
INSERT INTO `sdm_pks_perijinan` VALUES ('7', '4', 'Izin mengizinkan', '2017-08-23', '2017-09-09', 'Daftar Hadir SDM.pdf', '1', '2017-08-30 10:11:12', '2', '2017-08-30 10:26:45', null);

-- ----------------------------
-- Table structure for sdm_pmb_pengajuan_bonus
-- ----------------------------
DROP TABLE IF EXISTS `sdm_pmb_pengajuan_bonus`;
CREATE TABLE `sdm_pmb_pengajuan_bonus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdm_ms_bagian_id` int(11) DEFAULT NULL,
  `tanggal` varchar(20) DEFAULT NULL,
  `pengajuan_bonus` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_pmb_pengajuan_bonus
-- ----------------------------
INSERT INTO `sdm_pmb_pengajuan_bonus` VALUES ('3', '1', '09-2017', '29478272.00', '2017-09-02 18:52:45', '2', '2017-09-02 18:52:45', null);

-- ----------------------------
-- Table structure for sdm_pmb_pengajuan_cuti
-- ----------------------------
DROP TABLE IF EXISTS `sdm_pmb_pengajuan_cuti`;
CREATE TABLE `sdm_pmb_pengajuan_cuti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdm_ms_bagian_id` int(11) DEFAULT NULL,
  `tanggal` varchar(20) DEFAULT NULL,
  `pengajuan_cuti` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_pmb_pengajuan_cuti
-- ----------------------------
INSERT INTO `sdm_pmb_pengajuan_cuti` VALUES ('5', '1', '09-2017', '5300000.00', '2017-09-02 23:14:50', '2', '2017-09-02 23:14:50', null);

-- ----------------------------
-- Table structure for sdm_pmb_pengajuan_gaji
-- ----------------------------
DROP TABLE IF EXISTS `sdm_pmb_pengajuan_gaji`;
CREATE TABLE `sdm_pmb_pengajuan_gaji` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` varchar(20) DEFAULT NULL,
  `sdm_ms_bagian_id` int(11) DEFAULT NULL,
  `total_gapok` decimal(20,2) DEFAULT NULL,
  `total_tunj_pengelolaan` decimal(20,2) DEFAULT NULL,
  `total_tunj_grade` decimal(20,2) DEFAULT NULL,
  `total_tunj_pph21` decimal(20,2) DEFAULT NULL,
  `total_biaya_jamsostek` decimal(20,2) DEFAULT NULL,
  `total_adm` decimal(20,2) DEFAULT NULL,
  `hutang_pph21` decimal(20,2) DEFAULT NULL,
  `hutang_jamsostek` decimal(20,2) DEFAULT NULL,
  `total` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_pmb_pengajuan_gaji
-- ----------------------------
INSERT INTO `sdm_pmb_pengajuan_gaji` VALUES ('8', '08-2017', '1', '8200000.00', '3000000.00', '2200000.00', '7845360.00', '717600.00', null, null, null, '21962960.00', '2017-09-01 01:38:39', '2', '2017-09-01 15:38:39', null);

-- ----------------------------
-- Table structure for sdm_pmb_pengajuan_lembur
-- ----------------------------
DROP TABLE IF EXISTS `sdm_pmb_pengajuan_lembur`;
CREATE TABLE `sdm_pmb_pengajuan_lembur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` varchar(20) DEFAULT NULL,
  `sdm_ms_bagian_id` int(11) DEFAULT NULL,
  `pengajuan_lembur` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_pmb_pengajuan_lembur
-- ----------------------------
INSERT INTO `sdm_pmb_pengajuan_lembur` VALUES ('1', '08-2017', '1', '176880.00', '2017-09-02 00:27:43', '2', '2017-09-02 14:27:43', null);

-- ----------------------------
-- Table structure for sdm_pmb_pengajuan_lembur-failed
-- ----------------------------
DROP TABLE IF EXISTS `sdm_pmb_pengajuan_lembur-failed`;
CREATE TABLE `sdm_pmb_pengajuan_lembur-failed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` varchar(20) DEFAULT NULL,
  `pengajuan_lembur` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_pmb_pengajuan_lembur-failed
-- ----------------------------
INSERT INTO `sdm_pmb_pengajuan_lembur-failed` VALUES ('1', '08-2017', '90000000.00', '2017-08-31 01:54:27', '2', '2017-08-31 15:55:15', '2');

-- ----------------------------
-- Table structure for sdm_pmb_pengajuan_sppd
-- ----------------------------
DROP TABLE IF EXISTS `sdm_pmb_pengajuan_sppd`;
CREATE TABLE `sdm_pmb_pengajuan_sppd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdm_ms_bagian_id` int(11) DEFAULT NULL,
  `tanggal` varchar(20) DEFAULT NULL,
  `pengajuan_sppd` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_pmb_pengajuan_sppd
-- ----------------------------
INSERT INTO `sdm_pmb_pengajuan_sppd` VALUES ('4', '1', '09-2017', '99010.00', '2017-09-04 11:49:59', '2', '2017-09-04 11:49:59', null);

-- ----------------------------
-- Table structure for sdm_pmb_pengajuan_thr
-- ----------------------------
DROP TABLE IF EXISTS `sdm_pmb_pengajuan_thr`;
CREATE TABLE `sdm_pmb_pengajuan_thr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdm_ms_bagian_id` int(11) DEFAULT NULL,
  `tahun` smallint(6) DEFAULT NULL,
  `pengajuan_thr` decimal(20,2) DEFAULT NULL,
  `total_pph21` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_pmb_pengajuan_thr
-- ----------------------------
INSERT INTO `sdm_pmb_pengajuan_thr` VALUES ('2', '1', '2017', '6250000.00', null, '2017-09-02 18:20:52', '2', '2017-09-02 18:20:52', null);

-- ----------------------------
-- Table structure for sdm_pmb_pengajuan_tunjangan_pakaian
-- ----------------------------
DROP TABLE IF EXISTS `sdm_pmb_pengajuan_tunjangan_pakaian`;
CREATE TABLE `sdm_pmb_pengajuan_tunjangan_pakaian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdm_ms_bagian_id` int(11) DEFAULT NULL,
  `tanggal` varchar(20) DEFAULT NULL,
  `pengajuan_tunjangan_pakaian` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_pmb_pengajuan_tunjangan_pakaian
-- ----------------------------
INSERT INTO `sdm_pmb_pengajuan_tunjangan_pakaian` VALUES ('4', '1', '09-2017', '31500000.00', '2017-09-02 20:23:28', '2', '2017-09-02 20:23:28', null);

-- ----------------------------
-- Table structure for sdm_prh_kinerja_detail
-- ----------------------------
DROP TABLE IF EXISTS `sdm_prh_kinerja_detail`;
CREATE TABLE `sdm_prh_kinerja_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdm_prh_kinerja_pegawai_id` int(11) NOT NULL,
  `sdm_ms_list_kinerja_id` int(11) DEFAULT NULL,
  `rating` tinyint(4) DEFAULT NULL,
  `nilai` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_prh_kinerja_detail
-- ----------------------------
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('33', '8', '1', '5', '75', '2017-08-22 16:46:26', '2', '2017-08-22 16:46:26', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('34', '8', '2', '5', '75', '2017-08-22 16:46:26', '2', '2017-08-22 16:46:26', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('35', '8', '3', '4', '40', '2017-08-22 16:46:26', '2', '2017-08-22 16:46:26', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('36', '8', '4', '4', '40', '2017-08-22 16:46:26', '2', '2017-08-22 16:46:26', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('37', '8', '5', '4', '20', '2017-08-22 16:46:26', '2', '2017-08-22 16:46:26', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('38', '8', '6', '5', '75', '2017-08-22 16:46:26', '2', '2017-08-22 16:46:26', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('39', '8', '7', '5', '75', '2017-08-22 16:46:26', '2', '2017-08-22 16:46:26', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('40', '8', '8', '2', '10', '2017-08-22 16:46:27', '2', '2017-08-22 16:46:27', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('41', '8', '9', '3', '30', '2017-08-22 16:46:27', '2', '2017-08-22 16:46:27', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('42', '9', '1', '5', '75', '2017-08-22 16:48:47', '2', '2017-08-22 16:48:47', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('43', '9', '2', '5', '75', '2017-08-22 16:48:47', '2', '2017-08-22 16:48:47', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('44', '9', '3', '5', '50', '2017-08-22 16:48:47', '2', '2017-08-22 16:48:47', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('45', '9', '4', '5', '50', '2017-08-22 16:48:47', '2', '2017-08-22 16:48:47', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('46', '9', '5', '4', '20', '2017-08-22 16:48:47', '2', '2017-08-22 16:48:47', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('47', '9', '6', '4', '60', '2017-08-22 16:48:47', '2', '2017-08-22 16:48:47', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('48', '9', '7', '5', '75', '2017-08-22 16:48:47', '2', '2017-08-22 16:48:47', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('49', '9', '8', '4', '20', '2017-08-22 16:48:47', '2', '2017-08-22 16:48:47', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('50', '9', '9', '4', '40', '2017-08-22 16:48:47', '2', '2017-08-22 16:48:47', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('51', '10', '1', '4', '60', '2017-09-06 15:07:43', '2', '2017-09-06 15:07:43', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('52', '10', '2', '5', '75', '2017-09-06 15:07:43', '2', '2017-09-06 15:07:43', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('53', '10', '3', '4', '40', '2017-09-06 15:07:43', '2', '2017-09-06 15:07:43', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('54', '10', '4', '4', '40', '2017-09-06 15:07:43', '2', '2017-09-06 15:07:43', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('55', '10', '5', '5', '25', '2017-09-06 15:07:43', '2', '2017-09-06 15:07:43', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('56', '10', '6', '5', '75', '2017-09-06 15:07:43', '2', '2017-09-06 15:07:43', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('57', '10', '7', '5', '75', '2017-09-06 15:07:43', '2', '2017-09-06 15:07:43', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('58', '10', '8', '4', '20', '2017-09-06 15:07:43', '2', '2017-09-06 15:07:43', null);
INSERT INTO `sdm_prh_kinerja_detail` VALUES ('59', '10', '9', '3', '30', '2017-09-06 15:07:43', '2', '2017-09-06 15:07:43', null);

-- ----------------------------
-- Table structure for sdm_prh_kinerja_pegawai
-- ----------------------------
DROP TABLE IF EXISTS `sdm_prh_kinerja_pegawai`;
CREATE TABLE `sdm_prh_kinerja_pegawai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` enum('manajerial','staff') DEFAULT NULL,
  `sdm_ms_pegawai_nik` varchar(50) NOT NULL,
  `semester` enum('1','2') DEFAULT '1',
  `tahun` smallint(6) DEFAULT NULL,
  `total_nilai` smallint(6) DEFAULT NULL,
  `total_rating` smallint(6) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_prh_kinerja_pegawai
-- ----------------------------
INSERT INTO `sdm_prh_kinerja_pegawai` VALUES ('8', 'staff', '412414', '1', '2017', null, null, '2017-08-22 16:46:26', '2', '2017-08-22 16:46:26', null);
INSERT INTO `sdm_prh_kinerja_pegawai` VALUES ('9', 'staff', '412414', '1', '2017', '465', '1', '2017-08-22 16:48:46', '2', '2017-08-22 16:48:46', null);
INSERT INTO `sdm_prh_kinerja_pegawai` VALUES ('10', 'staff', '412414', '1', '2017', '440', '1', '2017-09-06 15:07:43', '2', '2017-09-06 15:07:43', null);

-- ----------------------------
-- Table structure for sdm_prh_tunjangan_pakaian
-- ----------------------------
DROP TABLE IF EXISTS `sdm_prh_tunjangan_pakaian`;
CREATE TABLE `sdm_prh_tunjangan_pakaian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tahun` smallint(6) DEFAULT NULL,
  `sdm_ms_pegawai_nik` varchar(50) NOT NULL,
  `tunjangan_pakaian` decimal(20,2) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_prh_tunjangan_pakaian
-- ----------------------------
INSERT INTO `sdm_prh_tunjangan_pakaian` VALUES ('6', '2017', '94848125128571', '5500000.00', null, '2017-08-07 10:34:47', null, '2017-08-07 10:34:47');
INSERT INTO `sdm_prh_tunjangan_pakaian` VALUES ('7', '2017', '412414', '15000000.00', null, '2017-08-07 10:34:47', null, '2017-08-07 10:34:47');
INSERT INTO `sdm_prh_tunjangan_pakaian` VALUES ('8', '2017', '26464363252', '15000000.00', null, '2017-08-07 10:34:47', null, '2017-08-07 10:34:47');
INSERT INTO `sdm_prh_tunjangan_pakaian` VALUES ('9', '2017', '085182651276571', '15000000.00', null, '2017-08-07 10:34:47', null, '2017-08-07 10:34:47');

-- ----------------------------
-- Table structure for sdm_prl_kredit
-- ----------------------------
DROP TABLE IF EXISTS `sdm_prl_kredit`;
CREATE TABLE `sdm_prl_kredit` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sdm_ms_pegawai_id` int(11) unsigned DEFAULT NULL,
  `sdm_ms_pegawai_nik` varchar(30) NOT NULL,
  `jenis_potongan` enum('karyawan','kendaraan') DEFAULT NULL,
  `total_pinjaman` varchar(20) DEFAULT NULL,
  `angsuran_perbulan` varchar(20) DEFAULT NULL,
  `jangka_waktu_pinjaman` varchar(20) DEFAULT NULL,
  `potongan` decimal(20,2) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_prl_kredit
-- ----------------------------
INSERT INTO `sdm_prl_kredit` VALUES ('1', null, '26464363252', 'karyawan', '200000', '100000', '2', '5000.00', 'hahahahah', '2017-06-08 15:28:02', '2', '2017-06-08 15:28:02', null);
INSERT INTO `sdm_prl_kredit` VALUES ('2', null, '', 'karyawan', '1', '1', '123', null, '1wtrgre', '2017-08-15 11:02:17', '2', '2017-08-15 11:02:17', null);
INSERT INTO `sdm_prl_kredit` VALUES ('3', null, '', 'kendaraan', '1', '1', '123', null, 'dfftygugygy', '2017-08-15 11:03:48', '2', '2017-08-15 11:03:48', null);

-- ----------------------------
-- Table structure for sdm_prl_potongan
-- ----------------------------
DROP TABLE IF EXISTS `sdm_prl_potongan`;
CREATE TABLE `sdm_prl_potongan` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sdm_ms_pegawai_id` int(11) DEFAULT NULL,
  `sdm_ms_pegawai_nik` varchar(30) NOT NULL,
  `bulan` varchar(12) NOT NULL,
  `tahun` smallint(4) unsigned DEFAULT NULL,
  `bpjs_tk_p` decimal(20,2) DEFAULT NULL,
  `bpjs_tk_k` decimal(20,2) DEFAULT NULL,
  `bpjs_kes_p` decimal(20,2) DEFAULT NULL,
  `bpjs_kes_k` decimal(20,2) DEFAULT NULL,
  `jaminan_pensiun_p` decimal(20,2) DEFAULT NULL,
  `jaminan_pensiun_k` decimal(20,2) DEFAULT NULL,
  `angsuran_karyawan` decimal(20,2) DEFAULT NULL,
  `1_jam` decimal(20,2) DEFAULT NULL,
  `2_jam` decimal(20,2) DEFAULT NULL,
  `3_jam` decimal(20,2) DEFAULT NULL,
  `4_jam` decimal(20,2) DEFAULT NULL,
  `izin_sakit` decimal(20,2) DEFAULT NULL,
  `alpha` decimal(20,2) DEFAULT NULL,
  `kredit_karyawan` decimal(20,2) DEFAULT NULL,
  `kredit_kendaraan` decimal(20,2) DEFAULT NULL,
  `pph21` decimal(20,2) DEFAULT NULL,
  `potongan_lain` decimal(20,2) DEFAULT NULL,
  `status` enum('1','2') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_prl_potongan
-- ----------------------------
INSERT INTO `sdm_prl_potongan` VALUES ('1', null, '94848125128571', 'Juni', '2017', '106000.00', '50000.00', '100000.00', '25000.00', '50000.00', '25000.00', null, '5000.00', '20000.00', '15000.00', '20000.00', '0.00', '40000.00', null, null, '1489216.47', '8.00', null, '2017-06-08 15:26:03', '2', '2017-08-23 14:15:12', '2');
INSERT INTO `sdm_prl_potongan` VALUES ('2', null, '6464363252', 'Juni', '2017', '127200.00', '60000.00', '120000.00', '30000.00', '60000.00', '30000.00', null, '4000.00', '0.00', '12000.00', '16000.00', '8000.00', '0.00', null, null, '1780320.00', null, null, '2017-06-08 15:26:03', '2', '2017-06-14 09:40:50', '2');
INSERT INTO `sdm_prl_potongan` VALUES ('3', null, '26464363252', 'Juni', '2017', '127200.00', '60000.00', '120000.00', '30000.00', '60000.00', '30000.00', null, '4000.00', '0.00', '0.00', '16000.00', '16000.00', '16000.00', null, null, '1902555.99', null, null, '2017-06-08 15:26:03', '2', '2017-08-23 14:15:12', '2');
INSERT INTO `sdm_prl_potongan` VALUES ('4', null, '94848125128571', 'July', '2017', '106000.00', '50000.00', '100000.00', '25000.00', null, null, null, '5000.00', '20000.00', '60000.00', '20000.00', '10000.00', '20000.00', null, null, '1362600.00', '400000.00', null, '2017-07-05 13:51:13', '2', '2017-08-23 14:15:12', '2');
INSERT INTO `sdm_prl_potongan` VALUES ('5', null, '412414', 'July', '2017', '127200.00', '60000.00', '120000.00', '30000.00', null, null, null, null, null, null, null, null, null, null, null, '1732320.00', null, null, '2017-07-05 13:51:13', '2', '2017-08-23 14:15:12', '2');
INSERT INTO `sdm_prl_potongan` VALUES ('6', null, '26464363252', 'July', '2017', '127200.00', '60000.00', '120000.00', '30000.00', null, null, null, null, null, null, null, null, null, null, null, '1732320.00', '200000000.00', null, '2017-07-05 13:51:14', '2', '2017-08-23 14:15:12', '2');
INSERT INTO `sdm_prl_potongan` VALUES ('7', null, '085182651276571', 'July', '2017', '127200.00', '60000.00', '120000.00', '30000.00', null, null, null, null, null, null, null, null, null, null, null, '1732320.00', null, null, '2017-07-05 13:51:14', '2', '2017-08-23 14:15:12', '2');
INSERT INTO `sdm_prl_potongan` VALUES ('8', null, '94848125128571', 'Agustus', '2017', '106000.00', '50000.00', '100000.00', '25000.00', null, null, null, null, null, null, null, null, null, null, null, '1724400.00', null, null, '2017-08-15 10:49:39', '2', '2017-08-30 20:26:07', '2');
INSERT INTO `sdm_prl_potongan` VALUES ('9', null, '412414', 'Agustus', '2017', '127200.00', '60000.00', '120000.00', '30000.00', null, null, null, '30000.00', '40000.00', '30000.00', '0.00', '20000.00', '80000.00', null, null, '2346720.00', null, null, '2017-08-15 10:49:39', '2', '2017-08-30 20:26:07', '2');
INSERT INTO `sdm_prl_potongan` VALUES ('10', null, '26464363252', 'Agustus', '2017', '127200.00', '60000.00', '120000.00', '30000.00', null, null, null, null, null, null, null, null, null, null, null, '1749120.00', null, null, '2017-08-15 10:49:39', '2', '2017-08-30 20:26:07', '2');
INSERT INTO `sdm_prl_potongan` VALUES ('11', null, '085182651276571', 'Agustus', '2017', '127200.00', '60000.00', '120000.00', '30000.00', null, null, null, null, null, null, null, null, null, null, null, '2025120.00', null, null, '2017-08-15 10:49:39', '2', '2017-08-30 20:26:07', '2');
INSERT INTO `sdm_prl_potongan` VALUES ('12', null, '94848125128571', 'September', '2017', '186560.00', '88000.00', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2017-09-06 14:33:05', '2', '2017-09-06 14:33:05', null);
INSERT INTO `sdm_prl_potongan` VALUES ('13', null, '412414', 'September', '2017', '158576.00', '74800.00', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2017-09-06 14:33:05', '2', '2017-09-06 14:33:05', null);
INSERT INTO `sdm_prl_potongan` VALUES ('14', null, '26464363252', 'September', '2017', '122960.00', '58000.00', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2017-09-06 14:33:05', '2', '2017-09-06 14:33:05', null);
INSERT INTO `sdm_prl_potongan` VALUES ('15', null, '085182651276571', 'September', '2017', '298496.00', '140800.00', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2017-09-06 14:33:05', '2', '2017-09-06 14:33:05', null);
INSERT INTO `sdm_prl_potongan` VALUES ('16', null, '05718512941829', 'September', '2017', '298496.00', '140800.00', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2017-09-06 14:33:05', '2', '2017-09-06 14:33:05', null);

-- ----------------------------
-- Table structure for sdm_tbt_perjalanan_dinas
-- ----------------------------
DROP TABLE IF EXISTS `sdm_tbt_perjalanan_dinas`;
CREATE TABLE `sdm_tbt_perjalanan_dinas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sdm_ms_pegawai_id` int(11) unsigned DEFAULT NULL,
  `sdm_ms_pegawai_nik` varchar(30) NOT NULL,
  `sdm_ms_biaya_perjalanan_dinas_id` int(11) DEFAULT NULL,
  `tujuan_id` int(11) DEFAULT NULL,
  `golongan` varchar(2) DEFAULT NULL,
  `jenis_nominal` varchar(20) DEFAULT NULL,
  `nominal` decimal(20,2) DEFAULT NULL,
  `tarif_pulang` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_tbt_perjalanan_dinas
-- ----------------------------
INSERT INTO `sdm_tbt_perjalanan_dinas` VALUES ('2', null, '94848125128571', null, '1', 'D', 'rata_rata', '840000.00', '104032.00', '2017-07-10 23:08:45', '2', '2017-07-10 23:08:45', null);
INSERT INTO `sdm_tbt_perjalanan_dinas` VALUES ('3', null, '94848125128571', '33', null, 'B', 'rata_rata', '99000.00', '10.00', '2017-09-13 13:59:19', '2', '2017-09-04 11:49:22', null);
INSERT INTO `sdm_tbt_perjalanan_dinas` VALUES ('4', null, '94848125128571', '33', null, 'B', 'manado_ntt', '102600.00', null, '2017-09-06 14:50:24', '2', '2017-09-06 14:50:24', null);

-- ----------------------------
-- Table structure for sdm_tbt_perjanjian_kerja
-- ----------------------------
DROP TABLE IF EXISTS `sdm_tbt_perjanjian_kerja`;
CREATE TABLE `sdm_tbt_perjanjian_kerja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdm_ms_pegawai_nik` varchar(30) NOT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_akhir` date DEFAULT NULL,
  `no_telp` varchar(20) DEFAULT NULL,
  `file` text,
  `notif_status` enum('0','1') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_tbt_perjanjian_kerja
-- ----------------------------
INSERT INTO `sdm_tbt_perjanjian_kerja` VALUES ('1', '412414', '2017-07-17', '2017-07-20', 'NK08512', 'exemple01.pdf', '1', '2017-07-10 10:44:19', '2', '2017-08-30 10:30:42', null);
INSERT INTO `sdm_tbt_perjanjian_kerja` VALUES ('2', '94848125128571', '2017-07-10', '2017-09-07', '412782147', 'exemple02.pdf', '1', '2017-07-11 10:52:03', '2', '2017-08-30 10:30:46', null);

-- ----------------------------
-- Table structure for sdm_trans_cuti
-- ----------------------------
DROP TABLE IF EXISTS `sdm_trans_cuti`;
CREATE TABLE `sdm_trans_cuti` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sdm_ms_pegawai_id` int(10) unsigned DEFAULT NULL,
  `sdm_ms_pegawai_nik` varchar(30) NOT NULL,
  `sdm_ms_jenis_cuti_id` int(10) unsigned NOT NULL,
  `tgl_awal` date NOT NULL,
  `tgl_akhir` date NOT NULL,
  `jumlah_hari` int(2) unsigned NOT NULL,
  `alasan` varchar(200) NOT NULL,
  `no_telp` varchar(20) DEFAULT NULL,
  `alamat_cuti` text,
  `pimpinan` varchar(50) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sdm_trans_cuti_sdm_ms_pegawai_id_foreign` (`sdm_ms_pegawai_id`),
  KEY `sdm_trans_cuti_sdm_ms_jenis_cuti_foreign` (`sdm_ms_jenis_cuti_id`),
  CONSTRAINT `sdm_trans_cuti_sdm_ms_jenis_cuti_foreign` FOREIGN KEY (`sdm_ms_jenis_cuti_id`) REFERENCES `sdm_ms_jenis_cuti` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sdm_trans_cuti
-- ----------------------------
INSERT INTO `sdm_trans_cuti` VALUES ('1', null, '94848125128571', '1', '2017-05-23', '2017-05-25', '2', 'CT', null, 'adaw wkwk', '-', '0', '2017-05-29 05:36:05', '2', '2017-05-29 05:41:01', '2');
INSERT INTO `sdm_trans_cuti` VALUES ('9', null, '085182651276571', '6', '2017-07-12', '2017-07-24', '12', 'CB', null, null, null, '0', '2017-07-10 22:03:24', '2', '2017-07-10 22:03:24', null);
INSERT INTO `sdm_trans_cuti` VALUES ('10', null, '94848125128571', '1', '2017-09-03', '2017-09-29', '4', 'CT', null, 'wadaw', '-', '0', '2017-07-19 16:45:58', '2', '2017-07-19 16:45:58', null);
INSERT INTO `sdm_trans_cuti` VALUES ('11', null, '412414', '1', '2017-09-03', '2017-09-29', '8', 'CT', null, 'kp sukabirus. rt 03/01', '-', '0', '2017-08-15 11:19:09', '2', '2017-08-15 11:19:09', null);
INSERT INTO `sdm_trans_cuti` VALUES ('12', null, '94848125128571', '1', '2017-09-03', '2017-09-29', '8', 'CT', null, 'kp sukabirus. rt 03/01', '-', '0', '2017-08-15 11:19:09', '2', '2017-08-15 11:19:09', null);
INSERT INTO `sdm_trans_cuti` VALUES ('13', null, '26464363252', '1', '2017-09-22', '2017-09-25', '1', 'CT', null, 'pokoana eta', '-', '0', '2017-09-04 13:46:08', '2', '2017-09-04 13:46:08', null);
INSERT INTO `sdm_trans_cuti` VALUES ('14', null, '05718512941829', '1', '2017-09-11', '2017-09-18', '7', 'CT', null, 'aaaa', '-', '0', '2017-09-06 14:35:48', '2', '2017-09-06 14:35:48', null);
SET FOREIGN_KEY_CHECKS=1;
